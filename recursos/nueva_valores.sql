-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 08, 2015 at 07:44 PM
-- Server version: 5.5.38-0+wheezy1
-- PHP Version: 5.4.4-14+deb7u12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `policia_municipal`
--

-- --------------------------------------------------------

--
-- Table structure for table `valores`
--

CREATE TABLE IF NOT EXISTS `valores` (
  `cod_val` int(11) NOT NULL COMMENT 'Codigo del Valor',
  `des_val` varchar(25) NOT NULL COMMENT 'Descripcion del Valor',
  `val_val` double(9,3) NOT NULL COMMENT 'Valor del Valor',
  `con_val` varchar(100) NOT NULL COMMENT 'Concepto del Valor'
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `valores`
--

INSERT INTO `valores` (`cod_val`, `des_val`, `val_val`, `con_val`) VALUES
(1, 'BV', 15.000, 'Días de Bono Vacacional para personal Contratado según LOT'),
(2, 'AG', 90.000, 'Días de Aguinaldos'),
(3, 'UT', 127.000, 'Valor de la Unidad Tributaria'),
(4, 'SSO', 0.040, 'Porcentaje de Retención Seguro Social Obligatorio'),
(5, 'LPH', 0.010, 'Porcentaje de Retención Fondo Ahorro Obligatorio para Vivienda'),
(6, 'SPF', 0.005, 'Porcentaje de Retención Pérdida Involuntaria de Empleo (PIE)'),
(7, 'CAH', 0.100, 'Porcentaje de Retención Caja de Ahorros (CAPREAMCE)'),
(8, 'SFU', 60.000, 'Monto Seguro Funerario (SOVENPFA)'),
(9, 'SM', 2047.520, 'Monto de Sueldo Mínimo'),
(10, 'SMMSSO', 5.000, 'Cantidad de Sueldos Máximos para el SSO'),
(12, 'APSSO', 0.090, 'Porcentaje de Aporte Seguro Social Obligatorio'),
(13, 'APSPF', 0.020, 'Porcentaje de Aporte Pérdida Involuntaria de Empleo (PIE)'),
(14, 'APCAH', 0.080, 'Porcentaje de Aporte Caja de Ahorros (CAPREAMCE)'),
(15, 'PRMHOG', 2.500, 'Cantidad de Unidades Tributarias Prima por Hogar'),
(16, 'PRMHIJ', 0.500, 'Cantidad de Unidades Tributarias Prima por Hijos'),
(17, 'PRMANT1', 1.500, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 6 a 8 años de antiguedad'),
(18, 'PRMPRO1', 2.000, 'Cantidad de Unidades Tributarias Prima por Profesionalización 1er Tipo'),
(19, 'PRMJER', 300.000, 'Monto Prima Jerarquía'),
(20, 'PRMHJE', 1.000, 'Cantidad de Unidades Tributarias Prima por Hijos Excepcionales'),
(21, 'PRMOTR1', 2.500, 'Cantidad de Unidades Tributarias Prima por Profesionalización 3er Tipo'),
(22, 'APLPH', 0.020, 'Porcentaje de Aporte Fondo Ahorro Obligatorio para Vivienda'),
(23, 'PRMANT2', 2.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 9 a 11 años de antiguedad'),
(24, 'PRMANT3', 2.500, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 12 a 14 años de antiguedad'),
(25, 'PRMANT4', 3.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad después de 15 años'),
(27, 'VACPAG1', 40.000, 'Días de Bono Vacacional para personal fijo 1er Quinquenio'),
(28, 'VACPAG2', 40.000, 'Días de Bono Vacacional para personal fijo 2do Quinquenio'),
(29, 'VACPAG3', 40.000, 'Días de Bono Vacacional para personal fijo 3er Quinquenio'),
(30, 'VACPAG4', 40.000, 'Días de Bono Vacacional para personal fijo 4to Quinquenio'),
(31, 'VACPAG5', 40.000, 'Días de Bono Vacacional para personal fijo 5to Quinquenio'),
(32, 'VACDIF1', 20.000, 'Días de disfrute de Vacaciones para personal fijo 1er Quinquenio'),
(33, 'VACDIF2', 23.000, 'Días de disfrute de Vacaciones para personal fijo 2do Quinquenio'),
(34, 'VACDIF3', 25.000, 'Días de disfrute de Vacaciones para personal fijo 3er Quinquenio'),
(35, 'VACDIF4', 28.000, 'Días de disfrute de Vacaciones para personal fijo 4to Quinquenio'),
(36, 'VACDIF5', 30.000, 'Días de disfrute de Vacaciones para personal fijo 5to Quinquenio'),
(38, 'DESPIO', 0.010, 'Porcentaje de Monte Pio'),
(39, 'PRMPRO2', 2.500, 'Cantidad de Unidades Tributarias Prima por Profesionalización 2do Tipo'),
(40, 'PRMOTR2', 2.500, 'Cantidad de Unidades Tributarias Prima por Profesionalización 4to Tipo'),
(41, 'IVA', 0.120, 'Impuesto al Valor Agregado'),
(42, 'CCT', 2.750, 'Comisión gastos administrativos Cesta Ticket'),
(43, 'MED_2012', 1500.000, 'Monto para gastos Médicos y de Farmacia para el año 2012'),
(44, 'PRMANT0', 1.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 3 a 5 años de antiguedad'),
(45, 'PRMOTR3', 2.500, 'Cantidad de Unidades Tributarias Prima por Profesionalizacion Titulo Doctorado'),
(46, 'SUSTRAC_ISRL', 63.330, 'Sustraendo para el ISRL cuando retancion sea 1%'),
(47, 'PART_IVA', 72.000, 'Codigo de la Partida correspondiente al IVA'),
(48, 'PRESUP_MAX', 35.000, 'Número de partidas por página en reporte General de Presupuesto'),
(49, 'APRFONJUB', 0.030, 'Aporte de Fondo de Jubilaciones'),
(50, 'RETFONJUB', 0.030, 'Retención de Fondo de Jubilaciones'),
(51, 'prueba de valores', 12000.000, 'prueba'),
(52, 'VALCES', 0.750, 'Porcentaje diario para el pago de cesta tiket'),
(53, 'PRIRIE', 410.500, 'PRIMA DE RIEGO CON MONTO EN BS.'),
(54, 'PRIPAT', 2.000, 'PRIMA DE PATRULLERO MONTO EN BS'),
(55, 'PRICOO', 2.000, 'PRIMA DE COORDINADOR MONTO EN BS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `valores`
--
ALTER TABLE `valores`
  ADD PRIMARY KEY (`cod_val`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `valores`
--
ALTER TABLE `valores`
  MODIFY `cod_val` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del Valor',AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
