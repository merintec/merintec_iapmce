-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-05-2015 a las 21:27:05
-- Versión del servidor: 5.5.38-0+wheezy1
-- Versión de PHP: 5.4.4-14+deb7u12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `zuly-1406-2015`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE IF NOT EXISTS `actividades` (
  `cod_act` varchar(5) NOT NULL COMMENT 'codigo de la Actividad',
  `nom_act` varchar(150) NOT NULL COMMENT 'nombre de la actividad'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adt_ing_cat`
--

CREATE TABLE IF NOT EXISTS `adt_ing_cat` (
  `cod_cat` int(11) NOT NULL COMMENT 'Codigo de Categoría',
  `des_cat` varchar(50) NOT NULL COMMENT 'Descripción de Categoría'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las Categorias de los recibos para Auditorias';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adt_ing_pro`
--

CREATE TABLE IF NOT EXISTS `adt_ing_pro` (
  `serie_id` int(11) NOT NULL,
  `recibo` int(11) NOT NULL,
  `monto` decimal(9,2) NOT NULL,
  `cod_cat` int(2) NOT NULL,
  `con_esp` varchar(50) NOT NULL COMMENT 'Condición Especial'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anular_form`
--

CREATE TABLE IF NOT EXISTS `anular_form` (
  `cod_anl` int(11) NOT NULL,
  `fch_anl` date NOT NULL,
  `tip_anl` varchar(1) NOT NULL,
  `num_anl` int(5) unsigned zerofill NOT NULL,
  `mot_anl` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los Formularios Anulados';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaciones`
--

CREATE TABLE IF NOT EXISTS `asignaciones` (
  `cod_cnp` int(11) NOT NULL COMMENT 'Codigo de la asignacion',
  `con_cnp` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `ncuo_cnp` int(3) NOT NULL COMMENT 'Numero de cuotas de la asignacion',
  `per_cnp` varchar(1) NOT NULL COMMENT 'Permanencia de la asignacion',
  `mcuo_cnp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la asignacion',
  `por_cnp` int(3) NOT NULL COMMENT 'Porcentaje de asignacion',
  `bpor_cnp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_con` int(11) NOT NULL COMMENT 'Codigo del tipo de concesion',
  `ced_per` varchar(12) NOT NULL COMMENT 'La cedula de la persona',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_cnp` date NOT NULL COMMENT 'Fecha de Registro de la asignacion',
  `ncp_cnp` int(3) NOT NULL COMMENT 'N?mero de cuotas pagadas',
  `des_cnp` varchar(1) NOT NULL COMMENT 'Aplica para Descuentos de ley'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE IF NOT EXISTS `auditoria` (
  `cod_aud` int(11) NOT NULL COMMENT 'C?digo de Auditor?a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci?n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci?n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu? la acci?n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci?n',
  `hora` time NOT NULL COMMENT 'hora de la acci?n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_backup`
--

CREATE TABLE IF NOT EXISTS `auditoria_backup` (
  `cod_aud` int(11) NOT NULL COMMENT 'C?digo de Auditor?a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci?n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci?n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu? la acci?n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci?n',
  `hora` time NOT NULL COMMENT 'hora de la acci?n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE IF NOT EXISTS `banco` (
  `cod_ban` int(11) NOT NULL,
  `nom_ban` varchar(50) DEFAULT NULL,
  `cue_ban` varchar(20) DEFAULT NULL,
  `des_ban` varchar(50) NOT NULL COMMENT 'Destino de la Cuenta'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco_conciliacion`
--

CREATE TABLE IF NOT EXISTS `banco_conciliacion` (
  `cod_ban_conc` int(11) NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `mes_ban_conc` int(11) NOT NULL,
  `ano_ban_conc` int(4) NOT NULL,
  `sal_ban_conc` decimal(9,2) NOT NULL,
  `obs_ban_conc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las concialiaciones bancarias';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco_movimientos`
--

CREATE TABLE IF NOT EXISTS `banco_movimientos` (
  `cod_ban_mov` int(11) NOT NULL,
  `fch_ban_mov` date NOT NULL,
  `tip_ban_mov` varchar(10) NOT NULL,
  `mon_ban_mov` double(9,2) NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `des_ban_mov` varchar(255) NOT NULL,
  `obs_ban_mov` varchar(255) NOT NULL,
  `ref_ban_mov` varchar(50) NOT NULL,
  `sta_ban_mov` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de los movimientos bancarios';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE IF NOT EXISTS `cargos` (
  `cod_car` int(11) NOT NULL COMMENT 'codigo del cargo',
  `num_car` int(3) unsigned zerofill NOT NULL COMMENT 'numero del cargo',
  `nom_car` varchar(100) NOT NULL COMMENT 'nombre del cargo',
  `cod_sue` int(2) NOT NULL COMMENT 'codigo del sueldo',
  `est_car` varchar(1) NOT NULL COMMENT 'estado del cargo',
  `cod_tcar` int(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `ced_per` varchar(12) NOT NULL,
  `fch_asg` date NOT NULL COMMENT 'Fecha de asignacion  del cargo',
  `des_car` varchar(1) NOT NULL COMMENT 'Se efectuan los descuentos de ley por este Cargo',
  `fch_vac` date NOT NULL,
  `cod_hor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE IF NOT EXISTS `compras` (
  `cod_com` int(11) NOT NULL,
  `fch_com` date NOT NULL,
  `idn_par_mov` varchar(50) NOT NULL,
  `nor_com` int(4) unsigned zerofill NOT NULL,
  `frm_com` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `sol_com` varchar(20) DEFAULT NULL,
  `tip_com` varchar(15) NOT NULL,
  `for_com` varchar(15) DEFAULT NULL,
  `adl_com` double(9,2) DEFAULT NULL,
  `fre_com` varchar(255) DEFAULT NULL,
  `mon_com` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `rif_pro` varchar(10) NOT NULL,
  `npr_com` int(2) NOT NULL,
  `iva_com` int(2) NOT NULL,
  `ela_com` varchar(50) NOT NULL,
  `rev_com` varchar(50) NOT NULL,
  `obs_com` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concesiones`
--

CREATE TABLE IF NOT EXISTS `concesiones` (
  `cod_con` int(11) NOT NULL COMMENT 'Codigo del tipo de concesion',
  `nom_con` varchar(60) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre del tipo de concesion',
  `des_con` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de concesion'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `constancias_per`
--

CREATE TABLE IF NOT EXISTS `constancias_per` (
  `cod_con` int(11) NOT NULL,
  `fch_con` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `nom_dep` varchar(100) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `des_con` varchar(50) NOT NULL,
  `sue_con` varchar(1) NOT NULL,
  `prm_con` varchar(1) NOT NULL,
  `cst_con` varchar(1) NOT NULL,
  `sue_mon` decimal(9,2) NOT NULL,
  `prm_mon` decimal(9,2) NOT NULL,
  `cst_mon` decimal(9,2) NOT NULL,
  `mot_con` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Constancias Solicitadas';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cst_tk_pagada`
--

CREATE TABLE IF NOT EXISTS `cst_tk_pagada` (
  `cod_cst` int(11) NOT NULL COMMENT 'Código de Cesta Ticket Pagada',
  `mes_cst` int(11) NOT NULL COMMENT 'Mes de la Cesta Ticket pagada',
  `ano_cst` int(11) NOT NULL COMMENT 'Año de de la Cesta Ticket pagada',
  `ced_per` int(11) NOT NULL COMMENT 'Cédula del Personal de la Cesta Ticket pagada',
  `dias_cst` int(11) NOT NULL COMMENT 'Total de días del mes de la Cesta Ticket pagada',
  `dias_cst_pag` int(11) NOT NULL COMMENT 'Total de días pagados por mes de la Cesta Ticket pagada',
  `ut_cst` int(11) NOT NULL COMMENT 'Bs. por día de la Cesta Ticket pagada',
  `mnt_cst` int(11) NOT NULL COMMENT 'Total Bs. por personal de la Cesta Ticket pagada'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de la Cesta Ticket pagada';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE IF NOT EXISTS `cuentas` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `num_cue` varchar(20) NOT NULL COMMENT 'numero de cuenta de la persona',
  `tip_cue` varchar(1) NOT NULL COMMENT 'tipo de la cuenta de la persona',
  `fcam_cue` date NOT NULL COMMENT 'fecha de cambio del numero de cuenta',
  `ban_cue` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deducciones`
--

CREATE TABLE IF NOT EXISTS `deducciones` (
  `cod_dsp` int(11) NOT NULL COMMENT 'Codigo de la Deduccion',
  `con_dsp` varchar(100) NOT NULL COMMENT 'Concepto de la Deduccion',
  `ncuo_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas de la deduccion',
  `per_dsp` varchar(1) NOT NULL COMMENT 'Permanencia de la Deduccion',
  `mcuo_dsp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la deduccion',
  `por_dsp` int(3) NOT NULL COMMENT 'Porcentaje de deduccion',
  `bpor_dsp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_des` int(11) NOT NULL COMMENT 'Codigo del Descuento',
  `ced_per` varchar(12) NOT NULL COMMENT 'Cedula del Personal',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_dsp` date NOT NULL COMMENT 'Fecha de Registro de la Deduccion',
  `ncp_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas pagadas'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencias`
--

CREATE TABLE IF NOT EXISTS `dependencias` (
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `nom_dep` varchar(150) NOT NULL COMMENT 'nombre de la dependencia',
  `cod_act` varchar(5) NOT NULL COMMENT 'actividad de la dependencia',
  `cod_pro` varchar(5) NOT NULL COMMENT 'programa de la dependencia'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuentos`
--

CREATE TABLE IF NOT EXISTS `descuentos` (
  `cod_des` int(11) NOT NULL COMMENT 'Codigo del tipo de descuento',
  `nom_des` varchar(50) NOT NULL COMMENT 'Nombre del tipo de descuento',
  `des_des` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de descuento'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `educacion`
--

CREATE TABLE IF NOT EXISTS `educacion` (
  `cod_edu` int(11) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `niv_edu` varchar(50) NOT NULL,
  `ins_edu` varchar(100) NOT NULL,
  `fch_edu` date NOT NULL,
  `reg_edu` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los Estudios Realizados';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `egresos`
--

CREATE TABLE IF NOT EXISTS `egresos` (
  `cod_egr` int(11) NOT NULL,
  `fch_egr` date NOT NULL,
  `paga_imp` varchar(20) NOT NULL COMMENT 'Si paga impuesto indique que tipo',
  `nor_egr` int(4) unsigned zerofill NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `chq_egr` varchar(25) NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `con_egr` varchar(255) NOT NULL,
  `ret_iva_egr` int(3) NOT NULL,
  `ret_isrl_egr` int(2) NOT NULL,
  `ela_egr` varchar(50) NOT NULL,
  `rev_egr` varchar(50) NOT NULL,
  `apr_egr` varchar(50) NOT NULL,
  `cont_egr` varchar(50) NOT NULL,
  `obs_egr` varchar(255) DEFAULT NULL,
  `ded_egr` double(9,2) NOT NULL COMMENT 'Otras Deducciones',
  `sin_par_egr` decimal(9,2) NOT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `sta_ban_egr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE IF NOT EXISTS `entradas` (
  `cod_ntr` int(11) NOT NULL COMMENT 'C?digo de la Entrada',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `fecha_ntr` date NOT NULL COMMENT 'fecha de la entrada',
  `hora_ntr` char(15) NOT NULL COMMENT 'Hora de la Entrada'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Entradas de los Usuarios al Sistema';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas_pagos`
--

CREATE TABLE IF NOT EXISTS `facturas_pagos` (
  `cod_fac_pag` int(11) NOT NULL,
  `cod_pag` int(11) NOT NULL,
  `fch_fac_pag` date NOT NULL,
  `num_fac_pag` varchar(15) NOT NULL,
  `con_fac_pag` varchar(15) NOT NULL,
  `mon_fac_pag` double(9,2) NOT NULL,
  `iva_fac_pag` double(9,2) NOT NULL,
  `isrl_fac_pag` double(9,2) NOT NULL,
  `iva_pag_pag` date NOT NULL,
  `iva_pag_mes` int(2) NOT NULL,
  `iva_pag_ano` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las facturas pagadas';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familiares`
--

CREATE TABLE IF NOT EXISTS `familiares` (
  `cod_fam` int(2) NOT NULL COMMENT 'C?digo del Familiar',
  `ced_per` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C?dula del Personal',
  `ced_fam` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C?dula del  del Familiar',
  `nom_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre  del Familiar',
  `ape_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Apellido  del Familiar',
  `sex_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Sexo del Familiar',
  `fnac_fam` date NOT NULL COMMENT 'Fecha de Nacimiento del Familiar',
  `par_fam` varchar(25) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Parentesco  del Familiar',
  `est_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Estudia el Familiar?',
  `obs_fam` varchar(255) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Observaciones acerca del Familiar'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Datos de los Familiares del Personal';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `feriados`
--

CREATE TABLE IF NOT EXISTS `feriados` (
  `fch_frd` date NOT NULL,
  `des_frd` varchar(50) NOT NULL,
  `tip_frd` varchar(25) NOT NULL,
  `tck_frd` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de dias Feridos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE IF NOT EXISTS `grupos` (
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  `nom_grp` varchar(20) NOT NULL COMMENT 'nombre del grupo',
  `des_grp` varchar(255) NOT NULL COMMENT 'descripcion del grupo'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`cod_grp`, `nom_grp`, `des_grp`) VALUES
(1, 'SuperUsuarios', 'Con todos los privilegios'),
(2, 'Administración', 'Grupo con todos los privilegios del área de Usuarios'),
(3, 'Personal', 'Grupo para el personal adscrito a la institución'),
(4, 'Pasantes', 'Privilegios Mínimos'),
(5, 'BienesP', 'Grupo de Pasantes'),
(6, 'Secretaria', 'Secretaria de la Contraloría'),
(7, 'Asist Administración', 'Asistentes Asignados en el Area de Administración');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE IF NOT EXISTS `horarios` (
  `cod_hor` int(11) NOT NULL,
  `nom_hor` varchar(25) NOT NULL,
  `fch_dsd` date NOT NULL,
  `fch_hst` date NOT NULL,
  `lun_ini_hor` time NOT NULL,
  `lun_fin_hor` time NOT NULL,
  `lun_ini_hor2` time NOT NULL,
  `lun_fin_hor2` time NOT NULL,
  `mar_ini_hor` time NOT NULL,
  `mar_fin_hor` time NOT NULL,
  `mar_ini_hor2` time NOT NULL,
  `mar_fin_hor2` time NOT NULL,
  `mie_ini_hor` time NOT NULL,
  `mie_fin_hor` time NOT NULL,
  `mie_ini_hor2` time NOT NULL,
  `mie_fin_hor2` time NOT NULL,
  `jue_ini_hor` time NOT NULL,
  `jue_fin_hor` time NOT NULL,
  `jue_ini_hor2` time NOT NULL,
  `jue_fin_hor2` time NOT NULL,
  `vie_ini_hor` time NOT NULL,
  `vie_fin_hor` time NOT NULL,
  `vie_ini_hor2` time NOT NULL,
  `vie_fin_hor2` time NOT NULL,
  `sab_ini_hor` time NOT NULL,
  `sab_fin_hor` time NOT NULL,
  `dom_ini_hor` time NOT NULL,
  `dom_fin_hor` time NOT NULL,
  `tol_hor` int(11) NOT NULL,
  `obs_hor` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horarios`
--

INSERT INTO `horarios` (`cod_hor`, `nom_hor`, `fch_dsd`, `fch_hst`, `lun_ini_hor`, `lun_fin_hor`, `lun_ini_hor2`, `lun_fin_hor2`, `mar_ini_hor`, `mar_fin_hor`, `mar_ini_hor2`, `mar_fin_hor2`, `mie_ini_hor`, `mie_fin_hor`, `mie_ini_hor2`, `mie_fin_hor2`, `jue_ini_hor`, `jue_fin_hor`, `jue_ini_hor2`, `jue_fin_hor2`, `vie_ini_hor`, `vie_fin_hor`, `vie_ini_hor2`, `vie_fin_hor2`, `sab_ini_hor`, `sab_fin_hor`, `dom_ini_hor`, `dom_fin_hor`, `tol_hor`, `obs_hor`) VALUES
(1, 'Horario Normal', '2012-01-01', '2012-07-19', '08:15:00', '11:55:00', '14:40:00', '00:00:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 15, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inasistencias`
--

CREATE TABLE IF NOT EXISTS `inasistencias` (
  `cod_ina` int(11) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `fch_ina` date NOT NULL,
  `tip_ina` varchar(30) NOT NULL,
  `des_ina` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Inasistencias del personal';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias`
--

CREATE TABLE IF NOT EXISTS `incidencias` (
  `cod_inc` int(11) NOT NULL COMMENT 'Codigo de la asignacion',
  `con_inc` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `mes_inc` int(2) NOT NULL COMMENT 'Mes en que se paga la incidencia',
  `ano_inc` int(4) NOT NULL COMMENT 'Ano en que se paga la incidencia',
  `fini_inc` date NOT NULL COMMENT 'Fecha de inicio del Periodo de Inicidencia',
  `ffin_inc` date NOT NULL COMMENT 'Fecha de fin del Periodo de Inicidencia'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias_sueldos`
--

CREATE TABLE IF NOT EXISTS `incidencias_sueldos` (
  `cod_inc` int(11) NOT NULL COMMENT 'Codigo de incidencias',
  `cod_sue` int(2) NOT NULL COMMENT 'Codigo del sueldo',
  `mnt_inc` double(9,2) NOT NULL COMMENT 'Monto o porcentaje de la incidencia',
  `bas_inc` varchar(1) NOT NULL COMMENT 'Base para el calculo de la Incidencia'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de inicidencias para cada sueldo';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ing_pat_vhi`
--

CREATE TABLE IF NOT EXISTS `ing_pat_vhi` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `serie` int(3) NOT NULL,
  `inicio` int(8) NOT NULL,
  `fin` int(8) NOT NULL,
  `tipo` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los ingresos por Patente Vehicular';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `justificativos_per`
--

CREATE TABLE IF NOT EXISTS `justificativos_per` (
  `cod_sol_jus` int(11) NOT NULL,
  `fch_sol_jus` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_jus` int(2) NOT NULL,
  `ini_sol_jus` date NOT NULL,
  `fin_sol_jus` date NOT NULL,
  `obs_sol_jus` longtext NOT NULL,
  `mot_sol_jus` varchar(25) NOT NULL,
  `apro_sol_jus` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicinas`
--

CREATE TABLE IF NOT EXISTS `medicinas` (
  `cod_med` int(11) NOT NULL COMMENT 'codigo de pago de medicinas',
  `fch_med` date NOT NULL COMMENT 'fecha de registro del pago de medicinas',
  `fch_pag_med` date NOT NULL COMMENT 'fecha del pago de las medicinas',
  `mnt_med` decimal(9,2) NOT NULL COMMENT 'monto anual',
  `obs_med` text NOT NULL COMMENT 'observaciones, comentarios o información adicional'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las medicinas pagadas a los Funcionarios';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicinas_per`
--

CREATE TABLE IF NOT EXISTS `medicinas_per` (
  `cod_med_per` int(11) NOT NULL,
  `cod_med` int(11) NOT NULL,
  `ced_per` int(11) NOT NULL,
  `mnt_med_per` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_asign`
--

CREATE TABLE IF NOT EXISTS `nominapre_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(50) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_deduc`
--

CREATE TABLE IF NOT EXISTS `nominapre_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_pagar`
--

CREATE TABLE IF NOT EXISTS `nominapre_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  `int_sue` double(9,2) NOT NULL COMMENT 'Sueldo Integral',
  `fnj_des` double(9,3) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `fnj_apr` double(9,3) NOT NULL COMMENT 'aporte de fondo de jubilaciones',
  `lph_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje FAOV',
  `spf_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje PIE',
  `sso_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje SSO',
  `cah_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje de Caja de ahorro',
  `fnj_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje Fondo Jubilaciones',
  `lph_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte FAOV',
  `spf_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte PIE',
  `sso_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte SSO',
  `cah_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Caja de ahorro',
  `fnj_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Fondo Jubilaciones'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_asign`
--

CREATE TABLE IF NOT EXISTS `nomina_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(60) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_deduc`
--

CREATE TABLE IF NOT EXISTS `nomina_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_incidencias`
--

CREATE TABLE IF NOT EXISTS `nomina_incidencias` (
  `ano_inc` int(4) NOT NULL,
  `mes_inc` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `mnt_inc` double(9,2) NOT NULL,
  `con_inc` varchar(255) NOT NULL,
  `ivss_inc` double(9,2) NOT NULL,
  `spf_inc` double(9,2) NOT NULL,
  `cah_inc` double(9,2) NOT NULL,
  `bnoc_inc` double(9,2) NOT NULL,
  `bvac_inc` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_pagar`
--

CREATE TABLE IF NOT EXISTS `nomina_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  `int_sue` double(9,2) NOT NULL COMMENT 'Sueldo Integral',
  `fnj_des` double(9,3) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `fnj_apr` double(9,3) NOT NULL COMMENT 'aporte de fondo de jubilaciones',
  `lph_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje FAOV',
  `spf_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje PIE',
  `sso_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje SSO',
  `cah_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje de Caja de ahorro',
  `fnj_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje Fondo Jubilaciones',
  `lph_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte FAOV',
  `spf_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte PIE',
  `sso_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte SSO',
  `cah_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Caja de ahorro',
  `fnj_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Fondo Jubilaciones'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficios_enviados`
--

CREATE TABLE IF NOT EXISTS `oficios_enviados` (
  `cod_ofi` int(11) NOT NULL,
  `tip_ofi` varchar(10) NOT NULL,
  `num_ofi` int(4) unsigned zerofill NOT NULL,
  `fch_ofi` date NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `rdp_ofi` varchar(75) NOT NULL,
  `rpr_ofi` varchar(75) NOT NULL,
  `ddp_ofi` varchar(75) NOT NULL,
  `dpr_ofi` varchar(75) NOT NULL,
  `des_ofi` varchar(255) NOT NULL,
  `nds_ofi` int(2) NOT NULL,
  `fen_ofi` date DEFAULT NULL,
  `ftr_ofi` date DEFAULT NULL,
  `frs_ofi` date DEFAULT NULL,
  `ref_ofi` varchar(7) NOT NULL,
  `obs_ofi` varchar(255) NOT NULL,
  `est_ofi` varchar(1) NOT NULL COMMENT 'Estado del Oficio'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Oficios Enviados';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficios_recibidos`
--

CREATE TABLE IF NOT EXISTS `oficios_recibidos` (
  `cod_ofr` int(11) NOT NULL,
  `fch_ofr` date NOT NULL,
  `fch_emi_ofr` date NOT NULL,
  `num_ofr` varchar(60) NOT NULL,
  `rem_ofr` varchar(60) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `con_ofr` varchar(150) NOT NULL,
  `ane_ofr` varchar(2) NOT NULL,
  `obs_ofr` varchar(255) NOT NULL,
  `prc_ofr` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Oficios Recibidos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagados`
--

CREATE TABLE IF NOT EXISTS `pagados` (
  `ced_per` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagados_frc`
--

CREATE TABLE IF NOT EXISTS `pagados_frc` (
  `cod_car` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE IF NOT EXISTS `pagos` (
  `cod_pag` int(11) NOT NULL,
  `fch_pag` date NOT NULL,
  `nor_pag` int(4) unsigned zerofill NOT NULL,
  `frm_pag` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `frm_com` int(6) unsigned zerofill NOT NULL,
  `mon_pag` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `ela_pag` varchar(50) NOT NULL,
  `rev_pag` varchar(50) NOT NULL,
  `apr_pag` varchar(50) NOT NULL,
  `obs_pag` varchar(255) DEFAULT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `frm_egr_iva` int(6) unsigned zerofill NOT NULL COMMENT 'numero de formulacio para pagar iva',
  `frm_egr_isrl` int(6) unsigned zerofill NOT NULL COMMENT 'numero de formulacio para pagar isrl'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidas_compras`
--

CREATE TABLE IF NOT EXISTS `partidas_compras` (
  `cod_pro_com` int(11) NOT NULL,
  `cod_com` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_com` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidas_pagos`
--

CREATE TABLE IF NOT EXISTS `partidas_pagos` (
  `cod_pro_pag` int(11) NOT NULL,
  `cod_pag` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `isrl_pro_pag` varchar(2) DEFAULT NULL,
  `mon_pro_pag` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `part_presup`
--

CREATE TABLE IF NOT EXISTS `part_presup` (
  `cod_par` int(11) NOT NULL,
  `sec_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `pro_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `act_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `ram_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `par_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `gen_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `esp_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `sub_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `des_par` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `obs_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `part_presup_mov`
--

CREATE TABLE IF NOT EXISTS `part_presup_mov` (
  `cod_par_mov` int(11) NOT NULL,
  `tip_par_mov` varchar(15) NOT NULL,
  `fch_par_mov` date NOT NULL,
  `ano_par_mov` varchar(10) NOT NULL,
  `idn_par_mov` varchar(50) NOT NULL,
  `con_par_mov` longtext NOT NULL,
  `mon_par_mov` double(9,2) NOT NULL,
  `fin_par_mov` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `ing_prm` varchar(1) NOT NULL COMMENT 'permisos para ingresar',
  `mod_prm` varchar(1) NOT NULL COMMENT 'permisos para modificar',
  `con_prm` varchar(1) NOT NULL COMMENT 'permisos para consultar',
  `eli_prm` varchar(1) NOT NULL COMMENT 'permisos para eliminar',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  `cod_sec` int(2) NOT NULL COMMENT 'codigo de la seccion'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Permisologia para los Usuarios del Sistema';

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`ing_prm`, `mod_prm`, `con_prm`, `eli_prm`, `cod_grp`, `cod_sec`) VALUES
('', '', '', '', 5, 11),
('', '', '', '', 5, 2),
('', '', '', '', 5, 8),
('', '', '', '', 5, 3),
('', '', '', '', 5, 7),
('', '', '', '', 5, 6),
('', '', '', '', 5, 1),
('', '', '', '', 5, 14),
('', '', '', '', 5, 15),
('', '', '', '', 5, 13),
('', '', '', '', 5, 12),
('', '', '', '', 5, 4),
('', '', '', '', 5, 5),
('', '', '', '', 5, 16),
('', '', '', '', 5, 10),
('', '', '', '', 5, 9),
('A', 'A', 'A', 'A', 5, 17),
('', '', '', '', 4, 18),
('', '', '', '', 4, 19),
('', '', '', '', 4, 11),
('', '', '', '', 4, 2),
('', '', '', '', 4, 8),
('', '', '', '', 4, 3),
('', '', '', '', 4, 7),
('', '', '', '', 4, 6),
('', '', '', '', 4, 1),
('', '', '', '', 4, 14),
('', '', '', '', 4, 23),
('', '', '', '', 4, 20),
('', '', '', '', 4, 21),
('', '', '', '', 4, 22),
('', '', '', '', 4, 15),
('', '', '', '', 4, 13),
('', '', '', '', 4, 12),
('', '', '', '', 4, 4),
('', '', '', '', 4, 5),
('', '', '', '', 4, 16),
('', '', '', '', 4, 10),
('', '', '', '', 4, 9),
('', '', '', '', 4, 17),
('A', 'A', 'A', 'A', 4, 24),
('', '', '', '', 4, 25),
('', '', '', '', 4, 26),
('', '', '', '', 6, 18),
('', '', '', '', 6, 19),
('', '', '', '', 6, 11),
('', '', '', '', 6, 2),
('', '', '', '', 6, 8),
('', '', '', '', 6, 3),
('', '', '', '', 6, 7),
('', '', '', '', 6, 6),
('', '', 'A', '', 6, 1),
('', '', '', '', 6, 14),
('', '', '', '', 6, 23),
('A', '', 'A', '', 6, 20),
('A', '', 'A', '', 6, 21),
('A', '', 'A', '', 6, 22),
('', '', '', '', 6, 15),
('', '', 'A', '', 6, 13),
('', '', '', '', 6, 12),
('', '', 'A', '', 6, 4),
('', '', '', '', 6, 5),
('', '', '', '', 6, 16),
('', '', '', '', 6, 10),
('A', 'A', 'A', 'A', 6, 9),
('A', 'A', 'A', 'A', 6, 27),
('', '', '', '', 6, 17),
('', '', '', '', 6, 24),
('', '', 'A', '', 6, 25),
('A', '', 'A', '', 6, 26),
('', '', '', '', 7, 18),
('', '', 'A', '', 7, 19),
('', '', '', '', 7, 11),
('', '', '', '', 7, 2),
('', '', '', '', 7, 8),
('', '', '', '', 7, 3),
('', '', '', '', 7, 7),
('', '', '', '', 7, 6),
('A', 'A', 'A', '', 7, 1),
('', '', '', '', 7, 14),
('', '', '', '', 7, 23),
('A', '', 'A', '', 7, 20),
('A', '', 'A', '', 7, 21),
('A', '', 'A', '', 7, 22),
('', '', '', '', 7, 15),
('', '', 'A', '', 7, 13),
('', '', '', '', 7, 12),
('', '', 'A', '', 7, 4),
('', '', '', '', 7, 5),
('A', 'A', 'A', '', 7, 16),
('A', 'A', 'A', '', 7, 10),
('', '', '', '', 7, 9),
('', '', '', '', 7, 27),
('', '', '', '', 7, 17),
('', '', '', '', 7, 24),
('', '', 'A', '', 7, 25),
('A', 'A', 'A', 'A', 7, 26),
('', '', '', '', 3, 18),
('', '', '', '', 3, 28),
('A', 'A', 'A', 'A', 3, 19),
('', '', '', '', 3, 11),
('', '', '', '', 3, 2),
('', '', '', '', 3, 8),
('', '', '', '', 3, 3),
('', '', '', '', 3, 7),
('', '', '', '', 3, 6),
('', '', 'A', '', 3, 1),
('', '', '', '', 3, 14),
('', '', '', '', 3, 23),
('A', '', 'A', '', 3, 20),
('A', '', 'A', '', 3, 21),
('A', '', 'A', '', 3, 22),
('', '', 'A', '', 3, 15),
('', '', 'A', '', 3, 13),
('', '', '', '', 3, 12),
('', '', 'A', '', 3, 4),
('', '', '', '', 3, 5),
('', '', '', '', 3, 16),
('', '', '', '', 3, 10),
('', '', '', '', 3, 9),
('', '', '', '', 3, 27),
('', '', '', '', 3, 24),
('', '', '', '', 3, 17),
('', '', 'A', '', 3, 25),
('A', '', 'A', '', 3, 26),
('', 'A', 'A', 'A', 1, 18),
('', '', '', '', 1, 28),
('', '', '', '', 1, 19),
('', '', '', '', 1, 11),
('A', 'A', 'A', 'A', 1, 2),
('A', 'A', 'A', 'A', 1, 8),
('A', 'A', 'A', 'A', 1, 3),
('A', 'A', 'A', 'A', 1, 7),
('A', 'A', 'A', 'A', 1, 6),
('A', 'A', 'A', 'A', 1, 1),
('A', 'A', 'A', 'A', 1, 14),
('', '', '', '', 1, 23),
('A', '', 'A', '', 1, 20),
('', '', '', '', 1, 21),
('', '', '', '', 1, 22),
('A', 'A', 'A', 'A', 1, 15),
('A', 'A', 'A', 'A', 1, 13),
('A', 'A', 'A', 'A', 1, 12),
('A', 'A', 'A', 'A', 1, 4),
('', '', '', '', 1, 5),
('', '', '', '', 1, 16),
('', '', '', '', 1, 10),
('', '', '', '', 1, 9),
('', '', '', '', 1, 27),
('', '', '', '', 1, 24),
('', '', '', '', 1, 17),
('', '', '', '', 1, 25),
('A', 'A', 'A', 'A', 1, 26),
('A', 'A', 'A', 'A', 2, 18),
('', '', '', '', 2, 28),
('', '', '', '', 2, 19),
('', '', '', '', 2, 11),
('A', 'A', 'A', 'A', 2, 2),
('A', 'A', 'A', 'A', 2, 8),
('A', 'A', 'A', 'A', 2, 3),
('A', 'A', 'A', 'A', 2, 7),
('A', 'A', 'A', 'A', 2, 6),
('A', 'A', 'A', 'A', 2, 1),
('A', 'A', 'A', 'A', 2, 14),
('', '', '', '', 2, 23),
('', '', '', '', 2, 20),
('', '', '', '', 2, 21),
('', '', '', '', 2, 22),
('A', 'A', 'A', 'A', 2, 15),
('A', 'A', 'A', 'A', 2, 13),
('A', 'A', 'A', 'A', 2, 12),
('A', 'A', 'A', 'A', 2, 4),
('', '', '', '', 2, 5),
('', '', '', '', 2, 16),
('', '', '', '', 2, 10),
('', '', '', '', 2, 9),
('', '', '', '', 2, 27),
('', '', '', '', 2, 24),
('', '', '', '', 2, 17),
('', '', '', '', 2, 25),
('A', 'A', 'A', 'A', 2, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_per`
--

CREATE TABLE IF NOT EXISTS `permisos_per` (
  `cod_sol_perm` int(11) NOT NULL,
  `fch_sol_perm` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_perm` int(2) NOT NULL,
  `ini_sol_perm` date NOT NULL,
  `fin_sol_perm` date NOT NULL,
  `tip_sol_perm` varchar(20) NOT NULL,
  `obs_sol_perm` longtext NOT NULL,
  `mot_sol_perm` varchar(25) NOT NULL,
  `apro_sol_perm` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `nac_per` varchar(1) NOT NULL COMMENT 'nacionalidad de la persona',
  `nom_per` varchar(50) NOT NULL COMMENT 'nombre de la persona',
  `ape_per` varchar(50) NOT NULL COMMENT 'apellido de la persona',
  `sex_per` varchar(1) NOT NULL COMMENT 'sexo de la persona',
  `fnac_per` date NOT NULL COMMENT 'fecha de nacimiento de la persona',
  `lnac_per` varchar(50) NOT NULL COMMENT 'lugar de nacimiento de la persona',
  `cor_per` varchar(50) NOT NULL COMMENT 'correo de la persona',
  `pro_per` varchar(50) NOT NULL COMMENT 'profesion de la persona',
  `abr_per` varchar(10) NOT NULL COMMENT 'Abreviatura de Profesión',
  `dir_per` varchar(255) NOT NULL COMMENT 'direccion de la persona',
  `tel_per` varchar(12) NOT NULL COMMENT 'telefono de la persona',
  `cel_per` varchar(12) NOT NULL COMMENT 'celular de la persona',
  `lph_des` varchar(1) NOT NULL COMMENT 'Descuento de ley politica habitacional',
  `spf_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro de paro forzoso',
  `sso_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro social obligatorio',
  `cah_des` varchar(1) NOT NULL COMMENT 'Descuento de caja de ahorro',
  `sfu_des` varchar(1) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Servicio funerario',
  `hog_asg` varchar(1) NOT NULL COMMENT 'Prima por hogar',
  `hij_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos',
  `ant_asg` varchar(1) NOT NULL COMMENT 'Prima por antiguedad',
  `pro_asg` varchar(1) NOT NULL COMMENT 'Prima por profesionalizaci?n',
  `hje_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos excepcionales',
  `jer_asg` varchar(1) NOT NULL COMMENT 'Prima por Jerarquia',
  `otr_asg` varchar(1) NOT NULL COMMENT 'Prima por Postgrado',
  `fch_reg` date NOT NULL COMMENT 'Fecha de registro en el personal',
  `fnj_des` varchar(1) NOT NULL COMMENT 'Fondo de Jubilaciones'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_compras`
--

CREATE TABLE IF NOT EXISTS `productos_compras` (
  `cod_pro_com` int(11) NOT NULL,
  `cod_com` int(11) NOT NULL,
  `cnt_pro_com` int(4) NOT NULL,
  `uni_pro_com` int(4) NOT NULL,
  `con_pro_com` varchar(255) NOT NULL,
  `pun_pro_com` double(9,3) NOT NULL,
  `exc_pro_com` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_part_mov`
--

CREATE TABLE IF NOT EXISTS `productos_part_mov` (
  `cod_pro_par_mov` int(11) NOT NULL,
  `cod_par_mov` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_par_mov` double(9,2) NOT NULL,
  `tip_pro_par_mov` varchar(10) NOT NULL DEFAULT 'Ingreso'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='partidas que pertenecen a un movimiento presupuestario';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programas`
--

CREATE TABLE IF NOT EXISTS `programas` (
  `cod_pro` varchar(5) NOT NULL COMMENT 'codigo del Programa',
  `nom_pro` varchar(150) NOT NULL COMMENT 'nombre del Programa'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `programas`
--

INSERT INTO `programas` (`cod_pro`, `nom_pro`) VALUES
('1', 'PROGRAMA 01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prog_movimientos`
--

CREATE TABLE IF NOT EXISTS `prog_movimientos` (
  `cod_mov` int(11) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `accion` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  `fch_asg` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `fch_vac` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prog_mov_pagos`
--

CREATE TABLE IF NOT EXISTS `prog_mov_pagos` (
  `cod_pag` int(11) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `con_pag` varchar(150) NOT NULL,
  `fch_pag` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_vac` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
  `cod_pro` int(4) unsigned zerofill NOT NULL,
  `fch_pro` date NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `nom_pro` varchar(50) NOT NULL,
  `dir_pro` varchar(255) NOT NULL,
  `act_pro` varchar(50) NOT NULL,
  `cap_pro` double(9,2) NOT NULL,
  `cas_pro` double(9,2) NOT NULL,
  `tel_pro` varchar(11) NOT NULL,
  `fax_pro` varchar(11) NOT NULL,
  `per_pro` varchar(1) NOT NULL,
  `ofi_reg_pro` varchar(50) NOT NULL,
  `num_reg_pro` int(5) NOT NULL,
  `tom_reg_pro` varchar(25) NOT NULL,
  `fol_reg_pro` varchar(4) NOT NULL,
  `fch_reg_pro` date NOT NULL,
  `num_mod_pro` int(5) NOT NULL,
  `tom_mod_pro` varchar(25) NOT NULL,
  `fol_mod_pro` varchar(4) NOT NULL,
  `fch_mod_pro` date NOT NULL,
  `pat_pro` varchar(25) NOT NULL,
  `ivss_pro` varchar(25) NOT NULL,
  `ince_pro` varchar(25) NOT NULL,
  `nom_rep_pro` varchar(50) NOT NULL,
  `ape_rep_pro` varchar(50) NOT NULL,
  `ced_rep_pro` varchar(8) NOT NULL,
  `dir_rep_pro` varchar(255) NOT NULL,
  `tel_rep_pro` varchar(11) NOT NULL,
  `car_rep_pro` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los Proveedores';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retenciones_isrl`
--

CREATE TABLE IF NOT EXISTS `retenciones_isrl` (
  `cod_ret_isrl` int(11) NOT NULL,
  `com_ret_isrl` int(5) unsigned zerofill NOT NULL,
  `egr_ret_isrl` int(6) unsigned zerofill NOT NULL,
  `fch_ret_isrl` date NOT NULL,
  `obs_ret_isrl` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retenciones_iva`
--

CREATE TABLE IF NOT EXISTS `retenciones_iva` (
  `cod_ret_iva` int(11) NOT NULL,
  `com_ret_iva` int(8) unsigned zerofill NOT NULL,
  `egr_ret_iva` int(6) unsigned zerofill NOT NULL,
  `fch_ret_iva` date NOT NULL,
  `obs_ret_iva` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE IF NOT EXISTS `secciones` (
  `cod_sec` int(2) NOT NULL COMMENT 'codigo de la seccion',
  `nom_sec` varchar(25) NOT NULL COMMENT 'nombre de la seccion',
  `des_sec` varchar(255) NOT NULL COMMENT 'descripcion de la seccion',
  `dir_sec` varchar(50) NOT NULL COMMENT 'direccion para apuntar a la seccion',
  `tar_sec` varchar(15) NOT NULL COMMENT 'target de apertura de la seccion',
  `ord_sec` double(9,2) NOT NULL COMMENT 'orden de aparicion en el menu'
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 COMMENT='Secciones Disponibles para los Usuarios';

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`cod_sec`, `nom_sec`, `des_sec`, `dir_sec`, `tar_sec`, `ord_sec`) VALUES
(1, 'Personal', 'Datos del Personal adscrito a: ', 'personal.php', 'contenido', 7.00),
(2, 'Dependencias', 'Dependencias adcritas a:', 'dependencias.php', 'contenido', 2.00),
(3, 'Cargos', 'Cargos adscritos a:', 'cargos.php', 'contenido', 4.00),
(4, 'Nomina', 'Nomina adscrita a:', 'nomina.php', 'contenido', 12.00),
(5, 'Incidencias', 'Incidencias adcritas a:', 'incidencias.php', 'contenido', 13.00),
(6, 'Descuentos', 'Descuentos del Personal adscrito a:', 'descuentos.php', 'contenido', 6.00),
(7, 'Concesiones', 'Concesiones del Personal adscrito a:', 'concesiones.php', 'contenido', 5.00),
(8, 'Sueldos', 'Tabla de Sueldos', 'sueldos.php', 'contenido', 3.00),
(9, 'Correspondencia Enviada', 'Registro de correspondencia Interna y Externa de: ', 'oficios_enviados.php', 'contenido', 15.00),
(10, 'Comp./Pago./Egre.', 'Módulo de compras de: ', 'compras.php', 'contenido', 14.00),
(11, 'Actividades', 'Actividades administrativas de:', 'actividades.php', 'contenido', 1.00),
(12, 'Pre-Nomina', 'Prenomina Adscrita a: ', 'nominapre.php', 'contenido', 11.00),
(13, 'Cesta Ticket', 'Reporte de Cesta Ticket del personal adscrito a:', 'cesta_ticket.php', 'contenido', 10.00),
(14, 'Inasistencias', 'Registro de las inasistencias del personal adscrito a:', 'inasistencias.php', 'contenido', 8.00),
(15, 'Feriados', 'Registro de días feriados para', 'feriados.php', 'contenido', 9.00),
(16, 'Beneficiarios', 'Registro de Bemeficiarios registrados a ', 'proveedores.php', 'contenido', 13.50),
(17, 'Control de Bienes', 'Sistema de Control de Bienes', 'bienes/index.php', 'contenido', 16.00),
(18, 'Valores', 'Valores utilizados para los cáculos', 'valores.php', 'contenido', 0.00),
(19, 'Presupuesto', 'Manipulación de Partidas y Movimientos Presupuestarios', 'presupuesto.php', 'contenido', 0.10),
(20, 'Solicitud Vacaciones', 'Registro de la Solicitud de Vacaciones', 'vacaciones.php', 'contenido', 8.50),
(21, 'Solicitud de Permiso', 'Registro de la Solicitud de Permisos', 'permisos.php', 'contenido', 8.60),
(22, 'Sol. Justificativos', 'Registro de la Solicitud de Justificativos', 'justificativos.php', 'contenido', 8.70),
(23, 'Pago de Medicinas', 'Pago de gastos medicos y de farmacia a los empleados de la contraloría', 'medicinas.php', 'contenido', 8.30),
(24, 'Ingresos', 'Auditoria Ingresos', 'ing_index.php', 'contenido', 16.00),
(25, 'Conformar Cheques', 'Confirmación de cheques emitidos', 'conformar_cheques.php', 'contenido', 17.00),
(26, 'Constancias', 'Solicitud de Constancias', 'constancias.php', 'contenido', 17.00),
(27, 'Correspondencia Recibida', 'Registro de Correspondencia recibida por:', 'oficios_recibidos.php', 'contenido', 15.01),
(28, 'Bancos', 'Bancos y movimientos de ', 'bancos.php', 'contenido', 0.05);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos`
--

CREATE TABLE IF NOT EXISTS `sueldos` (
  `cod_sue` int(2) NOT NULL COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos_ant`
--

CREATE TABLE IF NOT EXISTS `sueldos_ant` (
  `cod_sue` int(2) NOT NULL COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sueldos_ant`
--

INSERT INTO `sueldos_ant` (`cod_sue`, `mon_sue`, `des_sue`) VALUES
(1, 14865.00, 'Contralor Municipal'),
(2, 8661.62, 'Jefe de Oficinas'),
(3, 9706.98, 'Director'),
(4, 6278.98, 'Auditor I'),
(5, 4447.61, 'Asistente Administrativo'),
(6, 3270.30, 'Auxiliar de Mantenimiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temp_presup`
--

CREATE TABLE IF NOT EXISTS `temp_presup` (
  `fch_prs` date NOT NULL,
  `det_prs` varchar(255) NOT NULL,
  `nor_prs` varchar(7) NOT NULL,
  `asg_prs` double(9,2) NOT NULL,
  `inc_prs` double(9,2) NOT NULL,
  `ing_prs` double(9,2) NOT NULL,
  `egr_prs` double(9,2) NOT NULL,
  `com_prs` double(9,2) NOT NULL,
  `cau_prs` double(9,2) NOT NULL,
  `pag_prs` double(9,2) NOT NULL,
  `rin_prs` double(9,2) NOT NULL,
  `generado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='temporal para generar ejecucion presupuestaria';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_cargos`
--

CREATE TABLE IF NOT EXISTS `tipos_cargos` (
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipos_cargos`
--

INSERT INTO `tipos_cargos` (`cod_tcar`, `nom_tcar`, `abr_tcar`) VALUES
(1, 'Dirección', 'DR'),
(2, 'Fijo', 'FJ'),
(3, 'Contratado', 'CT'),
(4, 'Obrero fijo', 'OF'),
(5, 'Alto Nivel', 'AN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `cod_usr` varchar(12) NOT NULL COMMENT 'codigo del usuario',
  `nom_usr` varchar(50) NOT NULL COMMENT 'nombre del usuario',
  `ape_usr` varchar(50) NOT NULL COMMENT 'apellidos del usuario',
  `freg_usr` date NOT NULL COMMENT 'fecha de registro del usuario',
  `log_usr` varchar(15) NOT NULL COMMENT 'login del usuario',
  `pas_usr` varchar(15) NOT NULL COMMENT 'password del usuario',
  `sts_usr` varchar(1) NOT NULL DEFAULT 'A' COMMENT 'Estado en el que se encuentra el Usuario',
  `int_usr` int(1) NOT NULL DEFAULT '0' COMMENT 'Intentos fallidos de inicio de sesi?n',
  `fcam_usr` date NOT NULL COMMENT 'fecha de cambio del usuario',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Usuarios del Sistema';

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`cod_usr`, `nom_usr`, `ape_usr`, `freg_usr`, `log_usr`, `pas_usr`, `sts_usr`, `int_usr`, `fcam_usr`, `cod_grp`) VALUES
('11111111111', 'Administrador', 'Administrador', '2011-02-16', 'admin', 'controlador', 'A', 0, '2016-09-22', 1),
('15031097', 'Luis Felipe', 'Márquez Briceño', '2011-01-01', 'felix', 'nueva', 'A', 0, '2016-11-11', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacaciones_per`
--

CREATE TABLE IF NOT EXISTS `vacaciones_per` (
  `cod_sol_vac` int(11) NOT NULL,
  `fch_sol_vac` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `dias_sol_vac` int(2) NOT NULL,
  `ini_sol_vac` date NOT NULL,
  `fin_sol_vac` date NOT NULL,
  `tip_sol_vac` varchar(20) NOT NULL,
  `peri_sol_vac` date NOT NULL,
  `perf_sol_vac` date NOT NULL,
  `obs_sol_vac` longtext NOT NULL,
  `apro_sol_vac` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vac_dias_per`
--

CREATE TABLE IF NOT EXISTS `vac_dias_per` (
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `fch_pag_vac` date NOT NULL,
  `dias_vac` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de días de Vacaciones a disfrutar';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores`
--

CREATE TABLE IF NOT EXISTS `valores` (
  `cod_val` int(11) NOT NULL COMMENT 'Codigo del Valor',
  `des_val` varchar(25) NOT NULL COMMENT 'Descripcion del Valor',
  `val_val` double(9,3) NOT NULL COMMENT 'Valor del Valor',
  `con_val` varchar(100) NOT NULL COMMENT 'Concepto del Valor'
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `valores`
--

INSERT INTO `valores` (`cod_val`, `des_val`, `val_val`, `con_val`) VALUES
(1, 'BV', 15.000, 'Días de Bono Vacacional para personal Contratado según LOT'),
(2, 'AG', 90.000, 'Días de Aguinaldos'),
(3, 'UT', 127.000, 'Valor de la Unidad Tributaria'),
(4, 'SSO', 0.040, 'Porcentaje de Retención Seguro Social Obligatorio'),
(5, 'LPH', 0.010, 'Porcentaje de Retención Fondo Ahorro Obligatorio para Vivienda'),
(6, 'SPF', 0.005, 'Porcentaje de Retención Pérdida Involuntaria de Empleo (PIE)'),
(7, 'CAH', 0.080, 'Porcentaje de Retención Caja de Ahorros (CAPREAMCE)'),
(8, 'SFU', 60.000, 'Monto Seguro Funerario (SOVENPFA)'),
(9, 'SM', 2047.520, 'Monto de Sueldo Mínimo'),
(10, 'SMMSSO', 5.000, 'Cantidad de Sueldos Máximos para el SSO'),
(12, 'APSSO', 0.090, 'Porcentaje de Aporte Seguro Social Obligatorio'),
(13, 'APSPF', 0.020, 'Porcentaje de Aporte Pérdida Involuntaria de Empleo (PIE)'),
(14, 'APCAH', 0.080, 'Porcentaje de Aporte Caja de Ahorros (CAPREAMCE)'),
(15, 'PRMHOG', 2.500, 'Cantidad de Unidades Tributarias Prima por Hogar'),
(16, 'PRMHIJ', 0.500, 'Cantidad de Unidades Tributarias Prima por Hijos'),
(17, 'PRMANT1', 1.500, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 6 a 8 años de antiguedad'),
(18, 'PRMPRO1', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalización 1er Tipo'),
(19, 'PRMJER', 300.000, 'Monto Prima Jerarquía'),
(20, 'PRMHJE', 1.000, 'Cantidad de Unidades Tributarias Prima por Hijos Excepcionales'),
(21, 'PRMOTR1', 2.500, 'Cantidad de Unidades Tributarias Otra Prima 1er Tipo'),
(22, 'APLPH', 0.020, 'Porcentaje de Aporte Fondo Ahorro Obligatorio para Vivienda'),
(23, 'PRMANT2', 2.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 9 a 11 años de antiguedad'),
(24, 'PRMANT3', 2.500, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 12 a 14 años de antiguedad'),
(25, 'PRMANT4', 3.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad después de 15 años'),
(27, 'VACPAG1', 40.000, 'Días de Bono Vacacional para personal fijo 1er Quinquenio'),
(28, 'VACPAG2', 40.000, 'Días de Bono Vacacional para personal fijo 2do Quinquenio'),
(29, 'VACPAG3', 40.000, 'Días de Bono Vacacional para personal fijo 3er Quinquenio'),
(30, 'VACPAG4', 40.000, 'Días de Bono Vacacional para personal fijo 4to Quinquenio'),
(31, 'VACPAG5', 40.000, 'Días de Bono Vacacional para personal fijo 5to Quinquenio'),
(32, 'VACDIF1', 20.000, 'Días de disfrute de Vacaciones para personal fijo 1er Quinquenio'),
(33, 'VACDIF2', 23.000, 'Días de disfrute de Vacaciones para personal fijo 2do Quinquenio'),
(34, 'VACDIF3', 25.000, 'Días de disfrute de Vacaciones para personal fijo 3er Quinquenio'),
(35, 'VACDIF4', 28.000, 'Días de disfrute de Vacaciones para personal fijo 4to Quinquenio'),
(36, 'VACDIF5', 30.000, 'Días de disfrute de Vacaciones para personal fijo 5to Quinquenio'),
(38, 'DESPIO', 0.000, 'Porcentaje de Monte Pio'),
(39, 'PRMPRO2', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalización 2do Tipo'),
(40, 'PRMOTR2', 3.000, 'Cantidad de Unidades Tributarias Otra Prima 2do Tipo'),
(41, 'IVA', 0.120, 'Impuesto al Valor Agregado'),
(42, 'CCT', 2.750, 'Comisión gastos administrativos Cesta Ticket'),
(43, 'MED_2012', 1500.000, 'Monto para gastos Médicos y de Farmacia para el año 2012'),
(44, 'PRMANT0', 1.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 3 a 5 años de antiguedad'),
(45, 'PRMOTR3', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalizacion Titulo Doctorado'),
(46, 'SUSTRAC_ISRL', 63.330, 'Sustraendo para el ISRL cuando retancion sea 1%'),
(47, 'PART_IVA', 72.000, 'Codigo de la Partida correspondiente al IVA'),
(48, 'PRESUP_MAX', 35.000, 'Número de partidas por página en reporte General de Presupuesto'),
(49, 'APRFONJUB', 0.030, 'Aporte de Fondo de Jubilaciones'),
(50, 'RETFONJUB', 0.030, 'Retención de Fondo de Jubilaciones');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_asignaciones_per`
--
CREATE TABLE IF NOT EXISTS `vista_asignaciones_per` (
`cod_cnp` int(11)
,`ced_per` varchar(12)
,`nom_con` varchar(60)
,`con_cnp` varchar(100)
,`ncuo_cnp` int(3)
,`ncp_cnp` int(3)
,`nom_car` varchar(100)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_banco_mov`
--
CREATE TABLE IF NOT EXISTS `vista_banco_mov` (
`codigo` int(11)
,`desde` varchar(17)
,`concepto` varchar(255)
,`monto` varbinary(22)
,`sin_partida` varchar(11)
,`banco` int(11)
,`referencia` varchar(50)
,`operacion` varchar(11)
,`fecha` date
,`tipo_mov` varchar(10)
,`estado` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_cargos_per`
--
CREATE TABLE IF NOT EXISTS `vista_cargos_per` (
`ced_per` varchar(12)
,`cod_car` int(11)
,`cod_dep` varchar(5)
,`num_car` int(3) unsigned zerofill
,`nom_car` varchar(100)
,`fch_asg` date
,`fch_vac` date
,`des_car` varchar(1)
,`nom_per` varchar(50)
,`ape_per` varchar(50)
,`nom_tcar` varchar(50)
,`mon_sue` double(9,2)
,`des_sue` varchar(50)
,`mon_sue_ant` double(9,2)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vista_cheques`
--

CREATE TABLE IF NOT EXISTS `vista_cheques` (
  `fch_egr` date DEFAULT NULL,
  `nom_pro` varchar(50) DEFAULT NULL,
  `chq_egr` varchar(25) DEFAULT NULL,
  `nom_ban` varchar(50) DEFAULT NULL,
  `cod_egr` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_movimientos`
--
CREATE TABLE IF NOT EXISTS `vista_movimientos` (
`cod_mov` int(11)
,`ced_per` varchar(12)
,`cod_car` int(11)
,`accion` int(11)
,`estado` int(1)
,`fch_asg` date
,`des_car` varchar(1)
,`fch_vac` date
,`cod_dep` varchar(5)
,`num_car` int(3) unsigned zerofill
,`nom_car` varchar(100)
,`cod_sue` int(2)
,`cod_tcar` int(11)
,`des_sue` varchar(50)
,`mon_sue` double(9,2)
,`nom_tcar` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominapre_proc`
--
CREATE TABLE IF NOT EXISTS `vista_nominapre_proc` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominaspre_pagadas`
--
CREATE TABLE IF NOT EXISTS `vista_nominaspre_pagadas` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominas_pagadas`
--
CREATE TABLE IF NOT EXISTS `vista_nominas_pagadas` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominpre_tot_sum`
--
CREATE TABLE IF NOT EXISTS `vista_nominpre_tot_sum` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
,`cod_tcar` int(11)
,`abr_tcar` varchar(2)
,`cod_dep` varchar(5)
,`nom_tcar` varchar(50)
,`tot_sue` double(19,2)
,`tot_lph` double(19,2)
,`tot_spf` double(19,2)
,`tot_sso` double(19,2)
,`tot_cah` double(19,2)
,`tot_sfu` double(19,2)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nomin_tot_sum`
--
CREATE TABLE IF NOT EXISTS `vista_nomin_tot_sum` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
,`cod_tcar` int(11)
,`abr_tcar` varchar(2)
,`cod_dep` varchar(5)
,`nom_tcar` varchar(50)
,`tot_sue` double(19,2)
,`tot_lph` double(19,2)
,`tot_spf` double(19,2)
,`tot_sso` double(19,2)
,`tot_cah` double(19,2)
,`tot_sfu` double(19,2)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vista_ordenespago`
--

CREATE TABLE IF NOT EXISTS `vista_ordenespago` (
  `fch_egr` date DEFAULT NULL,
  `nom_pro` varchar(50) DEFAULT NULL,
  `con_egr` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_partidas_comprometidas`
--
CREATE TABLE IF NOT EXISTS `vista_partidas_comprometidas` (
`cod_par` int(11)
,`sec_par` varchar(11)
,`pro_par` varchar(11)
,`act_par` varchar(11)
,`ram_par` varchar(11)
,`par_par` varchar(11)
,`gen_par` varchar(11)
,`esp_par` varchar(11)
,`sub_par` varchar(11)
,`des_par` varchar(255)
,`obs_par` varchar(11)
,`frm_com` int(6) unsigned zerofill
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_partidas_presupuesto`
--
CREATE TABLE IF NOT EXISTS `vista_partidas_presupuesto` (
`cod_par` int(11)
,`sec_par` varchar(11)
,`pro_par` varchar(11)
,`act_par` varchar(11)
,`ram_par` varchar(11)
,`par_par` varchar(11)
,`gen_par` varchar(11)
,`esp_par` varchar(11)
,`sub_par` varchar(11)
,`des_par` varchar(255)
,`obs_par` varchar(11)
,`ano_par_mov` varchar(10)
,`idn_par_mov` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_personal`
--
CREATE TABLE IF NOT EXISTS `vista_personal` (
`ced_per` varchar(12)
,`nombre` varchar(101)
,`nac_per` varchar(1)
,`abr_per` varchar(10)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_salida_sincargo`
--
CREATE TABLE IF NOT EXISTS `vista_salida_sincargo` (
`cod_pag` int(11)
,`mon_pag` double(9,2)
,`ced_per` varchar(12)
,`con_pag` varchar(150)
,`des_car` varchar(1)
,`fch_pag` date
,`cod_dep` varchar(5)
,`nom_dep` varchar(150)
,`cod_car` int(11)
,`nom_car` varchar(100)
,`fch_vac` date
,`cod_tcar` int(11)
,`nom_tcar` varchar(50)
,`abr_tcar` varchar(2)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_asignaciones_per`
--
DROP TABLE IF EXISTS `vista_asignaciones_per`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_asignaciones_per` AS select `asg`.`cod_cnp` AS `cod_cnp`,`asg`.`ced_per` AS `ced_per`,`cn`.`nom_con` AS `nom_con`,`asg`.`con_cnp` AS `con_cnp`,`asg`.`ncuo_cnp` AS `ncuo_cnp`,`asg`.`ncp_cnp` AS `ncp_cnp`,`c`.`nom_car` AS `nom_car` from ((`asignaciones` `asg` join `concesiones` `cn`) join `cargos` `c`) where ((`asg`.`cod_con` = `cn`.`cod_con`) and (`c`.`cod_car` = `asg`.`cod_car`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_banco_mov`
--
DROP TABLE IF EXISTS `vista_banco_mov`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_banco_mov` AS select `egresos`.`cod_egr` AS `codigo`,'egresos' AS `desde`,`egresos`.`con_egr` AS `concepto`,sum((`pagos`.`mon_pag` - `egresos`.`ded_egr`)) AS `monto`,`egresos`.`sin_par_egr` AS `sin_partida`,`egresos`.`cod_ban` AS `banco`,`egresos`.`chq_egr` AS `referencia`,`egresos`.`frm_egr` AS `operacion`,`egresos`.`fch_egr` AS `fecha`,'egreso' AS `tipo_mov`,`egresos`.`sta_ban_egr` AS `estado` from (`egresos` join `pagos`) where (`egresos`.`frm_egr` = `pagos`.`frm_egr`) group by `pagos`.`frm_egr` union (select `banco_movimientos`.`cod_ban_mov` AS `codigo`,'banco_movimientos' AS `desde`,`banco_movimientos`.`des_ban_mov` AS `concepto`,`banco_movimientos`.`mon_ban_mov` AS `monto`,'0' AS `sin_partida`,`banco_movimientos`.`cod_ban` AS `banco`,`banco_movimientos`.`ref_ban_mov` AS `referencia`,'' AS `operacion`,`banco_movimientos`.`fch_ban_mov` AS `fecha`,`banco_movimientos`.`tip_ban_mov` AS `tipo_mov`,`banco_movimientos`.`sta_ban_mov` AS `estado` from `banco_movimientos`) union (select `egresos`.`cod_egr` AS `codigo`,'egresos' AS `desde`,`egresos`.`con_egr` AS `concepto`,'0' AS `monto`,`egresos`.`sin_par_egr` AS `sin_partida`,`egresos`.`cod_ban` AS `banco`,`egresos`.`chq_egr` AS `referencia`,`egresos`.`frm_egr` AS `operacion`,`egresos`.`fch_egr` AS `fecha`,'egreso' AS `tipo_mov`,`egresos`.`sta_ban_egr` AS `estado` from `egresos` where ((`egresos`.`sin_par_egr` > 0) and (not(`egresos`.`frm_egr` in (select `pagos`.`frm_egr` from `pagos`)))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_cargos_per`
--
DROP TABLE IF EXISTS `vista_cargos_per`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_cargos_per` AS select `c`.`ced_per` AS `ced_per`,`c`.`cod_car` AS `cod_car`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`c`.`fch_asg` AS `fch_asg`,`c`.`fch_vac` AS `fch_vac`,`c`.`des_car` AS `des_car`,`p`.`nom_per` AS `nom_per`,`p`.`ape_per` AS `ape_per`,`t`.`nom_tcar` AS `nom_tcar`,`s`.`mon_sue` AS `mon_sue`,`s`.`des_sue` AS `des_sue`,`sa`.`mon_sue` AS `mon_sue_ant` from ((((`cargos` `c` join `tipos_cargos` `t`) join `sueldos` `s`) join `sueldos_ant` `sa`) join `personal` `p`) where ((`t`.`cod_tcar` = `c`.`cod_tcar`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`sa`.`cod_sue` = `c`.`cod_sue`) and (`p`.`ced_per` = `c`.`ced_per`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_movimientos`
--
DROP TABLE IF EXISTS `vista_movimientos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_movimientos` AS select `m`.`cod_mov` AS `cod_mov`,`m`.`ced_per` AS `ced_per`,`m`.`cod_car` AS `cod_car`,`m`.`accion` AS `accion`,`m`.`estado` AS `estado`,`m`.`fch_asg` AS `fch_asg`,`m`.`des_car` AS `des_car`,`m`.`fch_vac` AS `fch_vac`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`c`.`cod_sue` AS `cod_sue`,`c`.`cod_tcar` AS `cod_tcar`,`s`.`des_sue` AS `des_sue`,`s`.`mon_sue` AS `mon_sue`,`t`.`nom_tcar` AS `nom_tcar` from (((`prog_movimientos` `m` join `cargos` `c`) join `sueldos` `s`) join `tipos_cargos` `t`) where ((`c`.`cod_car` = `m`.`cod_car`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`t`.`cod_tcar` = `c`.`cod_tcar`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominapre_proc`
--
DROP TABLE IF EXISTS `vista_nominapre_proc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominapre_proc` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominaspre_pagadas`
--
DROP TABLE IF EXISTS `vista_nominaspre_pagadas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominaspre_pagadas` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominas_pagadas`
--
DROP TABLE IF EXISTS `vista_nominas_pagadas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominas_pagadas` AS select `nomina_pagar`.`ano_nom` AS `ano_nom`,`nomina_pagar`.`mes_nom` AS `mes_nom`,`nomina_pagar`.`por_nom` AS `por_nom` from `nomina_pagar` group by `nomina_pagar`.`ano_nom`,`nomina_pagar`.`mes_nom`,`nomina_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominpre_tot_sum`
--
DROP TABLE IF EXISTS `vista_nominpre_tot_sum`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominpre_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nominapre_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nomin_tot_sum`
--
DROP TABLE IF EXISTS `vista_nomin_tot_sum`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nomin_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nomina_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_partidas_comprometidas`
--
DROP TABLE IF EXISTS `vista_partidas_comprometidas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_partidas_comprometidas` AS (select `part`.`cod_par` AS `cod_par`,`part`.`sec_par` AS `sec_par`,`part`.`pro_par` AS `pro_par`,`part`.`act_par` AS `act_par`,`part`.`ram_par` AS `ram_par`,`part`.`par_par` AS `par_par`,`part`.`gen_par` AS `gen_par`,`part`.`esp_par` AS `esp_par`,`part`.`sub_par` AS `sub_par`,`part`.`des_par` AS `des_par`,`part`.`obs_par` AS `obs_par`,`com`.`frm_com` AS `frm_com` from ((`compras` `com` join `partidas_compras` `pcom`) join `part_presup` `part`) where ((`com`.`cod_com` = `pcom`.`cod_com`) and (`pcom`.`cod_par` = `part`.`cod_par`)) group by `part`.`cod_par`,`com`.`frm_com` order by `part`.`par_par`,`part`.`gen_par`,`part`.`esp_par`,`part`.`sub_par`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_partidas_presupuesto`
--
DROP TABLE IF EXISTS `vista_partidas_presupuesto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_partidas_presupuesto` AS (select `part_presup`.`cod_par` AS `cod_par`,`part_presup`.`sec_par` AS `sec_par`,`part_presup`.`pro_par` AS `pro_par`,`part_presup`.`act_par` AS `act_par`,`part_presup`.`ram_par` AS `ram_par`,`part_presup`.`par_par` AS `par_par`,`part_presup`.`gen_par` AS `gen_par`,`part_presup`.`esp_par` AS `esp_par`,`part_presup`.`sub_par` AS `sub_par`,`part_presup`.`des_par` AS `des_par`,`part_presup`.`obs_par` AS `obs_par`,`part_presup_mov`.`ano_par_mov` AS `ano_par_mov`,`part_presup_mov`.`idn_par_mov` AS `idn_par_mov` from ((`part_presup` join `part_presup_mov`) join `productos_part_mov`) where ((`part_presup`.`cod_par` = `productos_part_mov`.`cod_par`) and (`part_presup_mov`.`cod_par_mov` = `productos_part_mov`.`cod_par_mov`)) order by `part_presup`.`par_par`,`part_presup`.`gen_par`,`part_presup`.`esp_par`,`part_presup`.`sub_par`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_personal`
--
DROP TABLE IF EXISTS `vista_personal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_personal` AS select `personal`.`ced_per` AS `ced_per`,concat(`personal`.`nom_per`,_latin1' ',`personal`.`ape_per`) AS `nombre`,`personal`.`nac_per` AS `nac_per`,`personal`.`abr_per` AS `abr_per` from `personal`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_salida_sincargo`
--
DROP TABLE IF EXISTS `vista_salida_sincargo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_salida_sincargo` AS select `prog_mov_pagos`.`cod_pag` AS `cod_pag`,`prog_mov_pagos`.`mon_pag` AS `mon_pag`,`prog_mov_pagos`.`ced_per` AS `ced_per`,`prog_mov_pagos`.`con_pag` AS `con_pag`,`prog_mov_pagos`.`des_car` AS `des_car`,`prog_mov_pagos`.`fch_pag` AS `fch_pag`,`prog_mov_pagos`.`cod_dep` AS `cod_dep`,`prog_mov_pagos`.`nom_dep` AS `nom_dep`,`prog_mov_pagos`.`cod_car` AS `cod_car`,`prog_mov_pagos`.`nom_car` AS `nom_car`,`prog_mov_pagos`.`fch_vac` AS `fch_vac`,`prog_mov_pagos`.`cod_tcar` AS `cod_tcar`,`prog_mov_pagos`.`nom_tcar` AS `nom_tcar`,`prog_mov_pagos`.`abr_tcar` AS `abr_tcar` from (`prog_mov_pagos` left join `cargos` on((`cargos`.`ced_per` = `prog_mov_pagos`.`ced_per`))) where (not(`prog_mov_pagos`.`ced_per` in (select distinct `cargos`.`ced_per` AS `ced_per` from `cargos`)));

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`cod_act`);

--
-- Indices de la tabla `adt_ing_cat`
--
ALTER TABLE `adt_ing_cat`
  ADD PRIMARY KEY (`cod_cat`), ADD UNIQUE KEY `des_cat` (`des_cat`), ADD UNIQUE KEY `des_cat_2` (`des_cat`);

--
-- Indices de la tabla `adt_ing_pro`
--
ALTER TABLE `adt_ing_pro`
  ADD UNIQUE KEY `serie_id` (`serie_id`,`recibo`,`monto`,`cod_cat`);

--
-- Indices de la tabla `anular_form`
--
ALTER TABLE `anular_form`
  ADD PRIMARY KEY (`cod_anl`);

--
-- Indices de la tabla `asignaciones`
--
ALTER TABLE `asignaciones`
  ADD PRIMARY KEY (`cod_cnp`), ADD KEY `cod_con` (`cod_con`), ADD KEY `ced_per` (`ced_per`);

--
-- Indices de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  ADD PRIMARY KEY (`cod_aud`), ADD KEY `cod_usr` (`cod_usr`);

--
-- Indices de la tabla `auditoria_backup`
--
ALTER TABLE `auditoria_backup`
  ADD PRIMARY KEY (`cod_aud`), ADD KEY `cod_usr` (`cod_usr`);

--
-- Indices de la tabla `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`cod_ban`);

--
-- Indices de la tabla `banco_conciliacion`
--
ALTER TABLE `banco_conciliacion`
  ADD PRIMARY KEY (`cod_ban_conc`);

--
-- Indices de la tabla `banco_movimientos`
--
ALTER TABLE `banco_movimientos`
  ADD PRIMARY KEY (`cod_ban_mov`);

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`cod_car`), ADD UNIQUE KEY `num_car` (`num_car`,`cod_dep`), ADD KEY `cod_sue` (`cod_sue`), ADD KEY `cod_dep` (`cod_dep`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD KEY `id` (`cod_com`);

--
-- Indices de la tabla `concesiones`
--
ALTER TABLE `concesiones`
  ADD PRIMARY KEY (`cod_con`);

--
-- Indices de la tabla `constancias_per`
--
ALTER TABLE `constancias_per`
  ADD PRIMARY KEY (`cod_con`);

--
-- Indices de la tabla `cst_tk_pagada`
--
ALTER TABLE `cst_tk_pagada`
  ADD PRIMARY KEY (`cod_cst`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`ced_per`), ADD UNIQUE KEY `num_cue` (`num_cue`);

--
-- Indices de la tabla `deducciones`
--
ALTER TABLE `deducciones`
  ADD PRIMARY KEY (`cod_dsp`), ADD KEY `cod_des` (`cod_des`), ADD KEY `cod_per` (`ced_per`);

--
-- Indices de la tabla `dependencias`
--
ALTER TABLE `dependencias`
  ADD PRIMARY KEY (`cod_dep`);

--
-- Indices de la tabla `descuentos`
--
ALTER TABLE `descuentos`
  ADD PRIMARY KEY (`cod_des`);

--
-- Indices de la tabla `educacion`
--
ALTER TABLE `educacion`
  ADD PRIMARY KEY (`cod_edu`);

--
-- Indices de la tabla `egresos`
--
ALTER TABLE `egresos`
  ADD KEY `id` (`cod_egr`);

--
-- Indices de la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`cod_ntr`), ADD KEY `cod_usr` (`cod_usr`);

--
-- Indices de la tabla `facturas_pagos`
--
ALTER TABLE `facturas_pagos`
  ADD PRIMARY KEY (`cod_fac_pag`);

--
-- Indices de la tabla `familiares`
--
ALTER TABLE `familiares`
  ADD PRIMARY KEY (`cod_fam`), ADD UNIQUE KEY `ced_per` (`ced_per`,`nom_fam`);

--
-- Indices de la tabla `feriados`
--
ALTER TABLE `feriados`
  ADD PRIMARY KEY (`fch_frd`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`cod_grp`), ADD UNIQUE KEY `nom_grp` (`nom_grp`);

--
-- Indices de la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`cod_hor`);

--
-- Indices de la tabla `inasistencias`
--
ALTER TABLE `inasistencias`
  ADD PRIMARY KEY (`cod_ina`), ADD UNIQUE KEY `ced_per` (`ced_per`,`fch_ina`);

--
-- Indices de la tabla `incidencias`
--
ALTER TABLE `incidencias`
  ADD PRIMARY KEY (`cod_inc`), ADD UNIQUE KEY `mes_inc` (`mes_inc`,`ano_inc`,`fini_inc`,`ffin_inc`);

--
-- Indices de la tabla `incidencias_sueldos`
--
ALTER TABLE `incidencias_sueldos`
  ADD UNIQUE KEY `cod_inc` (`cod_inc`,`cod_sue`);

--
-- Indices de la tabla `ing_pat_vhi`
--
ALTER TABLE `ing_pat_vhi`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `justificativos_per`
--
ALTER TABLE `justificativos_per`
  ADD PRIMARY KEY (`cod_sol_jus`);

--
-- Indices de la tabla `medicinas`
--
ALTER TABLE `medicinas`
  ADD PRIMARY KEY (`cod_med`);

--
-- Indices de la tabla `medicinas_per`
--
ALTER TABLE `medicinas_per`
  ADD PRIMARY KEY (`cod_med_per`);

--
-- Indices de la tabla `nominapre_asign`
--
ALTER TABLE `nominapre_asign`
  ADD PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`);

--
-- Indices de la tabla `nominapre_deduc`
--
ALTER TABLE `nominapre_deduc`
  ADD PRIMARY KEY (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`);

--
-- Indices de la tabla `nominapre_pagar`
--
ALTER TABLE `nominapre_pagar`
  ADD PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`);

--
-- Indices de la tabla `nomina_asign`
--
ALTER TABLE `nomina_asign`
  ADD PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`);

--
-- Indices de la tabla `nomina_deduc`
--
ALTER TABLE `nomina_deduc`
  ADD PRIMARY KEY (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`);

--
-- Indices de la tabla `nomina_incidencias`
--
ALTER TABLE `nomina_incidencias`
  ADD PRIMARY KEY (`ano_inc`,`mes_inc`,`cod_car`);

--
-- Indices de la tabla `nomina_pagar`
--
ALTER TABLE `nomina_pagar`
  ADD PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`);

--
-- Indices de la tabla `oficios_enviados`
--
ALTER TABLE `oficios_enviados`
  ADD PRIMARY KEY (`cod_ofi`), ADD UNIQUE KEY `tip_ofi` (`tip_ofi`,`num_ofi`,`fch_ofi`,`rdp_ofi`), ADD KEY `cod_dep` (`cod_dep`);

--
-- Indices de la tabla `oficios_recibidos`
--
ALTER TABLE `oficios_recibidos`
  ADD PRIMARY KEY (`cod_ofr`);

--
-- Indices de la tabla `pagados`
--
ALTER TABLE `pagados`
  ADD PRIMARY KEY (`ced_per`);

--
-- Indices de la tabla `pagados_frc`
--
ALTER TABLE `pagados_frc`
  ADD PRIMARY KEY (`cod_car`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD KEY `id` (`cod_pag`);

--
-- Indices de la tabla `partidas_compras`
--
ALTER TABLE `partidas_compras`
  ADD PRIMARY KEY (`cod_pro_com`);

--
-- Indices de la tabla `partidas_pagos`
--
ALTER TABLE `partidas_pagos`
  ADD PRIMARY KEY (`cod_pro_pag`);

--
-- Indices de la tabla `part_presup`
--
ALTER TABLE `part_presup`
  ADD PRIMARY KEY (`cod_par`), ADD UNIQUE KEY `sector` (`par_par`,`gen_par`,`esp_par`,`sub_par`);

--
-- Indices de la tabla `part_presup_mov`
--
ALTER TABLE `part_presup_mov`
  ADD PRIMARY KEY (`cod_par_mov`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD KEY `cod_grp` (`cod_grp`), ADD KEY `cod_sec` (`cod_sec`);

--
-- Indices de la tabla `permisos_per`
--
ALTER TABLE `permisos_per`
  ADD PRIMARY KEY (`cod_sol_perm`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`ced_per`);

--
-- Indices de la tabla `productos_compras`
--
ALTER TABLE `productos_compras`
  ADD KEY `id` (`cod_pro_com`);

--
-- Indices de la tabla `productos_part_mov`
--
ALTER TABLE `productos_part_mov`
  ADD PRIMARY KEY (`cod_pro_par_mov`);

--
-- Indices de la tabla `programas`
--
ALTER TABLE `programas`
  ADD PRIMARY KEY (`cod_pro`);

--
-- Indices de la tabla `prog_movimientos`
--
ALTER TABLE `prog_movimientos`
  ADD PRIMARY KEY (`cod_mov`);

--
-- Indices de la tabla `prog_mov_pagos`
--
ALTER TABLE `prog_mov_pagos`
  ADD PRIMARY KEY (`cod_pag`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`cod_pro`), ADD UNIQUE KEY `rif_pro` (`rif_pro`), ADD UNIQUE KEY `nom_pro` (`nom_pro`);

--
-- Indices de la tabla `retenciones_isrl`
--
ALTER TABLE `retenciones_isrl`
  ADD PRIMARY KEY (`cod_ret_isrl`);

--
-- Indices de la tabla `retenciones_iva`
--
ALTER TABLE `retenciones_iva`
  ADD PRIMARY KEY (`cod_ret_iva`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`cod_sec`);

--
-- Indices de la tabla `sueldos`
--
ALTER TABLE `sueldos`
  ADD PRIMARY KEY (`cod_sue`);

--
-- Indices de la tabla `sueldos_ant`
--
ALTER TABLE `sueldos_ant`
  ADD PRIMARY KEY (`cod_sue`);

--
-- Indices de la tabla `temp_presup`
--
ALTER TABLE `temp_presup`
  ADD PRIMARY KEY (`generado`);

--
-- Indices de la tabla `tipos_cargos`
--
ALTER TABLE `tipos_cargos`
  ADD PRIMARY KEY (`cod_tcar`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`cod_usr`), ADD KEY `cod_grp` (`cod_grp`);

--
-- Indices de la tabla `vacaciones_per`
--
ALTER TABLE `vacaciones_per`
  ADD PRIMARY KEY (`cod_sol_vac`);

--
-- Indices de la tabla `valores`
--
ALTER TABLE `valores`
  ADD PRIMARY KEY (`cod_val`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adt_ing_cat`
--
ALTER TABLE `adt_ing_cat`
  MODIFY `cod_cat` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de Categoría';
--
-- AUTO_INCREMENT de la tabla `anular_form`
--
ALTER TABLE `anular_form`
  MODIFY `cod_anl` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `asignaciones`
--
ALTER TABLE `asignaciones`
  MODIFY `cod_cnp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion';
--
-- AUTO_INCREMENT de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  MODIFY `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de Auditor?a';
--
-- AUTO_INCREMENT de la tabla `auditoria_backup`
--
ALTER TABLE `auditoria_backup`
  MODIFY `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de Auditor?a';
--
-- AUTO_INCREMENT de la tabla `banco`
--
ALTER TABLE `banco`
  MODIFY `cod_ban` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `banco_conciliacion`
--
ALTER TABLE `banco_conciliacion`
  MODIFY `cod_ban_conc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `banco_movimientos`
--
ALTER TABLE `banco_movimientos`
  MODIFY `cod_ban_mov` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cargos`
--
ALTER TABLE `cargos`
  MODIFY `cod_car` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo del cargo';
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `cod_com` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `concesiones`
--
ALTER TABLE `concesiones`
  MODIFY `cod_con` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de concesion';
--
-- AUTO_INCREMENT de la tabla `constancias_per`
--
ALTER TABLE `constancias_per`
  MODIFY `cod_con` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cst_tk_pagada`
--
ALTER TABLE `cst_tk_pagada`
  MODIFY `cod_cst` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código de Cesta Ticket Pagada';
--
-- AUTO_INCREMENT de la tabla `deducciones`
--
ALTER TABLE `deducciones`
  MODIFY `cod_dsp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la Deduccion';
--
-- AUTO_INCREMENT de la tabla `descuentos`
--
ALTER TABLE `descuentos`
  MODIFY `cod_des` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de descuento';
--
-- AUTO_INCREMENT de la tabla `educacion`
--
ALTER TABLE `educacion`
  MODIFY `cod_edu` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `egresos`
--
ALTER TABLE `egresos`
  MODIFY `cod_egr` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `entradas`
--
ALTER TABLE `entradas`
  MODIFY `cod_ntr` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de la Entrada';
--
-- AUTO_INCREMENT de la tabla `facturas_pagos`
--
ALTER TABLE `facturas_pagos`
  MODIFY `cod_fac_pag` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `familiares`
--
ALTER TABLE `familiares`
  MODIFY `cod_fam` int(2) NOT NULL AUTO_INCREMENT COMMENT 'C?digo del Familiar';
--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `cod_grp` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del grupo',AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `horarios`
--
ALTER TABLE `horarios`
  MODIFY `cod_hor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `inasistencias`
--
ALTER TABLE `inasistencias`
  MODIFY `cod_ina` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `incidencias`
--
ALTER TABLE `incidencias`
  MODIFY `cod_inc` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion';
--
-- AUTO_INCREMENT de la tabla `ing_pat_vhi`
--
ALTER TABLE `ing_pat_vhi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `justificativos_per`
--
ALTER TABLE `justificativos_per`
  MODIFY `cod_sol_jus` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `medicinas`
--
ALTER TABLE `medicinas`
  MODIFY `cod_med` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo de pago de medicinas';
--
-- AUTO_INCREMENT de la tabla `medicinas_per`
--
ALTER TABLE `medicinas_per`
  MODIFY `cod_med_per` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `oficios_enviados`
--
ALTER TABLE `oficios_enviados`
  MODIFY `cod_ofi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `oficios_recibidos`
--
ALTER TABLE `oficios_recibidos`
  MODIFY `cod_ofr` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `cod_pag` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `partidas_compras`
--
ALTER TABLE `partidas_compras`
  MODIFY `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `partidas_pagos`
--
ALTER TABLE `partidas_pagos`
  MODIFY `cod_pro_pag` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `part_presup`
--
ALTER TABLE `part_presup`
  MODIFY `cod_par` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `part_presup_mov`
--
ALTER TABLE `part_presup_mov`
  MODIFY `cod_par_mov` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `permisos_per`
--
ALTER TABLE `permisos_per`
  MODIFY `cod_sol_perm` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos_compras`
--
ALTER TABLE `productos_compras`
  MODIFY `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos_part_mov`
--
ALTER TABLE `productos_part_mov`
  MODIFY `cod_pro_par_mov` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `prog_movimientos`
--
ALTER TABLE `prog_movimientos`
  MODIFY `cod_mov` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `prog_mov_pagos`
--
ALTER TABLE `prog_mov_pagos`
  MODIFY `cod_pag` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `cod_pro` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `retenciones_isrl`
--
ALTER TABLE `retenciones_isrl`
  MODIFY `cod_ret_isrl` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `retenciones_iva`
--
ALTER TABLE `retenciones_iva`
  MODIFY `cod_ret_iva` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `cod_sec` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo de la seccion',AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `sueldos`
--
ALTER TABLE `sueldos`
  MODIFY `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo';
--
-- AUTO_INCREMENT de la tabla `sueldos_ant`
--
ALTER TABLE `sueldos_ant`
  MODIFY `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `temp_presup`
--
ALTER TABLE `temp_presup`
  MODIFY `generado` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipos_cargos`
--
ALTER TABLE `tipos_cargos`
  MODIFY `cod_tcar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `vacaciones_per`
--
ALTER TABLE `vacaciones_per`
  MODIFY `cod_sol_vac` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `valores`
--
ALTER TABLE `valores`
  MODIFY `cod_val` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del Valor',AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
