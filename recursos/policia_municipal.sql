-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 25-05-2015 a las 18:31:11
-- Versión del servidor: 5.6.19-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `policia_municipal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE IF NOT EXISTS `actividades` (
  `cod_act` varchar(5) NOT NULL COMMENT 'codigo de la Actividad',
  `nom_act` varchar(150) NOT NULL COMMENT 'nombre de la actividad',
  PRIMARY KEY (`cod_act`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adt_ing_cat`
--

CREATE TABLE IF NOT EXISTS `adt_ing_cat` (
  `cod_cat` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de Categoría',
  `des_cat` varchar(50) NOT NULL COMMENT 'Descripción de Categoría',
  PRIMARY KEY (`cod_cat`),
  UNIQUE KEY `des_cat` (`des_cat`),
  UNIQUE KEY `des_cat_2` (`des_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las Categorias de los recibos para Auditorias' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adt_ing_pro`
--

CREATE TABLE IF NOT EXISTS `adt_ing_pro` (
  `serie_id` int(11) NOT NULL,
  `recibo` int(11) NOT NULL,
  `monto` decimal(9,2) NOT NULL,
  `cod_cat` int(2) NOT NULL,
  `con_esp` varchar(50) NOT NULL COMMENT 'Condición Especial',
  UNIQUE KEY `serie_id` (`serie_id`,`recibo`,`monto`,`cod_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anular_form`
--

CREATE TABLE IF NOT EXISTS `anular_form` (
  `cod_anl` int(11) NOT NULL AUTO_INCREMENT,
  `fch_anl` date NOT NULL,
  `tip_anl` varchar(1) NOT NULL,
  `num_anl` int(5) unsigned zerofill NOT NULL,
  `mot_anl` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_anl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los Formularios Anulados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaciones`
--

CREATE TABLE IF NOT EXISTS `asignaciones` (
  `cod_cnp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion',
  `con_cnp` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `ncuo_cnp` int(3) NOT NULL COMMENT 'Numero de cuotas de la asignacion',
  `per_cnp` varchar(1) NOT NULL COMMENT 'Permanencia de la asignacion',
  `mcuo_cnp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la asignacion',
  `por_cnp` int(3) NOT NULL COMMENT 'Porcentaje de asignacion',
  `bpor_cnp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_con` int(11) NOT NULL COMMENT 'Codigo del tipo de concesion',
  `ced_per` varchar(12) NOT NULL COMMENT 'La cedula de la persona',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_cnp` date NOT NULL COMMENT 'Fecha de Registro de la asignacion',
  `ncp_cnp` int(3) NOT NULL COMMENT 'N?mero de cuotas pagadas',
  `des_cnp` varchar(1) NOT NULL COMMENT 'Aplica para Descuentos de ley',
  PRIMARY KEY (`cod_cnp`),
  KEY `cod_con` (`cod_con`),
  KEY `ced_per` (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE IF NOT EXISTS `auditoria` (
  `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de Auditor?a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci?n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci?n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu? la acci?n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci?n',
  `hora` time NOT NULL COMMENT 'hora de la acci?n',
  PRIMARY KEY (`cod_aud`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema' AUTO_INCREMENT=39 ;

--
-- Volcado de datos para la tabla `auditoria`
--

INSERT INTO `auditoria` (`cod_aud`, `cod_usr`, `accion`, `detalle`, `tabla`, `fecha`, `hora`) VALUES
(1, '15031097', 'insertar', 'cod_val=0001;des_val=prueba de valores;val_val=12000;con_val=prueba', 'valores', '2015-05-22', '14:18:00'),
(2, '15031097', 'insertar', 'cod_val=;des_val=prueba de valores;val_val=12000;con_val=prueba', 'valores', '2015-05-25', '15:50:00'),
(3, '15031097', 'insertar', 'cod_dep=POL;nom_dep=POLICIA MUNICIPAL;cod_act=;cod_pro=', 'dependencias', '2015-05-25', '15:57:00'),
(4, '15031097', 'insertar', 'mon_sue=6000;des_sue=ASISTENTE ADMINISTRATIVO', 'sueldos', '2015-05-25', '16:07:00'),
(5, '15031097', 'insertar', 'mon_sue=6000;des_sue=SECRETARIO', 'sueldos', '2015-05-25', '16:07:00'),
(6, '15031097', 'insertar', 'mon_sue=6291;des_sue=ESCOLTA', 'sueldos', '2015-05-25', '16:09:00'),
(7, '15031097', 'insertar', 'mon_sue=9776.22;des_sue=ABOGADO', 'sueldos', '2015-05-25', '16:11:00'),
(8, '15031097', 'insertar', 'mon_sue=14664.34;des_sue=DIRECTOR GENERAL', 'sueldos', '2015-05-25', '16:15:00'),
(9, '15031097', 'insertar', 'mon_sue=11242.65;des_sue=DIRECTORA DE ADMINISTRACION', 'sueldos', '2015-05-25', '16:15:00'),
(10, '15031097', 'insertar', 'mon_sue=11242.65;des_sue=DIRECTORA RECURSOS HUMANOS', 'sueldos', '2015-05-25', '16:16:00'),
(11, '15031097', 'insertar', 'mon_sue=7923;des_sue=SERVIDOR AGREGADO', 'sueldos', '2015-05-25', '16:19:00'),
(12, '15031097', 'insertar', 'mon_sue=6672;des_sue=OFICIAL JEFE Nº2', 'sueldos', '2015-05-25', '16:20:00'),
(13, '15031097', 'insertar', 'mon_sue=6672;des_sue=OFICIAL JEFE Nº3', 'sueldos', '2015-05-25', '16:20:00'),
(14, '15031097', 'eliminar', 'mon_sue=6672.00;des_sue=OFICIAL JEFE Nº3', 'sueldos', '2015-05-25', '16:21:00'),
(15, '15031097', 'modificar', '', 'sueldos', '2015-05-25', '16:22:00'),
(16, '15031097', 'insertar', 'mon_sue=6291;des_sue=OFICIAL AGREGADO', 'sueldos', '2015-05-25', '16:22:00'),
(17, '15031097', 'insertar', 'mon_sue=7404;des_sue=SEPERVISOR', 'sueldos', '2015-05-25', '16:23:00'),
(18, '15031097', 'insertar', 'mon_sue=5880;des_sue=OFICIAL', 'sueldos', '2015-05-25', '16:24:00'),
(19, '15031097', 'insertar', 'mon_sue=5376.92;des_sue=Personal Civil', 'sueldos', '2015-05-25', '16:38:00'),
(20, '15031097', 'insertar', 'num_car=;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=;est_car=;cod_tcar=;cod_dep=;cod_hor=;des_car=X', 'cargos', '2015-05-25', '16:53:00'),
(21, '15031097', 'eliminar', 'num_car=000;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=0;est_car=;cod_tcar=0;cod_dep=;cod_hor=0;des_car=X', 'cargos', '2015-05-25', '16:53:00'),
(22, '15031097', 'insertar', 'num_car=1;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=1;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '16:59:00'),
(23, '15031097', 'insertar', 'num_car=2;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=1;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:06:00'),
(24, '15031097', 'insertar', 'num_car=3;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=1;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:06:00'),
(25, '15031097', 'insertar', 'num_car=4;nom_car=SECRETARIO;cod_sue=2;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:07:00'),
(26, '15031097', 'insertar', 'num_car=5;nom_car=SECRETARIO;cod_sue=2;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:07:00'),
(27, '15031097', 'insertar', 'num_car=6;nom_car=ESCOLTA;cod_sue=3;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:08:00'),
(28, '15031097', 'insertar', 'num_car=7;nom_car=ABOGADO;cod_sue=4;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:10:00'),
(29, '15031097', 'insertar', 'num_car=8;nom_car=OFICIAL Nº1;cod_sue=13;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:14:00'),
(30, '15031097', 'insertar', 'num_car=9;nom_car=OFICIAL Nº2;cod_sue=13;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:14:00'),
(31, '15031097', 'insertar', 'num_car=10;nom_car=OFICIAL Nº 1;cod_sue=13;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X', 'cargos', '2015-05-25', '17:14:00'),
(32, '15031097', 'modificar', '', 'cargos', '2015-05-25', '17:15:00'),
(33, '15031097', 'insertar', 'nom_con=Retroactivo aumento ;des_con=sASAsaS', 'concesiones', '2015-05-25', '17:20:00'),
(34, '15031097', 'insertar', 'nom_des=Comedor;des_des=Comedor', 'descuentos', '2015-05-25', '17:25:00'),
(35, '15031097', 'insertar', 'nom_des=Plan Coorporativo;des_des=Plan Coorporativo', 'descuentos', '2015-05-25', '17:26:00'),
(36, '15031097', 'insertar', 'nom_des=Reino del Mueble;des_des=Reino del Mueble', 'descuentos', '2015-05-25', '17:26:00'),
(37, '15031097', 'insertar', 'ced_per=16657064;nac_per=V;nom_per=JUAN JOSE;ape_per=MARQUEZ TREJO;sex_per=M;fnac_per=1993-04-15;lnac_per=MERIDA;cor_per=JUANJMT@GMAIL.COM;pro_per=ING.;abr_per=ING.;dir_per=EJIDO;tel_per=000000000;cel_per=000000000;fch_reg=2015-05-25;lph_des=A;spf_des=A;sso_des=A;cah_des=A;sfu_des=;hog_asg=A;hij_asg=A;ant_asg=A;pro_asg=A;hje_asg=;jer_asg=;otr_asg=;fnj_des=', 'personal', '2015-05-25', '17:49:00'),
(38, '15031097', 'insertar', 'ced_per=16657064;tit_edu=Ingeniero;niv_edu=Univ;ins_edu=IUTE;fch_edu=2015-05-19;reg_edu=S', 'educacion', '2015-05-25', '18:05:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_backup`
--

CREATE TABLE IF NOT EXISTS `auditoria_backup` (
  `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de Auditor?a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci?n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci?n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu? la acci?n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci?n',
  `hora` time NOT NULL COMMENT 'hora de la acci?n',
  PRIMARY KEY (`cod_aud`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE IF NOT EXISTS `banco` (
  `cod_ban` int(11) NOT NULL AUTO_INCREMENT,
  `nom_ban` varchar(50) DEFAULT NULL,
  `cue_ban` varchar(20) DEFAULT NULL,
  `des_ban` varchar(50) NOT NULL COMMENT 'Destino de la Cuenta',
  PRIMARY KEY (`cod_ban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco_conciliacion`
--

CREATE TABLE IF NOT EXISTS `banco_conciliacion` (
  `cod_ban_conc` int(11) NOT NULL AUTO_INCREMENT,
  `cod_ban` int(11) NOT NULL,
  `mes_ban_conc` int(11) NOT NULL,
  `ano_ban_conc` int(4) NOT NULL,
  `sal_ban_conc` decimal(9,2) NOT NULL,
  `obs_ban_conc` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ban_conc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las concialiaciones bancarias' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco_movimientos`
--

CREATE TABLE IF NOT EXISTS `banco_movimientos` (
  `cod_ban_mov` int(11) NOT NULL AUTO_INCREMENT,
  `fch_ban_mov` date NOT NULL,
  `tip_ban_mov` varchar(10) NOT NULL,
  `mon_ban_mov` double(9,2) NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `des_ban_mov` varchar(255) NOT NULL,
  `obs_ban_mov` varchar(255) NOT NULL,
  `ref_ban_mov` varchar(50) NOT NULL,
  `sta_ban_mov` int(11) NOT NULL,
  PRIMARY KEY (`cod_ban_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de los movimientos bancarios' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE IF NOT EXISTS `cargos` (
  `cod_car` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo del cargo',
  `num_car` int(3) unsigned zerofill NOT NULL COMMENT 'numero del cargo',
  `nom_car` varchar(100) NOT NULL COMMENT 'nombre del cargo',
  `cod_sue` int(2) NOT NULL COMMENT 'codigo del sueldo',
  `est_car` varchar(1) NOT NULL COMMENT 'estado del cargo',
  `cod_tcar` int(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `ced_per` varchar(12) NOT NULL,
  `fch_asg` date NOT NULL COMMENT 'Fecha de asignacion  del cargo',
  `des_car` varchar(1) NOT NULL COMMENT 'Se efectuan los descuentos de ley por este Cargo',
  `fch_vac` date NOT NULL,
  `cod_hor` int(11) NOT NULL,
  PRIMARY KEY (`cod_car`),
  UNIQUE KEY `num_car` (`num_car`,`cod_dep`),
  KEY `cod_sue` (`cod_sue`),
  KEY `cod_dep` (`cod_dep`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`cod_car`, `num_car`, `nom_car`, `cod_sue`, `est_car`, `cod_tcar`, `cod_dep`, `ced_per`, `fch_asg`, `des_car`, `fch_vac`, `cod_hor`) VALUES
(2, 001, 'ASISTENTE ADMINISTRATIVO', 1, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(3, 002, 'ASISTENTE ADMINISTRATIVO', 1, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(4, 003, 'ASISTENTE ADMINISTRATIVO', 1, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(5, 004, 'SECRETARIO', 2, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(6, 005, 'SECRETARIO', 2, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(7, 006, 'ESCOLTA', 3, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(8, 007, 'ABOGADO', 4, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(9, 008, 'OFICIAL Nº1', 13, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(10, 009, 'OFICIAL Nº2', 13, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1),
(11, 010, 'OFICIAL Nº3', 13, '', 2, 'POL', '', '0000-00-00', 'X', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE IF NOT EXISTS `compras` (
  `cod_com` int(11) NOT NULL AUTO_INCREMENT,
  `fch_com` date NOT NULL,
  `idn_par_mov` varchar(50) NOT NULL,
  `nor_com` int(4) unsigned zerofill NOT NULL,
  `frm_com` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `sol_com` varchar(20) DEFAULT NULL,
  `tip_com` varchar(15) NOT NULL,
  `for_com` varchar(15) DEFAULT NULL,
  `adl_com` double(9,2) DEFAULT NULL,
  `fre_com` varchar(255) DEFAULT NULL,
  `mon_com` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `rif_pro` varchar(10) NOT NULL,
  `npr_com` int(2) NOT NULL,
  `iva_com` int(2) NOT NULL,
  `ela_com` varchar(50) NOT NULL,
  `rev_com` varchar(50) NOT NULL,
  `obs_com` varchar(255) DEFAULT NULL,
  KEY `id` (`cod_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concesiones`
--

CREATE TABLE IF NOT EXISTS `concesiones` (
  `cod_con` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de concesion',
  `nom_con` varchar(60) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre del tipo de concesion',
  `des_con` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de concesion',
  PRIMARY KEY (`cod_con`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `concesiones`
--

INSERT INTO `concesiones` (`cod_con`, `nom_con`, `des_con`) VALUES
(1, 'Retroactivo aumento ', 'sASAsaS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `constancias_per`
--

CREATE TABLE IF NOT EXISTS `constancias_per` (
  `cod_con` int(11) NOT NULL AUTO_INCREMENT,
  `fch_con` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `nom_dep` varchar(100) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `des_con` varchar(50) NOT NULL,
  `sue_con` varchar(1) NOT NULL,
  `prm_con` varchar(1) NOT NULL,
  `cst_con` varchar(1) NOT NULL,
  `sue_mon` decimal(9,2) NOT NULL,
  `prm_mon` decimal(9,2) NOT NULL,
  `cst_mon` decimal(9,2) NOT NULL,
  `mot_con` varchar(120) NOT NULL,
  PRIMARY KEY (`cod_con`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Constancias Solicitadas' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cst_tk_pagada`
--

CREATE TABLE IF NOT EXISTS `cst_tk_pagada` (
  `cod_cst` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código de Cesta Ticket Pagada',
  `mes_cst` int(11) NOT NULL COMMENT 'Mes de la Cesta Ticket pagada',
  `ano_cst` int(11) NOT NULL COMMENT 'Año de de la Cesta Ticket pagada',
  `ced_per` int(11) NOT NULL COMMENT 'Cédula del Personal de la Cesta Ticket pagada',
  `dias_cst` int(11) NOT NULL COMMENT 'Total de días del mes de la Cesta Ticket pagada',
  `dias_cst_pag` int(11) NOT NULL COMMENT 'Total de días pagados por mes de la Cesta Ticket pagada',
  `ut_cst` int(11) NOT NULL COMMENT 'Bs. por día de la Cesta Ticket pagada',
  `mnt_cst` int(11) NOT NULL COMMENT 'Total Bs. por personal de la Cesta Ticket pagada',
  PRIMARY KEY (`cod_cst`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de la Cesta Ticket pagada' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE IF NOT EXISTS `cuentas` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `num_cue` varchar(20) NOT NULL COMMENT 'numero de cuenta de la persona',
  `tip_cue` varchar(1) NOT NULL COMMENT 'tipo de la cuenta de la persona',
  `fcam_cue` date NOT NULL COMMENT 'fecha de cambio del numero de cuenta',
  `ban_cue` varchar(50) NOT NULL,
  PRIMARY KEY (`ced_per`),
  UNIQUE KEY `num_cue` (`num_cue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`ced_per`, `num_cue`, `tip_cue`, `fcam_cue`, `ban_cue`) VALUES
('16657064', '12345678901234567890', '1', '2015-05-25', 'Venezuela');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deducciones`
--

CREATE TABLE IF NOT EXISTS `deducciones` (
  `cod_dsp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la Deduccion',
  `con_dsp` varchar(100) NOT NULL COMMENT 'Concepto de la Deduccion',
  `ncuo_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas de la deduccion',
  `per_dsp` varchar(1) NOT NULL COMMENT 'Permanencia de la Deduccion',
  `mcuo_dsp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la deduccion',
  `por_dsp` int(3) NOT NULL COMMENT 'Porcentaje de deduccion',
  `bpor_dsp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_des` int(11) NOT NULL COMMENT 'Codigo del Descuento',
  `ced_per` varchar(12) NOT NULL COMMENT 'Cedula del Personal',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_dsp` date NOT NULL COMMENT 'Fecha de Registro de la Deduccion',
  `ncp_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas pagadas',
  PRIMARY KEY (`cod_dsp`),
  KEY `cod_des` (`cod_des`),
  KEY `cod_per` (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencias`
--

CREATE TABLE IF NOT EXISTS `dependencias` (
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `nom_dep` varchar(150) NOT NULL COMMENT 'nombre de la dependencia',
  `cod_act` varchar(5) NOT NULL COMMENT 'actividad de la dependencia',
  `cod_pro` varchar(5) NOT NULL COMMENT 'programa de la dependencia',
  PRIMARY KEY (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dependencias`
--

INSERT INTO `dependencias` (`cod_dep`, `nom_dep`, `cod_act`, `cod_pro`) VALUES
('POL', 'POLICIA MUNICIPAL', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuentos`
--

CREATE TABLE IF NOT EXISTS `descuentos` (
  `cod_des` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de descuento',
  `nom_des` varchar(50) NOT NULL COMMENT 'Nombre del tipo de descuento',
  `des_des` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de descuento',
  PRIMARY KEY (`cod_des`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `descuentos`
--

INSERT INTO `descuentos` (`cod_des`, `nom_des`, `des_des`) VALUES
(1, 'Comedor', 'Comedor'),
(2, 'Plan Coorporativo', 'Plan Coorporativo'),
(3, 'Reino del Mueble', 'Reino del Mueble');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `educacion`
--

CREATE TABLE IF NOT EXISTS `educacion` (
  `cod_edu` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `niv_edu` varchar(50) NOT NULL,
  `ins_edu` varchar(100) NOT NULL,
  `fch_edu` date NOT NULL,
  `reg_edu` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_edu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Datos de los Estudios Realizados' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `educacion`
--

INSERT INTO `educacion` (`cod_edu`, `ced_per`, `tit_edu`, `niv_edu`, `ins_edu`, `fch_edu`, `reg_edu`) VALUES
(1, '16657064', 'Ingeniero', 'Univ', 'IUTE', '2015-05-19', 'S');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `egresos`
--

CREATE TABLE IF NOT EXISTS `egresos` (
  `cod_egr` int(11) NOT NULL AUTO_INCREMENT,
  `fch_egr` date NOT NULL,
  `paga_imp` varchar(20) NOT NULL COMMENT 'Si paga impuesto indique que tipo',
  `nor_egr` int(4) unsigned zerofill NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `chq_egr` varchar(25) NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `con_egr` varchar(255) NOT NULL,
  `ret_iva_egr` int(3) NOT NULL,
  `ret_isrl_egr` int(2) NOT NULL,
  `ela_egr` varchar(50) NOT NULL,
  `rev_egr` varchar(50) NOT NULL,
  `apr_egr` varchar(50) NOT NULL,
  `cont_egr` varchar(50) NOT NULL,
  `obs_egr` varchar(255) DEFAULT NULL,
  `ded_egr` double(9,2) NOT NULL COMMENT 'Otras Deducciones',
  `sin_par_egr` decimal(9,2) NOT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `sta_ban_egr` int(11) NOT NULL,
  KEY `id` (`cod_egr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE IF NOT EXISTS `entradas` (
  `cod_ntr` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de la Entrada',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `fecha_ntr` date NOT NULL COMMENT 'fecha de la entrada',
  `hora_ntr` char(15) NOT NULL COMMENT 'Hora de la Entrada',
  PRIMARY KEY (`cod_ntr`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Entradas de los Usuarios al Sistema' AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`cod_ntr`, `cod_usr`, `fecha_ntr`, `hora_ntr`) VALUES
(1, '15031097', '2015-05-19', '09:45:42 am'),
(2, '15031097', '2015-05-22', '02:17:25 pm'),
(3, '11111111111', '2015-05-25', '03:37:58 pm'),
(4, '11111111111', '2015-05-25', '03:38:46 pm'),
(5, '15031097', '2015-05-25', '03:50:38 pm');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas_pagos`
--

CREATE TABLE IF NOT EXISTS `facturas_pagos` (
  `cod_fac_pag` int(11) NOT NULL AUTO_INCREMENT,
  `cod_pag` int(11) NOT NULL,
  `fch_fac_pag` date NOT NULL,
  `num_fac_pag` varchar(15) NOT NULL,
  `con_fac_pag` varchar(15) NOT NULL,
  `mon_fac_pag` double(9,2) NOT NULL,
  `iva_fac_pag` double(9,2) NOT NULL,
  `isrl_fac_pag` double(9,2) NOT NULL,
  `iva_pag_pag` date NOT NULL,
  `iva_pag_mes` int(2) NOT NULL,
  `iva_pag_ano` int(4) NOT NULL,
  PRIMARY KEY (`cod_fac_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las facturas pagadas' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familiares`
--

CREATE TABLE IF NOT EXISTS `familiares` (
  `cod_fam` int(2) NOT NULL AUTO_INCREMENT COMMENT 'C?digo del Familiar',
  `ced_per` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C?dula del Personal',
  `ced_fam` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C?dula del  del Familiar',
  `nom_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre  del Familiar',
  `ape_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Apellido  del Familiar',
  `sex_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Sexo del Familiar',
  `fnac_fam` date NOT NULL COMMENT 'Fecha de Nacimiento del Familiar',
  `par_fam` varchar(25) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Parentesco  del Familiar',
  `est_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Estudia el Familiar?',
  `obs_fam` varchar(255) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Observaciones acerca del Familiar',
  PRIMARY KEY (`cod_fam`),
  UNIQUE KEY `ced_per` (`ced_per`,`nom_fam`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Datos de los Familiares del Personal' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `familiares`
--

INSERT INTO `familiares` (`cod_fam`, `ced_per`, `ced_fam`, `nom_fam`, `ape_fam`, `sex_fam`, `fnac_fam`, `par_fam`, `est_fam`, `obs_fam`) VALUES
(1, '16657064', '', 'Juan Andres', 'Marquez', 'M', '2015-05-27', 'H', 'S', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `feriados`
--

CREATE TABLE IF NOT EXISTS `feriados` (
  `fch_frd` date NOT NULL,
  `des_frd` varchar(50) NOT NULL,
  `tip_frd` varchar(25) NOT NULL,
  `tck_frd` varchar(2) NOT NULL,
  PRIMARY KEY (`fch_frd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de dias Feridos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE IF NOT EXISTS `grupos` (
  `cod_grp` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del grupo',
  `nom_grp` varchar(20) NOT NULL COMMENT 'nombre del grupo',
  `des_grp` varchar(255) NOT NULL COMMENT 'descripcion del grupo',
  PRIMARY KEY (`cod_grp`),
  UNIQUE KEY `nom_grp` (`nom_grp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`cod_grp`, `nom_grp`, `des_grp`) VALUES
(1, 'SuperUsuarios', 'Con todos los privilegios'),
(2, 'Administración', 'Grupo con todos los privilegios del área de Usuarios'),
(3, 'Personal', 'Grupo para el personal adscrito a la institución'),
(4, 'Pasantes', 'Privilegios Mínimos'),
(5, 'BienesP', 'Grupo de Pasantes'),
(6, 'Secretaria', 'Secretaria de la Contraloría'),
(7, 'Asist Administración', 'Asistentes Asignados en el Area de Administración');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE IF NOT EXISTS `horarios` (
  `cod_hor` int(11) NOT NULL AUTO_INCREMENT,
  `nom_hor` varchar(25) NOT NULL,
  `fch_dsd` date NOT NULL,
  `fch_hst` date NOT NULL,
  `lun_ini_hor` time NOT NULL,
  `lun_fin_hor` time NOT NULL,
  `lun_ini_hor2` time NOT NULL,
  `lun_fin_hor2` time NOT NULL,
  `mar_ini_hor` time NOT NULL,
  `mar_fin_hor` time NOT NULL,
  `mar_ini_hor2` time NOT NULL,
  `mar_fin_hor2` time NOT NULL,
  `mie_ini_hor` time NOT NULL,
  `mie_fin_hor` time NOT NULL,
  `mie_ini_hor2` time NOT NULL,
  `mie_fin_hor2` time NOT NULL,
  `jue_ini_hor` time NOT NULL,
  `jue_fin_hor` time NOT NULL,
  `jue_ini_hor2` time NOT NULL,
  `jue_fin_hor2` time NOT NULL,
  `vie_ini_hor` time NOT NULL,
  `vie_fin_hor` time NOT NULL,
  `vie_ini_hor2` time NOT NULL,
  `vie_fin_hor2` time NOT NULL,
  `sab_ini_hor` time NOT NULL,
  `sab_fin_hor` time NOT NULL,
  `dom_ini_hor` time NOT NULL,
  `dom_fin_hor` time NOT NULL,
  `tol_hor` int(11) NOT NULL,
  `obs_hor` longtext NOT NULL,
  PRIMARY KEY (`cod_hor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `horarios`
--

INSERT INTO `horarios` (`cod_hor`, `nom_hor`, `fch_dsd`, `fch_hst`, `lun_ini_hor`, `lun_fin_hor`, `lun_ini_hor2`, `lun_fin_hor2`, `mar_ini_hor`, `mar_fin_hor`, `mar_ini_hor2`, `mar_fin_hor2`, `mie_ini_hor`, `mie_fin_hor`, `mie_ini_hor2`, `mie_fin_hor2`, `jue_ini_hor`, `jue_fin_hor`, `jue_ini_hor2`, `jue_fin_hor2`, `vie_ini_hor`, `vie_fin_hor`, `vie_ini_hor2`, `vie_fin_hor2`, `sab_ini_hor`, `sab_fin_hor`, `dom_ini_hor`, `dom_fin_hor`, `tol_hor`, `obs_hor`) VALUES
(1, 'Horario Normal', '2012-01-01', '2012-07-19', '08:15:00', '11:55:00', '14:40:00', '00:00:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 15, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inasistencias`
--

CREATE TABLE IF NOT EXISTS `inasistencias` (
  `cod_ina` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `fch_ina` date NOT NULL,
  `tip_ina` varchar(30) NOT NULL,
  `des_ina` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_ina`),
  UNIQUE KEY `ced_per` (`ced_per`,`fch_ina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Inasistencias del personal' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias`
--

CREATE TABLE IF NOT EXISTS `incidencias` (
  `cod_inc` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion',
  `con_inc` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `mes_inc` int(2) NOT NULL COMMENT 'Mes en que se paga la incidencia',
  `ano_inc` int(4) NOT NULL COMMENT 'Ano en que se paga la incidencia',
  `fini_inc` date NOT NULL COMMENT 'Fecha de inicio del Periodo de Inicidencia',
  `ffin_inc` date NOT NULL COMMENT 'Fecha de fin del Periodo de Inicidencia',
  PRIMARY KEY (`cod_inc`),
  UNIQUE KEY `mes_inc` (`mes_inc`,`ano_inc`,`fini_inc`,`ffin_inc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias_sueldos`
--

CREATE TABLE IF NOT EXISTS `incidencias_sueldos` (
  `cod_inc` int(11) NOT NULL COMMENT 'Codigo de incidencias',
  `cod_sue` int(2) NOT NULL COMMENT 'Codigo del sueldo',
  `mnt_inc` double(9,2) NOT NULL COMMENT 'Monto o porcentaje de la incidencia',
  `bas_inc` varchar(1) NOT NULL COMMENT 'Base para el calculo de la Incidencia',
  UNIQUE KEY `cod_inc` (`cod_inc`,`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de inicidencias para cada sueldo';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ing_pat_vhi`
--

CREATE TABLE IF NOT EXISTS `ing_pat_vhi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `serie` int(3) NOT NULL,
  `inicio` int(8) NOT NULL,
  `fin` int(8) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los ingresos por Patente Vehicular' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `justificativos_per`
--

CREATE TABLE IF NOT EXISTS `justificativos_per` (
  `cod_sol_jus` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_jus` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_jus` int(2) NOT NULL,
  `ini_sol_jus` date NOT NULL,
  `fin_sol_jus` date NOT NULL,
  `obs_sol_jus` longtext NOT NULL,
  `mot_sol_jus` varchar(25) NOT NULL,
  `apro_sol_jus` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_jus`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicinas`
--

CREATE TABLE IF NOT EXISTS `medicinas` (
  `cod_med` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo de pago de medicinas',
  `fch_med` date NOT NULL COMMENT 'fecha de registro del pago de medicinas',
  `fch_pag_med` date NOT NULL COMMENT 'fecha del pago de las medicinas',
  `mnt_med` decimal(9,2) NOT NULL COMMENT 'monto anual',
  `obs_med` text NOT NULL COMMENT 'observaciones, comentarios o información adicional',
  PRIMARY KEY (`cod_med`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las medicinas pagadas a los Funcionarios' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicinas_per`
--

CREATE TABLE IF NOT EXISTS `medicinas_per` (
  `cod_med_per` int(11) NOT NULL AUTO_INCREMENT,
  `cod_med` int(11) NOT NULL,
  `ced_per` int(11) NOT NULL,
  `mnt_med_per` decimal(9,2) NOT NULL,
  PRIMARY KEY (`cod_med_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_asign`
--

CREATE TABLE IF NOT EXISTS `nominapre_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(50) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_deduc`
--

CREATE TABLE IF NOT EXISTS `nominapre_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_pagar`
--

CREATE TABLE IF NOT EXISTS `nominapre_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  `int_sue` double(9,2) NOT NULL COMMENT 'Sueldo Integral',
  `fnj_des` double(9,3) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `fnj_apr` double(9,3) NOT NULL COMMENT 'aporte de fondo de jubilaciones',
  `lph_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje FAOV',
  `spf_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje PIE',
  `sso_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje SSO',
  `cah_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje de Caja de ahorro',
  `fnj_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje Fondo Jubilaciones',
  `lph_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte FAOV',
  `spf_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte PIE',
  `sso_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte SSO',
  `cah_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Caja de ahorro',
  `fnj_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Fondo Jubilaciones',
  PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_asign`
--

CREATE TABLE IF NOT EXISTS `nomina_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(60) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_deduc`
--

CREATE TABLE IF NOT EXISTS `nomina_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_incidencias`
--

CREATE TABLE IF NOT EXISTS `nomina_incidencias` (
  `ano_inc` int(4) NOT NULL,
  `mes_inc` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `mnt_inc` double(9,2) NOT NULL,
  `con_inc` varchar(255) NOT NULL,
  `ivss_inc` double(9,2) NOT NULL,
  `spf_inc` double(9,2) NOT NULL,
  `cah_inc` double(9,2) NOT NULL,
  `bnoc_inc` double(9,2) NOT NULL,
  `bvac_inc` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_inc`,`mes_inc`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_pagar`
--

CREATE TABLE IF NOT EXISTS `nomina_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  `int_sue` double(9,2) NOT NULL COMMENT 'Sueldo Integral',
  `fnj_des` double(9,3) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `fnj_apr` double(9,3) NOT NULL COMMENT 'aporte de fondo de jubilaciones',
  `lph_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje FAOV',
  `spf_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje PIE',
  `sso_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje SSO',
  `cah_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje de Caja de ahorro',
  `fnj_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje Fondo Jubilaciones',
  `lph_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte FAOV',
  `spf_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte PIE',
  `sso_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte SSO',
  `cah_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Caja de ahorro',
  `fnj_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Fondo Jubilaciones',
  PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficios_enviados`
--

CREATE TABLE IF NOT EXISTS `oficios_enviados` (
  `cod_ofi` int(11) NOT NULL AUTO_INCREMENT,
  `tip_ofi` varchar(10) NOT NULL,
  `num_ofi` int(4) unsigned zerofill NOT NULL,
  `fch_ofi` date NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `rdp_ofi` varchar(75) NOT NULL,
  `rpr_ofi` varchar(75) NOT NULL,
  `ddp_ofi` varchar(75) NOT NULL,
  `dpr_ofi` varchar(75) NOT NULL,
  `des_ofi` varchar(255) NOT NULL,
  `nds_ofi` int(2) NOT NULL,
  `fen_ofi` date DEFAULT NULL,
  `ftr_ofi` date DEFAULT NULL,
  `frs_ofi` date DEFAULT NULL,
  `ref_ofi` varchar(7) NOT NULL,
  `obs_ofi` varchar(255) NOT NULL,
  `est_ofi` varchar(1) NOT NULL COMMENT 'Estado del Oficio',
  PRIMARY KEY (`cod_ofi`),
  UNIQUE KEY `tip_ofi` (`tip_ofi`,`num_ofi`,`fch_ofi`,`rdp_ofi`),
  KEY `cod_dep` (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Oficios Enviados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficios_recibidos`
--

CREATE TABLE IF NOT EXISTS `oficios_recibidos` (
  `cod_ofr` int(11) NOT NULL AUTO_INCREMENT,
  `fch_ofr` date NOT NULL,
  `fch_emi_ofr` date NOT NULL,
  `num_ofr` varchar(60) NOT NULL,
  `rem_ofr` varchar(60) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `con_ofr` varchar(150) NOT NULL,
  `ane_ofr` varchar(2) NOT NULL,
  `obs_ofr` varchar(255) NOT NULL,
  `prc_ofr` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ofr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Oficios Recibidos' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagados`
--

CREATE TABLE IF NOT EXISTS `pagados` (
  `ced_per` varchar(12) NOT NULL,
  PRIMARY KEY (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagados_frc`
--

CREATE TABLE IF NOT EXISTS `pagados_frc` (
  `cod_car` varchar(12) NOT NULL,
  PRIMARY KEY (`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE IF NOT EXISTS `pagos` (
  `cod_pag` int(11) NOT NULL AUTO_INCREMENT,
  `fch_pag` date NOT NULL,
  `nor_pag` int(4) unsigned zerofill NOT NULL,
  `frm_pag` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `frm_com` int(6) unsigned zerofill NOT NULL,
  `mon_pag` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `ela_pag` varchar(50) NOT NULL,
  `rev_pag` varchar(50) NOT NULL,
  `apr_pag` varchar(50) NOT NULL,
  `obs_pag` varchar(255) DEFAULT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `frm_egr_iva` int(6) unsigned zerofill NOT NULL COMMENT 'numero de formulacio para pagar iva',
  `frm_egr_isrl` int(6) unsigned zerofill NOT NULL COMMENT 'numero de formulacio para pagar isrl',
  KEY `id` (`cod_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidas_compras`
--

CREATE TABLE IF NOT EXISTS `partidas_compras` (
  `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT,
  `cod_com` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_com` double(9,2) NOT NULL,
  PRIMARY KEY (`cod_pro_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidas_pagos`
--

CREATE TABLE IF NOT EXISTS `partidas_pagos` (
  `cod_pro_pag` int(11) NOT NULL AUTO_INCREMENT,
  `cod_pag` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `isrl_pro_pag` varchar(2) DEFAULT NULL,
  `mon_pro_pag` double(9,2) NOT NULL,
  PRIMARY KEY (`cod_pro_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `part_presup`
--

CREATE TABLE IF NOT EXISTS `part_presup` (
  `cod_par` int(11) NOT NULL AUTO_INCREMENT,
  `sec_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `pro_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `act_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `ram_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `par_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `gen_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `esp_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `sub_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `des_par` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `obs_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cod_par`),
  UNIQUE KEY `sector` (`par_par`,`gen_par`,`esp_par`,`sub_par`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `part_presup_mov`
--

CREATE TABLE IF NOT EXISTS `part_presup_mov` (
  `cod_par_mov` int(11) NOT NULL AUTO_INCREMENT,
  `tip_par_mov` varchar(15) NOT NULL,
  `fch_par_mov` date NOT NULL,
  `ano_par_mov` varchar(10) NOT NULL,
  `idn_par_mov` varchar(50) NOT NULL,
  `con_par_mov` longtext NOT NULL,
  `mon_par_mov` double(9,2) NOT NULL,
  `fin_par_mov` date NOT NULL,
  PRIMARY KEY (`cod_par_mov`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `ing_prm` varchar(1) NOT NULL COMMENT 'permisos para ingresar',
  `mod_prm` varchar(1) NOT NULL COMMENT 'permisos para modificar',
  `con_prm` varchar(1) NOT NULL COMMENT 'permisos para consultar',
  `eli_prm` varchar(1) NOT NULL COMMENT 'permisos para eliminar',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  `cod_sec` int(2) NOT NULL COMMENT 'codigo de la seccion',
  KEY `cod_grp` (`cod_grp`),
  KEY `cod_sec` (`cod_sec`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Permisologia para los Usuarios del Sistema';

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`ing_prm`, `mod_prm`, `con_prm`, `eli_prm`, `cod_grp`, `cod_sec`) VALUES
('', '', '', '', 5, 11),
('', '', '', '', 5, 2),
('', '', '', '', 5, 8),
('', '', '', '', 5, 3),
('', '', '', '', 5, 7),
('', '', '', '', 5, 6),
('', '', '', '', 5, 1),
('', '', '', '', 5, 14),
('', '', '', '', 5, 15),
('', '', '', '', 5, 13),
('', '', '', '', 5, 12),
('', '', '', '', 5, 4),
('', '', '', '', 5, 5),
('', '', '', '', 5, 16),
('', '', '', '', 5, 10),
('', '', '', '', 5, 9),
('A', 'A', 'A', 'A', 5, 17),
('', '', '', '', 4, 18),
('', '', '', '', 4, 19),
('', '', '', '', 4, 11),
('', '', '', '', 4, 2),
('', '', '', '', 4, 8),
('', '', '', '', 4, 3),
('', '', '', '', 4, 7),
('', '', '', '', 4, 6),
('', '', '', '', 4, 1),
('', '', '', '', 4, 14),
('', '', '', '', 4, 23),
('', '', '', '', 4, 20),
('', '', '', '', 4, 21),
('', '', '', '', 4, 22),
('', '', '', '', 4, 15),
('', '', '', '', 4, 13),
('', '', '', '', 4, 12),
('', '', '', '', 4, 4),
('', '', '', '', 4, 5),
('', '', '', '', 4, 16),
('', '', '', '', 4, 10),
('', '', '', '', 4, 9),
('', '', '', '', 4, 17),
('A', 'A', 'A', 'A', 4, 24),
('', '', '', '', 4, 25),
('', '', '', '', 4, 26),
('', '', '', '', 6, 18),
('', '', '', '', 6, 19),
('', '', '', '', 6, 11),
('', '', '', '', 6, 2),
('', '', '', '', 6, 8),
('', '', '', '', 6, 3),
('', '', '', '', 6, 7),
('', '', '', '', 6, 6),
('', '', 'A', '', 6, 1),
('', '', '', '', 6, 14),
('', '', '', '', 6, 23),
('A', '', 'A', '', 6, 20),
('A', '', 'A', '', 6, 21),
('A', '', 'A', '', 6, 22),
('', '', '', '', 6, 15),
('', '', 'A', '', 6, 13),
('', '', '', '', 6, 12),
('', '', 'A', '', 6, 4),
('', '', '', '', 6, 5),
('', '', '', '', 6, 16),
('', '', '', '', 6, 10),
('A', 'A', 'A', 'A', 6, 9),
('A', 'A', 'A', 'A', 6, 27),
('', '', '', '', 6, 17),
('', '', '', '', 6, 24),
('', '', 'A', '', 6, 25),
('A', '', 'A', '', 6, 26),
('', '', '', '', 7, 18),
('', '', 'A', '', 7, 19),
('', '', '', '', 7, 11),
('', '', '', '', 7, 2),
('', '', '', '', 7, 8),
('', '', '', '', 7, 3),
('', '', '', '', 7, 7),
('', '', '', '', 7, 6),
('A', 'A', 'A', '', 7, 1),
('', '', '', '', 7, 14),
('', '', '', '', 7, 23),
('A', '', 'A', '', 7, 20),
('A', '', 'A', '', 7, 21),
('A', '', 'A', '', 7, 22),
('', '', '', '', 7, 15),
('', '', 'A', '', 7, 13),
('', '', '', '', 7, 12),
('', '', 'A', '', 7, 4),
('', '', '', '', 7, 5),
('A', 'A', 'A', '', 7, 16),
('A', 'A', 'A', '', 7, 10),
('', '', '', '', 7, 9),
('', '', '', '', 7, 27),
('', '', '', '', 7, 17),
('', '', '', '', 7, 24),
('', '', 'A', '', 7, 25),
('A', 'A', 'A', 'A', 7, 26),
('', '', '', '', 3, 18),
('', '', '', '', 3, 28),
('A', 'A', 'A', 'A', 3, 19),
('', '', '', '', 3, 11),
('', '', '', '', 3, 2),
('', '', '', '', 3, 8),
('', '', '', '', 3, 3),
('', '', '', '', 3, 7),
('', '', '', '', 3, 6),
('', '', 'A', '', 3, 1),
('', '', '', '', 3, 14),
('', '', '', '', 3, 23),
('A', '', 'A', '', 3, 20),
('A', '', 'A', '', 3, 21),
('A', '', 'A', '', 3, 22),
('', '', 'A', '', 3, 15),
('', '', 'A', '', 3, 13),
('', '', '', '', 3, 12),
('', '', 'A', '', 3, 4),
('', '', '', '', 3, 5),
('', '', '', '', 3, 16),
('', '', '', '', 3, 10),
('', '', '', '', 3, 9),
('', '', '', '', 3, 27),
('', '', '', '', 3, 24),
('', '', '', '', 3, 17),
('', '', 'A', '', 3, 25),
('A', '', 'A', '', 3, 26),
('', 'A', 'A', 'A', 1, 18),
('', '', '', '', 1, 28),
('', '', '', '', 1, 19),
('', '', '', '', 1, 11),
('A', 'A', 'A', 'A', 1, 2),
('A', 'A', 'A', 'A', 1, 8),
('A', 'A', 'A', 'A', 1, 3),
('A', 'A', 'A', 'A', 1, 7),
('A', 'A', 'A', 'A', 1, 6),
('A', 'A', 'A', 'A', 1, 1),
('A', 'A', 'A', 'A', 1, 14),
('', '', '', '', 1, 23),
('A', '', 'A', '', 1, 20),
('', '', '', '', 1, 21),
('', '', '', '', 1, 22),
('A', 'A', 'A', 'A', 1, 15),
('A', 'A', 'A', 'A', 1, 13),
('A', 'A', 'A', 'A', 1, 12),
('A', 'A', 'A', 'A', 1, 4),
('', '', '', '', 1, 5),
('', '', '', '', 1, 16),
('', '', '', '', 1, 10),
('', '', '', '', 1, 9),
('', '', '', '', 1, 27),
('', '', '', '', 1, 24),
('', '', '', '', 1, 17),
('', '', '', '', 1, 25),
('A', 'A', 'A', 'A', 1, 26),
('A', 'A', 'A', 'A', 2, 18),
('', '', '', '', 2, 28),
('', '', '', '', 2, 19),
('', '', '', '', 2, 11),
('A', 'A', 'A', 'A', 2, 2),
('A', 'A', 'A', 'A', 2, 8),
('A', 'A', 'A', 'A', 2, 3),
('A', 'A', 'A', 'A', 2, 7),
('A', 'A', 'A', 'A', 2, 6),
('A', 'A', 'A', 'A', 2, 1),
('A', 'A', 'A', 'A', 2, 14),
('', '', '', '', 2, 23),
('', '', '', '', 2, 20),
('', '', '', '', 2, 21),
('', '', '', '', 2, 22),
('A', 'A', 'A', 'A', 2, 15),
('A', 'A', 'A', 'A', 2, 13),
('A', 'A', 'A', 'A', 2, 12),
('A', 'A', 'A', 'A', 2, 4),
('', '', '', '', 2, 5),
('', '', '', '', 2, 16),
('', '', '', '', 2, 10),
('', '', '', '', 2, 9),
('', '', '', '', 2, 27),
('', '', '', '', 2, 24),
('', '', '', '', 2, 17),
('', '', '', '', 2, 25),
('A', 'A', 'A', 'A', 2, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_per`
--

CREATE TABLE IF NOT EXISTS `permisos_per` (
  `cod_sol_perm` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_perm` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_perm` int(2) NOT NULL,
  `ini_sol_perm` date NOT NULL,
  `fin_sol_perm` date NOT NULL,
  `tip_sol_perm` varchar(20) NOT NULL,
  `obs_sol_perm` longtext NOT NULL,
  `mot_sol_perm` varchar(25) NOT NULL,
  `apro_sol_perm` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_perm`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `nac_per` varchar(1) NOT NULL COMMENT 'nacionalidad de la persona',
  `nom_per` varchar(50) NOT NULL COMMENT 'nombre de la persona',
  `ape_per` varchar(50) NOT NULL COMMENT 'apellido de la persona',
  `sex_per` varchar(1) NOT NULL COMMENT 'sexo de la persona',
  `fnac_per` date NOT NULL COMMENT 'fecha de nacimiento de la persona',
  `lnac_per` varchar(50) NOT NULL COMMENT 'lugar de nacimiento de la persona',
  `cor_per` varchar(50) NOT NULL COMMENT 'correo de la persona',
  `pro_per` varchar(50) NOT NULL COMMENT 'profesion de la persona',
  `abr_per` varchar(10) NOT NULL COMMENT 'Abreviatura de Profesión',
  `dir_per` varchar(255) NOT NULL COMMENT 'direccion de la persona',
  `tel_per` varchar(12) NOT NULL COMMENT 'telefono de la persona',
  `cel_per` varchar(12) NOT NULL COMMENT 'celular de la persona',
  `lph_des` varchar(1) NOT NULL COMMENT 'Descuento de ley politica habitacional',
  `spf_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro de paro forzoso',
  `sso_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro social obligatorio',
  `cah_des` varchar(1) NOT NULL COMMENT 'Descuento de caja de ahorro',
  `sfu_des` varchar(1) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Servicio funerario',
  `hog_asg` varchar(1) NOT NULL COMMENT 'Prima por hogar',
  `hij_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos',
  `ant_asg` varchar(1) NOT NULL COMMENT 'Prima por antiguedad',
  `pro_asg` varchar(1) NOT NULL COMMENT 'Prima por profesionalizaci?n',
  `hje_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos excepcionales',
  `jer_asg` varchar(1) NOT NULL COMMENT 'Prima por Jerarquia',
  `otr_asg` varchar(1) NOT NULL COMMENT 'Prima por Postgrado',
  `fch_reg` date NOT NULL COMMENT 'Fecha de registro en el personal',
  `fnj_des` varchar(1) NOT NULL COMMENT 'Fondo de Jubilaciones',
  PRIMARY KEY (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`ced_per`, `nac_per`, `nom_per`, `ape_per`, `sex_per`, `fnac_per`, `lnac_per`, `cor_per`, `pro_per`, `abr_per`, `dir_per`, `tel_per`, `cel_per`, `lph_des`, `spf_des`, `sso_des`, `cah_des`, `sfu_des`, `hog_asg`, `hij_asg`, `ant_asg`, `pro_asg`, `hje_asg`, `jer_asg`, `otr_asg`, `fch_reg`, `fnj_des`) VALUES
('16657064', 'V', 'JUAN JOSE', 'MARQUEZ TREJO', 'M', '1993-04-15', 'MERIDA', 'JUANJMT@GMAIL.COM', 'ING.', 'ING.', 'EJIDO', '000000000', '000000000', 'A', 'A', 'A', 'A', '', 'A', 'A', 'A', 'A', '', '', '', '2015-05-25', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_compras`
--

CREATE TABLE IF NOT EXISTS `productos_compras` (
  `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT,
  `cod_com` int(11) NOT NULL,
  `cnt_pro_com` int(4) NOT NULL,
  `uni_pro_com` int(4) NOT NULL,
  `con_pro_com` varchar(255) NOT NULL,
  `pun_pro_com` double(9,3) NOT NULL,
  `exc_pro_com` varchar(2) DEFAULT NULL,
  KEY `id` (`cod_pro_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_part_mov`
--

CREATE TABLE IF NOT EXISTS `productos_part_mov` (
  `cod_pro_par_mov` int(11) NOT NULL AUTO_INCREMENT,
  `cod_par_mov` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_par_mov` double(9,2) NOT NULL,
  `tip_pro_par_mov` varchar(10) NOT NULL DEFAULT 'Ingreso',
  PRIMARY KEY (`cod_pro_par_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='partidas que pertenecen a un movimiento presupuestario' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programas`
--

CREATE TABLE IF NOT EXISTS `programas` (
  `cod_pro` varchar(5) NOT NULL COMMENT 'codigo del Programa',
  `nom_pro` varchar(150) NOT NULL COMMENT 'nombre del Programa',
  PRIMARY KEY (`cod_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `programas`
--

INSERT INTO `programas` (`cod_pro`, `nom_pro`) VALUES
('1', 'PROGRAMA 01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prog_movimientos`
--

CREATE TABLE IF NOT EXISTS `prog_movimientos` (
  `cod_mov` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `accion` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  `fch_asg` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `fch_vac` date NOT NULL,
  PRIMARY KEY (`cod_mov`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `prog_movimientos`
--

INSERT INTO `prog_movimientos` (`cod_mov`, `ced_per`, `cod_car`, `accion`, `estado`, `fch_asg`, `des_car`, `fch_vac`) VALUES
(1, '16657064', 0, 1, 1, '2015-05-25', 'X', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prog_mov_pagos`
--

CREATE TABLE IF NOT EXISTS `prog_mov_pagos` (
  `cod_pag` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `con_pag` varchar(150) NOT NULL,
  `fch_pag` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_vac` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
  `cod_pro` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fch_pro` date NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `nom_pro` varchar(50) NOT NULL,
  `dir_pro` varchar(255) NOT NULL,
  `act_pro` varchar(50) NOT NULL,
  `cap_pro` double(9,2) NOT NULL,
  `cas_pro` double(9,2) NOT NULL,
  `tel_pro` varchar(11) NOT NULL,
  `fax_pro` varchar(11) NOT NULL,
  `per_pro` varchar(1) NOT NULL,
  `ofi_reg_pro` varchar(50) NOT NULL,
  `num_reg_pro` int(5) NOT NULL,
  `tom_reg_pro` varchar(25) NOT NULL,
  `fol_reg_pro` varchar(4) NOT NULL,
  `fch_reg_pro` date NOT NULL,
  `num_mod_pro` int(5) NOT NULL,
  `tom_mod_pro` varchar(25) NOT NULL,
  `fol_mod_pro` varchar(4) NOT NULL,
  `fch_mod_pro` date NOT NULL,
  `pat_pro` varchar(25) NOT NULL,
  `ivss_pro` varchar(25) NOT NULL,
  `ince_pro` varchar(25) NOT NULL,
  `nom_rep_pro` varchar(50) NOT NULL,
  `ape_rep_pro` varchar(50) NOT NULL,
  `ced_rep_pro` varchar(8) NOT NULL,
  `dir_rep_pro` varchar(255) NOT NULL,
  `tel_rep_pro` varchar(11) NOT NULL,
  `car_rep_pro` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_pro`),
  UNIQUE KEY `rif_pro` (`rif_pro`),
  UNIQUE KEY `nom_pro` (`nom_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los Proveedores' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retenciones_isrl`
--

CREATE TABLE IF NOT EXISTS `retenciones_isrl` (
  `cod_ret_isrl` int(11) NOT NULL AUTO_INCREMENT,
  `com_ret_isrl` int(5) unsigned zerofill NOT NULL,
  `egr_ret_isrl` int(6) unsigned zerofill NOT NULL,
  `fch_ret_isrl` date NOT NULL,
  `obs_ret_isrl` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ret_isrl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retenciones_iva`
--

CREATE TABLE IF NOT EXISTS `retenciones_iva` (
  `cod_ret_iva` int(11) NOT NULL AUTO_INCREMENT,
  `com_ret_iva` int(8) unsigned zerofill NOT NULL,
  `egr_ret_iva` int(6) unsigned zerofill NOT NULL,
  `fch_ret_iva` date NOT NULL,
  `obs_ret_iva` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ret_iva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE IF NOT EXISTS `secciones` (
  `cod_sec` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo de la seccion',
  `nom_sec` varchar(25) NOT NULL COMMENT 'nombre de la seccion',
  `des_sec` varchar(255) NOT NULL COMMENT 'descripcion de la seccion',
  `dir_sec` varchar(50) NOT NULL COMMENT 'direccion para apuntar a la seccion',
  `tar_sec` varchar(15) NOT NULL COMMENT 'target de apertura de la seccion',
  `ord_sec` double(9,2) NOT NULL COMMENT 'orden de aparicion en el menu',
  PRIMARY KEY (`cod_sec`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Secciones Disponibles para los Usuarios' AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`cod_sec`, `nom_sec`, `des_sec`, `dir_sec`, `tar_sec`, `ord_sec`) VALUES
(1, 'Personal', 'Datos del Personal adscrito a: ', 'personal.php', 'contenido', 7.00),
(2, 'Dependencias', 'Dependencias adcritas a:', 'dependencias.php', 'contenido', 2.00),
(3, 'Cargos', 'Cargos adscritos a:', 'cargos.php', 'contenido', 4.00),
(4, 'Nomina', 'Nomina adscrita a:', 'nomina.php', 'contenido', 12.00),
(5, 'Incidencias', 'Incidencias adcritas a:', 'incidencias.php', 'contenido', 13.00),
(6, 'Tipos de Descuentos', 'Descuentos del Personal adscrito a:', 'descuentos.php', 'contenido', 6.00),
(7, 'Tipo de Asignaciones', 'Concesiones del Personal adscrito a:', 'concesiones.php', 'contenido', 5.00),
(8, 'Sueldos', 'Tabla de Sueldos', 'sueldos.php', 'contenido', 3.00),
(9, 'Correspondencia Enviada', 'Registro de correspondencia Interna y Externa de: ', 'oficios_enviados.php', 'contenido', 15.00),
(10, 'Comp./Pago./Egre.', 'Módulo de compras de: ', 'compras.php', 'contenido', 14.00),
(11, 'Actividades', 'Actividades administrativas de:', 'actividades.php', 'contenido', 1.00),
(12, 'Pre-Nomina', 'Prenomina Adscrita a: ', 'nominapre.php', 'contenido', 11.00),
(13, 'Cesta Ticket', 'Reporte de Cesta Ticket del personal adscrito a:', 'cesta_ticket.php', 'contenido', 10.00),
(14, 'Inasistencias', 'Registro de las inasistencias del personal adscrito a:', 'inasistencias.php', 'contenido', 8.00),
(15, 'Feriados', 'Registro de días feriados para', 'feriados.php', 'contenido', 9.00),
(16, 'Beneficiarios', 'Registro de Bemeficiarios registrados a ', 'proveedores.php', 'contenido', 13.50),
(17, 'Control de Bienes', 'Sistema de Control de Bienes', 'bienes/index.php', 'contenido', 16.00),
(18, 'Valores', 'Valores utilizados para los cáculos', 'valores.php', 'contenido', 0.00),
(19, 'Presupuesto', 'Manipulación de Partidas y Movimientos Presupuestarios', 'presupuesto.php', 'contenido', 0.10),
(20, 'Solicitud Vacaciones', 'Registro de la Solicitud de Vacaciones', 'vacaciones.php', 'contenido', 8.50),
(21, 'Solicitud de Permiso', 'Registro de la Solicitud de Permisos', 'permisos.php', 'contenido', 8.60),
(22, 'Sol. Justificativos', 'Registro de la Solicitud de Justificativos', 'justificativos.php', 'contenido', 8.70),
(23, 'Pago de Medicinas', 'Pago de gastos medicos y de farmacia a los empleados de la contraloría', 'medicinas.php', 'contenido', 8.30),
(24, 'Ingresos', 'Auditoria Ingresos', 'ing_index.php', 'contenido', 16.00),
(25, 'Conformar Cheques', 'Confirmación de cheques emitidos', 'conformar_cheques.php', 'contenido', 17.00),
(26, 'Constancias', 'Solicitud de Constancias', 'constancias.php', 'contenido', 17.00),
(27, 'Correspondencia Recibida', 'Registro de Correspondencia recibida por:', 'oficios_recibidos.php', 'contenido', 15.01),
(28, 'Bancos', 'Bancos y movimientos de ', 'bancos.php', 'contenido', 0.05);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos`
--

CREATE TABLE IF NOT EXISTS `sueldos` (
  `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  PRIMARY KEY (`cod_sue`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `sueldos`
--

INSERT INTO `sueldos` (`cod_sue`, `mon_sue`, `des_sue`) VALUES
(1, 6000.00, 'ASISTENTE ADMINISTRATIVO'),
(2, 6000.00, 'SECRETARIO'),
(3, 6291.00, 'ESCOLTA'),
(4, 9776.22, 'ABOGADO'),
(5, 14664.34, 'DIRECTOR GENERAL'),
(6, 11242.65, 'DIRECTORA DE ADMINISTRACION'),
(7, 11242.65, 'DIRECTORA RECURSOS HUMANOS'),
(8, 7923.00, 'SERVIDOR AGREGADO'),
(9, 6672.00, 'OFICIAL JEFE'),
(11, 6291.00, 'OFICIAL AGREGADO'),
(12, 7404.00, 'SEPERVISOR'),
(13, 5880.00, 'OFICIAL'),
(14, 5376.92, 'Personal Civil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos_ant`
--

CREATE TABLE IF NOT EXISTS `sueldos_ant` (
  `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  PRIMARY KEY (`cod_sue`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `sueldos_ant`
--

INSERT INTO `sueldos_ant` (`cod_sue`, `mon_sue`, `des_sue`) VALUES
(1, 14865.00, 'Contralor Municipal'),
(2, 8661.62, 'Jefe de Oficinas'),
(3, 9706.98, 'Director'),
(4, 6278.98, 'Auditor I'),
(5, 4447.61, 'Asistente Administrativo'),
(6, 3270.30, 'Auxiliar de Mantenimiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temp_presup`
--

CREATE TABLE IF NOT EXISTS `temp_presup` (
  `fch_prs` date NOT NULL,
  `det_prs` varchar(255) NOT NULL,
  `nor_prs` varchar(7) NOT NULL,
  `asg_prs` double(9,2) NOT NULL,
  `inc_prs` double(9,2) NOT NULL,
  `ing_prs` double(9,2) NOT NULL,
  `egr_prs` double(9,2) NOT NULL,
  `com_prs` double(9,2) NOT NULL,
  `cau_prs` double(9,2) NOT NULL,
  `pag_prs` double(9,2) NOT NULL,
  `rin_prs` double(9,2) NOT NULL,
  `generado` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`generado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='temporal para generar ejecucion presupuestaria' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_cargos`
--

CREATE TABLE IF NOT EXISTS `tipos_cargos` (
  `cod_tcar` int(11) NOT NULL AUTO_INCREMENT,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`cod_tcar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `tipos_cargos`
--

INSERT INTO `tipos_cargos` (`cod_tcar`, `nom_tcar`, `abr_tcar`) VALUES
(1, 'DIRECTIVO', 'DR'),
(2, 'EMPLEADO', 'EM'),
(3, 'CONTRATADO', 'CT'),
(4, 'OBREROS', 'OB');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `cod_usr` varchar(12) NOT NULL COMMENT 'codigo del usuario',
  `nom_usr` varchar(50) NOT NULL COMMENT 'nombre del usuario',
  `ape_usr` varchar(50) NOT NULL COMMENT 'apellidos del usuario',
  `freg_usr` date NOT NULL COMMENT 'fecha de registro del usuario',
  `log_usr` varchar(15) NOT NULL COMMENT 'login del usuario',
  `pas_usr` varchar(15) NOT NULL COMMENT 'password del usuario',
  `sts_usr` varchar(1) NOT NULL DEFAULT 'A' COMMENT 'Estado en el que se encuentra el Usuario',
  `int_usr` int(1) NOT NULL DEFAULT '0' COMMENT 'Intentos fallidos de inicio de sesi?n',
  `fcam_usr` date NOT NULL COMMENT 'fecha de cambio del usuario',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  PRIMARY KEY (`cod_usr`),
  KEY `cod_grp` (`cod_grp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Usuarios del Sistema';

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`cod_usr`, `nom_usr`, `ape_usr`, `freg_usr`, `log_usr`, `pas_usr`, `sts_usr`, `int_usr`, `fcam_usr`, `cod_grp`) VALUES
('11111111111', 'Administrador', 'Administrador', '2011-02-16', 'admin', 'controlador', 'A', 0, '2016-09-22', 1),
('15031097', 'Luis Felipe', 'Márquez Briceño', '2011-01-01', 'felix', 'nueva', 'A', 0, '2016-11-11', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacaciones_per`
--

CREATE TABLE IF NOT EXISTS `vacaciones_per` (
  `cod_sol_vac` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_vac` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `dias_sol_vac` int(2) NOT NULL,
  `ini_sol_vac` date NOT NULL,
  `fin_sol_vac` date NOT NULL,
  `tip_sol_vac` varchar(20) NOT NULL,
  `peri_sol_vac` date NOT NULL,
  `perf_sol_vac` date NOT NULL,
  `obs_sol_vac` longtext NOT NULL,
  `apro_sol_vac` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_vac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vac_dias_per`
--

CREATE TABLE IF NOT EXISTS `vac_dias_per` (
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `fch_pag_vac` date NOT NULL,
  `dias_vac` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de días de Vacaciones a disfrutar';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores`
--

CREATE TABLE IF NOT EXISTS `valores` (
  `cod_val` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del Valor',
  `des_val` varchar(25) NOT NULL COMMENT 'Descripcion del Valor',
  `val_val` double(9,3) NOT NULL COMMENT 'Valor del Valor',
  `con_val` varchar(100) NOT NULL COMMENT 'Concepto del Valor',
  PRIMARY KEY (`cod_val`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Volcado de datos para la tabla `valores`
--

INSERT INTO `valores` (`cod_val`, `des_val`, `val_val`, `con_val`) VALUES
(1, 'BV', 15.000, 'Días de Bono Vacacional para personal Contratado según LOT'),
(2, 'AG', 90.000, 'Días de Aguinaldos'),
(3, 'UT', 127.000, 'Valor de la Unidad Tributaria'),
(4, 'SSO', 0.040, 'Porcentaje de Retención Seguro Social Obligatorio'),
(5, 'LPH', 0.010, 'Porcentaje de Retención Fondo Ahorro Obligatorio para Vivienda'),
(6, 'SPF', 0.005, 'Porcentaje de Retención Pérdida Involuntaria de Empleo (PIE)'),
(7, 'CAH', 0.080, 'Porcentaje de Retención Caja de Ahorros (CAPREAMCE)'),
(8, 'SFU', 60.000, 'Monto Seguro Funerario (SOVENPFA)'),
(9, 'SM', 2047.520, 'Monto de Sueldo Mínimo'),
(10, 'SMMSSO', 5.000, 'Cantidad de Sueldos Máximos para el SSO'),
(12, 'APSSO', 0.090, 'Porcentaje de Aporte Seguro Social Obligatorio'),
(13, 'APSPF', 0.020, 'Porcentaje de Aporte Pérdida Involuntaria de Empleo (PIE)'),
(14, 'APCAH', 0.080, 'Porcentaje de Aporte Caja de Ahorros (CAPREAMCE)'),
(15, 'PRMHOG', 2.500, 'Cantidad de Unidades Tributarias Prima por Hogar'),
(16, 'PRMHIJ', 0.500, 'Cantidad de Unidades Tributarias Prima por Hijos'),
(17, 'PRMANT1', 1.500, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 6 a 8 años de antiguedad'),
(18, 'PRMPRO1', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalización 1er Tipo'),
(19, 'PRMJER', 300.000, 'Monto Prima Jerarquía'),
(20, 'PRMHJE', 1.000, 'Cantidad de Unidades Tributarias Prima por Hijos Excepcionales'),
(21, 'PRMOTR1', 2.500, 'Cantidad de Unidades Tributarias Otra Prima 1er Tipo'),
(22, 'APLPH', 0.020, 'Porcentaje de Aporte Fondo Ahorro Obligatorio para Vivienda'),
(23, 'PRMANT2', 2.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 9 a 11 años de antiguedad'),
(24, 'PRMANT3', 2.500, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 12 a 14 años de antiguedad'),
(25, 'PRMANT4', 3.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad después de 15 años'),
(27, 'VACPAG1', 40.000, 'Días de Bono Vacacional para personal fijo 1er Quinquenio'),
(28, 'VACPAG2', 40.000, 'Días de Bono Vacacional para personal fijo 2do Quinquenio'),
(29, 'VACPAG3', 40.000, 'Días de Bono Vacacional para personal fijo 3er Quinquenio'),
(30, 'VACPAG4', 40.000, 'Días de Bono Vacacional para personal fijo 4to Quinquenio'),
(31, 'VACPAG5', 40.000, 'Días de Bono Vacacional para personal fijo 5to Quinquenio'),
(32, 'VACDIF1', 20.000, 'Días de disfrute de Vacaciones para personal fijo 1er Quinquenio'),
(33, 'VACDIF2', 23.000, 'Días de disfrute de Vacaciones para personal fijo 2do Quinquenio'),
(34, 'VACDIF3', 25.000, 'Días de disfrute de Vacaciones para personal fijo 3er Quinquenio'),
(35, 'VACDIF4', 28.000, 'Días de disfrute de Vacaciones para personal fijo 4to Quinquenio'),
(36, 'VACDIF5', 30.000, 'Días de disfrute de Vacaciones para personal fijo 5to Quinquenio'),
(38, 'DESPIO', 0.000, 'Porcentaje de Monte Pio'),
(39, 'PRMPRO2', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalización 2do Tipo'),
(40, 'PRMOTR2', 3.000, 'Cantidad de Unidades Tributarias Otra Prima 2do Tipo'),
(41, 'IVA', 0.120, 'Impuesto al Valor Agregado'),
(42, 'CCT', 2.750, 'Comisión gastos administrativos Cesta Ticket'),
(43, 'MED_2012', 1500.000, 'Monto para gastos Médicos y de Farmacia para el año 2012'),
(44, 'PRMANT0', 1.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 3 a 5 años de antiguedad'),
(45, 'PRMOTR3', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalizacion Titulo Doctorado'),
(46, 'SUSTRAC_ISRL', 63.330, 'Sustraendo para el ISRL cuando retancion sea 1%'),
(47, 'PART_IVA', 72.000, 'Codigo de la Partida correspondiente al IVA'),
(48, 'PRESUP_MAX', 35.000, 'Número de partidas por página en reporte General de Presupuesto'),
(49, 'APRFONJUB', 0.030, 'Aporte de Fondo de Jubilaciones'),
(50, 'RETFONJUB', 0.030, 'Retención de Fondo de Jubilaciones'),
(51, 'prueba de valores', 12000.000, 'prueba');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_asignaciones_per`
--
CREATE TABLE IF NOT EXISTS `vista_asignaciones_per` (
`cod_cnp` int(11)
,`ced_per` varchar(12)
,`nom_con` varchar(60)
,`con_cnp` varchar(100)
,`ncuo_cnp` int(3)
,`ncp_cnp` int(3)
,`nom_car` varchar(100)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_banco_mov`
--
CREATE TABLE IF NOT EXISTS `vista_banco_mov` (
`codigo` int(11)
,`desde` varchar(17)
,`concepto` varchar(255)
,`monto` varbinary(22)
,`sin_partida` varchar(11)
,`banco` int(11)
,`referencia` varchar(50)
,`operacion` varchar(11)
,`fecha` date
,`tipo_mov` varchar(10)
,`estado` int(11)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_cargos_per`
--
CREATE TABLE IF NOT EXISTS `vista_cargos_per` (
`ced_per` varchar(12)
,`cod_car` int(11)
,`cod_dep` varchar(5)
,`num_car` int(3) unsigned zerofill
,`nom_car` varchar(100)
,`fch_asg` date
,`fch_vac` date
,`des_car` varchar(1)
,`nom_per` varchar(50)
,`ape_per` varchar(50)
,`nom_tcar` varchar(50)
,`mon_sue` double(9,2)
,`des_sue` varchar(50)
,`mon_sue_ant` double(9,2)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vista_cheques`
--

CREATE TABLE IF NOT EXISTS `vista_cheques` (
  `fch_egr` date DEFAULT NULL,
  `nom_pro` varchar(50) DEFAULT NULL,
  `chq_egr` varchar(25) DEFAULT NULL,
  `nom_ban` varchar(50) DEFAULT NULL,
  `cod_egr` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_movimientos`
--
CREATE TABLE IF NOT EXISTS `vista_movimientos` (
`cod_mov` int(11)
,`ced_per` varchar(12)
,`cod_car` int(11)
,`accion` int(11)
,`estado` int(1)
,`fch_asg` date
,`des_car` varchar(1)
,`fch_vac` date
,`cod_dep` varchar(5)
,`num_car` int(3) unsigned zerofill
,`nom_car` varchar(100)
,`cod_sue` int(2)
,`cod_tcar` int(11)
,`des_sue` varchar(50)
,`mon_sue` double(9,2)
,`nom_tcar` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominapre_proc`
--
CREATE TABLE IF NOT EXISTS `vista_nominapre_proc` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominaspre_pagadas`
--
CREATE TABLE IF NOT EXISTS `vista_nominaspre_pagadas` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominas_pagadas`
--
CREATE TABLE IF NOT EXISTS `vista_nominas_pagadas` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominpre_tot_sum`
--
CREATE TABLE IF NOT EXISTS `vista_nominpre_tot_sum` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
,`cod_tcar` int(11)
,`abr_tcar` varchar(2)
,`cod_dep` varchar(5)
,`nom_tcar` varchar(50)
,`tot_sue` double(19,2)
,`tot_lph` double(19,2)
,`tot_spf` double(19,2)
,`tot_sso` double(19,2)
,`tot_cah` double(19,2)
,`tot_sfu` double(19,2)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nomin_tot_sum`
--
CREATE TABLE IF NOT EXISTS `vista_nomin_tot_sum` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
,`cod_tcar` int(11)
,`abr_tcar` varchar(2)
,`cod_dep` varchar(5)
,`nom_tcar` varchar(50)
,`tot_sue` double(19,2)
,`tot_lph` double(19,2)
,`tot_spf` double(19,2)
,`tot_sso` double(19,2)
,`tot_cah` double(19,2)
,`tot_sfu` double(19,2)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vista_ordenespago`
--

CREATE TABLE IF NOT EXISTS `vista_ordenespago` (
  `fch_egr` date DEFAULT NULL,
  `nom_pro` varchar(50) DEFAULT NULL,
  `con_egr` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_partidas_comprometidas`
--
CREATE TABLE IF NOT EXISTS `vista_partidas_comprometidas` (
`cod_par` int(11)
,`sec_par` varchar(11)
,`pro_par` varchar(11)
,`act_par` varchar(11)
,`ram_par` varchar(11)
,`par_par` varchar(11)
,`gen_par` varchar(11)
,`esp_par` varchar(11)
,`sub_par` varchar(11)
,`des_par` varchar(255)
,`obs_par` varchar(11)
,`frm_com` int(6) unsigned zerofill
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_partidas_presupuesto`
--
CREATE TABLE IF NOT EXISTS `vista_partidas_presupuesto` (
`cod_par` int(11)
,`sec_par` varchar(11)
,`pro_par` varchar(11)
,`act_par` varchar(11)
,`ram_par` varchar(11)
,`par_par` varchar(11)
,`gen_par` varchar(11)
,`esp_par` varchar(11)
,`sub_par` varchar(11)
,`des_par` varchar(255)
,`obs_par` varchar(11)
,`ano_par_mov` varchar(10)
,`idn_par_mov` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_personal`
--
CREATE TABLE IF NOT EXISTS `vista_personal` (
`ced_per` varchar(12)
,`nombre` varchar(101)
,`nac_per` varchar(1)
,`abr_per` varchar(10)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_salida_sincargo`
--
CREATE TABLE IF NOT EXISTS `vista_salida_sincargo` (
`cod_pag` int(11)
,`mon_pag` double(9,2)
,`ced_per` varchar(12)
,`con_pag` varchar(150)
,`des_car` varchar(1)
,`fch_pag` date
,`cod_dep` varchar(5)
,`nom_dep` varchar(150)
,`cod_car` int(11)
,`nom_car` varchar(100)
,`fch_vac` date
,`cod_tcar` int(11)
,`nom_tcar` varchar(50)
,`abr_tcar` varchar(2)
);
-- --------------------------------------------------------

--
-- Estructura para la vista `vista_asignaciones_per`
--
DROP TABLE IF EXISTS `vista_asignaciones_per`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_asignaciones_per` AS select `asg`.`cod_cnp` AS `cod_cnp`,`asg`.`ced_per` AS `ced_per`,`cn`.`nom_con` AS `nom_con`,`asg`.`con_cnp` AS `con_cnp`,`asg`.`ncuo_cnp` AS `ncuo_cnp`,`asg`.`ncp_cnp` AS `ncp_cnp`,`c`.`nom_car` AS `nom_car` from ((`asignaciones` `asg` join `concesiones` `cn`) join `cargos` `c`) where ((`asg`.`cod_con` = `cn`.`cod_con`) and (`c`.`cod_car` = `asg`.`cod_car`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_banco_mov`
--
DROP TABLE IF EXISTS `vista_banco_mov`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_banco_mov` AS select `egresos`.`cod_egr` AS `codigo`,'egresos' AS `desde`,`egresos`.`con_egr` AS `concepto`,sum((`pagos`.`mon_pag` - `egresos`.`ded_egr`)) AS `monto`,`egresos`.`sin_par_egr` AS `sin_partida`,`egresos`.`cod_ban` AS `banco`,`egresos`.`chq_egr` AS `referencia`,`egresos`.`frm_egr` AS `operacion`,`egresos`.`fch_egr` AS `fecha`,'egreso' AS `tipo_mov`,`egresos`.`sta_ban_egr` AS `estado` from (`egresos` join `pagos`) where (`egresos`.`frm_egr` = `pagos`.`frm_egr`) group by `pagos`.`frm_egr` union (select `banco_movimientos`.`cod_ban_mov` AS `codigo`,'banco_movimientos' AS `desde`,`banco_movimientos`.`des_ban_mov` AS `concepto`,`banco_movimientos`.`mon_ban_mov` AS `monto`,'0' AS `sin_partida`,`banco_movimientos`.`cod_ban` AS `banco`,`banco_movimientos`.`ref_ban_mov` AS `referencia`,'' AS `operacion`,`banco_movimientos`.`fch_ban_mov` AS `fecha`,`banco_movimientos`.`tip_ban_mov` AS `tipo_mov`,`banco_movimientos`.`sta_ban_mov` AS `estado` from `banco_movimientos`) union (select `egresos`.`cod_egr` AS `codigo`,'egresos' AS `desde`,`egresos`.`con_egr` AS `concepto`,'0' AS `monto`,`egresos`.`sin_par_egr` AS `sin_partida`,`egresos`.`cod_ban` AS `banco`,`egresos`.`chq_egr` AS `referencia`,`egresos`.`frm_egr` AS `operacion`,`egresos`.`fch_egr` AS `fecha`,'egreso' AS `tipo_mov`,`egresos`.`sta_ban_egr` AS `estado` from `egresos` where ((`egresos`.`sin_par_egr` > 0) and (not(`egresos`.`frm_egr` in (select `pagos`.`frm_egr` from `pagos`)))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_cargos_per`
--
DROP TABLE IF EXISTS `vista_cargos_per`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_cargos_per` AS select `c`.`ced_per` AS `ced_per`,`c`.`cod_car` AS `cod_car`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`c`.`fch_asg` AS `fch_asg`,`c`.`fch_vac` AS `fch_vac`,`c`.`des_car` AS `des_car`,`p`.`nom_per` AS `nom_per`,`p`.`ape_per` AS `ape_per`,`t`.`nom_tcar` AS `nom_tcar`,`s`.`mon_sue` AS `mon_sue`,`s`.`des_sue` AS `des_sue`,`sa`.`mon_sue` AS `mon_sue_ant` from ((((`cargos` `c` join `tipos_cargos` `t`) join `sueldos` `s`) join `sueldos_ant` `sa`) join `personal` `p`) where ((`t`.`cod_tcar` = `c`.`cod_tcar`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`sa`.`cod_sue` = `c`.`cod_sue`) and (`p`.`ced_per` = `c`.`ced_per`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_movimientos`
--
DROP TABLE IF EXISTS `vista_movimientos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_movimientos` AS select `m`.`cod_mov` AS `cod_mov`,`m`.`ced_per` AS `ced_per`,`m`.`cod_car` AS `cod_car`,`m`.`accion` AS `accion`,`m`.`estado` AS `estado`,`m`.`fch_asg` AS `fch_asg`,`m`.`des_car` AS `des_car`,`m`.`fch_vac` AS `fch_vac`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`c`.`cod_sue` AS `cod_sue`,`c`.`cod_tcar` AS `cod_tcar`,`s`.`des_sue` AS `des_sue`,`s`.`mon_sue` AS `mon_sue`,`t`.`nom_tcar` AS `nom_tcar` from (((`prog_movimientos` `m` join `cargos` `c`) join `sueldos` `s`) join `tipos_cargos` `t`) where ((`c`.`cod_car` = `m`.`cod_car`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`t`.`cod_tcar` = `c`.`cod_tcar`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominapre_proc`
--
DROP TABLE IF EXISTS `vista_nominapre_proc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominapre_proc` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominaspre_pagadas`
--
DROP TABLE IF EXISTS `vista_nominaspre_pagadas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominaspre_pagadas` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominas_pagadas`
--
DROP TABLE IF EXISTS `vista_nominas_pagadas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominas_pagadas` AS select `nomina_pagar`.`ano_nom` AS `ano_nom`,`nomina_pagar`.`mes_nom` AS `mes_nom`,`nomina_pagar`.`por_nom` AS `por_nom` from `nomina_pagar` group by `nomina_pagar`.`ano_nom`,`nomina_pagar`.`mes_nom`,`nomina_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominpre_tot_sum`
--
DROP TABLE IF EXISTS `vista_nominpre_tot_sum`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominpre_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nominapre_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nomin_tot_sum`
--
DROP TABLE IF EXISTS `vista_nomin_tot_sum`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nomin_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nomina_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_partidas_comprometidas`
--
DROP TABLE IF EXISTS `vista_partidas_comprometidas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_partidas_comprometidas` AS (select `part`.`cod_par` AS `cod_par`,`part`.`sec_par` AS `sec_par`,`part`.`pro_par` AS `pro_par`,`part`.`act_par` AS `act_par`,`part`.`ram_par` AS `ram_par`,`part`.`par_par` AS `par_par`,`part`.`gen_par` AS `gen_par`,`part`.`esp_par` AS `esp_par`,`part`.`sub_par` AS `sub_par`,`part`.`des_par` AS `des_par`,`part`.`obs_par` AS `obs_par`,`com`.`frm_com` AS `frm_com` from ((`compras` `com` join `partidas_compras` `pcom`) join `part_presup` `part`) where ((`com`.`cod_com` = `pcom`.`cod_com`) and (`pcom`.`cod_par` = `part`.`cod_par`)) group by `part`.`cod_par`,`com`.`frm_com` order by `part`.`par_par`,`part`.`gen_par`,`part`.`esp_par`,`part`.`sub_par`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_partidas_presupuesto`
--
DROP TABLE IF EXISTS `vista_partidas_presupuesto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_partidas_presupuesto` AS (select `part_presup`.`cod_par` AS `cod_par`,`part_presup`.`sec_par` AS `sec_par`,`part_presup`.`pro_par` AS `pro_par`,`part_presup`.`act_par` AS `act_par`,`part_presup`.`ram_par` AS `ram_par`,`part_presup`.`par_par` AS `par_par`,`part_presup`.`gen_par` AS `gen_par`,`part_presup`.`esp_par` AS `esp_par`,`part_presup`.`sub_par` AS `sub_par`,`part_presup`.`des_par` AS `des_par`,`part_presup`.`obs_par` AS `obs_par`,`part_presup_mov`.`ano_par_mov` AS `ano_par_mov`,`part_presup_mov`.`idn_par_mov` AS `idn_par_mov` from ((`part_presup` join `part_presup_mov`) join `productos_part_mov`) where ((`part_presup`.`cod_par` = `productos_part_mov`.`cod_par`) and (`part_presup_mov`.`cod_par_mov` = `productos_part_mov`.`cod_par_mov`)) order by `part_presup`.`par_par`,`part_presup`.`gen_par`,`part_presup`.`esp_par`,`part_presup`.`sub_par`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_personal`
--
DROP TABLE IF EXISTS `vista_personal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_personal` AS select `personal`.`ced_per` AS `ced_per`,concat(`personal`.`nom_per`,_latin1' ',`personal`.`ape_per`) AS `nombre`,`personal`.`nac_per` AS `nac_per`,`personal`.`abr_per` AS `abr_per` from `personal`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_salida_sincargo`
--
DROP TABLE IF EXISTS `vista_salida_sincargo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_salida_sincargo` AS select `prog_mov_pagos`.`cod_pag` AS `cod_pag`,`prog_mov_pagos`.`mon_pag` AS `mon_pag`,`prog_mov_pagos`.`ced_per` AS `ced_per`,`prog_mov_pagos`.`con_pag` AS `con_pag`,`prog_mov_pagos`.`des_car` AS `des_car`,`prog_mov_pagos`.`fch_pag` AS `fch_pag`,`prog_mov_pagos`.`cod_dep` AS `cod_dep`,`prog_mov_pagos`.`nom_dep` AS `nom_dep`,`prog_mov_pagos`.`cod_car` AS `cod_car`,`prog_mov_pagos`.`nom_car` AS `nom_car`,`prog_mov_pagos`.`fch_vac` AS `fch_vac`,`prog_mov_pagos`.`cod_tcar` AS `cod_tcar`,`prog_mov_pagos`.`nom_tcar` AS `nom_tcar`,`prog_mov_pagos`.`abr_tcar` AS `abr_tcar` from (`prog_mov_pagos` left join `cargos` on((`cargos`.`ced_per` = `prog_mov_pagos`.`ced_per`))) where (not(`prog_mov_pagos`.`ced_per` in (select distinct `cargos`.`ced_per` AS `ced_per` from `cargos`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
