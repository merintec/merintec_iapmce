<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Deducciones del Personal</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['ced_per'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'personal_deducciones.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$pagina2 = 'personal_deducciones.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$tabla = "deducciones";	// nombre de la tabla
$ncampos = "11";			//numero de campos del formulario

$cod_dsp = $_POST["cod_dsp"];
$con_dsp = $_POST["con_dsp"];
$ncuo_dsp = $_POST["ncuo_dsp"];
$mcuo_dsp = $_POST["mcuo_dsp"];
$por_dsp = $_POST["por_dsp"];
$bpor_dsp = $_POST["bpor_dsp"];
$per_dsp = $_POST["per_dsp"];
$cod_des = $_POST["cod_des"];
$ced_per = $_GET["ced_per"];
$cod_car = $_POST["cod_car"];
$fch_dsp = $_POST["fch_dsp"];
$ncp_dsp = $_POST["ncp_dsp"];

$datos[0] = crear_datos ("con_dsp","Concepto",$_POST['con_dsp'],"1","100","alfanumericos");
$datos[1] = crear_datos ("ncuo_dsp","Numero de Cuotas",$_POST['ncuo_dsp'],"0","2","numericos");
$datos[2] = crear_datos ("per_dsp","Despuento Permanente",$_POST['per_dsp'],"0","1","alfabeticos");
$datos[3] = crear_datos ("mcuo_dsp","Monto de Cuotas",$_POST['mcuo_dsp'],"0","11","decimal");
$datos[4] = crear_datos ("por_dsp","Porcentaje de deduccion",$_POST['por_dsp'],"0","2","numericos");
$datos[5] = crear_datos ("bpor_dsp","Base del porcentaje",$_POST['bpor_dsp'],"0","1","alfabeticos");
$datos[6] = crear_datos ("cod_des","Tipo de Descuento",$_POST['cod_des'],"1","11","numericos");
$datos[7] = crear_datos ("ced_per","Cedula",$_POST['ced_per'],"1","12","numericos");
$datos[8] = crear_datos ("cod_car","Cargo",$_POST['cod_car'],"1","11","numericos");
$datos[9] = crear_datos ("fch_dsp","Fecha de Registro",$_POST['fch_dsp'],"1","10","fecha");
$datos[10] = crear_datos ("ncp_dsp","Cuotas Pagadas",$_POST['ncp_dsp'],"0","2","numericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Concepto";
		$datos[0]="con_dsp";
		$parametro[1]="Numero de Cuotas";
		$datos[1]="ncuo_dsp";
		$parametro[2]="Monto de Cuotas";
		$datos[2]="mcuo_dsp";
		$parametro[3]="Cedula";
		$datos[3]="ced_per";
		busqueda_varios(6,$buscando,$datos,$parametro,"cod_dsp");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_dsp = $row["cod_dsp"];
	    $con_dsp = $row["con_dsp"];
	    $ncuo_dsp = $row["ncuo_dsp"];
	    $mcuo_dsp = $row["mcuo_dsp"];
		$por_dsp = $row["por_dsp"];
		$bpor_dsp = $row["bpor_dsp"];
	    $per_dsp = $row["per_dsp"];
	    $cod_des = $row["cod_des"];
	    $ced_per = $row["ced_per"];
		$cod_car = $row["cod_car"];
	    $fch_dsp = $row["fch_dsp"];
	    $ncp_dsp = $row["ncp_dsp"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_dsp",$_POST["cod_dsp"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[7][0],$datos[7][2],'personal',$boton,'no','Personal');
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_dsp"],"cod_dsp",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_dsp","deducciones",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Deducciones C.I: <?php echo $viene_val; ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas">Descuento:</td>
                        <td><?php
			combo('cod_des', $cod_des, 'descuentos', $link, 0, 0, 1, "", 'cod_des', '', $boton,''); ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">C&eacute;dula:</td>
                        <td><input name="ced_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_per" readonly value="<?php if(! $existe) { echo $viene_val; } else { echo $ced_per; } ?>" <?php if ($boton=='Actualizar') { echo "readonly"; } ?> size="29" title="C&eacute;dula del Personal" />
                          <?php if ($boton=='Modificar') { echo $ced_per; } ?></td>
                      </tr>
					  <tr>
                        <td class="etiquetas">Cargo:</td>
                        <td><?php
			combo('cod_car', $cod_car, 'cargos', $link, 0, 1, 2, "", 'cod_car', '', $boton,'WHERE ced_per='.$viene_val); ?></td>
                      </tr>
                      <tr>
                      <tr>
                        <td width="25%" class="etiquetas">Concepto:</td>
                        <td width="75%"><input name="cod_dsp" type="hidden" id="cod_dsp" value="<?php if(! $existe) { echo $_POST["cod_dsp"]; } else { echo $cod_dsp; } ?>" size="35" />
                        <input name="con_dsp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="con_dsp" value="<?php if(! $existe) { echo $_POST["con_dsp"]; } else { echo $con_dsp; } ?>" size="35" title="Concepto del descuento">
                        <?php if ($boton=='Modificar') { echo $con_dsp; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">N&ordm; de Cuotas: </td>
                        <td><input name="ncuo_dsp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ncuo_dsp" value="<?php if(! $existe) { echo $_POST["ncuo_dsp"]; } else { echo $ncuo_dsp; } ?>" size="15" title="Numero de cuotas a pagar" />
                          <?php if ($boton=='Modificar') { echo $ncuo_dsp; } ?></td>
                      </tr>
 					  <tr>
                        <td class="etiquetas">Porcentaje de deducci&oacute;n </td>
                        <td><input name="por_dsp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="por_dsp" value="<?php if(! $existe) { echo $_POST["por_dsp"]; } else { echo $por_dsp; } ?>" size="1" maxlength="2" title="Porcentaje de la deducci&oacute;n" onkeypress=valor_acampo(0,"mcuo_dsp")> 
                          <?php if ($boton=='Modificar') { echo $por_dsp.'% '; } ?> 
						 <?php if ($boton != "Modificar") { echo '<class="etiquetas">Base: <select name="bpor_dsp" title="Base del porcentaje" onchange=valor_acampo(0,"mcuo_dsp")>
                          <option value="">Seleccione...</option>
                          <option value="B" '; if ($bpor_dsp == "B" || $_POST['bpor_dsp'] =="B") { echo 'selected'; } echo '>Sueldo Base</option>
                        </select></class>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="bpor_dsp" id="bpor_dsp" value="'.$bpor_dsp.'" >'; 
						    if ($bpor_dsp == "B") { echo 'del Sueldo Base'; } 
							if ($bpor_dsp == "I") { echo 'del Sueldo Integral'; }
						}?></td>
                      </tr>
		      <tr><td class="etiquetas">Permanente: </td><td><input name="per_dsp" id="per_dsp" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$per_dsp.'" '; } else { echo 'type="checkbox" value="S"'; } if($_POST["per_dsp"]=='S' || $per_dsp == 'S') { echo 'checked'; } ?> title="Marcar si el Descuento es Permanente" onclick=valor_acampo(1,"ncuo_dsp")>
                          <?php if ($boton=='Modificar') { if ($per_dsp=='S') {echo 'SI'; } else { echo 'NO'; } } ?>
			</td></tr>
                      <tr>
                        <td class="etiquetas">Monto de cuotas (Bs): </td>
                        <td><input name="mcuo_dsp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mcuo_dsp" value="<?php if(! $existe) { echo $_POST["mcuo_dsp"]; } else { echo $mcuo_dsp; } ?>" size="15" title="Monto de cuotas a pagar" />
                          <?php if ($boton=='Modificar') { echo $mcuo_dsp; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Registro: </td>
                        <td><input name="fch_dsp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_dsp" value="<?php if(! $existe) { echo $_POST["fch_dsp"]; } else { echo $fch_dsp; } ?>" size="15" title="Fecha de Registro de la Deducción" />
                          <?php if ($boton=='Modificar') { echo $fch_dsp; } ?><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_dsp,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">N&ordm; de Cuotas Pagadas: </td>
                        <td><input name="ncp_dsp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ncp_dsp" value="<?php if(! $existe) { echo $_POST["ncp_dsp"]; } else { echo $ncp_dsp; } ?>" size="15" title="Numero de cuotas Pagadas" />
                          <?php if ($boton=='Modificar') { echo $ncp_dsp; } ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_deducciones_personal.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
