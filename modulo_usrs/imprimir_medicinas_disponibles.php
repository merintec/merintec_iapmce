<?php include('../comunes/conexion_basedatos.php'); 
include ('../comunes/formularios_funciones.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); ?>
<?php include('../comunes/numerosaletras.php'); ?>

<?php 
    $idprint=$_GET['ano_med']; 
    $sql="select * from valores";
    $res = mysql_query($sql);
    while ($row = mysql_fetch_array($res))
    {
        $$row['des_val'] = $row['val_val'];
    }
    $var='MED_'; $var.=$idprint; $med = redondear ($$var,2,'','.');


	//consultamos los datos de  los pagos de medicias
	$counter = 1;
	$total_pagado = 0;
    $total_disponible = 0;
	$result=mysql_query("SELECT * FROM vista_personal ORDER BY nombre");
	while ($row=mysql_fetch_array($result))
	{   
	    $datos[1][$counter]=$row['nombre'];
        $datos[2][$counter]=$row['ced_per'];
	    $result2=mysql_query("SELECT mp.ced_per,sum(mp.mnt_med_per) monto FROM medicinas md,medicinas_per mp WHERE YEAR(md.fch_med)=".$idprint." AND md.cod_med = mp.cod_med AND mp.ced_per = ".$row['ced_per']);         
	    while ($row2=mysql_fetch_array($result2))
	    {
            $datos[3][$counter]=$row2['monto'];
            $datos[4][$counter]=$med-$row2['monto'];
            $total_pagado += $datos[3][$counter];
            $total_disponible += $datos[4][$counter];
        }
        $counter++;
	}
	mysql_free_result($result);
?>
<title>Impresi�n Pago de Medicinas</title>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<div><?php include ('../comunes/pagina_encabezado.php'); ?></div>
<table align="center" width="85%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="0" class="detallespago">
    <tr>
        <td align="center">
            <H2>MONTOS DISPONIBLES POR CONCEPTO DE <BR>GASTOS M�DICOS Y DE FARMACIA - A�O <?php echo $idprint; ?><br>Al <?php echo date('d').' de '.convertir_mes(date('m')).' de '.date('Y');  ?></H2>
            <H3>MONTO ANUAL POR TRABAJADOR: <?php echo redondear($med,2,".",","); ?></H3>
        </td>
    <tr>
</table>
<table align="center" width="85%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000" class="detallespago">
    <tr align="center"><td><b>N�</b></td><td><b>Nombre y Apellido</b></td><td><b>C�dula</b></td><td><b>Monto Pagado</b></td><td><b>Monto Disponible</b></td></tr>
<?php for ($j=1;$j<$counter;$j++){ ?>
     <tr height="20px">
        <td align="right">
            <?php echo ($j); ?>&nbsp;
        </td>
        <td align="left">
            &nbsp;<?php echo $datos[1][$j]; ?>
        </td>
        <td align="right">
            <?php echo redondear($datos[2][$j],0,".",","); ?>&nbsp;
        </td>
        <td align="right">
            <?php echo redondear($datos[3][$j],2,".",","); ?>&nbsp;
        </td>
        <td align="right">
            <?php echo redondear($datos[4][$j],2,".",","); ?>&nbsp;
        </td>
     </tr>
<?php }?>
     <tr class="tabla_total">
        <td align="right" colspan="3"><font size="-1">
            TOTAL MEDICINAS A�O <?php echo $idprint; ?>:&nbsp;</font>
        </td>
        <td align="right"><font size="-1"> 
            <?php echo redondear($total_pagado,2,".",","); ?>&nbsp;</font>
        </td>
        <td align="right"><font size="-1"> 
            <?php echo redondear($total_disponible,2,".",","); ?>&nbsp;</font>
        </td>
     </tr>
</table>
<br>
<?php echo $msg_pie_reporte; ?>
<div><input type="button" name="bt_print" value="Imprimir Solicitud" id="bt_print" onclick="this.style.visibility='hidden'; window.print();"></div>
