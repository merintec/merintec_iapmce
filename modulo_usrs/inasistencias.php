<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'inasistencias.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "inasistencias";	// nombre de la tabla
$ncampos = "4";			//numero de campos del formulario

      $cod_ina = $_POST["cod_ina"];
      $ced_per = $_POST["ced_per"];
      $fch_ina = $_POST["fch_ina"];
      $tip_ina = $_POST["tip_ina"];
      $des_ina = $_POST["des_ina"];

$datos[0] = crear_datos ("ced_per","Cedula del Personal",$_POST['ced_per'],"1","11","numericos");
$datos[1] = crear_datos ("fch_ina","Fecha de Inasistencia",$_POST['fch_ina'],"10","10","fecha");
$datos[2] = crear_datos ("tip_ina","Tipo de Inasistencia",$_POST['tip_ina'],"1","30","alfabeticos");
$datos[3] = crear_datos ("des_ina","Descripcion de Inasistencia",$_POST['des_ina'],"1","50","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="C�dula";
		$datos[0]="ced_per";
		$parametro[1]="Fecha";
		$datos[1]="fch_ina";
		$parametro[2]="Descripci�n";
		$datos[2]="des_ina";
		busqueda_varios(5,$buscando,$datos,$parametro,"cod_ina");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_ina = $row["cod_ina"];
	    $ced_per = $row["ced_per"];
	    $fch_ina = $row["fch_ina"];
	    $tip_ina = $row["tip_ina"];
	    $des_ina = $row["des_ina"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_ina",$_POST["cod_ina"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2]."' AND ".$datos[1][0]." = '".$datos[1][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_ina"],"cod_ina",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de Inasistencias</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">Personal:</td>
                        <td width="75%">
			<input name="cod_ina" type="hidden" id="cod_ina" value="<?php if(! $existe) { echo $_POST['cod_ina']; } else { echo $cod_ina; } ?>" size="35" title="Codigo de Inasistencia">
                        <?php combo('ced_per', $ced_per, 'vista_personal', $link, 0, 1, 0, "", 'ced_per', "", $boton,  "ORDER BY nombre"); ?>
			<?php //combo('ced_per', $ced_per, 'personal', $link, 0, 2, 0, "", 'ced_per', "", $boton, ""); ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Inicio Inasistencia: </td>
                        <td><input name="fch_ina" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_ina" value="<?php if(! $existe) { echo $_POST["fch_ina"]; } else { echo $fch_ina; } ?>" size="20" title="Fecha de la inasistencia" />
                          <?php if ($boton=='Modificar') { echo $fch_ina; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_ina,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Tipo Inasistencia:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tip_ina" title="Tipo de Inansistencia">
                          <option>Seleccione...</option>
                          <option value="Remunerada" '; if ($tip_ina == "Remunerada" || $_POST['tip_ina'] =="Remunerada") { echo 'selected'; } echo '>Remunerada</option>
                          <option value="No Remunerada" '; if ($tip_ina == "No Remunerada" || $_POST['tip_ina'] =="No Remunerada") { echo 'selected'; } echo '>No Remunerada</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tip_ina" id="tip_ina" value="'.$tip_ina.'" >'; 
						    if ($tip_ina == "Remunerada") { echo 'Remunerada'; } 
							if ($tip_ina == "No Remunerada") { echo 'No Remunerada'; }
						}?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripci�n:                          </td>
			<td>
                        <input name="des_ina" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="des_ina" value="<?php if(! $existe) { echo $_POST['des_ina']; } else { echo $des_ina; } ?>" size="35" title="Descripci�n o M�tivo de la Inasistencia" maxlength="50">
                        <?php if ($boton=='Modificar') { echo $des_ina; } ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =1; 
						$criterios[0] = "C�dula"; 
						$campos[0] ="ced_per";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } 
					     $funcion_combo = '"valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
                                             echo '<center>Buscar C�dula: '; 
                                             combo('ced_per2', $ced_per3, 'vista_personal', $link, 0, 1, 0, '', 'ced_per', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nombre");?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
