<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'feriados.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "feriados";	// nombre de la tabla
$ncampos = "4";			//numero de campos del formulario
$datos[0] = crear_datos ("fch_frd","Fecha Feriado",$_POST['fch_frd'],"10","10","fecha");
$datos[1] = crear_datos ("des_frd","Descripcion Feriado",$_POST['des_frd'],"1","50","alfanumericos");
$datos[2] = crear_datos ("tip_frd","Tipo de  Feriado",$_POST['tip_frd'],"1","25","alfabeticos");
$datos[3] = crear_datos ("tck_frd","Aplica Descuento de Cesta Ticket",$_POST['tck_frd'],"1","2","alfabeticos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha";
		$datos[0]="fch_frd";
		$parametro[1]="Descripci�n";
		$datos[1]="des_frd";
		busqueda_varios(4,$buscando,$datos,$parametro,"fch_frd");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $fch_frd = $row["fch_frd"];
	    $des_frd = $row["des_frd"];    
	    $tip_frd = $row["tip_frd"];
	    $tck_frd = $row["tck_frd"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"fch_frd",$_POST["fch_frd"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["fch_frd"],"fch_frd",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de D�as Feriados</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas">Fecha Feriado: </td>
                        <td width="75%">
                        <?php escribir_campo('fch_frd',$_POST["fch_frd"],$fch_frd,'readonly',15,20,'Fecha del D�a Feriado',$boton,$existe,'fecha')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripci�n:                          </td>
			            <td>
                        <?php escribir_campo('des_frd',$_POST["des_frd"],$des_frd,'',50,35,'Descripci�n del D�a Feriado',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Tipo de Feriado:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tip_frd" title="Tipo de Feriado">
                          <option>Seleccione...</option>
                          <option value="feriado" '; if ($tip_frd == "feriado" || $_POST['tip_frd'] =="feriado") { echo 'selected'; } echo '>Feriado</option>
                          <option value="municipal" '; if ($tip_frd == "municipal" || $_POST['tip_frd'] =="municipal") { echo 'selected'; } echo '>Feriado/Jubilo Municipal</option>
                          <option value="fiesta_nacional" '; if ($tip_frd == "fiesta_nacional" || $_POST['tip_frd'] =="fiesta_nacional") { echo 'selected'; } echo '>Fiesta/Jubilo Nacional</option>
                          <option value="no_laboral" '; if ($tip_frd == "no_laboral" || $_POST['tip_frd'] =="no_laboral") { echo 'selected'; } echo '>No Laboral</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tip_frd" id="tip_frd" value="'.$tip_frd.'" >'; 
						    if ($tip_frd == "feriado") { echo 'Feriado'; } 
						    if ($tip_frd == "municipal") { echo 'Feriado/Jubilo Municipal'; } 
						    if ($tip_frd == "fiesta_naciona") { echo 'Fiesta/Jubilo Nacional'; } 
						    if ($tip_frd == "no_laboral") { echo 'No Laboral'; } 
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Descuento Cesta Ticket:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tck_frd" title="Aplica descuento de Cesta Ticket?">
                          <option>Seleccione...</option>
                          <option value="SI" '; if ($tck_frd == "SI" || $_POST['tck_frd'] =="SI") { echo 'selected'; } echo '>SI</option>
                          <option value="NO" '; if ($tck_frd == "NO" || $_POST['tck_frd'] =="NO") { echo 'selected'; } echo '>NO</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tck_frd" id="tck_frd" value="'.$tck_frd.'" >'; 
						    if ($tck_frd == "SI") { echo 'SI'; } 
						    if ($tck_frd == "NO") { echo 'NO'; } 

						}?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$calen = '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].buscar_a,';
						$calen .= "'yyyy-mm-dd',this)";
						$calen .= '" title="Haga click aqui para elegir una fecha"/>';
						$ncriterios =2; 
						$criterios[0] = 'Fecha '.$calen; 
						$campos[0] ="fch_frd";
						$criterios[1] = "Descripci�n"; 
						$campos[1] ="des_frd";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); }  ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
