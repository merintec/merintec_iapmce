<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Datos de Educaci�n</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['ced_per'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$cod_edu = $_POST["cod_edu"];
$ced_per = $_GET["ced_per"];
$tit_edu = $_POST["tit_edu"];
$niv_edu = $_POST["niv_edu"];
$ins_edu = $_POST["ins_edu"];
$fch_edu = $_POST["fch_edu"];
$reg_edu = $_POST["reg_edu"];
$pagina = 'personal_educacion.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$pagina2 = 'personal_educacion.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$tabla = "educacion";	// nombre de la tabla
$ncampos = "6";		    //numero de campos del formulario
$datos[0] = crear_datos ("ced_per","Cedula",$_POST['ced_per'],"1","12","numericos");
$datos[1] = crear_datos ("tit_edu","Titulo Obtenido",$_POST['tit_edu'],"1","50","alfanumericos");
$datos[2] = crear_datos ("niv_edu","Nivel Educativo",$_POST['niv_edu'],"1","50","alfanumericos");
$datos[3] = crear_datos ("ins_edu","Instituci�n",$_POST['ins_edu'],"1","100","alfanumericos");
$datos[4] = crear_datos ("fch_edu","Fecha de Grado",$_POST['fch_edu'],"1","11","fecha");
$datos[5] = crear_datos ("reg_edu","Registrado",$_POST['reg_edu'],"0","1","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_edu = $row["cod_edu"];
	    $ced_per = $row["ced_per"];
        $tit_edu = $row["tit_edu"];
        $niv_edu = $row["niv_edu"];
        $ins_edu = $row["ins_edu"];
        $fch_edu = $row["fch_edu"];
        $reg_edu = $row["reg_edu"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_edu",$_POST["cod_edu"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
		$boton = comp_exist($datos[1][0],$datos[1][2]."' AND ced_per = '".$datos[0][2],$tabla,$boton,'si',"Titulos Botenidos");
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_edu"],"cod_edu",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_edu","educacion",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Estudios Realizados. C.I: <?php echo $viene_val; ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">T�tulo Obtenido:</td>
                        <td width="75%">
			<input name="cod_edu" type="hidden" id="cod_edu" value="<?php if(! $existe) { echo $_POST['cod_edu']; } else { echo $cod_edu; } ?>" title="Codigo del Estudio">
			    <input name="ced_per" type="hidden" id="ced_per" readonly value="<?php if(! $existe) { echo $viene_val; } else { echo $ced_per; } ?>" <?php if ($boton=='Actualizar') { echo "readonly"; } ?> size="29" title="C&eacute;dula del Personal" />
                        <input name="tit_edu" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="tit_edu" value="<?php if(! $existe) { echo $_POST['tit_edu']; } else { echo $tit_edu; } ?>" size="35" maxlength="50" title="T�tulo Obtenido">
                        <?php if ($boton=='Modificar') { echo $tit_edu; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Nivel:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="niv_edu" title="Nivel Educativo">
                          <option>Seleccione...</option>
                          <option value="Bach" '; if ($niv_edu == "Bach" || $_POST['niv_edu'] =="Bach") { echo 'selected'; } echo '>Bachiller</option>
                          <option value="TecM" '; if ($niv_edu == "TecM" || $_POST['niv_edu'] =="TecM") { echo 'selected'; } echo '>T�cnico Medio</option>
                          <option value="TecS" '; if ($niv_edu == "TecS" || $_POST['niv_edu'] =="TecS") { echo 'selected'; } echo '>T�cnico Superior</option>
                          <option value="Univ" '; if ($niv_edu == "Univ" || $_POST['niv_edu'] =="Univ") { echo 'selected'; } echo '>Universitario</option>
                          <option value="Esp" '; if ($niv_edu == "Esp" || $_POST['niv_edu'] =="Esp") { echo 'selected'; } echo '>Especialidad</option>
                          <option value="Msc" '; if ($niv_edu == "Msc" || $_POST['niv_edu'] =="Msc") { echo 'selected'; } echo '>Maestr�a</option>
                          <option value="Doc" '; if ($niv_edu == "Doc" || $_POST['niv_edu'] =="Doc") { echo 'selected'; } echo '>Doctorado</option>
                          <option value="Phd" '; if ($niv_edu == "Phd" || $_POST['niv_edu'] =="Phd") { echo 'selected'; } echo '>PHD</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="niv_edu" id="niv_edu" value="'.$niv_edu.'" >'; 
							if ($niv_edu == "Bach") { echo 'Bachiller'; }
						    if ($niv_edu == "TecM") { echo 'T�cnico Medio'; } 
							if ($niv_edu == "TecS") { echo 'T�cnico Superior'; }
						    if ($niv_edu == "Univ") { echo 'Universitario'; } 
							if ($niv_edu == "Esp") { echo 'Especialidad'; }
						    if ($niv_edu == "Msc") { echo 'Maestr�a'; } 
						    if ($niv_edu == "Doc") { echo 'Doctorado'; } 
							if ($niv_edu == "Phd") { echo 'PHD'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Instituto:</td>
                        <td><input name="ins_edu" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ins_edu" value="<?php if(! $existe) { echo $_POST['ins_edu']; } else { echo $ins_edu; } ?>" size="35" title="Instituci�n que emana el T�tulo" />
                          <?php if ($boton=='Modificar') { echo $ins_edu; } ?></td>
                      </tr>
                        <td class="etiquetas">Fecha de Grado: </td>
                        <td><input name="fch_edu" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_edu" value="<?php if(! $existe) { echo $_POST['fch_edu']; } else { echo $fch_edu; } ?>" size="20" title="Fecha de Grado" />
                          <?php if ($boton=='Modificar') { echo $fch_edu; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_edu,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
		      <tr>
 			<td class="etiquetas">Registrado:</td>
			<td><input name="reg_edu" id="reg_edu" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$reg_edu.'"'; } else { echo 'type="checkbox" value="S"'; } if($_POST["reg_edu"]=='S' || $reg_edu == 'S') { echo 'checked'; } ?> title="El T�tulo se encuentra Registrado?">
                          <?php if ($boton=='Modificar') { if ($reg_edu=='S') {echo 'SI'; } else { echo 'NO'; } } ?>
			</td>
		      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_educacion_personal.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
