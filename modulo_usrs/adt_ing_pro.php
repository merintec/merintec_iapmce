<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Recibos de Patente Vehicular</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_med'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'adt_ing_pro.php?id='.$_GET["id"].'&seccion='.$_GET["seccion"].'&tipo='.$_GET["tipo"];
$pagina2 = 'adt_ing_pro.php?id='.$_GET["id"].'&seccion='.$_GET["seccion"].'&tipo='.$_GET["tipo"];

//// consulta los datos de la serie
$sql_ing = "Select * from ing_pat_vhi where id=".$_GET['id'];
$busca_sql_ing = mysql_query($sql_ing);
if ($row_ing=mysql_fetch_array($busca_sql_ing)) {
    $id = $row_ing['id'];
    $fecha = $row_ing['fecha'];
    $serie = $row_ing['serie'];
    $inicio = $row_ing['inicio'];
    $fin = $row_ing['fin'];   
    $tipo = $row_ing['tipo'];
}
//// consulta para ver si existen datos guardados de la serie
if (!$_POST["confirmar"]) {
    $sql_ing_pro = "Select * from adt_ing_pro where serie_id=".$id." AND recibo >= ".$inicio." AND recibo <= ".$fin."";
    $busca_sql_ing_pro = mysql_query($sql_ing_pro);
    while ($row_ing_pro=mysql_fetch_array($busca_sql_ing_pro)) {
        $recibo = $row_ing_pro['recibo'];
        $var = 'monto'.$recibo;
        $$var = $row_ing_pro['monto'];
        $var2 = 'cod_cat'.$recibo;
        $$var2 = $row_ing_pro['cod_cat'];
        $var3 = 'con_esp'.$recibo;
        $$var3 = $row_ing_pro['con_esp'];
        $existe = 'SI';
        $boton = "Modificar";
        $suma_recibos += $row_ing_pro['monto'];
    }
}
$tabla = "adt_ing_pro";	// nombre de la tabla
$ncampos = "5";			//numero de campos del formulario

/// estructura repetitiva para crear la matriz de datos para guardar y modificar
for ($i = $inicio; $i <= $fin; $i++)
{ 
    $val_monto = $_POST['monto'.$i];
    $val_categoria = $_POST['cod_cat'.$i];
    $val_con_esp = $_POST['con_esp'.$i];
    $val_recibo = $i;
    $val_serie = $id;
    $datos[$i][0] = crear_datos ("serie_id","Serie",$val_serie,"0","11","numericos");        
    $datos[$i][1] = crear_datos ("recibo","Recibo",$val_recibo,"0","11","numericos");    
    $datos[$i][2] = crear_datos ("monto","Monto del Recibo ".$i,$val_monto,"0","12","decimal");   
    $datos[$i][3] = crear_datos ("cod_cat","Categoria del Recibo ".$i,$val_categoria,"0","2","numericos");   
    $datos[$i][4] = crear_datos ("con_esp","Condicion especial de Recibo ".$i,$val_con_esp,"0","50","alfabeticos");   
}
if ($_POST["confirmar"]=="Actualizar") 
{
    for ($i = $inicio; $i <= $fin; $i++)
    {
        if (!$error) {
            $validacion = validando_campos ($ncampos,$datos[$i]);
            if (!$validacion) { 
        		$error = "SI";
        	} 
        }
    }
    if ($validacion) {
        for ($i = $inicio; $i <= $fin; $i++)
        {
            // verificar si se guard� anteriormente un datos para actualizar
            $sql_ing_ver = "Select * from adt_ing_pro where recibo=".$i;
            $busca_sql_ing_ver = mysql_query($sql_ing_ver);
            if ($row_ing_ver=mysql_fetch_array($busca_sql_ing_ver)) {
                modificar_func($ncampos,$datos[$i],$tabla,"recibo",$i."' AND serie_id='".$datos[$i][0][2],$pagina);
	    	    auditoria_func ('modificar', '', $_POST["ant"], $tabla);   
	        }
	        else
	        {
	            insertar_func_nomina($ncampos,$datos[$i],$tabla,$pagina);
	        }
        } 
        return;    
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
    ///para validar
    for ($i = $inicio; $i <= $fin; $i++)
    {
        if (!$error) {
            $validacion = validando_campos ($ncampos,$datos[$i]);
            if (!$validacion) { 
        		$error = "SI";
        	} 
        }
    }
	if ($validacion) { 
		$boton = "Guardar";
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
     for ($i = $inicio; $i <= $fin; $i++)
     {
    	insertar_func($ncampos,$datos[$i],$tabla,$pagina);
    	//auditoria_func ('insertar', $ncampos, $datos, $tabla);            
     }
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
    echo '<SCRIPT> alert ("Esta opci�n se encuentra deshabilitada en esta secci�n."); </SCRIPT>';
    $boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Recibos de la Serie</td>
                  </tr>
                  <tr>
                    <td width="526">
                        <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0">
                            <tr align="center" class="etiquetas">
                                <td width="30px">N�</td><td width="50px">Serie</td><td width="50px">Recibo</td><td>Condici�n</td><td>Monto</td>
                                <?php if ($tipo=='C'){ ?>
                                    <td>Categor�a</td>
                                <?php } ?>
                            </tr>
                            
                        <?php 
                                for ($i = $inicio; $i <= $fin; $i++)
                        	    { 
                         	       $var3 = 'con_esp'.$i;
                        	       echo '<tr class="etiquetas_nomina2" align="right"><td>'.($i-$inicio+1).'&nbsp;</td>';
                        	       echo '<td align="center">'.($serie).'</td>';
                        	       echo '<td align="right">'.($i).'&nbsp;</td>';
                        	       echo '<td align="center">';
                                        if ($boton != "Modificar") { echo '<select name="con_esp'.$i.'" title="Sexo">
                                            <option Value="">Seleccione...</option>
                                            <option value="Nulo" '; if ($$var3 == "Nulo" || $_POST['con_esp'.$i] =="Nulo") { echo 'selected'; } echo '>Nulo</option>
                                            </select>'; }
                                        else { 
                						    echo '<input type="hidden" name="con_esp'.$i.'" id="con_esp" value="'.$$var3.'" >'; 
				                		    if ($$var3 == "Nulo") { echo 'Nulo'; }
				                  		}
                        	       echo '&nbsp;</td>';
                        	       echo '<td align="center">';    
                        	       $var1 = 'monto'.$i;                   	       
       	                           escribir_campo('monto'.$i,$_POST['monto'.$i],$$var1,'',11,11,'Monto del Recibo',$boton,$existe,'','onKeyDown="Saltar(event,this.form.monto'.($i+1).',this.form.monto'.$inicio.');"');
                        	       echo ' Bs. </td>';
                        	       if ($tipo=='C'){ 
                            	        $var2 = 'cod_cat'.$i;
                        	            echo '<td align="center">';                       	            
   	                                    combo('cod_cat'.$i, $$var2, 'adt_ing_cat', $link, 0, 0, 1, '', 'cod_cat', "", $boton, "ORDER BY des_cat","cod_cat"); 
                        	            echo '</td>';
                                   }
                        	       echo'</tr>';
                        	    }
                         ?>
                            <tr class="tabla_total">
                                <td width="30px" colspan="3" align="right" ><b>TOTAL SERIE&nbsp;</b></td><td align="center">---</td><td align="center"><b><?php echo $suma_recibos; ?> Bs.</b></td>
                                <?php if ($tipo=='C'){ ?>
                                    <td>&nbsp;</td>
                                <?php } ?> 
                            </tr>
                        </table>
                    </td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
