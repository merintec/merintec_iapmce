<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Montos del Pago de Medicinas</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_med'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$sql="select * from valores";
$res = mysql_query($sql);
while ($row = mysql_fetch_array($res))
{
    $$row['des_val'] = $row['val_val'];
}
$var='MED_'; $var.=date('Y'); $med = redondear ($$var,2,'','.');
$med;

$boton = "Verificar";
$existe = '';
$pagina = 'medicinas_per.php?cod_med='.$_GET["cod_med"].'&seccion='.$_GET["seccion"];
$pagina2 = 'medicinas_per.php?cod_med='.$_GET["cod_med"].'&seccion='.$_GET["seccion"];
$tabla = "medicinas_per";	// nombre de la tabla
$ncampos = "4";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_med_per","Codigo de pago de medicina a personal",$_POST['cod_med_per'],"0","11","numericos");
$datos[1] = crear_datos ("cod_med","Cod. pago de Medicinas",$_POST['cod_med'],"1","11","numericos");
$datos[2] = crear_datos ("ced_per","Personal",$_POST['ced_per'],"1","11","numericos");
$datos[3] = crear_datos ("mnt_med_per","Monto del pago de medicinas al personal seleccionado",$_POST['mnt_med_per'],"1","12","decimal");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_med_per = $row["cod_med_per"];
	    $cod_med = $row["cod_med"];
	    $ced_per = $row["ced_per"];
	    $mnt_med_per = $row["mnt_med_per"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_med_per",$_POST["cod_med_per"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar";
		$msg = "False";
		if ($_POST['mnt_med_per']>$med) 
		{ 
		    $mnt_disp = $med;
		    echo '<SCRIPT> alert ("'.$msg_med_max.' '.$mnt_disp.' Bs."); </SCRIPT>';
	        $boton = "Verificar";
	        $msg = "True";
		}
		$sql_pag_act = "Select * from medicinas_per mp, vista_personal vp where mp.cod_med=".$_POST['cod_med']." AND mp.ced_per = ".$_POST['ced_per']. " AND mp.ced_per=vp.ced_per";
		$busca_pag_act = mysql_query($sql_pag_act);
		if (($row_pag_act = mysql_fetch_array($busca_pag_act)) && ($msg == "False")){
		    echo '<SCRIPT> alert ("'.$msg_med_exe_alert.' '.$row_pag_act[nombre].'"); </SCRIPT>';
	        $boton = "Verificar";
	        $msg = "True";
		}
	    $sql_tot = "select m.*,sum(mp.mnt_med_per) as med_pag from medicinas m, medicinas_per mp where mp.ced_per=".$_POST['ced_per']." AND YEAR(m.fch_med)=".date('Y')." AND m.cod_med=mp.cod_med GROUP BY ced_per";
	    $busca_tot = mysql_query($sql_tot);
	    if (($row_tot=mysql_fetch_array($busca_tot)) && ($msg == "False"))
	    {
	        $tot_pag = $row_tot['med_pag'];
	        $tot_pag = $tot_pag + $_POST['mnt_med_per'];
	        $mnt_disp = $row_tot['mnt_med'] - $row_tot['med_pag'];
	        $mnt_disp = redondear($mnt_disp,2,'.',',');
	        if ($tot_pag > $row_tot['mnt_med']) 
	        {
	            echo '<SCRIPT> alert ("'.$msg_med_exe_pag_alert.' '.$mnt_disp.' Bs."); </SCRIPT>';
	            $boton = "Verificar";
	            $msg = "True";
	        }
	    }
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_med_per"],"cod_med_per",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_med_per","medicinas_per",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Montos a Pagar</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Personal:
                          </td>
		            <td><input name="cod_med_per" type="hidden" id="cod_med_per" value="<?php if (! $existe) { echo $_POST['cod_med_per']; } else { echo $cod_med_per; } ?>" title="Codigo del pago de medicinas al personal">
			<input name="cod_med" type="hidden" id="cod_med" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_med; } ?>" title="Codigo del pago de Medicinas">
                        <?php combo('ced_per', $ced_per, 'vista_personal', $link, 0, 1, 0, '', 'ced_per', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nombre");?></td>
                      </tr>
                       <tr>
                        <td width="25%" class="etiquetas">Monto:</td>
                        <td width="75%">
                        <input name="mnt_med_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mnt_med_per" value="<?php if(! $existe) { echo $_POST['mnt_med_per']; } else { echo $mnt_med_per; } ?>" size="10" maxlength="12" title="Monto del pago de medicinas al personal seleccionado">
                        <?php if ($boton=='Modificar') { echo $mnt_med_per; } ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_medicinas.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
