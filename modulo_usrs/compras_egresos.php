<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php
  if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'compras_egresos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "egresos";		// nombre de la tabla
$ncampos = "18";		//numero de campos del formulario
$datos[0]  = crear_datos ("cod_egr","C�digo",$_POST['cod_egr'],"0","11","numericos");
$datos[1]  = crear_datos ("nor_egr","Num Orden",$_POST['nor_egr'],"0","4","numericos");
$datos[2]  = crear_datos ("fch_egr","Fecha",$_POST['fch_egr'],"10","10","fecha");
$datos[3] = crear_datos ("frm_egr","Numero del formulario de Egreso",$_POST['frm_egr'],"0","6","numericos");
$datos[4]  = crear_datos ("rif_pro","Proveedor",$_POST['rif_pro'],"1","10","alfanumericos");
$datos[5]  = crear_datos ("cod_ban","Banco",$_POST['cod_ban'],"1","11","numericos");
$datos[6]  = crear_datos ("chq_egr","Cheque/Transferencia",$_POST['chq_egr'],"1","25","alfanumericos");
$datos[7] = crear_datos ("con_egr","Concepto",$_POST['con_egr'],"0","255","alfanumericos");
$datos[8] = crear_datos ("ret_iva_egr","Ret. IVA",$_POST['ret_iva_egr'],"0","3","numericos");
$datos[9] = crear_datos ("ret_isrl_egr","Ret. ISRL",$_POST['ret_isrl_egr'],"0","2","numericos");
$datos[10] = crear_datos ("ded_egr","Otras Deducciones",$_POST['ded_egr'],"1","12","decimal");
$datos[11] = crear_datos ("sin_par_egr","Monto sin Partida",$_POST['sin_par_egr'],"1","12","decimal");
$datos[12] = crear_datos ("ela_egr","Elaborado",$_POST['ela_egr'],"1","50","alfabeticos");
$datos[13] = crear_datos ("rev_egr","Revisado",$_POST['rev_egr'],"1","50","alfabeticos");
$datos[14] = crear_datos ("apr_egr","Aprobado",$_POST['apr_egr'],"1","50","alfabeticos");
$datos[15] = crear_datos ("cont_egr","Contabilizado",$_POST['cont_egr'],"1","50","alfabeticos");
$datos[16] = crear_datos ("obs_egr","Observaci�n",$_POST['obs_egr'],"0","255","alfanumericos");
$datos[17]  = crear_datos ("paga_imp","Pago de Impuestos?",$_POST['paga_imp'],"0","20","alfabeticos");


if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Num Control";
		$datos[0]="nor_egr";
		$parametro[1]="Fecha";
		$datos[1]="fch_egr";
		$parametro[2]="Concepto";
		$datos[2]="con_egr";
		busqueda_varios(5,$buscando,$datos,$parametro,"cod_egr");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_egr = $row["cod_egr"];
	    $fch_egr = $row["fch_egr"];
	    $nor_egr = $row["nor_egr"];
	    $cod_ban = $row["cod_ban"];
	    $chq_egr = $row["chq_egr"];
	    $rif_pro = $row["rif_pro"];
	    $con_egr = $row["con_egr"];
	    $ret_iva_egr = $row["ret_iva_egr"];
	    $ret_isrl_egr = $row["ret_isrl_egr"];
	    $ela_egr = $row["ela_egr"];
	    $rev_egr = $row["rev_egr"];
	    $apr_egr = $row["apr_egr"];
	    $cont_egr = $row["cont_egr"];
	    $obs_egr = $row["obs_egr"];
	    $ded_egr = $row["ded_egr"];
	    $sin_par_egr = $row["sin_par_egr"];
	    $frm_egr = $row["frm_egr"];
	    $paga_imp = $row["paga_imp"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///

	    //// Consultamos si tiene ordenes de pago asociadas para inhabilitar el boton de eliminar
            $sql_rela = "SELECT * FROM pagos WHERE frm_egr=".$frm_egr." OR frm_egr_IVA=".$frm_egr." OR frm_egr_ISRL=".$frm_egr;
            if (mysql_fetch_array(mysql_query($sql_rela))){
		$prm[3]='';
            }
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	///// Si se cambia el numero del formulario se verifica que sea el siguiente numero disponible (esto para casos de reimpresi�n del documento)
	if ($validacion)
	{
	    if ($_POST['frm_egr'] != $_POST['egr_ant'])
	    {
	        $sql = "SELECT MAX(frm_egr) AS frm_egr FROM pagos";
            $res = mysql_fetch_array(mysql_query($sql));
            $frm_next = $res['frm_egr'] + 1;
	        if ($frm_next != $_POST['frm_egr'])
            {
                echo '<SCRIPT>alert("'.$msg_pago_cambiofrm.' ('.$frm_next.')");</SCRIPT>';
		        $validacion = '';                
            }
            else { $validacion = 1; }
	    }
	}
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_egr",$_POST["cod_egr"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	$sql = "SELECT MAX(nor_egr) AS nor_egr FROM egresos WHERE YEAR(fch_egr)=";
	$sql .= $_POST["fch_egr"][0].$_POST["fch_egr"][1].$_POST["fch_egr"][2].$_POST["fch_egr"][3];
    	$res = mysql_fetch_array(mysql_query($sql));
    	$nor_egr = $res['nor_egr'] + 1;
	$datos[1]  = crear_datos ("nor_egr","Num Orden",$nor_egr,"0","4","numericos");

    //// Generar numero de formulario
	$sql = "SELECT MAX(frm_egr) AS frm_egr FROM egresos";
    $res = mysql_fetch_array(mysql_query($sql));
    $frm_egr = $res['frm_egr'] + 1;
	$datos[3]  = crear_datos ("frm_egr","Num Formulario",$frm_egr,"0","6","numericos");
	
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_egr"],"cod_egr",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
        //// Consultamos si tiene ordenes de pago asociadas para inhabilitar el boton de eliminar
            $sql_rela = "SELECT * FROM pagos WHERE frm_egr=".$frm_egr;
            if (mysql_fetch_array(mysql_query($sql_rela))){
		$prm[3]='';
            }

}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de Comprobantes de Egreso </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">N&uacute;mero de Control:</td>
                        <td width="75%">
			<input name="cod_egr" type="hidden" id="cod_egr" value="<?php if(! $existe) { echo $_POST['cod_egr']; } else { echo $cod_egr; } ?>" size="35" title="Codigo de la Orden de Compra/Servicio">
			<input name="nor_egr" type="hidden" id="nor_egr" value="<?php if(! $existe) { echo $_POST['nor_egr']; } else { echo $nor_egr; } ?>" size="35" title="Num Orden"><?php if ($boton != "Verificar" && $boton != "Guardar") { echo substr($fch_egr, 5, 2).substr($fch_egr, 2, 2).$nor_egr; } else echo 'Por Asignar...'; ?>                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">N� Formulario:</td>
                        <td width="75%">
			                <input name="frm_egr" type="<?php if ($boton == 'Verificar' || $boton=='Modificar' || $boton=='Guardar'){ echo 'hidden';} else {echo 'text';} ?>" id="frm_egr" value="<?php if(! $existe) { echo $_POST['frm_egr']; } else { echo $frm_egr; } ?>" size="15" title="Numero de Formulario"><?php if ($boton == "Modificar") { echo $frm_egr; } if ($boton == "Verificar" || $boton == "Guardar") { echo 'Por Asignar...'; }?>
                            <input type="hidden" name="egr_ant" id="egr_ant" value="<?php if ($existe) { echo $frm_egr;} else { echo $_POST["egr_ant"]; }?>">
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Registro:</td>
                        <td><input name="fch_egr" type="<?php if ($boton!='Verificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_egr" value="<?php if(! $existe) { echo $_POST['fch_egr']; } else { echo $fch_egr; } ?>" size="20" title="Fecha de Registro de la Orden" />
                          <?php if ($boton=='Modificar') { echo $fch_egr; } ?>
			  <?php if ($boton=='Actualizar' || $boton=='Guardar') { echo $_POST['fch_egr']; } ?>
			<?php if ($boton=='Verificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_egr,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
	              <tr>
                        <td class="etiquetas">Pagar Impuestos?:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="paga_imp" title="Paga impuestos?">
                          <option>Seleccione...</option>
                          <option value="" '; if ($paga_imp == "" || $_POST['paga_imp'] =="") { echo 'selected'; } echo '>NO</option>
                          <option value="iva" '; if ($paga_imp == "iva" || $_POST['paga_imp'] =="iva") { echo 'selected'; } echo '>IVA</option>
                          <option value="isrl" '; if ($paga_imp == "isrl" || $_POST['paga_imp'] =="isrl") { echo 'selected'; } echo '>ISRL</option>
                        </select>'; } 
			else 
			{ 
			    echo '<input type="hidden" name="paga_imp" id="paga_imp" value="'.$paga_imp.'" >'; 
			    if ($paga_imp == "") { echo 'NO'; } 
 			    if ($paga_imp == "iva") { echo 'IVA'; }
 			    if ($paga_imp == "isrl") { echo 'ISRL'; }
			}?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Proveedor:                          </td>
			            <td>
                        <?php combo('rif_pro', $rif_pro, 'proveedores', $link, 2, 0, 3, '', 'rif_pro', "", $boton, "ORDER BY nom_pro",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Banco:                          </td>
			            <td>
                        <?php combo('cod_ban', $cod_ban, 'banco', $link, 0, 0, 1, '', 'cod_ban', "", $boton, "",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Cheque/Transferencia:</td>
			            <td>
                        <input name="chq_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="chq_egr" value="<?php if(! $existe) { echo $_POST['chq_egr']; } else { echo $chq_egr; } ?>" size="15" title="Cheque">
                        <?php if ($boton=='Modificar') { echo $chq_egr; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Concepto:</td>
			            <td>
                        <input name="con_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="con_egr" value="<?php if(! $existe) { echo $_POST['con_egr']; } else { echo $con_egr; } ?>" size="35" title="Concepto del Egredo">
                        <?php if ($boton=='Modificar') { echo $con_egr; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Reteci&oacute;n de IVA:                          </td>
			            <td>
                        <input name="ret_iva_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ret_iva_egr" value="<?php if(! $existe) { echo $_POST['ret_iva_egr']; } else { echo $ret_iva_egr; } ?>" size="3" title="Porcentaje de Retenci&oacute;n de IVA">
                        <?php if ($boton=='Modificar') { echo $ret_iva_egr.'%'; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Retenci&oacute;n de ISRL:                          </td>
			            <td>
                        <input name="ret_isrl_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ret_isrl_egr" value="<?php if(! $existe) { echo $_POST['ret_isrl_egr']; } else { echo $ret_isrl_egr; } ?>" size="3" title="Procentaje de Retenci&oacute;n de ISRL">
                        <?php if ($boton=='Modificar') { echo $ret_isrl_egr.'%'; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto sin Partida:</td>
			            <td>
                        <input name="sin_par_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="sin_par_egr" value="<?php if(! $existe) { echo $_POST['sin_par_egr']; } else { echo $sin_par_egr; } ?>" size="15" title="Monto sin imputaci�n Presupuestaria">
                        <?php if ($boton=='Modificar') { echo $sin_par_egr; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Otras Deducciones:</td>
			            <td>
                        <input name="ded_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ded_egr" value="<?php if(! $existe) { echo $_POST['ded_egr']; } else { echo $ded_egr; } ?>" size="15" title="Monto Total Otras Deducciones">
                        <?php if ($boton=='Modificar') { echo $ded_egr; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Elaborado:</td>
			<td>
                        <input name="ela_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ela_egr" value="<?php if(! $existe) { echo $_POST['ela_egr']; } else { echo $ela_egr; } ?>" size="35" title="Persona que Elabora la Orden">
                        <?php if ($boton=='Modificar') { echo $ela_egr; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Administraci�n:</td>
			<td>
                        <input name="rev_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="rev_egr" value="<?php if(! $existe) { echo $_POST['rev_egr']; } else { echo $rev_egr; } ?>" size="35" title="Persona que Revisa la Orden">
                        <?php if ($boton=='Modificar') { echo $rev_egr; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Contralor(a):</td>
			<td>
                        <input name="apr_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="apr_egr" value="<?php if(! $existe) { echo $_POST['apr_egr']; } else { echo $apr_egr; } ?>" size="35" title="Persona que Aprueba la Orden">
                        <?php if ($boton=='Modificar') { echo $apr_egr; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Contabilizado:</td>
			<td>
                        <input name="cont_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="cont_egr" value="<?php if(! $existe) { echo $_POST['cont_egr']; } else { echo $cont_egr; } ?>" size="35" title="Persona que Contabiliza la Orden">
                        <?php if ($boton=='Modificar') { echo $cont_egr; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaci&oacute;n:                          </td>
			<td>
                        <input name="obs_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="obs_egr" value="<?php if(! $existe) { echo $_POST['obs_egr']; } else { echo $obs_egr; } ?>" size="35" title="Observaciones de la Orden">
                        <?php if ($boton=='Modificar') { echo $obs_egr; } ?></td>
                      </tr>

                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
		  <tr>
                    <td align="center"><?php if($boton=='Modificar'){ abrir_ventana('imprimir_egreso.php','v_imprimir','Imprimir Comprobante',"cod_egr=".$cod_egr."&paga_imp=".$paga_imp); } ?><?php if($boton=='Modificar'){ abrir_ventana('imprimir_egreso.php','v_imprimir','Imprimir Cheque',"cod_egr=".$cod_egr."&paga_imp=".$paga_imp."&cheque=on"); } ?></td>
                  </tr>
                  <?php if ($boton=="Modificar") { echo '<tr><td><hr></td></tr>'; 
		  echo '<tr><td> 
			<table width="100%" align="center" cellspacing="10">';
			?>
			<tr><td width="<?php echo (100/1).'%'; ?>" align="center">
			    <?php if ($permitir_modificar=='NO'){ echo "<b>Ordenes de Pago relacionadas al Egreso</b>"; }?>
			    <?php if($boton=='Modificar' && $permitir_modificar!='NO'){ abrir_ventana('egresos_pagos.php','v_Egresos','Ordenes de Pagos afectadas',"cod_egr=".$cod_egr."&rif=".$rif_pro."&frm_egr=".$frm_egr."&paga_imp=".$paga_imp."&seccion=".$_GET['seccion']); } ?></td></tr>
		  <tr>
		    <td>
			<?php include('capa_egreso_pago.php'); ?>
		    </td>
		  </tr>
			<?php
		  } ?>
                  <tr>
                    <td>
					<?php 
						$ncriterios =4; 
						$criterios[0] = "Concepto"; 
						$campos[0] ="con_egr";
						$criterios[1] = "N� Formulario"; 
						$campos[1] ="frm_egr"; 
						$criterios[2] = "Cod. Banco"; 
						$campos[2] ="cod_ban";
						$adicionaL_bus = '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=criterio3.checked=true;displayCalendar(document.forms[0].buscar_a,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>'; 
						$criterios[3] = "Fecha ".$adicionaL_bus; 
						$campos[3] ="fch_egr";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"criterio2.checked=true; valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
					     $cod_ban = '';
                         echo '<center>Buscar Cod. Banco: '; 
                                             combo('cod_ban2', $cod_ban, 'banco', $link, 0, 0, 1, '', 'cod_ban', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nom_ban",'');
                                             echo '</center>'; 

                                             echo '</center>'; 
				   	   }  ?></td>
                  </tr>

                    </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
