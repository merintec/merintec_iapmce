<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'dependencias.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "dependencias";	// nombre de la tabla
$ncampos = "4";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_dep","C�digo",$_POST['cod_dep'],"1","5","alfanumericos");
$datos[1] = crear_datos ("nom_dep","Nombre",$_POST['nom_dep'],"1","150","alfabeticos");
$datos[2] = crear_datos ("cod_act","C�digo de Actividad",$_POST['cod_act'],"0","3","alfanumericos");
$datos[3] = crear_datos ("cod_pro","C�digo de Programa",$_POST['cod_pro'],"0","1","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre";
		$datos[0]="nom_dep";
		$parametro[1]="Cod.";
		$datos[1]="cod_dep";
		busqueda_varios(4,$buscando,$datos,$parametro,"cod_dep");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_dep = $row["cod_dep"];
	    $nom_dep = $row["nom_dep"];
	    $cod_act = $row["cod_act"];
	    $cod_pro = $row["cod_pro"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_dep",$_POST["cod_dep"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_dep"],"cod_dep",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de la Dependencia</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">C&oacute;digo:</td>
                        <td width="75%">
                        <?php escribir_campo('cod_dep',$_POST["cod_dep"],$cod_dep,'readonly',5,35,'C�digo de la Dependencia',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Nombre:</td>
			            <td>
                        <?php escribir_campo('nom_dep',$_POST["nom_dep"],$nom_dep,'',150,35,'Nombre de la Dependencia',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Actividad:</td>
                        <td><?php combo('cod_act', $cod_act, 'actividades', $link, 0, 0, 1, "", 'cod_act', '', $boton,""); ?>
			</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Programa:</td>
                        <td><?php combo('cod_pro', $cod_pro, 'programas', $link, 0, 0, 1, "", 'cod_pro', 'cod_act', $boton,""); ?>
			</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =2; 
						$criterios[0] = "C�digo"; 
						$campos[0] ="cod_dep";
						$criterios[1] = "Nombre";
						$campos[1] = "nom_dep";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
