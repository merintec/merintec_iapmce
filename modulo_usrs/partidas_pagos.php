<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Codificaci&oacute;n Presupuestaria</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_pag'];
$viene_val2 = $_GET['frm_com'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'partidas_pagos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"].'&cod_pag='.$_GET["cod_pag"].'&frm_com='.$_GET["frm_com"].'&frm_pag='.$_GET["frm_pag"];
$pagina2 = 'partidas_pagos.php?cod_pag='.$_GET["cod_pag"].'&frm_com='.$_GET["frm_com"].'&frm_pag='.$_GET["frm_pag"].'&seccion='.$_GET["seccion"];
$tabla = "partidas_pagos";	// nombre de la tabla
$ncampos = "5";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_pro_pag","Cod. Presupuestario  ",$_POST['cod_pro_pag'],"0","11","numericos");
$datos[1] = crear_datos ("cod_pag","Cod. Compra",$_POST['cod_pag'],"1","11","numericos");
$datos[2] = crear_datos ("cod_par","Cod. Partidas",$_POST['cod_par'],"1","11","numericos");
$datos[3] = crear_datos ("isrl_pro_pag","Aplica retencion ISRL",$_POST['isrl_pro_pag'],"0","2","alfabeticos");
$datos[4] = crear_datos ("mon_pro_pag","Monto del compromiso por la Partida",$_POST['mon_pro_pag'],"1","12","decimal");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_pro_pag = $row["cod_pro_pag"];
	    $cod_pag = $row["cod_pag"];
	    $cod_par = $row["cod_par"];
	    $isrl_pro_pag = $row["isrl_pro_pag"];
	    $mon_pro_pag = $row["mon_pro_pag"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	// Validamos que el monto total de las partidas no exeda el monto total registrado para este pago
	if ($validacion) {
	    // Traemos el monto del pago
        $sql  = "SELECT mon_pag FROM pagos where cod_pag=".$viene_val;
	    $sql_buscar = mysql_query($sql);
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
	        $mon_pag = $resulta['mon_pag'];
	    }
	    // Consultamos el total de las partidas de este pago
	    $sql  = "SELECT sum(mon_pro_pag) as monto_pagado FROM partidas_pagos where cod_pag=".$viene_val;
	    $sql_buscar = mysql_query($sql);
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
	        $monto_pagado = $resulta[monto_pagado];
	        $disponible = $mon_pag - $monto_pagado;
	        $restar_actual=0;
	        if ($_POST["cod_par"]==$_POST["par_ant"])
	        {
	            $restar_actual = $_POST["mon_ant"]; 
	        }
	        if (($disponible+$restar_actual)<$_POST['mon_pro_pag'])
	        {
	            echo '<SCRIPT> alert ("'.$msg_pago_part_mayor.' ('.redondear($mon_pag,2,".",",").' Bs.)"); </SCRIPT>';
	            $validacion="";
	        }
	    }
	}

    // validación de montos comprometidos
	if ($validacion) {
	    $sql_part  ="Select sum(partidas_compras.mon_pro_com) as mon_pro_com from compras,partidas_compras where compras.frm_com=".$viene_val2;
	    $sql_part .=" AND compras.cod_com=partidas_compras.cod_com AND partidas_compras.cod_par=".$datos[2][2]." GROUP BY cod_par";
	    $sql_buscar = mysql_query($sql_part);
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
	        $monto_compromiso = $resulta[mon_pro_com];
	        if ($resulta[mon_pro_com]<$datos[4][2])
	        {
	            echo '<SCRIPT> alert ("'.$msg_pago_part_mayor_compromiso.' ('.redondear($resulta[mon_pro_com],2,".",",").' Bs.)"); </SCRIPT>';
	            $validacion="";
	        }
	    }
	}
	// validación de montos en ordenes de pago
	if ($validacion) {
	    $sql_part  ="Select sum(partidas_pagos.mon_pro_pag) as mon_pro_pag from pagos,partidas_pagos where pagos.frm_com=".$viene_val2;
	    $sql_part .=" AND pagos.cod_pag=partidas_pagos.cod_pag AND partidas_pagos.cod_par=".$datos[2][2]." GROUP BY cod_par";
	    $sql_buscar = mysql_query($sql_part);
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
    	    $restar_actual=0;
	        if ($_POST["cod_par"]==$_POST["par_ant"])
	        {
	            $restar_actual = $_POST["mon_ant"]; 
	        }
	        $monto_comparar=($resulta[mon_pro_pag]-$restar_actual)+$datos[4][2];
	        $monto_pagado = $resulta[mon_pro_pag]-$restar_actual;
	        if ($monto_compromiso<$monto_comparar)
	        {
	            $restante = $monto_compromiso - $monto_pagado;
	            $restante = redondear($restante,2,".",",");
	            echo '<SCRIPT> alert ("'.$msg_pago_part_mayor_totpag.$restante.'"); </SCRIPT>';
	            $validacion="";
	        }
	    }
	}
	// validación de montos en ordenes de pago
	if ($validacion) {
	    $sql_part  ="Select sum(partidas_pagos.mon_pro_pag) as mon_pro_pag from pagos,partidas_pagos where pagos.frm_com=".$viene_val2;
	    $sql_part .=" AND pagos.cod_pag=partidas_pagos.cod_pag AND partidas_pagos.cod_par=".$datos[2][2]." GROUP BY cod_par";
	    $sql_buscar = mysql_query($sql_part);
	    $restar_actual=0;
	    if ($_POST["cod_par"]==$_POST["par_ant"])
	    {
	        $restar_actual = $_POST["mon_ant"]; 
	    }
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
	        $monto_comparar=$resulta[mon_pro_pag]-$restar_actual+$datos[4][2];
	        if ($monto_compromiso<$monto_comparar)
	        {
	            echo '<SCRIPT> alert ("'.$msg_partidas_mayor.'"); </SCRIPT>';
	            $validacion="";
	        }
	    }
	}
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_pro_pag",$_POST["cod_pro_pag"],$pagina2);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	// Validamos que el monto total de las partidas no exeda el monto total registrado para este pago
	if ($validacion) {
	    // Traemos el monto del pago
        $sql  = "SELECT mon_pag FROM pagos where cod_pag=".$viene_val;
	    $sql_buscar = mysql_query($sql);
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
	        $mon_pag = $resulta['mon_pag'];
	    }
	    // Consultamos el total de las partidas de este pago
	    $sql  = "SELECT sum(mon_pro_pag) as monto_pagado FROM partidas_pagos where cod_pag=".$viene_val;
	    $sql_buscar = mysql_query($sql);
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
	        $monto_pagado = $resulta[monto_pagado];
	        $disponible = $mon_pag - $monto_pagado;
		   $restando = redondear($disponible-$_POST['mon_pro_pag'],2,'','.'); 
	        if ($restando<0)
	        {
	            echo '<SCRIPT> alert ("'.$msg_pago_part_mayor.' ('.redondear($mon_pag,2,".",",").' Bs.)"); </SCRIPT>';
	            $validacion="";
	        }
	    }
	}
    // validación de montos comprometidos
	if ($validacion) {
	    $sql_part  ="Select sum(partidas_compras.mon_pro_com) as mon_pro_com from compras,partidas_compras where compras.frm_com=".$viene_val2;
	    $sql_part .=" AND compras.cod_com=partidas_compras.cod_com AND partidas_compras.cod_par=".$datos[2][2]." GROUP BY cod_par";
	    $sql_buscar = mysql_query($sql_part);
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
	        $monto_compromiso = $resulta[mon_pro_com];
	        if ($resulta[mon_pro_com]<$datos[4][2])
	        {
	            echo '<SCRIPT> alert ("'.$msg_pago_part_mayor_compromiso.' ('.redondear($resulta[mon_pro_com],2,".",",").' Bs.)"); </SCRIPT>';
	            $validacion="";
	        }
	    }
	}
	// validación de montos en ordenes de pago
	if ($validacion) {
	    $sql_part  ="Select sum(partidas_pagos.mon_pro_pag) as mon_pro_pag from pagos,partidas_pagos where pagos.frm_com=".$viene_val2;
	    $sql_part .=" AND pagos.cod_pag=partidas_pagos.cod_pag AND partidas_pagos.cod_par=".$datos[2][2]." GROUP BY cod_par";
	    $sql_buscar = mysql_query($sql_part);
	    if ($resulta=mysql_fetch_array($sql_buscar)) {
	        $monto_comparar=$resulta[mon_pro_pag]+$datos[4][2];
	        $monto_pagado = $resulta[mon_pro_pag];
	        if ($monto_compromiso<$monto_comparar)
	        {
	            $restante = $monto_compromiso - $monto_pagado;
	            $restante = redondear($restante,2,".",",");
	            echo '<SCRIPT> alert ("'.$msg_pago_part_mayor_totpag.$restante.'"); </SCRIPT>';
	            $validacion="";
	        }
	    }
	}
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_pro_pag"],"cod_pro_pag",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_pro_pag","partidas_pagos",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Codificaci&oacute;n Presupuestaria de la Orden Pago <?php echo $_GET['frm_pag'] ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Partida:
                          </td>
		            <td><input name="cod_pro_pag" type="hidden" id="cod_pro_pag" value="<?php if (! $existe) { echo $_POST['cod_pro_pag']; } else { echo $cod_pro_pag; } ?>" title="Codigo de salida presupuestaria">
			<input name="cod_pag" type="hidden" id="cod_pag" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_pag; } ?>" title="Codigo del Egreso">
                        <?php combo('cod_par', $cod_par, 'vista_partidas_comprometidas', $link, 0, 5, 6, 9, 'cod_par', "", $boton, "WHERE frm_com=".$viene_val2." ORDER BY par_par,gen_par,esp_par,sub_par",''); ?><input type="hidden" name="par_ant" id="par_ant" value="<?php if ($existe) { echo $cod_par;} else { echo $_POST["par_ant"]; }?>"></td>
                      </tr>
                       <tr>
                        <td width="25%" class="etiquetas">Monto:</td>
                        <td width="75%">
                        <input name="mon_pro_pag" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_pro_pag" value="<?php if(! $existe) { echo $_POST['mon_pro_pag']; } else { echo $mon_pro_pag; } ?>" size="10" maxlength="12" title="Monto del Compromiso de Salida Partida">
                        <?php if ($boton=='Modificar') { echo $mon_pro_pag; } ?>
                        <input type="hidden" name="mon_ant" id="mon_ant" value="<?php if ($existe) { echo $mon_pro_pag;} else { echo $_POST["mon_ant"]; }?>"></td>
                      </tr>
		      <tr>
                        <td width="25%" class="etiquetas">Retenci&oacute;n ISRL?: </td>
			<td valign="top"> <?php if ($boton!="Modificar") { ?>
                  	<input name="isrl_pro_pag" type="radio" value="SI" <?php if (($_POST["isrl_pro_pag"]=='SI')||($isrl_pro_pag=='SI')) {echo 'checked'; }?>>SI
                  	<input name="isrl_pro_pag" type="radio" value="" <?php if (($_POST["isrl_pro_pag"]=='')||($isrl_pro_pag=='')) {echo 'checked'; }?>>NO<?php } else { echo '<input name="isrl_pro_pag" id="isrl_pro_pag" type="hidden" value="'.$isrl_pro_pag.'">'; if (!$isrl_pro_pag) { echo 'NO'; } else { echo 'SI'; }  }?>
              		</td> 
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_pagos_partidas.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
