<?php include('../comunes/conexion_basedatos.php'); 
include ('../comunes/formularios_funciones.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); ?>
<?php include('../comunes/numerosaletras.php'); ?>

<?php $idprint=$_GET['cod_med']; 
	//consultamos los datos de la permisos
    $result=mysql_query("select * from medicinas md WHERE md.cod_med='$idprint'");
	if ($row=mysql_fetch_array($result))
	{
		$existe = 'SI';  	
		$dia_reg = substr($row["fch_med"], 8, 2);
		$mes_reg = substr($row["fch_med"], 5, 2);
		$ano_reg = substr($row["fch_med"], 0, 4);
		$fecha_reg = "Ejido, ".$dia_reg." de ".convertir_mes($mes_reg)." de ".$ano_reg;
		
    	$dia_pag = substr($row["fch_pag_med"], 8, 2);
		$mes_pag = substr($row["fch_pag_med"], 5, 2);
		$ano_pag = substr($row["fch_pag_med"], 0, 4);
		$fecha_pag = $dia_pag." de ".convertir_mes($mes_pag)." de ".$ano_pag;
		$obs_med = $row["obs_med"];
		$i=0;
		$mnt_total = 0;
	    $result_per=mysql_query("select mp.*,vp.*,ct.*,tc.abr_tcar from medicinas_per mp, vista_personal vp, cuentas ct, tipos_cargos tc, cargos cg WHERE mp.cod_med='$idprint' AND mp.ced_per=vp.ced_per AND ct.ced_per=mp.ced_per AND tc.cod_tcar=cg.cod_tcar AND cg.ced_per=mp.ced_per ORDER BY tc.cod_tcar,mp.ced_per");
    	while ($row_per=mysql_fetch_array($result_per)){
    	    $ced_per[$i] = $row_per['ced_per'];
    	    $nombre[$i] = $row_per['nombre'];
    	    $mnt_med_per[$i] = $row_per['mnt_med_per'];
    	    $num_cue[$i] = $row_per['num_cue'];
    	    $abr_tcar[$i] = $row_per['abr_tcar'];
    	    $i++;
    	    $mnt_total += $row_per['mnt_med_per'];
    	}
    	$obs_partidas  = "Montos por partida: ";
     	$sql_mnt_tcar ="select tc.abr_tcar, tc.nom_tcar, sum(mp.mnt_med_per) as mnt_tip from tipos_cargos tc, cargos cg, medicinas_per mp where tc.cod_tcar=cg.cod_tcar AND cg.ced_per=mp.ced_per AND mp.cod_med = '$idprint' GROUP BY tc.cod_tcar";
    	$result_mnt_tcar = mysql_query($sql_mnt_tcar);
    	while ($row_mnt_tcar = mysql_fetch_array($result_mnt_tcar)) {
            $obs_partidas .= "<u>".$row_mnt_tcar['nom_tcar']." (".$row_mnt_tcar['abr_tcar'].")</u>: ".redondear($row_mnt_tcar['mnt_tip'],2,".",",")."&nbsp;&nbsp;&nbsp;";
    	}
	}
	mysql_free_result($result);
?>
<title>Impresi�n Pago de Medicinas</title>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<div><?php include ('../comunes/pagina_encabezado.php'); ?></div>
<table align="center" width="85%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="0" class="detallespago">
    <tr>
        <td align="right">
            <br><?php echo $fecha_reg; ?>
        </td>
    <tr>
    <tr>
        <td align="center">
            <H2>LISTIN DE PAGO</H2>
        </td>
    <tr>
    <tr>
        <td align="center" style="text-align:justify">
        CORRESPONDIENTE AL PAGO DE AYUDAS PARA GASTOS MEDICOS, DE LABORATORIO Y DE FARMACIA A LOS FUNCIONARIOS DE LA CONTRALOR�A MUNICIPAL CAMPO ELIAS. SEG�N RESOLUCION N� 0175-2012 NUMERAL 11, APROBADA POR LA CONTRALORA MUNICIPAL DE FECHA 17 DE AGOSTO DE 2012.<br><br>
LA CANTIDAD DE <b><?php $num_letras = convertir_a_letras($mnt_total,'mayusculas');  echo $num_letras; ?> (Bs. <?php echo redondear($mnt_total,2,".",","); ?>)</b> SER� DEBITADA DE LA CUENTA CORRIENTE N� 0102-0441-17-0000104281 DEL BANCO DE VENEZUELA A NOMBRE DE CONTRALOR�A MUNICIPAL CAMPO EL�AS Y ACREDITADA A TRAV�S DE TRANSFERENCIA POR INTERNET A LAS SIGUIENTES CUENTAS:<br><br>
        </td>
    <tr>
</table>
<table align="center" width="85%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000" class="detallespago">
    <tr align="center"><td><b>N�</b></td><td><b>Nombre y Apellido</b></td><td><b>C�dula</b></td><td><b>Cuenta N�</b></td><td><b>Monto</b></td><td><b>Firma</b></td></tr>
<?php for ($j=0;$j<$i;$j++){ ?>
     <tr height="20px">
        <td align="right">
            <?php echo ($j+1); ?>&nbsp;
        </td>
        <td align="left">
            &nbsp;<?php echo $nombre[$j]." (".$abr_tcar[$j].")"; ?>
        </td>
        <td align="right">
            <?php echo redondear($ced_per[$j],0,".",","); ?>&nbsp;
        </td>
        <td align="center">
            <?php echo $num_cue[$j]; ?>
        </td>
        <td align="right">
            <?php echo redondear($mnt_med_per[$j],2,".",","); ?>&nbsp;
        </td>
        <td align="right" width="150px">
            &nbsp;
        </td>
     </tr>
<?php }?>
     <tr class="tabla_total">
        <td align="right" colspan="4"><font size="-1">
            TOTAL DEL PAGO DE MEDICINAS:&nbsp;</font>
        </td>
        <td align="right"><font size="-1"> 
            <?php echo redondear($mnt_total,2,".",","); ?>&nbsp;</font>
        </td>
        <td align="right" width="150px">
            &nbsp;
        </td>
     </tr>
</table>
<br>
<table align="center" width="85%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000"  class="detallespago">
    <tr>
        <td width="50%" height="30" valign="top">
            &nbsp;<b>Observaciones: </b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $obs_partidas; ?><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $obs_med; ?>
        </td>
    </tr>
</table>
<?php echo $msg_pie_reporte; ?>
<div><input type="button" name="bt_print" value="Imprimir Solicitud" id="bt_print" onclick="this.style.visibility='hidden'; window.print();"></div>
