<?php 
    /// Definici�n del ancho del reporte
    $ancho_rep="700px";
    $ancho_rep_imp="500px";
    include('../comunes/conexion_basedatos.php'); 
    include ('../comunes/formularios_funciones.php'); 
    include ('../comunes/titulos.php'); 
    include ('../comunes/mensajes.php'); 
    include('../comunes/numerosaletras.php'); 
    $viene_val = $_GET['cod_pag'];
    //// obteniendo los datos
    $sql="SELECT * FROM compras, proveedores, pagos WHERE cod_pag=".$viene_val." AND pagos.frm_com=compras.frm_com AND compras.rif_pro=proveedores.rif_pro";
    $reg = mysql_fetch_array(mysql_query($sql));
    $cod_com = $reg['cod_com'];
    $fch_com = $reg['fch_com'];
    $frm_com = $reg['frm_com'];
    $fre_com = $reg['fre_com'];
    $rif_pro = $reg['rif_pro'];
    $ela_pag = $reg['ela_pag'];
    $rev_pag = $reg['rev_pag'];
    $apr_pag = $reg['apr_pag'];
    $obs_pag = $reg['obs_pag'];
    $afavor = $reg['nom_pro'];
    $monto_pago = $reg['mon_pag'];
    $monto_pago_letras = convertir_a_letras($monto_pago);
    $monto_pago_letras = ucwords(strtolower($monto_pago_letras));
    //// Manipulando un poco los datos para presentarlos
    $fecha1 = substr($reg["fch_pag"], 8, 2);
	$fecha2 = substr($reg["fch_pag"], 5, 2);
	$fecha3 = substr($reg["fch_pag"], 0, 4);
	$fecha3b = substr($reg["fch_pag"], 2, 2);
    $fecha = $fecha1.'/'.$fecha2.'/'.$fecha3;
    $fecha_presup = substr($reg["fch_com"], 0, 4);
	$norden=$fecha2.$fecha3b.$reg[nor_pag];
	
	/// Traemos los datos de las partidas del pago
	$part = 0;
	$total_part = 0;
	$sql = "SELECT pp.*,pg.mon_pro_pag FROM partidas_pagos pg, part_presup pp WHERE cod_pag=".$cod_pag." AND pg.cod_par=pp.cod_par";
	$res = mysql_query($sql);
	while ($reg = mysql_fetch_array ($res))
	{
        $datos_part[$part][0] = $reg[0];
        $datos_part[$part][1] = $reg[1];
        $datos_part[$part][2] = $reg[2];
        $datos_part[$part][3] = $reg[3];
        $datos_part[$part][4] = $reg[4];
        $datos_part[$part][5] = $reg[5];
        $datos_part[$part][6] = $reg[6];
        $datos_part[$part][7] = $reg[7];
        $datos_part[$part][8] = $reg[8];
        $datos_part[$part][9] = $reg[9];
        $datos_part[$part][10] = $reg[10];
        $datos_part[$part][11] = $reg[11];
        $part=$part+1;
        $total_part += $reg[11];
	}
?>
<html>
    <head>
        <title>Imprimir Orden de Pago</title>
        <style type="text/css">
        <!--
            body,td,th {
            	font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 12px;
            }
            .resp_datos {	
                font-size: 15px;
                font-weight: bold;
            }
            .montos_totales {	
                font-size: 14px;
                font-weight: bold;
            }
            #formulario {	
                color: RED;
                font-size: 25px;
                font-weight: bold;
            }
            #control {	
                font-size: 15px;
                font-weight: bold;
            }
            #fecha {	
                font-size: 15px;
                font-weight: bold;
            }
            #titulo {	
                font-size: 18px;
                font-weight: bold;
            }
            #titulos_seq {	
                font-size: 12px;
                font-weight: bold;
            }
            #total_pago {	
                font-size: 20px;
                font-weight: bold;
                color: #FF0000;
            }
            .titulos_cel {	
                font-size: 9px;
                font-weight: bold;
            }
            .datos_cel {	
                font-size: 9px;
            }
            .tabla_borde 
            {
                border:solid 1px #000000;
            }
            #printing {	
            	position:absolute;
            	z-index:1;
            	align: right;
            	top: 10px;
                border: 0px solid #000000;
                width: <?php echo $ancho_rep_imp; ?>;
                text-align: right;
            } 
            #pie_merintec {	
                font-size: 6pt;
            }
}
        -->
        </style>
    </head>
    <body>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr>
                <td width="1px"><img src="../imagenes/logo_tn_trn.png" alt="Logo <?php echo $organizacion;  ?>" height="72px" title="Logo <?php echo $organizacion;  ?>"/></td>
                <td width="260px" align="center">REP�BLICA BOLIVARIANA DE VENEZUELA<BR>CONTRALOR�A DEL MUNICIPIO JAUREGUI<BR>LA GRITA - ESTADO T�CHIRA<BR>RIF: <?php echo $organizacion_rif; ?></td>
                <td width="1px"><img src="../imagenes/logo_contraloria.png" alt="Logo <?php echo $organizacion;  ?>" height="72px" title="Logo <?php echo $organizacion;  ?>"/></td>
                <td align="right">
                    <span id="formulario">N� <?php echo $frm_com; ?></span><br>
                    N� de Control: <span id="control"><u><?php echo $norden; ?></u></span><br>
                    Fecha: <span id="fecha"><u><?php echo $fecha; ?></u></span><br>
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulo">
                    ORDEN DE PAGO
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr>
                <td width="500px">
                    A FAVOR DE:
                </td>
                <td colspan="2">
                    RIF: <span class="resp_datos"><?php echo $rif_pro; ?></span>
                </td>
            </tr>
            <tr>
                <td rowspan="2" align="center">
                    <span class="resp_datos"><?php echo $afavor; ?></span>
                </td>
                <td>
                    Compromiso:
                </td>
                <td>
                    Presupuesto:
                </td>
            </tr>
            <tr>
                <td align="center">
                    <span class="resp_datos"><?php echo $frm_com; ?></span>
                </td>
                <td align="center">
                    <span class="resp_datos"><?php echo $fecha_presup; ?></span>
                </td>
            </tr>
            <tr>
                <td>
                    Son:
                </td>
                <td colspan="2">
                    Monto del Pago:
                </td>
            </tr>
            <tr height="50px">
                <td align="center">
                    <span class="resp_datos"><?php echo $monto_pago_letras; ?></span>
                </td>
                <td align="right" colspan="2">
                    <span id="total_pago">Bs. <?php echo redondear($monto_pago,2,".",","); ?></span>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Concepto:
                </td>
            </tr>
            <tr height="50px">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="resp_datos"><?php echo $fre_com; ?></span>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Observaciones:
                </td>
            </tr>
            <tr  height="50px">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="resp_datos"><?php echo $obs_pag; ?></span>
                </td>
            </tr>
        </table>

        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulos_seq">
                    <BR>CONTROL PRESUPUESTARIO
                </td>
            </tr>
        </table>
         <table width="<?php echo $ancho_rep; ?>" height="200px" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000">
            <tr align="center" height="1">
                <td width="50px" class="titulos_cel">SEC.</td>
                <td width="50px"class="titulos_cel">PROG.</td>
                <td width="50px" class="titulos_cel">PRO</td>
                <td width="50px" class="titulos_cel">ACT.</td>
                <td width="50px"  class="titulos_cel">PAR.</td>
                <td width="50px" class="titulos_cel">GEN.</td>                
                <td width="50px" class="titulos_cel">ESP.</td>                
                <td width="50px" class="titulos_cel">SUB-ESP.</td>
                <td class="titulos_cel">MONTO</td>
            </tr>
            <?php
                for ($i=0;$i<$part;$i++)
                {
                    echo '<tr align="center" height="1" title="'.$datos_part[$i][9].'">
                    <td class="datos_cel">&nbsp;</td>
                    <td class="datos_cel">&nbsp;</td>
                    <td class="datos_cel">&nbsp;</td>
                    <td class="datos_cel">&nbsp;</td>
                    <td class="datos_cel">'.$datos_part[$i][5].'&nbsp;</td>
                    <td class="datos_cel">'.$datos_part[$i][6].'&nbsp;</td>
                    <td class="datos_cel">'.$datos_part[$i][7].'</td>
                    <td class="datos_cel">'.$datos_part[$i][8].'</td>
                    <td class="datos_cel" align="right">'.redondear($datos_part[$i][11],2,".",",").'&nbsp;</td>
                    </tr>'; 
                }
            ?>
            <tr align="center">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr align="center" height="1">
                <td COLSPAN="8" align="right" class="montos_totales">TOTAL EN PARTIDAS&nbsp;</td>
                <td align="right" class="montos_totales"><?php echo redondear($total_part,2,".",","); ?>&nbsp;</td>
            </tr>
        </table>
        <br>       
         <table width="<?php echo $ancho_rep; ?>" height="130px" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000">
            <tr align="center" height="1">
                <td id="titulos_seq" align="left" width="33.33%">
                    &nbsp;Elaborado: <?php echo $ela_pag; ?>
                </td>
                <td id="titulos_seq" align="left" width="33.33%">
                    &nbsp;Revisado: <?php echo $rev_pag; ?>
                </td>
                <td id="titulos_seq" align="left" width="33.33%">
                    &nbsp;Aprobado: <?php echo $apr_pag; ?>
                </td>
            </tr>
            <tr align="center">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr align="center" height="1">
                <td id="titulos_seq" align="left">
                    &nbsp;Fecha:
                </td>
                <td id="titulos_seq" align="left">
                    &nbsp;Fecha:
                </td>
                <td id="titulos_seq" align="left">
                    &nbsp;Fecha:
                </td>
            </tr>
        </table>    
        <br>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulos_seq">
                    RECIBIDO POR EL BENEFICIARIO
                </td>
            </tr>
        </table>          
        <table width="<?php echo $ancho_rep; ?>" height="110px" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000">
            <tr align="center" height="1" align="center">
                <td id="titulos_seq" width="28%">
                    Nombre y Apellido: 
                </td>
                <td id="titulos_seq" width="20%">
                    C.I. N�: 
                </td>
                <td id="titulos_seq" width="36%">
                    Firma y Sello: 
                </td>
                <td id="titulos_seq">
                    Fecha y Hora: 
                </td>
            </tr>
            <tr align="center">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td>
                    <?php echo $pie_ord; ?>
                </td>
            </tr>
        </table>
    </body>
</html>
<div id="printing">
<input type="image" src="../imagenes/imprimir.png" id="imprimir" name="imprimir" value="imprimir" title="<?php echo $msg_pago_imprimir; ?>" onclick="this.style.visibility='hidden'; window.print();"></div>
