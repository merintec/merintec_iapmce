<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$post_ftr_ofi = $_POST["ftr_ofi"];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'oficios_enviados.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "oficios_enviados";	// nombre de la tabla
$ncampos = "16";		//numero de campos del formulario
$datos[0] = crear_datos ("num_ofi","N�mero de oficio",$_POST['num_ofi'],"0","4","numericos");
$datos[1] = crear_datos ("tip_ofi","Tipo de oficio",$_POST['tip_ofi'],"1","10","alfanumericos");
$datos[2] = crear_datos ("fch_ofi","Fecha de Registro",$_POST['fch_ofi'],"1","10","fecha");
$datos[3] = crear_datos ("cod_dep","Departamento Remitente",$_POST['cod_dep'],"1","5","alfanumericos");
$datos[4] = crear_datos ("rdp_ofi","Departamento Remitente",$_POST['rdp_ofi'],"1","75","alfanumericos");
$datos[5] = crear_datos ("rpr_ofi","Persona Remitente",$_POST['rpr_ofi'],"1","75","alfabeticos");
$datos[6] = crear_datos ("ddp_ofi","Departamento Destino",$_POST['ddp_ofi'],"1","75","alfanumericos");
$datos[7] = crear_datos ("dpr_ofi","Persona Destino",$_POST['dpr_ofi'],"0","75","alfabeticos");
$datos[8] = crear_datos ("des_ofi","Descripci�n del oficio",$_POST['des_ofi'],"1","255","alfanumericos");
$datos[9] = crear_datos ("nds_ofi","N�mero de D�as para Respuesta",$_POST['nds_ofi'],"0","2","numericos");
$datos[10] = crear_datos ("fen_ofi","Fecha de Entrega",$_POST['fen_ofi'],"0","10","fecha");
$datos[11] = crear_datos ("ftr_ofi","Fecha tope para Respuesta",$_POST['ftr_ofi'],"0","10","fecha");
$datos[12] = crear_datos ("frs_ofi","Fecha de Respuesta",$_POST['frs_ofi'],"0","10","fecha");
$datos[13] = crear_datos ("ref_ofi","Referencia",$_POST['ref_ofi'],"0","7","alfanumericos");
$datos[14] = crear_datos ("obs_ofi","Observaci�n",$_POST['obs_ofi'],"0","255","alfanumericos");
$datos[15] = crear_datos ("est_ofi","Estado del Oficio",$_POST['est_ofi'],"0","1","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) 
	{ 
	    $tipo = "general";
	    $criterio_buscar = $_POST["criterio"];
	    $valor_buscar = $_POST["buscar_a"];
	    if ($criterio_buscar == "num_ofi") {
		$tipo = "individual";
		$valor_buscar = $_POST["buscar_a"][0].$_POST["buscar_a"][1].$_POST["buscar_a"][2].$_POST["buscar_a"][3];
		$valor_buscar .= "' AND YEAR(fch_ofi) = '";
		$valor_buscar .= $_POST["buscar_a"][5].$_POST["buscar_a"][6].$_POST["buscar_a"][7].$_POST["buscar_a"][8];
	    }
	    if ($criterio_buscar == "mes_ofi") {
		$tipo = "individual";
	    $criterio_buscar = "MONTH(fch_ofi)";
		$valor_buscar = $_POST["buscar_a"][5].$_POST["buscar_a"][6];
		$valor_buscar .= "' AND YEAR(fch_ofi) = '";
		$valor_buscar .= $_POST["buscar_a"][0].$_POST["buscar_a"][1].$_POST["buscar_a"][2].$_POST["buscar_a"][3];
	    }
	    if ($criterio_buscar == "ano_ofi") {
		$tipo = "individual";
	        $criterio_buscar = "YEAR(fch_ofi)";
		$valor_buscar = $_POST["buscar_a"][0].$_POST["buscar_a"][1].$_POST["buscar_a"][2].$_POST["buscar_a"][3];
	    }
	    if ($criterio_buscar == "Por Entregar") {
		$tipo = "individual";
		$criterio_buscar = "fen_ofi";
		$valor_buscar = "0000-00-00";
	    }
	    if ($criterio_buscar == "CE") {
		 $tipo = "individual";
		$criterio_buscar = "tip_ofi";
		$valor_buscar = "CE";
	    }
        if ($criterio_buscar == "CI") {
		$tipo = "individual";
		$criterio_buscar = "tip_ofi";
		$valor_buscar = "CI";
	    }
	    if ($_COOKIE['uspriv']!="2" && $_COOKIE['uscod']!="8029735"){
	        if ($tipo=="individual"){
	            $valor_buscar .= "' AND cod_dep = '".$_POST['cod_dep'];
	        }
            if ($tipo=="general"){
	            $valor_buscar .= "%' AND cod_dep LIKE '%".$_POST['cod_dep'];
	        }
	     	if ($_POST["buscar_a"]=="Todos"||$_POST["buscar_a"]=="todos"||$_POST["buscar_a"]=="TODOS") {
	            $tabla .= " WHERE cod_dep = '".$_POST['cod_dep']."'";
	            $valor_buscar = "Todos";
	        }   
	    }
	    if ($_COOKIE['uspriv']<="2"){
	     	if ($_POST["buscar_a"]=="Todos"||$_POST["buscar_a"]=="todos"||$_POST["buscar_a"]=="TODOS") {
	            $valor_buscar = "todos";
	        }   
	    }
	    $buscando = busqueda_func($valor_buscar,$criterio_buscar,$tabla,$pagina,$tipo);
	}
	elseif ($_POST["BuscarInd"]) { 
    	$tipo = "individual"; 
    	$buscando = busqueda_func($_POST["buscar_a"],"cod_ofi","$tabla",$pagina,$tipo);
	}
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Remitente";
		$datos[0]="rdp_ofi";	
		$parametro[1]="Dep. Destino";
		$datos[1]="ddp_ofi";	
		$parametro[2]="Per. Destino";
		$datos[2]="dpr_ofi";
		$parametro[3]="Descripci�n";
		$datos[3]="des_ofi";
		$parametro[4]="Fecha Tope";
		$datos[4]="ftr_ofi";
		busqueda_varios(7,$buscando,$datos,$parametro,"cod_ofi");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_ofi = $row["cod_ofi"];
	    $tip_ofi = $row["tip_ofi"];
	    $num_ofi = $row["num_ofi"];
	    $fch_ofi = $row["fch_ofi"];
	    $rdp_ofi = $row["rdp_ofi"];
	    $cod_dep = $row["cod_dep"];
	    $sql3 = "SELECT * FROM dependencias WHERE nom_dep = '".$rdp_ofi."';";
	    $res = mysql_fetch_array(mysql_query($sql3));
	    $cod_dep = $res['cod_dep'];
	    $rpr_ofi = $row["rpr_ofi"];
	    $ddp_ofi = $row["ddp_ofi"];
	    $dpr_ofi = $row["dpr_ofi"];
	    $des_ofi = $row["des_ofi"];
	    $nds_ofi = $row["nds_ofi"];
	    $fen_ofi = $row["fen_ofi"];
	    $ftr_ofi = $row["ftr_ofi"];
	    $frs_ofi = $row["frs_ofi"];
	    $ref_ofi = $row["ref_ofi"];
	    $obs_ofi = $row["obs_ofi"];
	    $est_ofi = $row["est_ofi"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Fecha_tope") 
{
	$post_ftr_ofi = calculo_fecha ($_POST['fen_ofi'],"+",$_POST['nds_ofi']);
	if (! $_POST['fen_ofi']) { $post_ftr_ofi =""; }
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_ofi",$_POST["cod_ofi"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$sql3 = "SELECT * FROM dependencias WHERE nom_dep = '".$_POST['rdp_ofi']."';";
	$res = mysql_fetch_array(mysql_query($sql3));
	$cod_dep = $res['cod_dep'];
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{  
    if ($_POST["tip_ofi"]=='CE'){
        $sql = "SELECT MAX(num_ofi) AS num_ofi FROM oficios_enviados WHERE tip_ofi='CE' AND YEAR(fch_ofi)=";
	    $sql .= $_POST["fch_ofi"][0].$_POST["fch_ofi"][1].$_POST["fch_ofi"][2].$_POST["fch_ofi"][3];
    }
    if ($_POST["tip_ofi"]=='CI'){
        $sql = "SELECT MAX(num_ofi) AS num_ofi FROM oficios_enviados WHERE tip_ofi='CI' AND rdp_ofi='".$_POST["rdp_ofi"]."' AND YEAR(fch_ofi)=";
	    $sql .= $_POST["fch_ofi"][0].$_POST["fch_ofi"][1].$_POST["fch_ofi"][2].$_POST["fch_ofi"][3];
    }
    	$res = mysql_fetch_array(mysql_query($sql));
    	$num_ofi = $res['num_ofi'] + 1;
	$datos[0] = crear_datos ("num_ofi","N�mero de oficio",$num_ofi,"0","4","numericos");
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_ofi"],"cod_ofi",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_ofi","oficios_enviados",$pagina);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos del Oficio</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">N� de Oficio:</td>
                        <td width="75%">
			<input name="cod_ofi" type="hidden" id="cod_ofi" value="<?php if(! $existe) { echo $_POST['cod_ofi']; } else { echo $cod_ofi; } ?>" size="35" title="Codigo del Oficio">
			<input name="num_ofi" type="hidden" id="num_ofi" value="<?php if(! $existe) { echo $_POST['num_ofi']; } else { echo $num_ofi; } ?>" size="35" title="N� del Oficio"><?php if ($boton != "Verificar" && $boton != "Guardar") { echo 'CMCE/'.$tip_ofi; if ($tip_ofi=='CI') { echo '/'.$cod_dep; } echo '-'.$num_ofi.'-'; echo $fch_ofi[0].$fch_ofi[1].$fch_ofi[2].$fch_ofi[3]; } else echo 'Por Asignar...'; ?>
                        </td>
                      </tr>
             <tr>
                        <td class="etiquetas">Tipo:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tip_ofi" title="Tipo de Correspondencia">
                          <option>Seleccione...</option>
                          <option value="CI" '; if ($tip_ofi == "CI" || $_POST['tip_ofi'] =="CI") { echo 'selected'; } echo '>Correspondencia Interna (CI)</option>
                          <option value="CE" '; if ($tip_ofi == "CE" || $_POST['tip_ofi'] =="CE") { echo 'selected'; } echo '>Correspondencia Externa (CE)</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tip_ofi" id="tip_ofi" value="'.$tip_ofi.'" >'; 
						    if ($tip_ofi == "CI") { echo 'Correspondencia Interna (CI)'; } 

							if ($tip_ofi == "CE") { echo 'Correspondecia Externa (CE)'; }
						}?></td>
			</tr>
		      <tr>
                        <td class="etiquetas">Fecha: </td>
                        <td><input name="fch_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_ofi" readonly value="<?php if(! $existe) { if (! $_POST['fch_ofi']) { echo date('Y-m-d'); } else { echo $_POST['fch_ofi']; } } else { echo $fch_ofi; } ?>" size="20" title="Fecha de Registro" />
                          <?php if ($boton=='Modificar') { echo $fch_ofi; } ?>
			</td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Dep. Remitente:</td>
                        <td width="75%">
			<?php if ($rdp_ofi == "" && $_POST["rdp_ofi"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$rdp_ofi = $row2['nom_dep'];
			$cod_dep = $row2['cod_dep'];						
			$rpr_ofi = $_COOKIE['usnombre'];
			} ?>
			<input name="cod_dep" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="cod_ofi" readonly value="<?php echo $cod_dep; ?>" size="35" title="Departamento Remitente">
			<input name="rdp_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="rdp_ofi" readonly value="<?php echo $rdp_ofi; ?>" size="35" title="Departamento Remitente">
                        <?php if ($boton=='Modificar') { echo $rdp_ofi; } ?></td>
                      </tr>
               
		      <tr>


		      <tr>
                        <td class="etiquetas">Per. Remitente:</td>
                        <td width="75%">
			<?php if ($rpr_ofi == "" && $_POST["rpr_ofi"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$rpr_ofi = $row2['nom_dep'];			
			$rpr_ofi = $_COOKIE['usnombre'];
			} ?>
			<input name="rpr_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="rpr_ofi" readonly value="<?php echo $rpr_ofi; ?>" size="35" title="Departamento Remitente">
                        <?php if ($boton=='Modificar') { echo $rpr_ofi; } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Dep. Destino:</td>
                        <td width="75%">
                        <input name="ddp_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ddp_ofi" value="<?php if(! $existe) { echo $_POST['ddp_ofi']; } else { echo $ddp_ofi; } ?>" size="35" title="Departamento Destino">
                        <?php if ($boton=='Modificar') { echo $ddp_ofi; } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Per. Destino:</td>
                        <td width="75%">
                        <input name="dpr_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="dpr_ofi" value="<?php if(! $existe) { echo $_POST['dpr_ofi']; } else { echo $dpr_ofi; } ?>" size="35" title="Persona Destino">
                        <?php if ($boton=='Modificar') { echo $dpr_ofi; } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Descripci�n:</td>
                        <td width="75%">
                        <input name="des_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="des_ofi" value="<?php if(! $existe) { echo $_POST['des_ofi']; } else { echo $des_ofi; } ?>" size="35" title="Descripci�n o motivo del Oficio">
                        <?php if ($boton=='Modificar') { echo $des_ofi; } ?></td>
                      </tr>
<tr>
                        <td class="etiquetas">Responder en:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="nds_ofi" title="D�as m�ximo para responder">
                          <option>Seleccione...</option>';
			for($i=0;$i<16;$i++){
			echo '<option value="'.$i.'"'; if ($nds_ofi == $i || $_POST['nds_ofi'] == $i) { echo 'selected'; } echo '>'.$i.'</option>';
			}
			echo '</select> d�as'; } 
						else 
						{ 
						    echo '<input type="hidden" name="nds_ofi" id="nds_ofi" value="'.$nds_ofi.'" > '.$nds_ofi.' d&iacute;as'; 
						}?></td>
                      </tr>
		      <?php if ($boton!="Verificar" && $boton!="Guardar") { ?>
		      <tr>
                        <td class="etiquetas">Fecha de Entrega: </td>
			<?php if ($fen_ofi == "0000-00-00") { $fen_ofi="";} ?>
                        <td><input name="fen_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fen_ofi" value="<?php if(! $existe) { echo $_POST['fen_ofi']; } else { echo $fen_ofi; } ?>" size="20" title="Fecha en que se Entrega a su Destino" onchange="confirmacion_func('Fecha_tope')";/>
                          <?php if ($boton=='Modificar') { echo $fen_ofi; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fen_ofi,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr> <?php } ?>
		      <?php if ($boton!="Verificar" && $boton!="Guardar") { ?>
		      <tr>
                        <td class="etiquetas">Fecha Tope: </td>
			<?php if ($ftr_ofi == "0000-00-00") { $ftr_ofi="";} ?>
                        <td><input name="ftr_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ftr_ofi" readonly value="<?php if(! $existe) { echo $post_ftr_ofi; } else { echo $ftr_ofi; } ?>" size="20" title="Fecha tope para respuesta" />
                          <?php if ($boton=='Modificar') { echo $ftr_ofi; } ?>
			</td>
                      </tr> <?php } ?>
		      <?php if ($boton!="Verificar" && $boton!="Guardar") { ?>
		      <tr>
                        <td class="etiquetas">Fecha de Respuesta: </td>
			<?php if ($frs_ofi == "0000-00-00") { $frs_ofi="";} ?>
                        <td><input name="frs_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="frs_ofi" value="<?php if(! $existe) { echo $_POST['frs_ofi']; } else { echo $frs_ofi; } ?>" size="20" title="Fecha en que se recibi&oacute; la respuesta" />
                          <?php if ($boton=='Modificar') { echo $frs_ofi; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].frs_ofi,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr> <?php } ?>
		      <tr>
                        <td class="etiquetas">Referencia:</td>
                        <td width="75%">
                        <input name="ref_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ref_ofi" value="<?php if(! $existe) { echo $_POST['ref_ofi']; } else { echo $ref_ofi; } ?>" size="35" title="Referencia a otro documento">
                        <?php if ($boton=='Modificar') { echo $ref_ofi; } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Observaci�n:</td>
                        <td width="75%">
                        <input name="obs_ofi" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="obs_ofi" value="<?php if(! $existe) { echo $_POST['obs_ofi']; } else { echo $obs_ofi; } ?>" size="35" title="Observaci�n">
                        <?php if ($boton=='Modificar') { echo $obs_ofi; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Estado:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="est_ofi" title="Estado">
                          <option>Seleccione...</option>
                          <option value="A" '; if ($est_ofi == "A" || $_POST['est_ofi'] =="A") { echo 'selected'; } echo '>Activo</option>
                          <option value="P" '; if ($est_ofi == "P" || $_POST['est_ofi'] =="P") { echo 'selected'; } echo '>Pasivo</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="est_ofi" id="est_ofi" value="'.$est_ofi.'" >'; 
						    if ($est_ofi == "A") { echo 'Activo'; } 
							if ($est_ofi == "F") { echo 'Pasivo'; }
						}?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php 
                            if ($rpr_ofi==$usnombre || $uscod==8029735){ 
                                include ('../comunes/botonera_usr.php'); 
                            }
                        ?>
                    </td>
                  </tr>
                  <tr>
                    <td>
			<?php 
			$ncriterios =9;
			$criterios[0] = "N� Oficio";
			$campos[0] = "num_ofi";
			$criterios[1] = "Dep. Destino";
			$campos[1] = "ddp_ofi";
			$criterios[2] = "Per. Destino"; 
			$campos[2] ="dpr_ofi";	
			$criterios[3] = "A�o-Mes"; 
			$campos[3] ="mes_ofi";	
			$criterios[4] = "A�o"; 
			$campos[4] ="ano_ofi";	
			$criterios[5] = "Referencia"; 
			$campos[5] ="ref_ofi";	
			$criterios[6] = "Por Entregar"; 
			$campos[6] ="Por Entregar";	
			$criterios[7] = "CI"; 
			$campos[7] ="CI";	
			$criterios[8] = "CE"; 
			$campos[8] ="CE";	
			if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {					
			crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
            <?php if ($uscod==8029735) { ?>      
                  <tr>
		    <td align="center">
			<input type="image" src="../imagenes/buscar.png" onclick=valor_acampo("Todos","buscar_a"),valor_acampo("BuscarInd","BuscarInd");  name="BuscarInd_btn" value="BuscarInd_btn" title="Ver Todos los Oficios Enviados"><br><font size="1"><b>Ver Todos</b></font>
		    </td>
		  </tr>
		  <?php } ?>
		  <tr>
		    <td>
			<?php include('capa_oficios_enviados.php'); ?>
		    </td>
		  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
