<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$sql="select * from valores";
$res = mysql_query($sql);
while ($row = mysql_fetch_array($res))
{
    $$row['des_val'] = $row['val_val'];
}
$boton = "Verificar";
$existe = '';
$pagina = 'medicinas.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "medicinas";	// nombre de la tabla
$ncampos = "5";		    //numero de campos del formulario
$datos[0] = crear_datos ("cod_med","Codigo de Pago de Medicinas",$_POST['cod_med'],"0","11","numericos");
$datos[1] = crear_datos ("fch_med","Fecha de Registro de Pago de Medicinas",$_POST['fch_med'],"1","10","fecha");
$datos[2] = crear_datos ("fch_pag_med","Fecha del Pago de Medicinas",$_POST['fch_pag_med'],"1","10","fecha");
$datos[3] = crear_datos ("mnt_med","Monto Anual",$_POST['mnt_med'],"1","11","decimal");
$datos[4] = crear_datos ("obs_med","Observación",$_POST['obs_med'],"0","255","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) 
	{ 
	    $tipo = "general";
	    $criterio_buscar = $_POST["criterio"];
	    $valor_buscar = $_POST["buscar_a"];
	    $buscando = busqueda_func($valor_buscar,$criterio_buscar,"$tabla",$pagina,$tipo);
	}
	elseif ($_POST["BuscarInd"]) { 
	$tipo = "individual"; 
	$buscando = busqueda_func($_POST["buscar_a"],"cod_med","$tabla",$pagina,$tipo);
	} 
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha de Registro";
		$datos[0]="fch_med";	
		$parametro[1]="Fecha del Pago";
		$datos[1]="fch_pag_med";	
		busqueda_varios(4,$buscando,$datos,$parametro,"cod_med");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_med = $row["cod_med"];
	    $fch_med = $row["fch_med"];
	    $fch_pag_med = $row["fch_pag_med"];
	    $mnt_med = $row["mnt_med"];
	    $obs_med = $row["obs_med"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_med",$_POST["cod_med"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{  
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_med"],"cod_med",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Pago de Medicinas</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		              <tr>
                        <td class="etiquetas">Fecha de Registro: </td>
                        <td>
                            <input name="cod_med" type="hidden" id="cod_med" value="<?php if(! $existe) { echo $_POST['cod_med']; } else { echo $cod_med; } ?>" size="35" title="Codigo de pago de medicinas">
                            <input name="fch_med" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_med"  value="<?php if(! $existe) { if (! $_POST['fch_med']) { echo date('Y-m-d'); } else { echo $_POST['fch_med']; } } else { echo $fch_med; } ?>" size="20" title="Fecha de Registro del Pago de Medicinas" />
                          <?php if ($boton=='Modificar') { echo $fch_med; } ?>
			            </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha de pago:</td>
                        <td>
                            <?php escribir_campo('fch_pag_med',$_POST["fch_pag_med"],$fch_pag_med,'',11,11,'Fecha en que se acreditará',$boton,$existe,'fecha')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto Anual:</td>
                        <td>
                            <?php $var='MED_'; $var.=date('Y'); $med = redondear ($$var,2,'','.'); ?>
                            <input name="mnt_med" type="hidden" readonly id="mnt_med" value="<?php if(! $existe) { echo $med; } else { echo $mnt_med; } ?>" size="20" title="Monto Anual"><?php if(! $existe) { echo redondear ($med,2,'.',','); } else { echo $mnt_med; } echo ' Bs.'?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td>
                            <?php escribir_campo('obs_med',$_POST["obs_med"],$obs_med,'',255,35,'Observaciones o Información adicional',$boton,$existe,'')?>
                        </td>
                      </tr>

         </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center">
                        <?php if($boton=='Modificar')
                              { 
                                  abrir_ventana('imprimir_medicinas_listin.php','v_imprimir','Imprimir Listin',"cod_med=".$cod_med); 
                              }
                        ?>
                         <?php if($boton=='Verificar')
                              { 
                                  abrir_ventana('imprimir_medicinas_disponibles.php','v_imprimir','Consultar Disponibilidad',"ano_med=".date(Y)); 
                              }
                        ?>
                    </td>
                  </tr>
                  <?php if ($existe || $boton=="Modificar") { ?>
       			  <tr><td width="<?php echo (100/1).'%'; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('medicinas_per.php','v_medicinas_per','Montos del Pago',"cod_med=".$cod_med."&seccion=".$_GET['seccion']); } ?></td></tr>
                  <tr>
                    <td>
                        <hr>
                        <center><b>Montos a Pagar</b></center>
                        <?php include('capa_medicinas.php'); ?>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php if ($_COOKIE['uspriv']==2) { ?> 
                  <tr>					<?php 
						$ncriterios =2; 
						$adicionaL_bus = '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=displayCalendar(document.forms[0].buscar_a,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>'; 
						$criterios[0] = "Fecha de registro ".$adicionaL_bus; 
						$campos[0] ="fch_med";
						$criterios[1] = "Fecha de pago ".$adicionaL_bus; 
						$campos[1] ="fch_pag_med";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
					      echo '<center>Buscar Fecha de Registro: '; 
                          combo('bus_fch', $bus_fch, 'medicinas', $link, 1, 1, '', '', 'cod_med', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY fch_med DESC LIMIT 0,12");
				   	   }
				   	     ?></td>
                  </tr>
                  <?php } ?>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
