<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Familiares del Personal</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['ced_per'];
$cod_fam = $_POST["cod_fam"];
$ced_per = $_GET["ced_per"];
$ced_fam = $_POST["ced_fam"];
$nom_fam = $_POST["nom_fam"];
$ape_fam = $_POST["ape_fam"];
$sex_fam = $_POST["sex_fam"];
$fnac_fam = $_POST["fnac_fam"];
$par_fam = $_POST["par_fam"];
$est_fam = $_POST["est_fam"];
$obs_fam = $_POST["obs_fam"];

include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'personal_familiares.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$pagina2 = 'personal_familiares.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$tabla = "familiares";	// nombre de la tabla
$ncampos = "9";		//numero de campos del formulario
$datos[0] = crear_datos ("ced_per","Cedula del Personal",$_POST['ced_per'],"1","12","numericos");
$datos[1] = crear_datos ("ced_fam","Cedula del Familiar",$_POST['ced_fam'],"0","12","numericos");
$datos[2] = crear_datos ("nom_fam","Nombre",$_POST['nom_fam'],"1","50","alfabeticos");
$datos[3] = crear_datos ("ape_fam","Apellido",$_POST['ape_fam'],"1","50","alfabeticos");
$datos[4] = crear_datos ("sex_fam","Sexo",$_POST['sex_fam'],"1","12","alfabeticos");
$datos[5] = crear_datos ("fnac_fam","Fecha de Nacimiento",$_POST['fnac_fam'],"1","10","fecha");
$datos[6] = crear_datos ("par_fam","Parentesco",$_POST['par_fam'],"1","25","alfabeticos");
$datos[7] = crear_datos ("est_fam","Estudia?",$_POST['est_fam'],"0","1","alfabeticos");
$datos[8] = crear_datos ("obs_fam","Observaciones",$_POST['obs_fam'],"0","255","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_fam = $row["cod_fam"];
	    $ced_per = $row["ced_per"];
	    $ced_fam = $row["ced_fam"];
	    $nom_fam = $row["nom_fam"];
	    $ape_fam = $row["ape_fam"];
	    $sex_fam = $row["sex_fam"];
	    $fnac_fam = $row["fnac_fam"];
	    $par_fam = $row["par_fam"];
	    $est_fam = $row["est_fam"];
	    $obs_fam = $row["obs_fam"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_fam",$_POST["cod_fam"],$pagina2);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
		$boton = comp_exist($datos[2][0],$datos[2][2]."' AND ced_per = '".$datos[0][2],$tabla,$boton,'si',"Nombres de Familiares de Personal");
		if ($_POST['ced_fam'])
		{
		$boton = comp_exist($datos[1][0],$datos[1][2]."' AND ced_per = '".$datos[0][2],$tabla,$boton,'si',"Cedulas de Familiares de Personal");
		}
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_fam"],"cod_fam",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_fam","familiares",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Familiares. C.I: <?php echo $viene_val; ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">C&eacute;dula:</td>
                        <td width="75%">
			<input name="cod_fam" type="hidden" id="cod_fam" value="<?php if(! $existe) { echo $_POST["cod_fam"]; } else { echo $cod_fam; } ?>" title="Codigo del Familiar">
			    <input name="ced_per" type="hidden" id="ced_per" readonly value="<?php if(! $existe) { echo $viene_val; } else { echo $ced_per; } ?>" <?php if ($boton=='Actualizar') { echo "readonly"; } ?> size="29" title="C&eacute;dula del Personal" />
                        <input name="ced_fam" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_fam" value="<?php if(! $existe) { echo $_POST['ced_fam']; } else { echo $ced_fam; } ?>" size="15" maxlength="8" title="C&eacute;dula del Familiar">
                        <?php if ($boton=='Modificar') { echo $ced_fam; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Nombre:</td>
                        <td><input name="nom_fam" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_fam" value="<?php if(! $existe) { echo $_POST['nom_fam']; } else { echo $nom_fam; } ?>" size="35" title="Nombre del Familiar" />
                          <?php if ($boton=='Modificar') { echo $nom_fam; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Apellido:</td>
                        <td><input name="ape_fam" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ape_fam" value="<?php if(! $existe) { echo $_POST['ape_fam']; } else { echo $ape_fam; } ?>" size="35" title="Apellido del Familiar" />
                          <?php if ($boton=='Modificar') { echo $ape_fam; } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Sexo:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="sex_fam" title="Sexo">
                          <option>Seleccione...</option>
                          <option value="M" '; if ($sex_fam == "M" || $_POST['sex_fam'] =="M") { echo 'selected'; } echo '>Masculino</option>
                          <option value="F" '; if ($sex_fam == "F" || $_POST['sex_fam'] =="F") { echo 'selected'; } echo '>Femenino</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="sex_fam" id="sex_fam" value="'.$sex_fam.'" >'; 
						    if ($sex_fam == "M") { echo 'Masculino'; } 
							if ($sex_fam == "F") { echo 'Femenino'; }
						}?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Fecha de Nacimiento: </td>
                        <td><input name="fnac_fam" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fnac_fam" value="<?php if(! $existe) { echo $_POST["fnac_fam"]; } else { echo $fnac_fam; } ?>" size="20" title="Fecha de Nacimiento" />
                          <?php if ($boton=='Modificar') { echo $fnac_fam; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fnac_fam,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Parentesco:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="par_fam" title="Parentesco con el Personal">
                          <option>Seleccione...</option>
                          <option value="C" '; if ($par_fam == "C" || $_POST['par_fam'] =="C") { echo 'selected'; } echo '>Conyuge</option>
                          <option value="H" '; if ($par_fam == "H" || $_POST['par_fam'] =="H") { echo 'selected'; } echo '>Hijo(a)</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="par_fam" id="par_fam" value="'.$par_fam.'" >'; 
						    if ($par_fam == "C") { echo 'Conyuge'; } 
					            if ($par_fam == "H" && $sex_fam == "M") { echo 'Hijo'; }
						    if ($par_fam == "H" && $sex_fam == "F") { echo 'Hija'; }
						}?></td>
                      </tr>
		      <tr>
 			<td class="etiquetas">Estudia:</td>
			<td><input name="est_fam" id="est_fam" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$est_fam.'"'; } else { echo 'type="checkbox" value="S"'; } if($_POST["est_fam"]=='S' || $est_fam == 'S') { echo 'checked'; } ?> title="Estudia Actualmente">
                          <?php if ($boton=='Modificar') { if ($est_fam=='S') {echo 'SI'; } else { echo 'NO'; } } ?>
			</td>
		      </tr>
		      <tr>
                        <td class="etiquetas">Observaci&oacute;n:</td>
                        <td><input name="obs_fam" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="obs_fam" value="<?php if(! $existe) { echo $_POST['obs_fam']; } else { echo $obs_fam; } ?>" size="35" title="Observaci&oacute;n sobre Familiar" />
                          <?php if ($boton=='Modificar') { echo $obs_fam; } ?></td>
                      </tr>

                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_familiares_personal.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
