<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Codificaci&oacute;n Presupuestaria</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_egr'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'egresos_productos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$pagina2 = 'egresos_productos.php?cod_egr='.$_GET["cod_egr"].'&seccion='.$_GET["seccion"];
$tabla = "productos_egresos";	// nombre de la tabla
$ncampos = "5";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_pro_egr","Cod. Presupuestario  ",$_POST['cod_pro_egr'],"0","11","numericos");
$datos[1] = crear_datos ("cod_egr","Cod. Egreso",$_POST['cod_egr'],"1","11","numericos");
$datos[2] = crear_datos ("cod_par","Cod. Partidas",$_POST['cod_par'],"1","11","numericos");
$datos[3] = crear_datos ("isrl_pro_egr","Aplica retencion ISRL",$_POST['isrl_pro_egr'],"0","2","alfabeticos");
$datos[4] = crear_datos ("mon_pro_egr","Monto del Egreso por la Partida",$_POST['mon_pro_egr'],"1","12","decimal");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_pro_egr = $row["cod_pro_egr"];
	    $cod_egr = $row["cod_egr"];
	    $cod_par = $row["cod_par"];
	    $isrl_pro_egr = $row["isrl_pro_egr"];
	    $mon_pro_egr = $row["mon_pro_egr"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_pro_egr",$_POST["cod_pro_egr"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_pro_egr"],"cod_pro_egr",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_pro_egr","productos_egresos",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Codificaci&oacute;n Presupuestaria</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Partida:
                          </td>
		            <td><input name="cod_pro_egr" type="hidden" id="cod_pro_egr" value="<?php if (! $existe) { echo $_POST['cod_pro_egr']; } else { echo $cod_pro_egr; } ?>" title="Codigo de salida presupuestaria">
			<input name="cod_egr" type="hidden" id="cod_egr" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_egr; } ?>" title="Codigo del Egreso">
                        <?php combo('cod_par', $cod_par, 'part_presup', $link, 0, 5, 6, 9, 'cod_par', "", $boton, "ORDER BY par_par,gen_par,esp_par,sub_par"); ?></td>
                      </tr>
                       <tr>
                        <td width="25%" class="etiquetas">Monto:</td>
                        <td width="75%">
                        <input name="mon_pro_egr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_pro_egr" value="<?php if(! $existe) { echo $_POST['mon_pro_egr']; } else { echo $mon_pro_egr; } ?>" size="10" maxlength="12" title="Monto del Egreso de Salida Partida">
                        <?php if ($boton=='Modificar') { echo $mon_pro_egr; } ?></td>
                      </tr>
		      <tr>
                        <td width="25%" class="etiquetas">Retenci&oacute;n ISRL?: </td>
			<td valign="top"> <?php if ($boton!="Modificar") { ?>
                  	<input name="isrl_pro_egr" type="radio" value="SI" <?php if (($_POST["isrl_pro_egr"]=='SI')||($isrl_pro_egr=='SI')) {echo 'checked'; }?>>SI
                  	<input name="isrl_pro_egr" type="radio" value="" <?php if (($_POST["isrl_pro_egr"]=='')||($isrl_pro_egr=='')) {echo 'checked'; }?>>NO<?php } else { echo '<input name="isrl_pro_egr" id="isrl_pro_egr" type="hidden" value="'.$isrl_pro_egr.'">'; if (!$isrl_pro_egr) { echo 'NO'; } else { echo 'SI'; }  }?>
              		</td> 
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_egresos_productos.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
