<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Productos de la Compras / Servicios</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_com'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'compras_productos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"].'&cod_com='.$_GET["cod_com"];
$pagina2 = 'compras_productos.php?cod_com='.$_GET["cod_com"].'&seccion='.$_GET["seccion"];
$tabla = "productos_compras";	// nombre de la tabla
$ncampos = "7";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_pro_com","Cod. de productos ",$_POST['cod_pro_com'],"0","11","numericos");
$datos[1] = crear_datos ("cod_com","Cod. Compra",$_POST['cod_com'],"1","11","numericos");
$datos[2] = crear_datos ("cnt_pro_com","Cantidad del Producto",$_POST['cnt_pro_com'],"1","4","numericos");
$datos[3] = crear_datos ("uni_pro_com","Unidades del Producto",$_POST['uni_pro_com'],"1","4","numericos");
$datos[4] = crear_datos ("con_pro_com","Concepto del Producto",$_POST['con_pro_com'],"1","255","alfanumericos");
$datos[5] = crear_datos ("pun_pro_com","Precio Unit. del Producto",$_POST['pun_pro_com'],"1","12","decimal");
$datos[6] = crear_datos ("exc_pro_com","Ex. IVA del Producto",$_POST['exc_pro_com'],"0","2","alfabeticos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_pro_com = $row["cod_pro_com"];
	    $cod_com = $row["cod_com"];
	    $cnt_pro_com = $row["cnt_pro_com"];
	    $uni_pro_com = $row["uni_pro_com"];
	    $con_pro_com = $row["con_pro_com"];
	    $pun_pro_com = $row["pun_pro_com"];
	    $exc_pro_com = $row["exc_pro_com"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	/// Verificamos que la suma de los productos no sea mayor al monto registrado como total del compromiso
	if ($validacion) {
		$sql_verifica = "Select *, SUM(pun_pro_com * cnt_pro_com) as exento from productos_compras where cod_com=".$viene_val." AND exc_pro_com='SI' AND cod_pro_com!=".$cod_pro_com." GROUP BY cod_com";
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$mon_exc = $sql_bus_ver['exento'];
		$sql_verifica = "Select *, SUM(pun_pro_com * cnt_pro_com) as base_imp from productos_compras where cod_com=".$viene_val." AND exc_pro_com='' AND cod_pro_com!=".$cod_pro_com." GROUP BY cod_com";		
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$mon_bas = $sql_bus_ver['base_imp'];
		$sql_verifica = "Select iva_com,mon_com from compras where cod_com=".$viene_val;
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$por_iva = $sql_bus_ver['iva_com'];
		$mon_ini = $sql_bus_ver['mon_com'];
		$comprometido = ($mon_bas * $por_iva / 100) + $mon_bas + $mon_exc;
		$mon_pend = $mon_ini - $comprometido;  
		$mon_act = $_POST['pun_pro_com'] * $_POST['cnt_pro_com'];
		if ($_POST['exc_pro_com']=='') {
			$mon_act = $mon_act * ($por_iva+100) / 100;
		}
		if ($mon_act > $mon_pend){
			echo '<SCRIPT>alert("'.$msg_productos_mayor.'(Solo quedan Bs. '.$mon_pend.' disponibles, incluyendo IVA)'.$msg_productos_mayor2.'");</SCRIPT>';
			$validacion = '';
		}
		else { $validacion = 1; }
	}	
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_pro_com",$_POST["cod_pro_com"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	/// Verificamos que la suma de los productos no sea mayor al monto registrado como total del compromiso
	if ($validacion) {
		$sql_verifica = "Select *, SUM(pun_pro_com * cnt_pro_com) as exento from productos_compras where cod_com=".$viene_val." AND exc_pro_com='SI' GROUP BY cod_com";
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$mon_exc = $sql_bus_ver['exento'];
		$sql_verifica = "Select *, SUM(pun_pro_com * cnt_pro_com) as base_imp from productos_compras where cod_com=".$viene_val." AND exc_pro_com='' GROUP BY cod_com";		
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$mon_bas = $sql_bus_ver['base_imp'];
		$sql_verifica = "Select iva_com,mon_com from compras where cod_com=".$viene_val;
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$por_iva = $sql_bus_ver['iva_com'];
		$mon_ini = $sql_bus_ver['mon_com'];
		$comprometido = ($mon_bas * $por_iva / 100) + $mon_bas + $mon_exc;
		$mon_pend = $mon_ini - $comprometido;  
		$mon_act = $_POST['pun_pro_com'] * $_POST['cnt_pro_com'];
		if ($_POST['exc_pro_com']=='') {
			$mon_act = $mon_act * ($por_iva+100) / 100;
		}
		if ($mon_act > $mon_pend){
			echo '<SCRIPT>alert("'.$msg_productos_mayor.'(Solo quedan Bs. '.$mon_pend.' disponibles, incluyendo IVA)'.$msg_productos_mayor2.'");</SCRIPT>';
			$validacion = '';
		}
		else { $validacion = 1; }
	}
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_pro_com"],"cod_pro_com",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_pro_com","productos_compras",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Productos de la Compras / Servicios</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas">Cantidad</td>
                        <td>
			<input name="cod_pro_com" type="hidden" id="cod_pro_com" value="<?php if(! $existe) { echo $_POST["cod_pro_com"]; } else { echo $cod_pro_com; } ?>" title="Codigo del producto compra">
			<input name="cod_com" type="hidden" id="cod_com" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_com; } ?>" title="Codigo de la compra">
			<input name="cnt_pro_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="cnt_pro_com" value="<?php if(! $existe) { echo $_POST['cnt_pro_com']; } else { echo $cnt_pro_com; } ?>" size="3" maxlength="4" title="Cantidad a Adquirir " />
                          <?php if ($boton=='Modificar') { echo $cnt_pro_com; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Unidades:</td>
                        <td width="75%">
                        <input name="uni_pro_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="uni_pro_com" value="<?php if(! $existe) { echo $_POST['uni_pro_com']; } else { echo $uni_pro_com; } ?>" size="3" maxlength="4" title="Unidades que contiene">
                        <?php if ($boton=='Modificar') { echo $uni_pro_com; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Concepto:</td>
                        <td><input name="con_pro_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="con_pro_com" value="<?php if(! $existe) { echo $_POST["con_pro_com"]; } else { echo $con_pro_com; } ?>" size="35" title="Descripci&oacute;n del Producto/Servico" />
                          <?php if ($boton=='Modificar') { echo $con_pro_com; } ?></td>
                      </tr>
                       <tr>
                        <td width="25%" class="etiquetas">Precio Unitario:</td>
                        <td width="75%">
                        <input name="pun_pro_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="pun_pro_com" value="<?php if(! $existe) { echo $_POST['pun_pro_com']; } else { echo $pun_pro_com; } ?>" size="10" maxlength="12" title="Precio Unitario">
                        <?php if ($boton=='Modificar') { echo $pun_pro_com; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Excento IVA: </td>
			<td valign="top"> <?php if ($boton!="Modificar") { ?>
                  	<input name="exc_pro_com" type="radio" value="SI" <?php if (($_POST[exc_pro_com]=='SI')||($exc_pro_com=='SI')) {echo 'checked'; }?>>SI
                  	<input name="exc_pro_com" type="radio" value="" <?php if (($_POST[exc_pro_com]=='')||($exc_pro_com=='')) {echo 'checked'; }?>>NO<?php } else { echo '<input name="exc_pro_com" id="exc_pro_com" type="hidden" value="'.$exc_pro_com.'">'; if (!$exc_pro_com) { echo 'NO'; } else { echo 'SI'; }  }?>
              		</td> 
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_compras_productos.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
