<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php
  if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'compras_servicios.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "compras";		// nombre de la tabla
$ncampos = "17";		//numero de campos del formulario
$datos[0]  = crear_datos ("cod_com","C�digo",$_POST['cod_com'],"0","11","numericos");
$datos[1]  = crear_datos ("fch_com","Fecha",$_POST['fch_com'],"10","10","fecha");
$datos[2]  = crear_datos ("idn_par_mov","Identificador del Movimiento",$_POST['idn_par_mov'],"1","50","alfanumericos");
$datos[3]  = crear_datos ("nor_com","Num Orden",$_POST['nor_com'],"0","4","numericos");
$datos[4]  = crear_datos ("frm_com","Num Formulario",$_POST['frm_com'],"0","6","numericos");
$datos[5]  = crear_datos ("sol_com","Solicitud",$_POST['sol_com'],"1","20","alfanumericos");
$datos[6]  = crear_datos ("tip_com","Tipo de Orden",$_POST['tip_com'],"1","15","alfabeticos");
$datos[7]  = crear_datos ("for_com","Forma de Pago",$_POST['for_com'],"1","15","alfabeticos");
$datos[8]  = crear_datos ("adl_com","Adelanto",$_POST['adl_com'],"0","12","decimal");
$datos[9]  = crear_datos ("fre_com","Descipcion Resumen",$_POST['fre_com'],"1","255","alfanumericos");
$datos[10]  = crear_datos ("rif_pro","Proveedor",$_POST['rif_pro'],"1","10","alfanumericos");
$datos[11]  = crear_datos ("npr_com","Num Productos",$_POST['npr_com'],"0","2","numericos");
$datos[12] = crear_datos ("mon_com","Monto Total",$_POST['mon_com'],"1","12","decimal");
$datos[13] = crear_datos ("iva_com","% IVA",$_POST['iva_com'],"1","2","numericos");
$datos[14] = crear_datos ("ela_com","Elaborado",$_POST['ela_com'],"1","50","alfanumericos");
$datos[15] = crear_datos ("rev_com","Revisado",$_POST['rev_com'],"1","50","alfanumericos");
$datos[16] = crear_datos ("obs_com","Observaci�n",$_POST['obs_com'],"0","255","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Num Control";
		$datos[0]="nor_com";
		$parametro[1]="Fecha";
		$datos[1]="fch_com";
		$parametro[2]="RIF Proveedor";
		$datos[2]="rif_pro";
		$parametro[3]="Tipo de Orden";
		$datos[3]="tip_com";
		$parametro[4]="Identificador";
		$datos[4]="idn_par_mov";
		busqueda_varios(7,$buscando,$datos,$parametro,"cod_com");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_com = $row["cod_com"];
	    $fch_com = $row["fch_com"];
	    $idn_par_mov = $row["idn_par_mov"];
	    $nor_com = $row["nor_com"];
   	    $frm_com = $row["frm_com"];
	    $sol_com = $row["sol_com"];
	    $tip_com = $row["tip_com"];
	    $for_com = $row["for_com"];
	    $adl_com = $row["adl_com"];
	    $fre_com = $row["fre_com"];
	    $rif_pro = $row["rif_pro"];
	    $npr_com = $row["npr_com"];
	    $mon_com = $row["mon_com"];
	    $iva_com = $row["iva_com"];
	    $ela_com = $row["ela_com"];
	    $rev_com = $row["rev_com"];
	    $obs_com = $row["obs_com"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///  verificamos si ya existen �rdenes de pago para el presente compromiso
	    $sql_verifica = "select * from pagos where frm_com =".$frm_com;
        $sql_bus_verifica = mysql_query($sql_verifica);
        if ($res_verifica=mysql_fetch_array($sql_bus_verifica)){
            $permitir_modificar='NO';
        }
        /// verificamos si tiene productos asociados
		$sql_rela = "SELECT * FROM productos_compras WHERE cod_com=". $cod_com ;
           if (mysql_fetch_array(mysql_query($sql_rela))){
			$prm[3]='';
		}
        /// verificamos si tiene partidas asociadas
		$sql_rela = "SELECT * FROM partidas_compras WHERE cod_com=". $cod_com ;
           if (mysql_fetch_array(mysql_query($sql_rela))){
			$prm[3]='';
		}

	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	///// Si se cambia el numero del formulario se verifica que sea el siguiente numero disponible (esto para casos de reimpresi�n del documento)
	if ($validacion)
	{
	    if ($_POST['frm_com'] != $_POST['com_ant'])
	    {
	        $sql = "SELECT MAX(frm_com) AS frm_com FROM compras";
            $res = mysql_fetch_array(mysql_query($sql));
            $frm_next = $res['frm_com'] + 1;
	        if ($frm_next != $_POST['frm_com'])
            {
                echo '<SCRIPT>alert("'.$msg_pago_cambiofrm.' ('.$frm_next.')");</SCRIPT>';
		        $validacion = '';                
            }
            else { $validacion = 1; }
	    }
	}
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_com",$_POST["cod_com"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar"){ 

    $sql_verifica = "select * from pagos where frm_com =".$frm_com;
    $sql_bus_verifica = mysql_query($sql_verifica);
    if ($res_verifica=mysql_fetch_array($sql_bus_verifica)){
        echo '<SCRIPT> alert ("'.$msg_modificar_no.'"); </SCRIPT>';
        $boton = "Modificar";
        $permitir_modificar='NO';
    }
    else {
        $boton = "Actualizar";
    }
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	$sql = "SELECT MAX(nor_com) AS nor_com FROM compras WHERE YEAR(fch_com)=";
	$sql .= $_POST["fch_com"][0].$_POST["fch_com"][1].$_POST["fch_com"][2].$_POST["fch_com"][3];
    	$res = mysql_fetch_array(mysql_query($sql));
    	$nor_com = $res['nor_com'] + 1;
	$datos[3]  = crear_datos ("nor_com","Num Orden",$nor_com,"0","4","numericos");
	
	//// Generar numero de formulario
	$sql = "SELECT MAX(frm_com) AS frm_com FROM compras";
    $res = mysql_fetch_array(mysql_query($sql));
    $frm_com = $res['frm_com'] + 1;
	$datos[4]  = crear_datos ("frm_com","Num Formulario",$frm_com,"0","6","numericos");
	
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
    $sql_verifica = "select * from pagos where frm_com =".$frm_com;
    $sql_bus_verifica = mysql_query($sql_verifica);
    if ($res_verifica=mysql_fetch_array($sql_bus_verifica)){
        echo '<SCRIPT> alert ("'.$msg_eliminar_no.'"); </SCRIPT>';
        $boton = "Modificar";
        $permitir_modificar='NO';
    }
    else {
        eliminar_func($_POST["cod_com"],"cod_com",$tabla,$pagina);
	    auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	    return;
    }
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";

        /// verificamos si tiene productos asociados
		$sql_rela = "SELECT * FROM productos_compras WHERE cod_com=". $cod_com ;
           if (mysql_fetch_array(mysql_query($sql_rela))){
			$prm[3]='';
		}
        /// verificamos si tiene partidas asociadas
		$sql_rela = "SELECT * FROM partidas_compras WHERE cod_com=". $cod_com ;
           if (mysql_fetch_array(mysql_query($sql_rela))){
			$prm[3]='';
		}

}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de Compromisos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">N&uacute;mero de Control:</td>
                        <td width="75%">
			<input name="cod_com" type="hidden" id="cod_com" value="<?php if(! $existe) { echo $_POST['cod_com']; } else { echo $cod_com; } ?>" size="35" title="Codigo de la Orden de Compra/Servicio">
			<input name="nor_com" type="hidden" id="nor_com" value="<?php if(! $existe) { echo $_POST['nor_com']; } else { echo $nor_com; } ?>" size="35" title="Num Orden"><?php if ($boton != "Verificar" && $boton != "Guardar") { echo substr($fch_com, 5, 2).substr($fch_com, 2, 2).$nor_com; } else echo 'Por Asignar...'; ?>                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">N� Formulario:</td>
                        <td width="75%">
			                <input name="frm_com" type="<?php if ($boton == 'Verificar' || $boton=='Modificar' || $boton=='Guardar'){ echo 'hidden';} else {echo 'text';} ?>" id="frm_com" value="<?php if(! $existe) { echo $_POST['frm_com']; } else { echo $frm_com; } ?>" size="15" title="Numero de Formulario"><?php if ($boton == "Modificar") { echo $frm_com; } if ($boton == "Verificar" || $boton == "Guardar") { echo 'Por Asignar...'; }?>
                            <input type="hidden" name="com_ant" id="com_ant" value="<?php if ($existe) { echo $frm_com;} else { echo $_POST["com_ant"]; }?>">
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha:</td>
                        <td><input onchange="submit();" name="fch_com" type="<?php if ($boton!='Verificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_com" value="<?php if(! $existe) { echo $_POST['fch_com']; } else { echo $fch_com; } ?>" size="20" title="Fecha de Registro de la Orden" />
                          <?php if ($boton=='Modificar') { echo $fch_com; } ?>
			  <?php if ($boton=='Actualizar' || $boton=='Guardar') { echo $_POST['fch_com']; } ?>
			<?php if ($boton=='Verificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_com,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
		      <tr>
                        <td width="25%" class="etiquetas">Identificador del Presupuesto:</td>
                        <td width="75%">
                        <?php
			    if ($boton=="Modificar"){ 
				escribir_campo('idn_par_mov',$_POST["idn_par_mov"],$idn_par_mov,' ',50,15,'Identificador del Presupuesto Destino',$boton,$existe,'','','');
			    }
			    else {
				echo '<select name="idn_par_mov">';
				echo '<option>Seleccione...</option>';
				$ano_par_mov = strtotime($fch_com);
				$ano_par_mov = date('Y',$ano_par_mov);
				echo $sql_idn = "SELECT * FROM part_presup_mov WHERE ano_par_mov = '".$ano_par_mov."' AND tip_par_mov = 'Asignaci�n' ";
				$bus_idn = mysql_query($sql_idn);
				while ($res_idn = mysql_fetch_array($bus_idn)) {
					$add_select = '';
                                        if ($res_idn["idn_par_mov"]==$idn_par_mov){ $add_select = 'selected=selected'; }
					echo '<option '.$add_select.' >'.$res_idn["idn_par_mov"].'</option>';
				}
				echo '</select>';
			    }
			?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Solicitud de Precios:                          </td>
			            <td>
                        <input name="sol_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="sol_com" value="<?php if(! $existe) { echo $_POST['sol_com']; } else { echo $sol_com; } ?>" size="20" title="Num de Solicitud de Precios">
                        <?php if ($boton=='Modificar') { echo $sol_com; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Tipo de Orden:                          </td>
			<td valign="top"> <?php if ($boton!="Modificar") { ?>
                  	<input name="tip_com" type="radio" value="Compra" <?php if (($_POST[tip_com]=='Compra')||($tip_com=='Compra')) {echo 'checked'; }?>>Compra
                  	<input name="tip_com" type="radio" value="Servicio" <?php if (($_POST[tip_com]=='Servicio')||($tip_com=='Servicio')) {echo 'checked'; }?>>Servicio
                  	<input name="tip_com" type="radio" value="Otro" <?php if (($_POST[tip_com]=='Otro')||($tip_com=='Otro')) {echo 'checked'; }?>>Otro<?php } else { echo '<input name="tip_com" id="tip_com" type="hidden" value="'.$tip_com.'">'.$tip_com; }?>        
              		</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Forma de Pago: </td>
			<td valign="top"> <?php if ($boton!="Modificar") { ?>
                  	<input name="for_com" type="radio" value="Contado" <?php if (($_POST[for_com]=='Contado')||($for_com=='Contado')) {echo 'checked'; }?>>Contado
                  	<input name="for_com" type="radio" value="Cr�dito" <?php if (($_POST[for_com]=='Cr�dito')||($for_com=='C�dito')) {echo 'checked'; }?>>Cr�dito<?php } else { echo '<input name="for_com" id="for_com" type="hidden" value="'.$for_com.'">'.$for_com; }?>
              		</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Adelanto:</td>
			            <td>
                        <input name="adl_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="adl_com" value="<?php if(! $existe) { echo $_POST['adl_com']; } else { echo $adl_com; } ?>" size="15" title="Monto de Adelanto">
                        <?php if ($boton=='Modificar') { echo $adl_com; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripci�n Resumen:                          </td>
			            <td>
                        <input name="fre_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fre_com" value="<?php if(! $existe) { echo $_POST['fre_com']; } else { echo $fre_com; } ?>" size="35" title="Resumen de la Descripci�n">
                        <?php if ($boton=='Modificar') { echo $fre_com; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Proveedor:                          </td>
			            <td>
                        <?php combo('rif_pro', $rif_pro, 'proveedores', $link, 2, 0, 3, '', 'rif_pro', "", $boton, "ORDER BY nom_pro",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto Total:                          </td>
			            <td>
                        <input name="mon_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_com" value="<?php if(! $existe) { echo $_POST['mon_com']; } else { echo $mon_com; } ?>" size="15" title="Monto Total de la transacci�n">
                        <?php if ($boton=='Modificar') { echo $mon_com.' Bs.'; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">IVA Aplicado:                          </td>
			            <td>
                        <input name="iva_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="iva_com" value="<?php if(! $existe) { echo $_POST['iva_com']; } else { echo $iva_com; } ?>" size="3" title="Monto del IVA a aplicar">
                        <?php if ($boton=='Modificar') { echo $iva_com.'%'; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Elaborado:</td>
			<td>
                        <input name="ela_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ela_com" value="<?php if(! $existe) { echo $_POST['ela_com']; } else { echo $ela_com; } ?>" size="35" title="Persona que Elabora la Orden">
                        <?php if ($boton=='Modificar') { echo $ela_com; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Revisado:</td>
			<td>
                        <input name="rev_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="rev_com" value="<?php if(! $existe) { echo $_POST['rev_com']; } else { echo $rev_com; } ?>" size="35" title="Persona que Revisa la Orden">
                        <?php if ($boton=='Modificar') { echo $rev_com; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaci&oacute;n:                          </td>
			<td>
                        <input name="obs_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="obs_com" value="<?php if(! $existe) { echo $_POST['obs_com']; } else { echo $obs_com; } ?>" size="35" title="Observaciones de la Orden">
                        <?php if ($boton=='Modificar') { echo $obs_com; } ?></td>
                      </tr>

                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
		  <tr>
                    <td align="center"><?php if($boton=='Modificar'){ abrir_ventana('imprimir_compra.php','v_imprimir','Imprimir Orden',"cod_com=".$cod_com); } ?></td>
                  </tr>
		  <?php if ($boton=="Modificar") { echo '<tr><td><hr></td></tr>'; 
		  echo '<tr><td> 
			<table width="100%" align="center" cellspacing="10">';
			?>
			<tr><td width="<?php echo (100/1).'%'; ?>" align="center">
			    <?php if ($permitir_modificar=='NO'){ echo "<b>Conceptos relacionados al compromiso</b>"; }?>
			    <?php if($boton=='Modificar' && $permitir_modificar!='NO'){ abrir_ventana('compras_productos.php','v_Productos','Productos de la Compra/Servicio',"cod_com=".$cod_com."&seccion=".$_GET['seccion']); } ?></td></tr>
		  <tr>
		    <td>
			<?php include('capa_compras_productos.php'); ?>
		    </td>
		  </tr>
			<?php
			echo '</table>
		        </td></tr>';
		  echo '<tr><td> 
			<table width="100%" align="center" cellspacing="10">';
			?>
			<tr><td width="<?php echo (100/1).'%'; ?>" align="center">
			    <?php if ($permitir_modificar=='NO'){ echo "<b>Codificaci�n Presupuestaria</b>"; }?>
			    <?php if($boton=='Modificar' && $permitir_modificar!='NO'){ abrir_ventana('partidas_compras.php','v_Productos','Codificaci�n Presupuestaria',"cod_com=".$cod_com."&seccion=".$_GET['seccion']."&year=".date(Y,strtotime($fch_com))); } ?></td></tr>
		  <tr>
		    <td>
			<?php include('capa_compras_partidas.php'); ?>
		    </td>
		  </tr>
			<?php
			echo '</table>
		        </td></tr>';
		  } ?>
                  <tr>
                    <td>
					<?php 
						$ncriterios =2; 
						$criterios[0] = "Rif"; 
						$campos[0] ="rif_pro";
						$adicionaL_bus = '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=criterio1.checked=true;displayCalendar(document.forms[0].buscar_a,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>'; 
						$criterios[1] = "Fecha ".$adicionaL_bus; 
						$campos[1] ="fch_com";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"criterio0.checked=true; valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
                                             echo '<center>Buscar RIF: '; 
                                             combo('rif_pro2', $rif_pro, 'proveedores', $link, 2, 0, 3, '', 'rif_pro', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nom_pro",'');
                                             echo '</center>'; 
				   	   }  ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
