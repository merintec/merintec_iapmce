<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$post_dias_sol_vac = $_POST["dias_sol_vac"];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'vacaciones.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "vacaciones_per";	// nombre de la tabla
$ncampos = "15";		//numero de campos del formulario
$datos[0] = crear_datos ("cod_sol_vac","Codigo de Solicitud",$_POST['cod_sol_vac'],"0","11","numericos");
$datos[1] = crear_datos ("fch_sol_vac","Fecha de Registro de Solicitud",$_POST['fch_sol_vac'],"1","10","fecha");
$datos[2] = crear_datos ("ced_per","C�dula del Solicitante",$_POST['ced_per'],"1","11","numericos");
$datos[3] = crear_datos ("nom_per","Nombre del Solicitante",$_POST['nom_per'],"1","100","alfabeticos");
$datos[4] = crear_datos ("nom_dep","Departamento",$_POST['nom_dep'],"1","100","alfanumericos");
$datos[5] = crear_datos ("nom_car","Cargo",$_POST['nom_car'],"0","100","alfabeticos");
$datos[6] = crear_datos ("dias_sol_vac","D�as a Disfrutar",$_POST['dias_sol_vac'],"1","2","numericos");
$datos[7] = crear_datos ("ini_sol_vac","Periodo de Disfrute - Fecha de Inicio",$_POST['ini_sol_vac'],"1","10","fecha");
$datos[8] = crear_datos ("fin_sol_vac","Periodo de Disfrute - Fecha de Fin",$_POST['fin_sol_vac'],"0","10","fecha");
$datos[9] = crear_datos ("tip_sol_vac","Tipo de Vacaciones",$_POST['tip_sol_vac'],"0","20","alfanumericos");
$datos[10] = crear_datos ("peri_sol_vac","Correspondiente al Periodo - Fecha Inicio",$_POST['peri_sol_vac'],"0","10","fecha");
$datos[11] = crear_datos ("perf_sol_vac","Correspondiente al Periodo - Fecha Fin",$_POST['perf_sol_vac'],"0","10","fecha");
$datos[12] = crear_datos ("obs_sol_vac","Observaci�n",$_POST['obs_sol_vac'],"0","255","alfanumericos");
$datos[13] = crear_datos ("apro_sol_vac","Estado de la solicitud",$_POST['apro_sol_vac'],"0","2","alfanumericos");
$datos[14] = crear_datos ("fch_ing","Fecha de Ingreso",$_POST['fch_ing'],"10","10","fecha");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) 
	{ 
	    $tipo = "general";
	    $criterio_buscar = $_POST["criterio"];
	    $valor_buscar = $_POST["buscar_a"];
	    $buscando = busqueda_func($valor_buscar,$criterio_buscar,"$tabla",$pagina,$tipo);
	}
	elseif ($_POST["BuscarInd"]) { 
	$tipo = "individual"; 
	$buscando = busqueda_func($_POST["buscar_a"],"cod_sol_vac","$tabla",$pagina,$tipo);
	} 
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre de Solicitante";
		$datos[0]="nom_car";	
		$parametro[1]="D�as a Disfrutar";
		$datos[1]="dias_sol_vac";	
		$parametro[2]="Desde";
		$datos[2]="ini_sol_vac";
		$parametro[3]="Hasta";
		$datos[3]="fin_sol_vac";
		busqueda_varios(7,$buscando,$datos,$parametro,"cod_sol_vac");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_sol_vac = $row["cod_sol_vac"];
	    $fch_sol_vac = $row["fch_sol_vac"];
	    $ced_per = $row["ced_per"];
	    $nom_per = $row["nom_per"];
	    $nom_dep = $row["nom_dep"];
	    $nom_car = $row["nom_car"];
	    $fch_ing = $row["fch_ing"];
	    $dias_sol_vac = $row["dias_sol_vac"];
	    $ini_sol_vac = $row["ini_sol_vac"];
	    $fin_sol_vac = $row["fin_sol_vac"];
	    $tip_sol_vac = $row["tip_sol_vac"];
	    $peri_sol_vac = $row["peri_sol_vac"];
   	    $perf_sol_vac = $row["perf_sol_vac"];
	    $obs_sol_vac = $row["obs_sol_vac"];
	    $apro_sol_vac = $row["apro_sol_vac"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Fecha_tope_Verificar" || $_POST["confirmar"]=="Fecha_tope_Actualizar") 
{
    if ($_POST['dias_sol_vac']>$_POST['dias_sol_dis']) {
        echo '<SCRIPT> alert ("Precauci�n. '.'\n\n'.'D�as a disfrutar es mayor a los d�as Disponibles."); </SCRIPT>';
        $_POST['dias_sol_vac']='';
        $dias_sol_vac = '';
    }
    if ($_POST['dias_sol_vac']=='Seleccione...'){ 
        echo '<SCRIPT> alert ("Debe Indicar D�as a Disfrutar"); </SCRIPT>';
        $post_fin_sol_vac ="";
    }
    elseif ($_POST['dias_sol_vac']==1) {
        $post_fin_sol_vac = $_POST['ini_sol_vac'];
    }
    else {
    	$post_fin_sol_vac = calculo_fecha ($_POST['ini_sol_vac'],"+",$_POST['dias_sol_vac']-1);
	    if (! $_POST['ini_sol_vac']) { $post_fin_sol_vac =""; }
	}
	$boton = str_replace("Fecha_tope_","",$_POST["confirmar"]);
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_sol_vac",$_POST["cod_sol_vac"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
    $post_fin_sol_vac = $_POST["fin_sol_vac"];
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
    $post_fin_sol_vac = $_POST["fin_sol_vac"];
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{  
     $post_fin_sol_vac = $_POST["fin_sol_vac"];
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_sol_vac"],"cod_sol_vac",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_sol_vac","Vacaciones Solicitadas",$pagina);
	return;
}
?>
<?php
///// Verificar cuantos d�as de vacaciones han sido asignados
if (!$_POST['ced_per'] && !$ced_per)
{
    $ced_per_reg = $_COOKIE['uscod'];
}
else
{
    $ced_per_reg = $ced_per;
}
$sql_dias = "select sum(dias_vac) as dias_asg from vac_dias_per where ced_per = ".$ced_per_reg." GROUP BY ced_per";
$sql_res = mysql_query($sql_dias);
while ($sql_row = mysql_fetch_array($sql_res))
{
    $dias_asg = $sql_row['dias_asg'];
}

///// Verificar cuantos d�as de vacaciones han disrutado
$sql_dias = "select sum(dias_sol_vac) as dias_dis from vacaciones_per where apro_sol_vac = 'A' AND ced_per = ".$ced_per_reg." GROUP BY ced_per";
$sql_res = mysql_query($sql_dias);
while ($sql_row = mysql_fetch_array($sql_res))
{
    $dias_dis = $sql_row['dias_dis'];
}

///// Calcular cuantos d�as de vacaciones quedan por Disfrutar

    $dias_pen = $dias_asg - $dias_dis;


?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Solicitud de Vacaciones</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		              <tr>
                        <td class="etiquetas">Fecha: </td>
                        <td>
                            <input name="cod_sol_vac" type="hidden" id="cod_sol_vac" value="<?php if(! $existe) { echo $_POST['cod_sol_vac']; } else { echo $cod_sol_vac; } ?>" size="35" title="Codigo de la solicitud">
                            <input name="fch_sol_vac" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_sol_vac" readonly value="<?php if(! $existe) { if (! $_POST['fch_sol_vac']) { echo date('Y-m-d'); } else { echo $_POST['fch_sol_vac']; } } else { echo $fch_sol_vac; } ?>" size="20" title="Fecha de Solicitud" />
                          <?php if ($boton=='Modificar') { echo $fch_sol_vac; } ?>
			</td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Departamento:</td>
                        <td width="75%">
			<?php if ($nom_dep == "" && $_POST["nom_dep"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_dep = $row2['nom_dep'];	
			} ?>
			<input name="nom_dep" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_dep" readonly value="<?php echo $nom_dep; ?>" size="35" title="Departamento">
                        <?php if ($boton=='Modificar') { echo $nom_dep; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Cargo:</td>
                        <td width="75%">
			<?php if ($nom_car == "" && $_POST["nom_car"] == "") { 
			$sql2 = "SELECT c.fch_asg, c.nom_car, d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_car = $row2['nom_car'];			
			$fch_ing = $row2['fch_asg'];		
			} ?>
			<input name="nom_car" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_car" readonly value="<?php echo $nom_car; ?>" size="35" title="Cargo que desempe�a">
                        <?php if ($boton=='Modificar') { echo $nom_car; } ?></td>
                      </tr>    
                      <tr>
                        <td class="etiquetas">Fecha de Ingreso:</td>
                        <td width="75%">
			<input name="fch_ing" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_ing" readonly value="<?php echo $fch_ing; ?>" size="35" title="Cargo que desempe�a">
                        <?php if ($boton=='Modificar') { echo $fch_ing; } ?></td>
                      </tr>                      
                      
                      
                      
		      <tr>
                        <td class="etiquetas">C�dula:</td>
                        <td width="75%">
			<?php if ($ced_per == "" && $_POST["ced_per"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$ced_per = $_COOKIE['uscod'];
			} ?>
			<input name="ced_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_per" readonly value="<?php echo $ced_per; ?>" size="35" title="C�dula del solicitante">
                        <?php if ($boton=='Modificar') { echo $ced_per; } ?></td>
                      </tr>
                      

		      <tr>
                        <td class="etiquetas">Nombre:</td>
                        <td width="75%">
			<?php if ($nom_per == "" && $_POST["nom_per"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_per = $_COOKIE['usnombre'];
			} ?>
			<input name="nom_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_per" readonly value="<?php echo $nom_per; ?>" size="35" title="Nombre y Apellido del solicitante">
                        <?php if ($boton=='Modificar') { echo $nom_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Tipo de Vacaciones:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tip_sol_vac" title="Tipo de Vacaciones">
                          <option>Seleccione...</option>
                          <option value="A" '; if ($tip_sol_vac == "A" || $_POST['tip_sol_vac'] =="A") { echo 'selected'; } echo '>Atrasadas</option>
                          <option value="R" '; if ($tip_sol_vac == "R" || $_POST['tip_sol_vac'] =="R") { echo 'selected'; } echo '>Reglamentarias</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tip_sol_vac" id="tip_sol_vac" value="'.$tip_sol_vac.'" >'; 
						    	if ($tip_sol_vac == "A") { echo 'Atrasadas'; } 
							if ($tip_sol_vac == "R") { echo 'Reglamentarias'; }
						}?></td>
                      <tr>
                        <td class="etiquetas">D�as a Disfrutar:</td>
                        <td><?php if ($boton != "Modificar" && $boton != "Guardar") { 
                        $add_ini = 'onchange="confirmacion_func(';
                        $add_ini .= "'Fecha_tope_".$boton."')";
                        $add_ini .= '"';
                        echo '<select name="dias_sol_vac" title="D�as a disfrutar en el per�odo de vaciones" '.$add_ini.'>
                          <option>Seleccione...</option>';
			for($i=1;$i<=30;$i++){
			echo '<option value="'.$i.'"'; if ($dias_sol_vac == $i || $_POST['dias_sol_vac'] == $i) { echo 'selected'; } echo '>'.$i.' d�as </option>';
			}
			echo '</select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="dias_sol_vac" id="dias_sol_vac" value="'.$dias_sol_vac.'" > '.$dias_sol_vac.' d&iacute;as'; 
						}?>
						<?php echo '('.$dias_pen.' d�as disponibles)' ; 
						echo '<input type="hidden" name="dias_sol_dis" id="dias_sol_dis" value="'.$dias_pen.'" > ';
						?>
						
						</td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Per�odo de Disfrute: </td>
			<?php if ($ini_sol_vac == "0000-00-00") { $ini_sol_vac="";} ?>
			<?php if ($fin_sol_vac == "0000-00-00") { $fin_sol_vac="";} ?>			
                        <td>del: <input name="ini_sol_vac" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ini_sol_vac" value="<?php if(! $existe) { echo $_POST['ini_sol_vac']; } else { echo $ini_sol_vac; } ?>" size="8" title="Fecha de inicio del per�odo de vacaciones a Disfrutar" onchange="confirmacion_func('Fecha_tope_<?php echo $boton; ?>')"><?php if ($boton=='Modificar') { echo $ini_sol_vac; } ?><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].ini_sol_vac,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?>
&nbsp;&nbsp;&nbsp;al: <input name="fin_sol_vac" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fin_sol_vac" readonly value="<?php if(! $existe) { echo $post_fin_sol_vac; } else { echo $fin_sol_vac; } ?>" size="8" title="Fecha de fin del per�odo de vacaciones a Disfrutar" />
                          <?php if ($boton=='Modificar') { echo $fin_sol_vac; } ?>
			             </td>
                      </tr> 
                      <tr>
                        <td width="25%" class="etiquetas">Correspondiente al Per�odo Lab.:</td>
                        <td>
                            del: <?php escribir_campo('peri_sol_vac',$_POST["peri_sol_vac"],$peri_sol_vac,'',255,8,'Fecha inicio del per�odo laborado al que corresponden las vacaciones solicitadas',$boton,$existe,'fecha'); ?>
                            &nbsp; al: <?php escribir_campo('perf_sol_vac',$_POST["perf_sol_vac"],$perf_sol_vac,'',255,8,'Fecha fin del per�odo laborado al que corresponden las vacaciones solicitadas',$boton,$existe,'fecha'); ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td>
                            <?php escribir_campo('obs_sol_vac',$_POST["obs_sol_vac"],$obs_sol_vac,'',255,35,'Observaciones o Informaci�n adicional',$boton,$existe,'')?>
                        </td>
                      </tr>

		      <?php if ($boton!='Verificar' && $boton!='Guardar') { ?>
                      <tr>
                        <td class="etiquetas">Estado:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="apro_sol_vac" title="Estado">
                          <option>Por Aprobar...</option>
                          <option value="A" '; if ($apro_sol_vac == "A" || $_POST['apro_sol_vac'] =="A") { echo 'selected'; } echo '>Aprobadas</option>
                          <option value="R" '; if ($apro_sol_vac == "R" || $_POST['apro_sol_vac'] =="R") { echo 'selected'; } echo '>Rechazadas</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="apro_sol_vac" id="apro_sol_vac" value="'.$apro_sol_vac.'" >'; 
						    	if (!$apro_sol_vac) { echo 'Por Aprobar'; } 
    					    	if ($apro_sol_vac == "A") { echo 'Aprobadas'; } 
    							if ($apro_sol_vac == "R") { echo 'Rechazadas'; }
						}?></td>
                      </tr>
		      <?php }?>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php if($boton=='Modificar' && $apro_sol_vac == 'A'){ abrir_ventana('imprimir_vacaciones.php','v_imprimir','Imprimir Solicitud',"cod_sol_vac=".$cod_sol_vac); } ?></td>
                  </tr>
                  <tr>
                    <td>
                        <hr>
                        <center><b>Solicitud de Vacaciones Efectuadas</b></center>
                        <?php include('capa_vacaciones.php'); ?>
                    </td>
                  </tr>
                  <?php if ($_COOKIE['uspriv']==2) { ?> 
                  <tr>
                    <td>
					<?php 
						$ncriterios =1; 
						$criterios[0] = "C�dula"; 
						$campos[0] ="ced_per";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } 
					     $funcion_combo = '"valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
                                             echo '<center>Buscar C�dula: '; 

                                             combo('ced_per2', $ced_per3, 'vista_personal', $link, 0, 1, 0, '', 'ced_per', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nombre");?></td>
                  </tr>
                  <?php } ?>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
