<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$post_dias_sol_jus = $_POST["dias_sol_jus"];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'justificativos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "justificativos_per";	// nombre de la tabla
$ncampos = "12";		//numero de campos del formulario
$datos[0] = crear_datos ("cod_sol_jus","Codigo de Solicitud",$_POST['cod_sol_jus'],"0","11","numericos");
$datos[1] = crear_datos ("fch_sol_jus","Fecha de Registro de Solicitud",$_POST['fch_sol_jus'],"1","10","fecha");
$datos[2] = crear_datos ("ced_per","C�dula del Solicitante",$_POST['ced_per'],"1","11","numericos");
$datos[3] = crear_datos ("nom_per","Nombre del Solicitante",$_POST['nom_per'],"1","100","alfabeticos");
$datos[4] = crear_datos ("nom_dep","Departamento",$_POST['nom_dep'],"1","100","alfanumericos");
$datos[5] = crear_datos ("nom_car","Cargo",$_POST['nom_car'],"0","100","alfabeticos");
$datos[6] = crear_datos ("dias_sol_jus","D�as del Permiso",$_POST['dias_sol_jus'],"1","2","numericos");
$datos[7] = crear_datos ("ini_sol_jus","Periodo de Permiso - Fecha de Inicio",$_POST['ini_sol_jus'],"1","10","fecha");
$datos[8] = crear_datos ("fin_sol_jus","Periodo de Permiso - Fecha de Fin",$_POST['fin_sol_jus'],"0","10","fecha");
$datos[9] = crear_datos ("mot_sol_jus","Motivo del Permiso",$_POST['mot_sol_jus'],"1","20","alfanumericos");
$datos[10] = crear_datos ("obs_sol_jus","Observaci�n",$_POST['obs_sol_jus'],"0","255","alfanumericos");
$datos[11] = crear_datos ("apro_sol_jus","Estado de la solicitud",$_POST['apro_sol_jus'],"0","2","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) 
	{ 
	    $tipo = "general";
	    $criterio_buscar = $_POST["criterio"];
	    $valor_buscar = $_POST["buscar_a"];
	    $buscando = busqueda_func($valor_buscar,$criterio_buscar,"$tabla",$pagina,$tipo);
	}
	elseif ($_POST["BuscarInd"]) { 
	$tipo = "individual"; 
	$buscando = busqueda_func($_POST["buscar_a"],"cod_sol_jus","$tabla",$pagina,$tipo);
	} 
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre de Solicitante";
		$datos[0]="nom_per";	
		$parametro[1]="D�as";
		$datos[1]="dias_sol_jus";	
		$parametro[2]="Motivo";
		$datos[2]="mot_sol_jus";	
		$parametro[3]="Desde";
		$datos[3]="ini_sol_jus";
		$parametro[4]="Hasta";
		$datos[4]="fin_sol_jus";
		busqueda_varios(7,$buscando,$datos,$parametro,"cod_sol_jus");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_sol_jus = $row["cod_sol_jus"];
	    $fch_sol_jus = $row["fch_sol_jus"];
	    $ced_per = $row["ced_per"];
	    $nom_per = $row["nom_per"];
	    $nom_dep = $row["nom_dep"];
	    $nom_car = $row["nom_car"];
	    $dias_sol_jus = $row["dias_sol_jus"];
	    $ini_sol_jus = $row["ini_sol_jus"];
	    $fin_sol_jus = $row["fin_sol_jus"];
   	    $mot_sol_jus = $row["mot_sol_jus"];
	    $obs_sol_jus = $row["obs_sol_jus"];
	    $apro_sol_jus = $row["apro_sol_jus"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Fecha_tope_Verificar" || $_POST["confirmar"]=="Fecha_tope_Actualizar") 
{
    if ($_POST['dias_sol_jus']=='Seleccione...'){ 
        echo '<SCRIPT> alert ("Debe Indicar D�as del Permiso"); </SCRIPT>';
        $post_fin_sol_jus ="";
    }
    else {
   	    $post_fin_sol_jus = calculo_fecha ($_POST['ini_sol_jus'],"+",$_POST['dias_sol_jus']-1);
    	if ($post_fin_sol_jus=='1969-12-31'){ $post_fin_sol_jus = $_POST['ini_sol_jus']; }
	    if (! $_POST['ini_sol_jus']) { $post_fin_sol_jus =""; }
	}
	$boton = str_replace("Fecha_tope_","",$_POST["confirmar"]);
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_sol_jus",$_POST["cod_sol_jus"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
    $post_fin_sol_jus = $_POST["fin_sol_jus"];
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
    $post_fin_sol_jus = $_POST["fin_sol_jus"];
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{  
     $post_fin_sol_jus = $_POST["fin_sol_jus"];
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_sol_jus"],"cod_sol_jus",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_sol_jus","Vacaciones Solicitadas",$pagina);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Solicitud de Justificativos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		              <tr>
                        <td class="etiquetas">Fecha: </td>
                        <td>
                            <input name="cod_sol_jus" type="hidden" id="cod_sol_jus" value="<?php if(! $existe) { echo $_POST['cod_sol_jus']; } else { echo $cod_sol_jus; } ?>" size="35" title="Codigo de la solicitud">
                            <input name="fch_sol_jus" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_sol_jus" readonly value="<?php if(! $existe) { if (! $_POST['fch_sol_jus']) { echo date('Y-m-d'); } else { echo $_POST['fch_sol_jus']; } } else { echo $fch_sol_jus; } ?>" size="20" title="Fecha de Solicitud" />
                          <?php if ($boton=='Modificar') { echo $fch_sol_jus; } ?>
			</td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Departamento:</td>
                        <td width="75%">
			<?php if ($nom_dep == "" && $_POST["nom_dep"] == "") { 

			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_dep = $row2['nom_dep'];	
			} ?>
			<input name="nom_dep" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_dep" readonly value="<?php echo $nom_dep; ?>" size="35" title="Departamento">
                        <?php if ($boton=='Modificar') { echo $nom_dep; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Cargo:</td>
                        <td width="75%">
			<?php if ($nom_car == "" && $_POST["nom_car"] == "") { 
			$sql2 = "SELECT c.fch_asg, c.nom_car, d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_car = $row2['nom_car'];			
			$fch_ing = $row2['fch_asg'];		
			} ?>
			<input name="nom_car" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_car" readonly value="<?php echo $nom_car; ?>" size="35" title="Cargo que desempe�a">
                        <?php if ($boton=='Modificar') { echo $nom_car; } ?></td>
                      </tr>    

		      <tr>
                        <td class="etiquetas">C�dula:</td>
                        <td width="75%">
			<?php if ($ced_per == "" && $_POST["ced_per"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$ced_per = $_COOKIE['uscod'];
			} ?>
			<input name="ced_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_per" readonly value="<?php echo $ced_per; ?>" size="35" title="C�dula del solicitante">
                        <?php if ($boton=='Modificar') { echo $ced_per; } ?></td>
                      </tr>
                      

		      <tr>
                        <td class="etiquetas">Nombre:</td>
                        <td width="75%">
			<?php if ($nom_per == "" && $_POST["nom_per"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_per = $_COOKIE['usnombre'];
			} ?>
			<input name="nom_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_per" readonly value="<?php echo $nom_per; ?>" size="35" title="Nombre y Apellido del solicitante">
                        <?php if ($boton=='Modificar') { echo $nom_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Motivos de Justificativo:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="mot_sol_jus" title="Tipo de Vacaciones" >
                          <option>Seleccione...</option>
                          <option value="fuera" '; if ($mot_sol_jus == "fuera" || $_POST['mot_sol_jus'] =="fuera") { echo 'selected'; } echo '>D�a(s) Laborado(s) Fuera de Oficina</option>
                          <option value="personales" '; if ($mot_sol_jus == "personales" || $_POST['mot_sol_jus'] =="personales") { echo 'selected'; } echo '>Otra Situaci�n notificada al Jefe Inmediato</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="mot_sol_jus" id="mot_sol_jus" value="'.$mot_sol_jus.'" >'; 
						    if ($mot_sol_jus == "fuera") { echo 'D�a Laborado Fuera de Oficina'; } 
						    if ($mot_sol_jus == "personales") { echo 'Otra Situaci�n notificada al Jefe Inmediato'; } 
						}?></td>
                      <tr>
                        <td class="etiquetas">D�as del Justificativo:</td>
                        <td><?php if ($boton != "Modificar" && $boton != "Guardar") { 
                        $add_ini = 'onchange="confirmacion_func(';
                        $add_ini .= "'Fecha_tope_".$boton."')";
                        $add_ini .= '"';
                        echo '<select name="dias_sol_jus" title="D�as a disfrutar en el per�odo de vaciones" '.$add_ini.'>
                          <option>Seleccione...</option>';
            $i_ini=1; $i_fin=15;
			for($i=$i_ini;$i<=$i_fin;$i++){
			echo '<option value="'.$i.'"'; if ($dias_sol_jus == $i || $_POST['dias_sol_jus'] == $i) { echo 'selected'; } echo '>'.$i.' d�as </option>';
			}
			echo '</select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="dias_sol_jus" id="dias_sol_jus" value="'.$dias_sol_jus.'" > '.$dias_sol_jus.' d&iacute;as'; 
						}?>
						
						</td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Per�odo del Justificativo: </td>
			<?php if ($ini_sol_jus == "0000-00-00") { $ini_sol_jus="";} ?>
			<?php if ($fin_sol_jus == "0000-00-00") { $fin_sol_jus="";} ?>			
                        <td>del: <input name="ini_sol_jus" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ini_sol_jus" value="<?php if(! $existe) { echo $_POST['ini_sol_jus']; } else { echo $ini_sol_jus; } ?>" size="8" title="Fecha de inicio del per�odo de vacaciones a Disfrutar" onchange="confirmacion_func('Fecha_tope_<?php echo $boton; ?>')"><?php if ($boton=='Modificar') { echo $ini_sol_jus; } ?><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].ini_sol_jus,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?>
&nbsp;&nbsp;&nbsp;al: <input name="fin_sol_jus" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fin_sol_jus" readonly value="<?php if(! $existe) { echo $post_fin_sol_jus; } else { echo $fin_sol_jus; } ?>" size="8" title="Fecha de fin del per�odo de vacaciones a Disfrutar" />
                          <?php if ($boton=='Modificar') { echo $fin_sol_jus; } ?>
			             </td>
                      </tr> 
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td>
                            <?php escribir_campo('obs_sol_jus',$_POST["obs_sol_jus"],$obs_sol_jus,'',255,35,'Observaciones o Informaci�n adicional',$boton,$existe,'')?>
                        </td>
                      </tr>
		      <?php if ($boton!='Verificar' && $boton!='Guardar') { ?>
		      <?php }?>
		      <?php if ($boton!='Verificar' && $boton!='Guardar') { ?>
                      <tr>
                        <td class="etiquetas">Estado:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="apro_sol_jus" title="Estado">
                          <option value="N" '; if ($apro_sol_jus == "N" || $_POST['apro_sol_jus'] =="N" || $apro_sol_jus == "" || $_POST['apro_sol_jus'] =="") { echo 'selected'; } echo '>Seleccione...</option>
                          <option value="A" '; if ($apro_sol_jus == "A" || $_POST['apro_sol_jus'] =="A") { echo 'selected'; } echo '>Aprobada</option>
                          <option value="R" '; if ($apro_sol_jus == "R" || $_POST['apro_sol_jus'] =="R") { echo 'selected'; } echo '>Rechazada</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="apro_sol_jus" id="apro_sol_jus" value="'.$apro_sol_jus.'" >'; 
						    	if (!$apro_sol_jus) { echo 'Por Aprobar'; } 
    					    	if ($apro_sol_jus == "A") { echo 'Aprobada'; } 
    							if ($apro_sol_jus == "R") { echo 'Rechazada'; }
						}?></td>
                      </tr>
		      <?php }?>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php if($boton=='Modificar' && $apro_sol_jus == 'A'){ abrir_ventana('imprimir_jusaciones.php','v_imprimir','Imprimir Solicitud',"cod_sol_jus=".$cod_sol_jus); } ?></td>
                  </tr>
                  <tr>
                    <td>
                        <hr>
                        <center><b>Solicitud de Justificaciones Efectuadas</b></center>
                        <?php include('capa_justificativos.php'); ?>
                    </td>
                  </tr>
                  <?php if ($_COOKIE['uspriv']==2) { ?> 
                  <tr>
                    <td>
					<?php 
						$ncriterios =1; 
						$criterios[0] = "C�dula"; 
						$campos[0] ="ced_per";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } 
					     $funcion_combo = '"valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
                                             echo '<center>Buscar C�dula: '; 

                                             combo('ced_per2', $ced_per3, 'vista_personal', $link, 0, 1, 0, '', 'ced_per', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nombre");?></td>
                  </tr>
                  <?php } ?>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
