<?php 
if ($_GET[frm_egr]) { $frm_egr = $_GET[frm_egr]; }
if ($_GET[rif]) { $rif_pro = $_GET[rif]; }
?>
<table width="100%" border="1" cellpadding="0" cellspacing="0" align="center" id="tabla" name="tabla" class="etiquetas"> 
<tr>
    <td colspan="12" align="center">
        Detalles de los pagos Asociados al Comprobante de Egreso
    </td>
</tr>
<tr id="titulo" name="titulo" class="detalles_egreso">
<td align="center"><b>N�</b></td>
<td align="center"><b>FECHA</b></td>
<td align="center"><b>PAG.</b></td>
<td align="center"><b>PRESUP.</b></td>
<td align="center"><b>CANT. FACT.</b></td>
<td align="center"><b>BASE ISRL</b></td>
<td align="center"><b>BASE IVA</b></td>
<td align="center"><b>MONTO EXENTO</b></td>
<td align="center"><b>TOTAL FACTURAS</b></td>
<td align="center"><b>RET. ISRL</b></td>
<td align="center"><b>RET. IVA</b></td>
<td align="center"><b>TOTAL PAGAR</b></td>
<?php
    $numero = 1;
    $pagar_cheque = 0;
if ($paga_imp==''){
    /// Consultamos los datos de cada pago involucrado
    $sql = "SELECT com.frm_com, com.rif_pro, pag.fch_pag, pag.cod_pag, pag.frm_pag, pag.frm_egr, sum(ppagos.mon_pro_pag) as monto_pago FROM compras com, pagos pag, partidas_pagos ppagos WHERE com.frm_com=pag.frm_com AND pag.cod_pag=ppagos.cod_pag AND rif_pro='".$rif_pro."' AND pag.frm_egr='".$frm_egr."' GROUP BY ppagos.cod_pag";
 }
if ($paga_imp=='iva'){
    /// Consultamos los datos de cada pago involucrado
    $sql = "SELECT com.frm_com, com.rif_pro, pag.fch_pag, pag.cod_pag, pag.frm_pag, pag.frm_egr, sum(ppagos.mon_pro_pag) as monto_pago, pag.frm_egr_iva FROM compras com, pagos pag, partidas_pagos ppagos WHERE com.frm_com=pag.frm_com AND pag.cod_pag=ppagos.cod_pag AND pag.frm_egr_iva='".$frm_egr."' GROUP BY ppagos.cod_pag";
 }
	//echo $sql;
    $sql_res = mysql_query($sql);
    while ($row=mysql_fetch_array($sql_res)){
        $fecha = $row['fch_pag'];
        $fecha = strtotime($fecha);
        $fecha = date("d-m-Y", $fecha);
        $orden_pago = $row['frm_pag'];
        $monto_pago = $row['monto_pago'];
        $cod_pag = $row['cod_pag'];
        $frm_egr = $row['frm_egr'];
        $ord_com = $row['frm_com'];

        //// consultamos el porcentaje de iva aplicado al compromiso
        $sql_3 = "SELECT * from compras where frm_com='$ord_com'";
        $res3 = mysql_fetch_array(mysql_query($sql_3));
        $iva_com = $res3['iva_com'];
        
        //// consultamos el porcentaje de retenciones iva e isrl 
        $sql_3 = "SELECT * from egresos where frm_egr='$frm_egr'";
        $res3 = mysql_fetch_array(mysql_query($sql_3));
        $iva_ret = $res3['ret_iva_egr'];
        $isrl_ret = $res3['ret_isrl_egr'];
        $sin_par_egr = $res3['sin_par_egr'];
        $ded_egr = $res3['ded_egr'];
           
        //// consultamos los datos de las facturas involucradas en cada pago relacionado con el egreso
        $sql_2 = "SELECT count(*) as facts, sum(fpag.mon_fac_pag) as monto, sum(fpag.iva_fac_pag) as iva, sum(fpag.isrl_fac_pag) as isrl FROM pagos pag, facturas_pagos fpag where fpag.cod_pag = ".$cod_pag." AND fpag.cod_pag = pag.cod_pag";
        $res = mysql_fetch_array(mysql_query($sql_2));    
        $mon_iva = redondear(($res["iva"]*$iva_com/100),2,"",".");
        $exento = redondear(($res["monto"]-$res["iva"]),2,"",".");
        $exento = redondear(($exento-$mon_iva),2,"",".");
        //// Calculamos las retenciones
        $mon_iva_ret = redondear(($mon_iva*$iva_ret/100),2,"",".");
        $mon_isrl_ret = redondear(($res["isrl"]*$isrl_ret/100),2,"",".");
        if ($isrl_ret==1) {
            /// cargamos el valor del sustraendo cuando el porcentaje de ISRL sea 1% 
            $sql_val="select * from valores WHERE des_val='SUSTRAC_ISRL'";
      	    $res_val = mysql_query($sql_val);
       	    while ($row_val = mysql_fetch_array($res_val))
            {
               $$row_val['des_val'] = $row_val['val_val'];
            }
            $mon_isrl_ret = redondear(($mon_isrl_ret-$SUSTRAC_ISRL),2,"",".");
        }
        //// si el monto de la retencion es menor a 0 la retencion isrl es 0
        if ($mon_isrl_ret<0){
            $mon_isrl_ret=0;
        }
        
        /// calculamos el neto a pagar
        $pagar = redondear(($res["monto"]-$mon_iva_ret),2,"",".");
        $pagar = redondear(($pagar-$mon_isrl_ret),2,"",".");

		if ($paga_imp){
        $pagar = redondear($mon_iva_ret,2,"",".");
        $pagar = redondear(($pagar-$mon_isrl_ret),2,"",".");
		}
        
	    echo '<tr style="display:visibe" title="'.$row2[9].'"  class="detalles_egreso"><td align="right">'.$numero.'</td>';
	    echo'<td align="center">'.$fecha.'&nbsp;</td>';
	    echo'<td align="center">'.$orden_pago.'&nbsp;</td>';
	    echo'<td align="right">'.redondear($monto_pago,2,".",",").'&nbsp;</td>';
	    echo'<td align="center">'.$res["facts"].'&nbsp;</td>';
	    echo'<td align="right">'.redondear ($res["isrl"],2,".",",").'&nbsp;</td>';
	    echo'<td align="right">'.redondear ($res["iva"],2,".",",").'&nbsp;</td>';
	    echo'<td align="right">'.redondear ($exento,2,".",",").'&nbsp;</td>';
	    echo'<td align="right">'.redondear ($res["monto"],2,".",",").'&nbsp;</td>'; 
	    echo'<td align="right">'.redondear ($mon_isrl_ret,2,".",",").'&nbsp;</td>';
	    echo'<td align="right">'.redondear ($mon_iva_ret,2,".",",").'&nbsp;</td>';
	    echo'<td align="right">'.redondear ($pagar,2,".",",").'&nbsp;</td>';    
    	$numero ++;
    	$pagar_cheque = redondear ($pagar_cheque + $pagar,2,"","."); 
    }                          
    $pagar_cheque = redondear ($pagar_cheque + $sin_par_egr,2,"",".");
    $pagar_cheque = redondear ($pagar_cheque - $ded_egr,2,"",".");
?>
<tr>
    <td colspan="11" align="right">
       SIN IMPUTACI�N PRESUPUESTARIA:
    </td>
    <td align="right">
        <?php echo redondear ($sin_par_egr,2,".",",").'&nbsp;'; ?>
    </td>    
</tr>
<tr>
    <td colspan="11" align="right">
       OTRAS DEDUCCIONES:
    </td>
    <td align="right">
        <?php echo redondear ($ded_egr,2,".",",").'&nbsp;'; ?>
    </td>    
</tr>
<tr>
    <td colspan="11" align="right">
         MONTO DEL CHEQUE A SER EMITIDO:
    </td>
    <td align="right">
        <?php echo redondear ($pagar_cheque,2,".",",").'&nbsp;'; ?>
    </td>    
</tr>
</table>
