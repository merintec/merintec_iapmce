<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Cuentas del Personal</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['ced_per'];

$ced_per = $_GET['ced_per'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'personal_cuentas.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$pagina2 = 'personal_cuentas.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$tabla = "cuentas";	// nombre de la tabla
$ncampos = "5";		//numero de campos del formulario
$datos[0] = crear_datos ("ced_per","Cedula",$_POST['ced_per'],"1","12","numericos");
$datos[1] = crear_datos ("num_cue","Numero de Cuenta",$_POST['num_cue'],"20","20","numericos");
$datos[2] = crear_datos ("tip_cue","Tipo de Cuenta",$_POST['tip_cue'],"1","1","alfanumericos");
$datos[3] = crear_datos ("ban_cue","Banco",$_POST['ban_cue'],"1","100","alfanumericos");
$datos[4] = crear_datos ("fcam_cue","Fecha de Cambio",$_POST['fcam_cue'],"1","11","fecha");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $ced_per = $row["ced_per"];
	    $num_cue = $row["num_cue"];
		$tip_cue = $row["tip_cue"];
	    $fcam_cue = $row["fcam_cue"];
	    $ban_cue = $row["ban_cue"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"ced_per",$_POST["ced_per"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
		$boton = comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',"Cedulas de Cuentas de Personal");
                $boton = comp_exist($datos[1][0],$datos[1][2],$tabla,$boton,'si',"N Cuentas de Personal");
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["ced_per"],"ced_per",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"ced_per","cuentas",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Cuenta Bancaria C.I: <?php echo $viene_val; ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas">C&eacute;dula:</td>
                        <td><input name="ced_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_per" readonly value="<?php if(! $existe) { echo $viene_val; } else { echo $ced_per; } ?>" <?php if ($boton=='Actualizar') { echo "readonly"; } ?> size="29" title="C&eacute;dula del Personal" />
                          <?php if ($boton=='Modificar') { echo $ced_per; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Numero de Cuenta:</td>
                        <td width="75%">
                        <input name="num_cue" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="num_cue" value="<?php if(! $existe) { echo $_POST["num_cue"]; } else { echo $num_cue; } ?>" size="35" maxlength="20" title="N&uacute;mero de cuenta bancaria">
                        <?php if ($boton=='Modificar') { echo $num_cue; } ?></td>
                      </tr>
                      <tr>
					    <td width="25%" class="etiquetas">Tipo de Cuenta:</td>
					    <td>
                        <?php if ($boton != "Modificar") { echo '<select name="tip_cue" title="Tipo de Cuenta">
                          <option>Seleccione...</option>
                          <option value="1" '; if ($tip_cue == "1" || $_POST['tip_cue'] =="1") { echo 'selected'; } echo '>Corriente</option>
                          <option value="2" '; if ($tip_cue == "2" || $_POST['tip_cue'] =="2") { echo 'selected'; } echo '>Ahorro</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tip_cue" id="tip_cue" value="'.$tip_cue.'" >'; 
						    if ($tip_cue == "1") { echo 'Corriente'; } 
							if ($tip_cue == "2") { echo 'Ahorro'; }
						}?></td>
					  </tr>
                      <tr>
                        <td class="etiquetas">Banco:</td>
                        <td><input name="ban_cue" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ban_cue" value="<?php if(! $existe) { echo $_POST["ban_cue"]; } else { echo $ban_cue; } ?>" size="15" title="Banco al que pertenece la cueta" />
                          <?php if ($boton=='Modificar') { echo $ban_cue; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">&Uacute;ltima Modificaci&oacute;n: </td>
                        <td><input name="fcam_cue" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" readonly id="fcam_cue" value="<?php if(! $fcam_cue) { echo date('Y-m-d'); } else { echo date('Y-m-d'); } ?>" size="20" title="Fecha de Cambio/Actualizaci&oacute;n" />
						  <?php if ($boton=='Modificar') { echo $fcam_cue; } ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_cuentas_personal.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
