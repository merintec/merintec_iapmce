<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Codificaci&oacute;n Presupuestaria</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_par_mov'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'presup_part_mov.php?opt='.$_GET["opt"].'&cod_par_mov='.$_GET["cod_par_mov"].'&seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$pagina2 = 'presup_part_mov.php?opt='.$_GET["opt"].'&cod_par_mov='.$_GET["cod_par_mov"].'&seccion='.$_GET["seccion"];
$tabla = "productos_part_mov";	// nombre de la tabla
$ncampos = "5";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_pro_par_mov","Cod. partida en Movimiento",$_POST['cod_pro_par_mov'],"0","11","numericos");
$datos[1] = crear_datos ("cod_par_mov","Cod. movimiento",$_POST['cod_par_mov'],"1","11","numericos");
$datos[2] = crear_datos ("cod_par","Cod. Partidas",$_POST['cod_par'],"1","11","numericos");
$datos[3] = crear_datos ("mon_pro_par_mov","Monto del movimiento de la Partida",$_POST['mon_pro_par_mov'],"1","12","decimal");
$datos[4] = crear_datos ("tip_pro_par_mov","Egreso o Ingreso",$_POST['tip_pro_par_mov'],"1","15","alfanumericos");

if ($_GET['opt']=="asi") { $opt = "Asignación"; }
if ($_GET['opt']=="inc") { $opt = "Incremento"; }
if ($_GET['opt']=="trs") { $opt = "Traslado"; }
if ($_GET['opt']=="reint") { $opt = "Reintegro"; }

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_pro_par_mov = $row["cod_pro_par_mov"];
	    $cod_par_mov = $row["cod_par_mov"];
	    $cod_par = $row["cod_par"];
	    $mon_pro_par_mov = $row["mon_pro_par_mov"];
	    $tip_pro_par_mov = $row["tip_pro_par_mov"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_pro_par_mov",$_POST["cod_pro_par_mov"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_pro_par_mov"],"cod_pro_par_mov",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_pro_par_mov","productos_part_mov",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Partidas Afectadas en <?php echo $opt; ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Partida:
                          </td>
		            <td><input name="cod_pro_par_mov" type="hidden" id="cod_pro_par_mov" value="<?php if (! $existe) { echo $_POST['cod_pro_par_mov']; } else { echo $cod_pro_par_mov; } ?>" title="Codigo de la partida del movimiento de presupuesto">
			<input name="cod_par_mov" type="hidden" id="cod_par_mov" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_par_mov; } ?>" title="Codigo del Movimiento">
                        <?php combo('cod_par', $cod_par, 'part_presup', $link, 0, 5, 6, 9, 'cod_par', "", $boton, "ORDER BY par_par,gen_par,esp_par,sub_par"); ?></td>
                      </tr>
                       <tr>
                        <td width="25%" class="etiquetas">Monto:</td>
                        <td width="75%">
                        <input name="mon_pro_par_mov" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_pro_par_mov" value="<?php if(! $existe) { echo $_POST['mon_pro_par_mov']; } else { echo $mon_pro_par_mov; } ?>" size="10" maxlength="12" title="Monto del Egreso de Salida Partida">
                        <?php if ($boton=='Modificar') { echo $mon_pro_par_mov; } ?></td>
                      </tr>
                      <?php if ($_GET['opt']=="trs") { ?>
                      <tr>
                        <td class="etiquetas">Egreso o Ingreso?:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tip_pro_par_mov" title="Sale o Ingresa?">
                          <option>Seleccione...</option>
                          <option value="Ingreso" '; if ($tip_pro_par_mov == "Ingreso" || $_POST['tip_pro_par_mov'] =="Ingreso") { echo 'selected'; } echo '>Ingreso</option>
                          <option value="Egreso" '; if ($tip_pro_par_mov == "Egreso" || $_POST['tip_pro_par_mov'] =="Egreso") { echo 'selected'; } echo '>Egreso</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="text" name="tip_pro_par_mov" id="tip_pro_par_mov" value="'.$tip_pro_par_mov.'" >'; 
						    if ($tip_pro_par_mov == "Ingreso") { echo 'Ingreso'; } 
							if ($tip_pro_par_mov == "Egreso") { echo 'Egreso'; }
							if ($tip_pro_par_mov == "Reintegro") { echo 'Reintegro'; }
						}?></td>
                      </tr>
                      <?php }
                        if ($_GET['opt']=="asi" || $_GET['opt']=="inc") { ?><tr><td> <input name="tip_pro_par_mov" type="hidden" id="tip_pro_par_mov" value="Ingreso" title="Egreso o Ingreso?"> <?php }
                        if ($_GET['opt']=="reint") { ?><tr><td> <input name="tip_pro_par_mov" type="hidden" id="tip_pro_par_mov" value="Reintegro" title="Reintegro"> <?php }
                      ?></td></tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_presup_partidas.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
