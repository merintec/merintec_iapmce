<?php 
    /// Definici�n del ancho del reporte
    $ancho_rep="700px";
    $ancho_rep_imp="500px";
    include('../comunes/conexion_basedatos.php'); 
    include ('../comunes/formularios_funciones.php'); 
    include ('../comunes/titulos.php'); 
    include ('../comunes/mensajes.php'); 
    $viene_val = $_GET['cod_com'];
    //// obteniendo los datos
    $sql="SELECT * FROM compras, proveedores WHERE cod_com=".$viene_val." AND compras.rif_pro=proveedores.rif_pro";
    $reg = mysql_fetch_array(mysql_query($sql));
    $cod_com = $reg['cod_com'];
    $fch_com = $reg['fch_com'];
    $nor_com = $reg['nor_com'];
    $frm_com = $reg['frm_com'];
    $sol_com = $reg['sol_com'];
    $tip_com = $reg['tip_com'];
    $for_com = $reg['for_com'];
    $adl_com = $reg['adl_com'];
    $fre_com = $reg['fre_com'];
    $mon_com = $reg['mon_com'];
    $rif_pro = $reg['rif_pro'];
    $npr_com = $reg['npr_com'];
    $iva_com = $reg['iva_com'];
    $ela_com = $reg['ela_com'];
    $rev_com = $reg['rev_com'];
    $obs_com = $reg['obs_com'];
    $afavor = $reg['nom_pro'];
    //// Manipulando un poco los datos para presentarlos
    $fecha1 = substr($reg["fch_com"], 8, 2);
	$fecha2 = substr($reg["fch_com"], 5, 2);
	$fecha3 = substr($reg["fch_com"], 0, 4);
	$fecha3b = substr($reg["fch_com"], 2, 2);
    $fecha = $fecha1.'/'.$fecha2.'/'.$fecha3;
	$norden=$fecha2.$fecha3b.$reg[nor_com];
	
	/// Traemos los datos de los productos de la compra
	$prod = 0;
	$exento = 0;
	$base_iva = 0;
	$total = 0;
	$sql = "SELECT * FROM productos_compras WHERE cod_com=".$cod_com;
	$res = mysql_query($sql);
	while ($reg = mysql_fetch_array ($res))
	{
        $datos_prod[$prod][0] = $reg[2];
        $datos_prod[$prod][1] = $reg[3];
        $datos_prod[$prod][2] = $reg[4];
        $datos_prod[$prod][4] = $reg[5];
        $datos_prod[$prod][5] = redondear(($reg[5]*$reg[2]),2,"",".");       
        if ($reg[6]=='SI') 
        { 
            $datos_prod[$prod][3] = 'E'; 
            $exento += $datos_prod[$prod][5]; 
        } 
        else 
        { 
            $datos_prod[$prod][3] = $iva_com; 
            $base_iva +=  $datos_prod[$prod][5]; 
        }
        $prod=$prod+1;
	}
	$monto_iva = redondear(($base_iva * $iva_com),2,"",".");
	$monto_iva = redondear(($monto_iva / 100),2,"",".");

	$total = redondear(($exento + $base_iva + $monto_iva),2,"",".");
	
	/// Traemos los datos de las partidas de la compra
	$part = 0;
	$total_part = 0;
	$sql = "SELECT pp.*,pc.mon_pro_com FROM partidas_compras pc, part_presup pp WHERE cod_com=".$cod_com." AND pc.cod_par=pp.cod_par";
	$res = mysql_query($sql);
	while ($reg = mysql_fetch_array ($res))
	{
        $datos_part[$part][0] = $reg[0];
        $datos_part[$part][1] = $reg[1];
        $datos_part[$part][2] = $reg[2];
        $datos_part[$part][3] = $reg[3];
        $datos_part[$part][4] = $reg[4];
        $datos_part[$part][5] = $reg[5];
        $datos_part[$part][6] = $reg[6];
        $datos_part[$part][7] = $reg[7];
        $datos_part[$part][8] = $reg[8];
        $datos_part[$part][9] = $reg[9];
        $datos_part[$part][10] = $reg[10];
        $datos_part[$part][11] = $reg[11];
        $part=$part+1;
        $total_part += $reg[11];
	}
?>
<html>
    <head>
        <title>Imprimir Orden de Compomiso</title>
        <style type="text/css">
        <!--
            body,td,th {
            	font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 12px;
            }
            .resp_datos {	
                font-size: 15px;
                font-weight: bold;
            }
            .montos_totales {	
                font-size: 14px;
                font-weight: bold;
            }
            #formulario {	
                color: RED;
                font-size: 25px;
                font-weight: bold;
            }
            #control {	
                font-size: 15px;
                font-weight: bold;
            }
            #fecha {	
                font-size: 15px;
                font-weight: bold;
            }
            #titulo {	
                font-size: 18px;
                font-weight: bold;
            }
            #titulos_seq {	
                font-size: 12px;
                font-weight: bold;
            }
            .titulos_cel {	
                font-size: 9px;
                font-weight: bold;
            }
            .datos_cel {	
                font-size: 9px;
            }
            .tabla_borde 
            {
                border:solid 1px #000000;
            }
            #printing {	
            	position:absolute;
            	z-index:1;
            	align: right;
            	top: 10px;
                border: 0px solid #000000;
                width: <?php echo $ancho_rep_imp; ?>;
                text-align: right;
            }
            #pie_merintec {	
                font-size: 6pt;
            }
}
        -->
        </style>
    </head>
    <body>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr>
                <td width="1px"><img src="../imagenes/logo_tn_trn.png" alt="Logo <?php echo $organizacion;  ?>" height="72px" title="Logo <?php echo $organizacion;  ?>"/></td>
                <td width="260px" align="center">REP�BLICA BOLIVARIANA DE VENEZUELA<BR>CONTRALOR�A DEL MUNICIPIO JAUREGUI<BR>LA GRITA - ESTADO TACHIRA<BR>RIF: <?php echo $organizacion_rif; ?></td>
                <td width="1px"><img src="../imagenes/logo_contraloria.png" alt="Logo <?php echo $organizacion;  ?>" height="72px" title="Logo <?php echo $organizacion;  ?>"/></td>
                <td align="right">
                    <span id="formulario">N� <?php echo $frm_com; ?></span><br>
                    N� de Control: <span id="control"><u><?php echo $norden; ?></u></span><br>
                    Fecha: <span id="fecha"><u><?php echo $fecha; ?></u></span><br>
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulo">
                    ORDEN DE COMPROMISO
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr>
                <td width="500px">
                    A FAVOR DE:
                </td>
                <td>
                    RIF: <span class="resp_datos"><?php echo $rif_pro; ?></span>
                </td>
            </tr>
            <tr>
                <td rowspan="2" align="center">
                    <span class="resp_datos"><?php echo $afavor; ?></span>
                </td>
                <td>
                    Consulta de Precios:
                </td>
            </tr>
            <tr>
                <td align="center">
                    <span class="resp_datos"><?php echo $sol_com; ?></span>
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td width="33.33%">
                    Tipo:
                </td>
                <td width="33.33%">
                    Forma de Pago:
                </td>
                <td width="33.33%">
                    Adelanto:
                </td>
            </tr>
            <tr align="center">
                <td>
                    <span class="resp_datos"><?php echo $tip_com; ?></span>
                </td>
                <td>
                    <span class="resp_datos"><?php echo $for_com; ?></span>
                </td>
                <td align="right">
                    <span class="resp_datos">Bs. <?php echo redondear($adl_com,2,".",","); ?></span>
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulos_seq">
                    DETALLES DE LOS PRODUCTOS, SERVICIOS U OTROS
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" height="300px" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000">
            <tr align="center" height="1">
                <td width="20px" class="titulos_cel">CANT.</td>
                <td width="20px"class="titulos_cel">UNID.</td>
                <td class="titulos_cel">DESCRIPCI�N</td>
                <td width="20px" class="titulos_cel">IVA</td>
                <td width="100px" class="titulos_cel">PRECIO<BR>UNITARIO</td>
                <td width="100px" class="titulos_cel">PRECIO TOTAL</td>
            </tr>
            <?php
                for ($i=0;$i<$prod;$i++)
                {
                    echo '<tr align="center" height="1">
                    <td class="datos_cel" align="right">'.$datos_prod[$i][0].'&nbsp;</td>
                    <td class="datos_cel" align="right">'.$datos_prod[$i][1].'&nbsp;</td>
                    <td class="datos_cel" align="left">'.$datos_prod[$i][2].'</td>
                    <td class="datos_cel">'.$datos_prod[$i][3].'</td>
                    <td class="datos_cel" align="right">'.redondear($datos_prod[$i][4],2,".",",").'&nbsp;</td>
                    <td class="datos_cel" align="right">'.redondear($datos_prod[$i][5],2,".",",").'&nbsp;</td>
                    </tr>'; 
                }
            ?>
            <tr align="center">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr align="center" height="1">
                <td colspan="5" align="right" class="montos_totales">Monto Exento&nbsp;</td>
                <td align="right" class="montos_totales"><?php echo redondear($exento,2,".",","); ?>&nbsp;</td>
            </tr>
            <tr align="center" height="1">
                <td colspan="5" align="right" class="montos_totales">Base Imponible&nbsp;</td>
                <td align="right" class="montos_totales"><?php echo redondear($base_iva,2,".",","); ?>&nbsp;</td>
            </tr>     
            <tr align="center" height="1">
                <td colspan="5" align="right" class="montos_totales">IVA SEG�N ALICUOTA <U>&nbsp;&nbsp;<?php echo $iva_com; ?>&nbsp;&nbsp;</U> %&nbsp;</td>
                <td align="right" class="montos_totales"><?php echo redondear($monto_iva,2,".",","); ?>&nbsp;</td>
            </tr>          
            <tr align="center" height="1">
                <td colspan="5" align="right" class="montos_totales">TOTAL GENERAL&nbsp;</td>
                <td align="right" class="montos_totales"><?php echo redondear($total,2,".",","); ?>&nbsp;</td>
            </tr>            
            
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulos_seq">
                    <BR>CONTROL PRESUPUESTARIO
                </td>
            </tr>
        </table>
         <table width="<?php echo $ancho_rep; ?>" height="200px" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000">
            <tr align="center" height="1">
                <td width="50px" class="titulos_cel">SEC.</td>
                <td width="50px"class="titulos_cel">PROG.</td>
                <td width="50px" class="titulos_cel">PRO</td>
                <td width="50px" class="titulos_cel">ACT.</td>
                <td width="50px"  class="titulos_cel">PAR.</td>
                <td width="50px" class="titulos_cel">GEN.</td>                
                <td width="50px" class="titulos_cel">ESP.</td>                
                <td width="50px" class="titulos_cel">SUB-ESP.</td>
                <td class="titulos_cel">MONTO</td>
            </tr>
            <?php
                for ($i=0;$i<$part;$i++)
                {
                    echo '<tr align="center" height="1" title="'.$datos_part[$i][9].'">
                    <td class="datos_cel">&nbsp;</td>
                    <td class="datos_cel">&nbsp;</td>
                    <td class="datos_cel">&nbsp;</td>
                    <td class="datos_cel">&nbsp;</td>
                    <td class="datos_cel">'.$datos_part[$i][5].'&nbsp;</td>
                    <td class="datos_cel">'.$datos_part[$i][6].'&nbsp;</td>
                    <td class="datos_cel">'.$datos_part[$i][7].'</td>
                    <td class="datos_cel">'.$datos_part[$i][8].'</td>
                    <td class="datos_cel" align="right">'.redondear($datos_part[$i][11],2,".",",").'&nbsp;</td>
                    </tr>'; 
                }
            ?>
            <tr align="center">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr align="center" height="1">
                <td COLSPAN="8" align="right" class="montos_totales">TOTAL EN PARTIDAS&nbsp;</td>
                <td align="right" class="montos_totales"><?php echo redondear($total_part,2,".",","); ?>&nbsp;</td>
            </tr>
        </table>
        <br>       
         <table width="<?php echo $ancho_rep; ?>" height="130px" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000">
            <tr align="center" height="1">
                <td id="titulos_seq" align="left" width="50%">
                    &nbsp;Elaborado: <?php echo $ela_com; ?>
                </td>
                <td id="titulos_seq" align="left" width="50%">
                    &nbsp;Aprobado: <?php echo $rev_com; ?>
                </td>
            </tr>
            <tr align="center">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr align="center" height="1">
                <td id="titulos_seq" align="left">
                    &nbsp;Fecha:
                </td>
                <td id="titulos_seq" align="left">
                    &nbsp;Fecha:
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td>
                    <?php echo $pie_ord; ?>
                </td>
            </tr>
        </table>
    </body>
</html>
<div id="printing">
<input type="image" src="../imagenes/imprimir.png" id="imprimir" name="imprimir" value="imprimir" title="<?php echo $msg_compromiso_imprimir; ?>" onclick="this.style.visibility='hidden'; window.print();"></div>
