<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Asignaciones del Personal</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['ced_per'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'personal_asignaciones.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$pagina2 = 'personal_asignaciones.php?ced_per='.$_GET["ced_per"].'&seccion='.$_GET["seccion"];
$tabla = "asignaciones";	// nombre de la tabla
$ncampos = "12";			//numero de campos del formulario
$con_cnp=$_POST['con_cnp'];
$ncuo_cnp=$_POST['ncuo_cnp'];
$per_cnp=$_POST['per_cnp='];
$ced_per=$_GET['ced_per'];
$mcuo_cnp=$_POST['mcuo_cnp'];
$por_cnp = $_POST["por_cnp"];
$bpor_cnp = $_POST["bpor_cnp"];
$per_cnp = $_POST["per_cnp"];
$cod_con = $_POST["cod_con"];
$cod_car = $_POST["cod_car"];
$fch_cnp = $_POST["fch_cnp"];
$ncp_cnp = $_POST["ncp_cnp"];
$des_cnp = $_POST["des_cnp"];


$datos[0] = crear_datos ("con_cnp","Concepto",$_POST['con_cnp'],"1","100","alfanumericos");
$datos[1] = crear_datos ("ncuo_cnp","Numero de Cuotas",$_POST['ncuo_cnp'],"0","2","numericos");
$datos[2] = crear_datos ("per_cnp","Asignacion Permanente",$_POST['per_cnp'],"0","1","alfabeticos");
$datos[3] = crear_datos ("mcuo_cnp","Monto de Cuotas",$_POST['mcuo_cnp'],"0","11","decimal");
$datos[4] = crear_datos ("por_cnp","Porcentaje de deduccion",$_POST['por_cnp'],"0","2","numericos");
$datos[5] = crear_datos ("bpor_cnp","Base del porcentaje",$_POST['bpor_cnp'],"0","1","alfabeticos");
$datos[6] = crear_datos ("cod_con","Tipo de Descuento",$_POST['cod_con'],"1","11","numericos");
$datos[7] = crear_datos ("ced_per","Cedula",$_POST['ced_per'],"1","12","numericos");
$datos[8] = crear_datos ("cod_car","Cargo",$_POST['cod_car'],"1","11","numericos");
$datos[9] = crear_datos ("fch_cnp","Fecha de Registro",$_POST['fch_cnp'],"1","10","fecha");
$datos[10] = crear_datos ("ncp_cnp","Cuotas Pagadas",$_POST['ncp_cnp'],"0","2","numericos");
$datos[11] = crear_datos ("des_cnp","Aplica para Salario Integral",$_POST['des_cnp'],"0","1","alfabeticos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Concepto";
		$datos[0]="con_cnp";
		$parametro[1]="Numero de Cuotas";
		$datos[1]="ncuo_cnp";
		$parametro[2]="Monto de Cuotas";
		$datos[2]="mcuo_cnp";
		$parametro[3]="Cedula";
		$datos[3]="ced_per";
		busqueda_varios(6,$buscando,$datos,$parametro,"cod_cnp");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_cnp = $row["cod_cnp"];
	    $con_cnp = $row["con_cnp"];
	    $ncuo_cnp = $row["ncuo_cnp"];
	    $mcuo_cnp = $row["mcuo_cnp"];
		$por_cnp = $row["por_cnp"];
		$bpor_cnp = $row["bpor_cnp"];
	    $per_cnp = $row["per_cnp"];
	    $cod_con = $row["cod_con"];
	    $ced_per = $row["ced_per"];
		$cod_car = $row["cod_car"];
	    $fch_cnp = $row["fch_cnp"];
	    $ncp_cnp = $row["ncp_cnp"];
	    $des_cnp = $row["des_cnp"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_cnp",$_POST["cod_cnp"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[7][0],$datos[7][2],'personal',$boton,'no','Personal');
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_cnp"],"cod_cnp",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_cnp","asignaciones",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Asignaciones C.I: <?php echo $viene_val; ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas">Concesi&oacute;n:</td>
                        <td><?php
			combo('cod_con', $cod_con, 'concesiones', $link, 0, 0, 1, "", 'cod_con', '', $boton,''); ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">C&eacute;dula:</td>
                        <td><input name="ced_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_per" readonly value="<?php if(! $existe) { echo $viene_val; } else { echo $ced_per; } ?>" <?php if ($boton=='Actualizar') { echo "readonly"; } ?> size="29" title="C&eacute;dula del Personal" />
                          <?php if ($boton=='Modificar') { echo $ced_per; } ?></td>
                      </tr>
					  <tr>
                        <td class="etiquetas">Cargo:</td>
                        <td><?php
			combo('cod_car', $cod_car, 'cargos', $link, 0, 1, 2, "", 'cod_car', '', $boton,'WHERE ced_per='.$viene_val); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Concepto:</td>
                        <td width="75%"><input name="cod_cnp" type="hidden" id="cod_cnp" value="<?php if(! $existe) { echo $_POST["cod_cnp"]; } else { echo $cod_cnp; } ?>" size="35" />
                        <input name="con_cnp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="con_cnp" value="<?php if(! $existe) { echo $_POST["con_cnp"]; } else { echo $con_cnp; } ?>" size="35" title="Concepto del descuento">
                        <?php if ($boton=='Modificar') { echo $con_cnp; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">N&ordm; de Cuotas: </td>
                        <td><input name="ncuo_cnp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ncuo_cnp" value="<?php if(! $existe) { echo $_POST["ncuo_cnp"]; } else { echo $ncuo_cnp; } ?>" size="15" title="Numero de cuotas a pagar" />
                          <?php if ($boton=='Modificar') { echo $ncuo_cnp; } ?></td>
                      </tr>
					  <tr>
                        <td class="etiquetas">Porcentaje de Asignaci&oacute;n </td>
                        <td><input name="por_cnp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="por_cnp" value="<?php if(! $existe) { echo $_POST["por_cnp"]; } else { echo $por_cnp; } ?>" size="2" maxlength="3" title="Porcentaje de la Asignaci&oacute;n" onkeypress=valor_acampo(0,"mcuo_cnp")> 
                          <?php if ($boton=='Modificar') { echo $por_cnp.'%'; } ?> 
						 <?php if ($boton != "Modificar") { echo '<class="etiquetas">Base: <select name="bpor_cnp" title="Base del porcentaje" onchange=valor_acampo(0,"mcuo_cnp")>
                          <option value="">Seleccione...</option>
                          <option value="B" '; if ($bpor_cnp == "B" || $_POST['bpor_cnp'] =="B") { echo 'selected'; } echo '>Sueldo Base</option>
                          <option value="U" '; if ($bpor_cnp == "UT" || $_POST['bpor_cnp'] =="UT") { echo 'selected'; } echo '>Unidad Tributaria</option>
                        </select></class>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="bpor_cnp" id="bpor_cnp" value="'.$bpor_cnp.'" >'; 
						    if ($bpor_cnp == "B") { echo 'del Sueldo Base'; } 
							if ($bpor_cnp == "U") { echo 'de la Utidad Tributaria'; }
						}?></td>
                      </tr>
		      <tr><td class="etiquetas">Permanente: </td><td><input name="per_cnp" id="per_cnp" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$per_cnp.'" '; } else { echo 'type="checkbox" value="S"'; } if($_POST["per_cnp"]=='S' || $per_cnp == 'S') { echo 'checked'; } ?> title="Marcar si el Descuento es Permanente" onclick=valor_acampo(1,"ncuo_cnp")>
                          <?php if ($boton=='Modificar') { if ($per_cnp=='S') {echo 'SI'; } else { echo 'NO'; } } ?>
			</td></tr>
                      <tr>
                        <td class="etiquetas">Monto de cuotas (Bs): </td>
                        <td><input name="mcuo_cnp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mcuo_cnp" value="<?php if(! $existe) { echo $_POST["mcuo_cnp"]; } else { echo $mcuo_cnp; } ?>" size="15" title="Monto de cuotas a pagar" />
                          <?php if ($boton=='Modificar') { echo $mcuo_cnp; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Registro: </td>
                        <td><input name="fch_cnp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_cnp" value="<?php if(! $existe) { echo $_POST["fch_cnp"]; } else { echo $fch_cnp; } ?>" size="15" title="Fecha de Registro de la Deducción" />
                          <?php if ($boton=='Modificar') { echo $fch_cnp; } ?><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_cnp,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">N&ordm; de Cuotas Pagadas: </td>
                        <td><input name="ncp_cnp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>"  id="ncp_cnp" value="<?php if(! $existe) { echo $_POST["ncp_cnp"]; } else { echo $ncp_cnp; } ?>" size="15" title="Numero de cuotas Pagadas" />
                          <?php if ($boton=='Modificar') { echo $ncp_cnp; } ?></td>
                      </tr>
		      <tr><td class="etiquetas">Aplica para Salario Integral: </td><td><input name="des_cnp" id="des_cnp" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$des_cnp.'" '; } else { echo 'type="checkbox" value="S"'; } if($_POST["des_cnp"]=='S' || $des_cnp == 'S') { echo 'checked'; } ?> title="Marcar si Aplica para Salario Integral" onclick=valor_acampo(1,"ncuo_cnp")>
                          <?php if ($boton=='Modificar') { if ($des_cnp=='S') {echo 'SI'; } else { echo 'NO'; } } ?>
			</td></tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_asignaciones_personal.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
