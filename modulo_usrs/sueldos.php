<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'sueldos.php?seccion='.$_GET["seccion"];
$tabla = "sueldos";	// nombre de la tabla
$ncampos = "2";		//numero de campos del formulario
$datos[0] = crear_datos ("mon_sue","Monto",$_POST['mon_sue'],"1","11","decimal");
$datos[1] = crear_datos ("des_sue","Descripción",$_POST['des_sue'],"1","50","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Monto";
		$datos[0]="mon_sue";
		$parametro[1]="Descripción";
		$datos[1]="des_sue";
		busqueda_varios(4,$buscando,$datos,$parametro,"cod_sue");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_sue = $row["cod_sue"];
	    $mon_sue = $row["mon_sue"];
	    $des_sue = $row["des_sue"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{	$boton = "Actualizar";
	$boton = comp_exist($datos[1][0],$datos[1][2]."' AND cod_sue <> '".$_POST['cod_sue'],$tabla,$boton,'si',$_GET["nom_sec"]);
	if ($boton == "Actualizar")
	{ $validacion = validando_campos ($ncampos,$datos); }
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_sue",$_POST["cod_sue"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[1][0],$datos[1][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_sue"],"cod_sue",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de Sueldos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">Descripcion:</td>
                        <td width="75%">
			<input name="cod_sue" type="hidden" id="cod_sue" value="<?php if(! $existe) { echo $_POST["cod_sue"]; } else { echo $cod_sue; } ?>" size="35" title="Codigo de Sueldo">
                        <input name="des_sue" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="des_sue" value="<?php if(! $existe) { echo $_POST['des_sue']; } else { echo $des_sue; } ?>" size="35" title="Descripción del Sueldo">
                        <?php if ($boton=='Modificar') { echo $des_sue; } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Monto:</td>
                        <td width="75%">
                        <input name="mon_sue" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_sue" value="<?php if(! $existe) { echo $_POST['mon_sue']; } else { echo $mon_sue; } ?>" size="35" title="Monto del Sueldo">
                        <?php if ($boton=='Modificar') { echo $mon_sue; } ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
			<?php 
			$ncriterios =2;
			$criterios[0] = "Descripción";
			$campos[0] = "des_sue";
			$criterios[1] = "Monto"; 
			$campos[1] ="mon_sue";
			if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {					
			crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
