<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'cargos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "cargos";	// nombre de la tabla
$ncampos = "6";		//numero de campos del formulario
if (! $_POST['num_car'] && $_POST['cod_dep'] ) { 
    $sql = "SELECT MAX(num_car) AS num_car FROM cargos WHERE cod_dep = '".$_POST['cod_dep']."'";  
    $res = mysql_fetch_array(mysql_query($sql));
    $num_car = $res['num_car'] + 1;
}
else { $num_car = $_POST['num_car']; }
$datos[0] = crear_datos ("num_car","Numero",$num_car,"0","3","numericos");
$datos[1] = crear_datos ("nom_car","Nombre",$_POST['nom_car'],"1","100","alfanumericos");
$datos[2] = crear_datos ("cod_sue","Sueldo",$_POST['cod_sue'],"1","2","numericos");
$datos[3] = crear_datos ("est_car","Estado",$_POST['est_car'],"0","1","alfabeticos");
$datos[4] = crear_datos ("cod_tcar","Tipo",$_POST['cod_tcar'],"1","3","numericos");
$datos[5] = crear_datos ("cod_dep","C�digo de la Dependencia",$_POST['cod_dep'],"1","5","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Dep";
		$datos[0]="cod_dep";
		$parametro[1]="numero";
		$datos[1]="num_car";
		$parametro[2]="Nombre";
		$datos[2]="nom_car";
		busqueda_varios(5,$buscando,$datos,$parametro,"cod_car");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_car = $row["cod_car"];
	    $num_car = $row["num_car"];
	    $nom_car = $row["nom_car"];
	    $cod_sue = $row["cod_sue"];
	    $est_car = $row["est_car"];
		$cod_tcar = $row["cod_tcar"];
	    $cod_dep = $row["cod_dep"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_car",$_POST["cod_car"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_car"],"cod_car",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de la Solicitud</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">Nombre:</td>
                        <td width="75%">
			<input name="cod_car" type="hidden" id="cod_car" value="<?php if(! $existe) { echo $_POST["cod_car"]; } else { echo $cod_car; } ?>" size="35" title="Codigo del Cargo">
			<input name="num_car" type="hidden" id="num_car" value="<?php if(! $existe) { echo $_POST["num_car"]; } else { echo $num_car; } ?>" size="35" title="N�mero del Cargo">
                        <input name="nom_car" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_car" value="<?php if(! $existe) { echo $_POST["nom_car"]; } else { echo $nom_car; } ?>" size="35" title="Nombre del Cargo">
                        <?php if ($boton=='Modificar') { echo $nom_car; } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Sueldo:</td>
                        <td width="75%">
                        <?php combo('cod_sue', $cod_sue, 'sueldos', $link, 0, 2, 1, "", 'cod_sue', "", $boton, ""); ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Estado:</td>
                        <td width="75%">
			<?php if ($est_car == "D" || $_POST["est_car"] == "D") { $est_car_out = "Disponible";} ?>
			<?php if ($est_car == "" || $_POST["est_car"] == "") { $est_car_out = "Disponible";} ?>
			<?php if ($est_car == "A" || $_POST["est_car"] == "A") { $est_car_out = "Asignado";} ?>
                        <input name="est_car" type="hidden" id="est_car" readonly value="<?php if(! $existe) { echo $_POST["est_car"]; } else { echo $est_car; } ?>" size="35" title="Estado del Cargo">
			<input name="est_car_out" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="est_car_out" readonly value="<?php echo $est_car_out; ?>" size="35" title="Estado del Cargo">
                        <?php if ($boton=='Modificar') { echo $est_car_out; } ?></td>
                      </tr>
		      <tr>
		        <td class="etiquetas">Tipo:</td>
		        <td><?php combo('cod_tcar', $cod_tcar, 'tipos_cargos', $link, 0, 0, 1, "", 'cod_tcar', "", $boton,""); ?></td>
		        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
			<?php 
			$ncriterios =3;
			$criterios[0] = "Nombre";
			$campos[0] = "nom_car";
			$criterios[1] = "Estado"; 
			$campos[1] ="est_car";
			$criterios[2] = "Codigo Dep.";
			$campos[2] = "cod_dep";	
			if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {					
			crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
		  <tr>
		    <td>
			<?php crear_capa('cargos','capa_cargos.php',481,200,'visible',0); ?>
		    </td>
		  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
