<?php
// Menu
$msg_men_salir = 'Haga click para finalizar la Sesi�n del usuario: '.$_COOKIE["usnombre"]; 
$msg_men_psw = 'Cambiar contrase�a propia'; 
$msg_men_adm = 'Cambiar a la Secci&oacute;n de Administraci�n del Sistema'; 
$msg_men_usr = 'Cambiar a la Secci&oacute;n de Manejo'; 
$msg_men_help = 'Descargar Archivo PDF del Manual de Usuario'; 
// Mensajes
$msg_no_server = "Error: No se ha logrado conectar al Servidor de Datos.".'\n'."Es posible que aun no se hayan configurado los datos de conexi�n. ".'\n'."Consulte el Manual del sistema o contacte al Administrador del Sistema.";
$msg_no_db = "Error: No se ha logrado conectar a la Base de Datos.".'\n'."Es posible que aun no se haya instalado la base de datos del sistema. ".'\n'."Consulte el Manual del sistema o contacte al Administrador del Sistema.";
$msg_db_exist = "La Base de datos ya se encuentra creada en le servidor.".'\n\n'."Por Favor Contacte al Administrador del Sistema.";
$msg_db_creada = "Base de datos creada.".'\n'."Estructura y datos importados.".'\n\n'."Proceso de Instalaci�n finalizado Exitosamente.";
$msg_db_nocreada = "La Base de datos NO ha sido creada.".'\n\n'."Por Favor Contacte al Administrador del Sistema.";
$msg_db_archivo = "Debe especificar un archivo sql v�lido.".'\n\n'."Por Favor vuelva a intentar.";
$inactividad = "La sesi�n ha expirado por inactividad. ".'\n\n'."Ser�s dirigido a la pagina de incio de sesi�n.";
$inactividad_capa = "La sesi�n ha expirado por inactividad. ".'\n\n'."La ventana ser� cerrada.";

$msg_usr_noidentificado = 'Usuario no identificado.<br><br> Debe iniciar Sesi&oacute;n';
$msg_usr_noidentificado_alert = 'Usuario no identificado.\n\nDebe iniciar Sesi�n';
$msg_usr_invalido = 'Lo sentimos el Usuario: <span class="inicio_mensaje_datos">'.$_POST[userlog].'</span> no se encuentra Registrado';
$msg_usr_invalido_alert = 'Lo sentimos el Usuario: '.$_POST[userlog].' no se encuentra Registrado';
$msg_usr_suspencion = 'Haz alcanzado el n&uacute;mero m&aacute;ximo de intentos fallidos.<br> Se ha bloqueado tu cuenta: <span class="inicio_mensaje_datos">'.$_POST[userlog].'</span>.<br> Contacta al administrador para m&aacute;s Informaci&oacute;n ';
$msg_usr_suspencion_alert = 'Haz alcanzado el n�mero m�ximo de intentos fallidos.\n Se ha bloqueado tu cuenta: '.$_POST[userlog].'.\n\nContacta al administrador para m�s Informaci�n.';
$msg_usr_suspendido = 'El Usuario: <span class="inicio_mensaje_datos">'.$_POST[userlog].'</span> est� Suspendido. <br>Por favor contacte al Administrador.';
$msg_usr_suspendido_alert = 'El Usuario: '.$_POST[userlog].' est� Suspendido. \n\nPor favor contacte al Administrador.';
$msg_usr_suspendido_porcambiodeclave_alert = 'El Usuario: '.$_POST[userlog].' ha sido Suspendido. \nPor expiraci�n de contrase�a.\n\nPor favor contacte al Administrador.';
$msg_usr_suspendido_porcambiodeclave = 'El Usuario: <span class="inicio_mensaje_datos">'.$_POST[userlog].'</span> ha sido Suspendido. <br>Por expiraci�n de contrase�a. <br>Por favor contacte al Administrador.';
$msg_paswd_invalida = 'Contrase�a Incorrecta para el usuario <span class="inicio_mensaje_datos">'.$_POST[userlog].'</span>. <br>Intente Nuevamente. ';
$msg_paswd_invalida_alert = 'Contrase�a Incorrecta para el usuario '.$_POST[userlog].'. \n\nIntente Nuevamente.';
$msg_iniciar_sesion = 'Haga click para ir a la pantalla de inicio de sesi&oacute;n';
$msg_cerrar_sesion = 'Haga click para finalizar la Sesi�n del usuario: '.$_COOKIE["usnombre"]; 
$msg_inicio_sincampos = 'Introduzca sus datos y presione Entrar';
$msg_inicio_sincampos_alert = 'Debe especificar un nombre de Usuario y una Contrase�a. \n\n Intente Nuevamente.';
$msg_cambio_pass_alert = 'Debes hacer cambio de Contrase�a. Te quedan: ';
$msg_cambio_pass_alert2 = ' d�as. \n\n Ve a la secci�n de cambio de contrase�a.';
$msg_cambio_pass_camb = 'La contrase�a actual no es correcta';
$msg_cambio_pass_camb2 = 'La nueva contrase�a y la de confirmacion no son iguales';
$msg_datos_en_blanco = 'Hay datos en blanco, debe llenarlos para efectuar los cambios';
// Botones
$msg_btn_entrar = "Hacer click aqu� para iniciar sesi&oacute;n.";
$msg_btn_buscar = "Hacer click aqu� para iniciar b&uacute;squeda.";
$msg_btn_["Verificar"] = "Hacer click aqu� para iniciar verificaci&oacute;n de los datos.";
$msg_btn_["Modificar"] = "Hacer click aqu� para iniciar modificaci&oacute;n de los datos.";
$msg_btn_["Guardar"] = "Hacer click aqu� para guardar los datos actuales.";
$msg_btn_["Actualizar"] = "Hacer click aqu� para efectuarla actualizaci&oacute;n.";
$msg_btn_["Eliminar"] = "Hacer click aqu� para eliminar el registro actual.";
$msg_btn_criterio = "Hacer click aqu� para seleccionar el criterio de b&uacute;squeda.";
$msg_btn_cerrarV = "Hacer click aqu� para cerrar esta ventana"; 
$msg_btn_nuevo = "Haga Click para preparar el formulario para un Nuevo Registro";

// Campos
$msg_campo_userlog = "Introduzca el nombre de usuario para iniciar sesi�n.";
$msg_campo_userpass = "Introduzca la contrase�a para iniciar sesi�n.";
$msg_campo_busqueda = "Introduzca la palabra que desea buscar.";

// B�squeda
$msg_busqueda_vacia = "No se han especificado datos para la b�squeda.";
$msg_busqueda_sinresultado = "No existen resultados para la busqueda.";
$msg_busqueda_resultadosmul = "Existe m�s de un resultados para la busqueda.";
$msg_busqueda_ver = "Buscar este registro";
$msg_busqueda_modificar = "Modificar este registro";
$msg_busqueda_eliminar = "Eliminar este registro"; 
$msg_busqueda_dos_fechas_alert = "Debe suministrar Fecha de Inicio y Fecha de Fin";


// Eliminaci�n
$msg_elim_exito = "El registro se ha eliminado exitosamente.";
$msg_elim_noexito = "No se pudo eliminar la informaci�n.";

// Insersi�n
$msg_ins_exito = "La informaci�n se almacen� exitosamente.";

// Modificaci�n
$msg_mod_exito = "La informaci�n se actualiz� correctamente.";
$msg_mod_noexito = "No se pudo actualizar la informaci�n.";

// Comprobaci�n de existencia
$msg_existe_si = "El registro ya existe. Intente Nuevamente";
$msg_existe_no = "El registro no existe. Intente Nuevamente";

// Comprobaci�n de registros en de existencia
$msg_existe_max = "Ha alcanzado el numero maximo de registros para:";

// Nomina
$msg_falt_dat_nomina="Se necesitan mas datos para generar o consultar la nomina (Mes, A�o y Porcion)";
$msg_nomina_ya_pagada="Esta nomina ya fue procesada, verifiquelo consultando la nomina";
$msg_nomina_no_pagada="Esta nomina no ha sido procesada, verifiquelo consultando la nomina";
$msg_txt_procesado = "Este archivo ya fue procesado.";
$msg_txt_procesado_alert = "Este archivo ya fue procesado.".'\n\n'."Si desea puede descargar una copia del archivo.";
$msg_procesado_cancel_alert = "La operaci�n ha sido Cancelada.";

// Pre-Nomina
$msg_falt_dat_prenomina="Se necesitan mas datos para generar o consultar la prenomina (Mes, A�o y Porcion)";
$msg_prenomina_ya_pagada="Esta prenomina ya fue procesada, verifiquelo consultando la prenomina";
$msg_prenomina_no_pagada="Esta prenomina no ha sido procesada, verifiquelo consultando la prenomina";

// Incidencias
$msg_falt_dat_incidencias="Se necesitan mas datos para generar o consultar incidencias ".'\n'."(Todos los campos son requeridos)";
$msg_incidencias_ya_registrada="Esta incidencia ya fue registrada, verifiquelo consultando la incidencia.";
$msg_incidencias_no_registrada="Esta incidencia no ha sido registrada, verifique los datos suministrados.";

// Pie de Pagina
$msg_pie = "Luis M&aacute;rquez | Juan M&aacute;rquez";
$msg_pie2 = "A.C. MERINTEC RL";
$msg_pie3 = 'Vis�tenos en: <strong><a href="http://www.merintec.com.ve" target="_blank">http://www.merintec.com.ve</a></strong>';

// Medicinas
$msg_med_max = "El monto solicitado no puede superar el monto anual: ";
$msg_med_exe_pag_alert = "El monto solicitado supera el monto disponible de: ";
$msg_med_exe_alert = "Ya se encuentra un registro en �ste pago para el personal: ";

// Compromiso 
$msg_partida_nodisp = "ERROR: Monto Mayor al Disponible en la Partida";
$msg_modificar_no = "No es permitido Modificar el Compromiso. ".'\n'."Debido a que ya tiene una Orden de Pago Registrada.";
$msg_eliminar_no = "No es permitido Eliminar el Compromiso. ".'\n'."Debido a que ya tiene una Orden de Pago Registrada.";
$msg_productos_mayor = "El Monto total de los productos no puede superar al monto registrado como total del compromiso.".'\n';
$msg_productos_mayor2 = "".'\n\n'."Por favor verifique los datos suministrados.";
$msg_partidas_mayor = "El Monto total de las partidas no puede superar al monto registrado como total del compromiso.".'\n';
$msg_partidas_mayor2 = "".'\n\n'."Por favor verifique los datos suministrados.";
$msg_compromiso_imprimir = "Imprimir Orden de Compromiso Actual";

// Ordenes de Pago 
$msg_pago_mayor = "Monto total de los pagos excede el compromiso";
$msg_pago_mayor2 = "".'\n\n'."Por favor verifique los datos suministrados.";
$msg_pago_modificar_no = "No es permitido Modificar el Pago. ".'\n'."Debido a que ya tiene un Comprobante de Egreso Registrado.";
$msg_pago_eliminar_no = "No es permitido Eliminar el Compromiso. ".'\n'."Debido a que ya tiene un Comprobante de Egreso Registrado.";
$msg_pago_cambiofrm = "Si se desea cambiar el n�mero de formulario debe ser el proximo disponible.";
$msg_pago_fac_mayor = "Monto total de las facturas excede el monto del Pago.";
$msg_pago_part_mayor = "La suma total de las partidas excede el Monto Total del Pago.";
$msg_pago_part_mayor_compromiso = "El monto de la partida en este pago supera el monto comprometido.";
$msg_pago_part_mayor_totpag = "Esta partida ya cuenta con otro(s) pago(s) relacionados al compromiso.".'\n'."Solo restan por pagar: ";
$msg_pago_imprimir = "Imprimir Orden de Pago Actual";

// Comprobante de Egreso
$msg_egreso_imprimir = "Imprimir Comprobante de Egreso Actual";

// DATOS de Autoridades
$CONTRALOR_TIT = "DIRECTOR GENERAL";
$CONTRALOR = "JOSE OLINTO VALERO LEON";
$CONTRALOR_CI = "V-10.107.709";

$ADMIN_TIT = "DIR. RECURSOS HUMANOS";
$ADMIN = "LCDA. LUBBY NATHALY SALAS LACRUZ";
$ADMIN_CI = "V-11.319.912";

// Reportes
$msg_pie_carta ='
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
    <tr>
        <td align="center"><hr>
        </td>
    </tr>
    <tr>
        <td align="center"><font size="1px">
            <b>Direcci�n:</b> Final Prolongaci�n Calle Via La Vega. Ejido, Estado M�rida<br>
            <b>Tel�fono:</b> 0274-221xxxx  <b>e-mail:</b> <a href="mailto:xxxx@xxxmail.com">xxxx@xxxmail.com</a> </font>
        </td>
    </tr>
</table>';
$msg_pie_reporte='

<br><br><br><br>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#000000">
    <tr>
    <td width="50%" align="center"><hr width="70%">'.$CONTRALOR.'<BR>'.$CONTRALOR_TIT.'</td>

    <td width="50%" align="center"><hr width="70%">'.$ADMIN.'<br>'.$ADMIN_TIT.'</td>
    <tr>
</table>';
$msg_pie_reporte2='
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#000000">
    <tr>
    <td width="50%" align="center"><hr width="70%">'.$ADMIN.'<br>'.$ADMIN_TIT.'</td>
    <td width="50%" align="center"><hr width="70%">RECIB� CONFORME<br>&nbsp;</td>
    <tr>
</table>';


$msg_pie_administrador='
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#000000">
    <tr>
        <td align="center"><hr width="40%">'.$ADMIN.'<br>'.$ADMIN_TIT.'</td>
    <tr>
</table>';
/// Ordenes y Comprobantes
$pie_ord = $organizacion2.'. <b>Direcci�n:</b> '.$organizacion_dir.'<br><hr><span id="pie_merintec">Sistema Automatizado de Control de Contralor�as. A.C. Merintec RL <a href="http://www.merintec.com.ve/" target="_blank">http://www.merintec.com.ve</a></span>';

/// FIDEICOMISOS
$msg_falt_dat_fide="Se necesitan mas datos para generar o consultar Fideicomiso (Mes, A�o)";
$msg_fide_ya_pagado="Este fideicomiso ya fue procesado, puede verificar presionando consultar";
$msg_fide_no_pagado="Este fideicomiso NO se ha procesado. Debe generarlo primero.";
?>