<?php //archivo que contiene todas las funciones utilizadas en los formularios ?>
<script language="JavaScript">
function Saltar(e,t,u)
{
var k=null;
(e.keyCode) ? k=e.keyCode : k=e.which;
if(k==13) (!t) ? B(u) : t.focus();
}
function B(u)
{
u.focus()
return true;
}</script>
<SCRIPT>
function valor_acampo(valor, campo)
{
   document.form1[campo].value = valor;
}
function actualizar_padre()
{
	window.opener.document.form1.confirmar.value = 'actualizar padre';
        window.opener.document.form1.submit();
}
function confirmacion_delista_func(accion, valor)
{
	<?php include ('../comunes/mensajes.php'); ?>
	if (accion == 'Eliminar' || accion == 'Actualizar') 
	{
		var aceptar = confirm(" \u00BFRealmente desea " + accion + " la informaci\u00f3n? ");
		if (aceptar) 
		{ 
		}
		else return false;
	}
        document.form1.confirmar.value = accion + " de la lista";
		document.form1.confirmar_val.value = valor; 
        document.form1.submit();
}
function confirmacion_func(accion)
{
	<?php include ('../comunes/mensajes.php'); ?>
	if (accion == 'Procesar') 
	{
		var aceptar = confirm(" \u00BFRealmente desea " + accion + " la informaci\u00f3n? ");
		if (aceptar) 
		{ valor_acampo("SI","procesar_val");
		}
		else { alert ("<?php echo $msg_procesado_cancel_alert; ?>"); return false};
	}
	if (accion == 'Eliminar' || accion == 'Actualizar') 
	{
		var aceptar = confirm(" \u00BFRealmente desea " + accion + " la informaci\u00f3n? ");
		if (aceptar) 
		{ 
		}
		else { return false};
	}
        document.form1.confirmar.value = accion; 
        document.form1.submit();
}

// funciones para ocultar las filas de una tabla
function ocultar_celda(tabla) {
  var elementos=document.getElementById(tabla);
      elementos=elementos.getElementsByTagName('tr')
  for (k = 1; k< elementos.length; k++) {
               elementos[k].style.display = "none";
    }
}
function mostrar_celda(celda) {
    ocultar_celda('tabla');
    var elementos = document.getElementsByName(celda);
    for (k = 0; k< elementos.length; k++) {
               elementos[k].style.display = "";
    }
}
// funciones para ocultar opciones en combos anidados
function ocultar_opcion(combo) {
  var elementos=document.getElementById(combo);
      elementos=elementos.getElementsByTagName('option');
      elementos[0].selected = "true";
  for (k = 1; k< elementos.length; k++) {
               elementos[k].style.display = "none";
    }
}
function mostrar_opcion(combo, opcion_mostrar) {
	ocultar_opcion(combo);
    var elementos = document.getElementsByName(opcion_mostrar);
    for (k = 0; k< elementos.length; k++) {
               elementos[k].style.display = "";
    }
}

</SCRIPT>
<?php
$existe='';


// funcion para escribir un campo
function escribir_campo($nombre,$valor,$valor2,$en_actualizar,$maximo,$tamano,$titulo,$boton,$existe,$fecha,$adicional,$oculto){
    echo '<input '.$adicional.' name="'.$nombre.'" type="';
    if ($boton=='Modificar' || $oculto!='') { echo 'hidden'; } else { echo 'text'; }
    echo '" id="'.$nombre.'" value="';
    if(! $existe) { echo $valor; } else { echo $valor2; }
    echo '" ';
    if ($boton=='Actualizar') { echo $en_actualizar; }
    if ($boton=='Guardar'){ echo ' readonly '; }
    echo ' maxlength="'.$maximo.'" size="'.$tamano.'" title="'.$titulo.'">';
    if ($boton!='Modificar' && $fecha=='fecha') { 
        echo '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17"';
        if ((($boton=='Verificar') || ($boton=='Actualizar' && $en_actualizar!="readonly"))) 
        { echo ' onclick="displayCalendar(document.forms[0].'.$nombre.',';
          echo "'yyyy-mm-dd'";
          echo ',this)"';
        }
        echo  ' title="Haga click aqui para elegir una fecha">';
    }

    if ($boton=='Modificar' && $oculto=='') { echo $valor2; }
}

// Crear matriz de datos
// Genera una una matriz con todos los datos que se manipulan en el formulario 
function crear_datos ($nombre,$etiqueta,$valor,$min,$max,$tipo)
{
	$dato[0] = "$nombre";	//nombre del campo
	$dato[1] = "$etiqueta";	//etiqueta del campo 
	$dato[2] = "$valor";	//valor del campo
	$dato[3] = "$min";	//longitud minima
	$dato[4] = "$max";	//longitud Maxima
	$dato[5] = "$tipo";	//tipo de datos
	return $dato;
}
// Crear Busqueda
function crear_busqueda_func ($ncriterios,$criterios,$campos,$boton)
{
	include ('../comunes/mensajes.php');
	echo '<table width=100%><tr><td><hr></td></tr><tr><td align="center">Busqueda por:';
	$contador = 0;
	while ($contador < $ncriterios)
	{
	  echo '<input name="criterio" id="criterio'.$contador.'" type="radio" value="'.$campos[$contador].'" ';
	  if ($contador==0) { echo 'checked="checked" '; }
	  if (($boton=='Guardar')||($boton=='Actualizar')) { echo 'disabled'; }
	  echo 'title="'.$msg_btn_criterio.'"> ';
	  echo $criterios[$contador];
	  $contador ++;
	  if ($contador == 3) { echo '<br>'; }
	}
	echo '</td></tr>';
        echo '<tr><td align="center">';
	echo '<input name="buscar_a" type="text" id="buscar_a" value="'.$_POST["buscar_a"].'"';
	if (($boton=="Guardar")||($boton=="Actualizar")) { echo "disabled"; }
	echo ' title="'.$msg_campo_busqueda.'">';
	echo '<input type="Submit" name="Buscar" value="Buscar" title="'.$msg_btn_buscar.'"';
	if (($boton=='Guardar')||($boton=='Actualizar')) { echo 'disabled'; }
	echo '></td></tr><tr><td><hr></td></tr></table>';
}
// Busqueda
function busqueda_func ($buscar,$criterio,$tabla,$pagina,$tipo,$adicional)
{
	include ('../comunes/mensajes.php');
	// verificacion de criterio de Busqueda
	if ((! $buscar)||(! $criterio)||(! $tabla))
	{ 
		echo '<SCRIPT> alert ("'.$msg_busqueda_vacia.'");</SCRIPT>';
		echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}
	else
	{
		if ($tipo == "general")
		{
		    // echo "select * from $tabla WHERE $criterio LIKE '%$buscar%'";
 		    $result=mysql_query("select * from $tabla WHERE $criterio LIKE '%$buscar%'");
		}
		if ($tipo == "individual")
		{
		    // echo "select * from $tabla WHERE $criterio = '$buscar'";
		    $result=mysql_query("select * from $tabla WHERE $criterio = '$buscar' $adicional");
		}
		if ($buscar == "Todos" || $buscar == "todos" || $buscar == "TODOS")
		{
		    // echo "select * from $tabla";
		    $result=mysql_query("select * from $tabla");
		}
		if ($buscar == "Ultimo" || $buscar == "ultimo" || $buscar == "ULTIMOS" || $buscar == "last")
		{ 
		    $result2=mysql_query("select * from $tabla");
		    while ($result=mysql_fetch_array($result2)){
		        $ncampos = mysql_num_fields($result2);
		        $sql_tmp = "SELECT * FROM ".$tabla." WHERE ";
		        for ($i=0;$i<$ncampos;$i++){
		            if ($i>0) { $sql_tmp .= " AND "; }
		            $sql_tmp .= mysql_field_name($result2, $i)."='".$result[$i]."'";
		        }
		    }
		    $result = mysql_query($sql_tmp);
		}
		if (mysql_num_rows($result) == 0)
		{
		    echo '<SCRIPT>alert("'.$msg_busqueda_sinresultado.'");</SCRIPT>';
		    echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
		    return $result;
		}
		if (mysql_num_rows($result) > 1)
		{
		    echo '<SCRIPT>alert("'.$msg_busqueda_resultadosmul.'");</SCRIPT>';
		    return $result;
		}
		return $result;
	}
	
}
// Eliminaci�n
function eliminar_func ($elim,$criterio,$tabla,$pagina)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$sql="Delete From $tabla Where $criterio='$elim'";
	mysql_query($sql);
	if (mysql_error()){ 
		echo '<SCRIPT>alert("'.$msg_elim_noexito.' \nError: '.mysql_error().'");</SCRIPT>';  
		echo '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}
	else {
		echo '<SCRIPT>alert("'.$msg_elim_exito.'");</SCRIPT>';
		echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}
}
// Insersi�n
function insertar_func($ncampos,$datos,$tabla)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $campos .= $datos[$contador][0].',';
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $campos .= $datos[$contador][0];
	}
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $valores .= "'".$datos[$contador][2]."',";
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $valores .= "'".$datos[$contador][2]."'";
	}
    $sql = "insert into $tabla ($campos) values ($valores)";
	mysql_query($sql);
    //echo $sql; echo mysql_error(); return;
	echo '<SCRIPT>alert("'.$msg_ins_exito.'");</SCRIPT>';
	echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';	
}
///// inserta y devuelve el id en el que insert� el valor
function insertar_func_return($ncampos,$datos,$tabla)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $campos .= $datos[$contador][0].',';
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $campos .= $datos[$contador][0];
	}
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $valores .= "'".$datos[$contador][2]."',";
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $valores .= "'".$datos[$contador][2]."'";
	}
	$sql = "begin";
	mysql_query($sql);
	$sql = "insert into $tabla ($campos) values ($valores)";	
	if (mysql_query($sql)) {
     echo '<SCRIPT>alert("'.$msg_ins_exito.'");</SCRIPT>';
       $resultado=mysql_insert_id();
	   mysql_query("commit");
	}
    else {
        $resultado = '';
	 }
    return $resultado;
}

// Modificaci�n
function modificar_func($ncampos,$datos,$tabla,$condicion,$valorcondicion,$pagina,$return)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$sql = "Update $tabla Set ";
	$contador = 0;
	while ($contador < $ncampos - 1) 
        {
	    $campo = $datos[$contador][0];
	    $sql .= "$campo='".$datos[$contador][2]."', ";
	    $contador = $contador + 1;
	}
	if ($contador == $ncampos - 1)
	{
	    $campo = $datos[$contador][0];
	    $sql .= "$campo='".$datos[$contador][2]."' ";
	}
	$sql .= "Where $condicion='".$valorcondicion."'";
	mysql_query($sql);
	if (mysql_error()){ 
		echo '<SCRIPT>alert("'.$msg_mod_noexito.' \nError: '.mysql_error().'");</SCRIPT>';
		echo '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}
	else {
		echo '<SCRIPT>alert("'.$msg_mod_exito.'");</SCRIPT>';
		if (!$return){ 
		    echo '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
		}
	}	
}
// Comprobar existencia
function comp_exist($campos,$valores,$tabla,$boton,$sino,$en)
{
	include ('../comunes/mensajes.php');
	$sql="select $campos from $tabla where $campos = '$valores'";
	if($sino=='si'){
		if($reg=mysql_fetch_array(mysql_query($sql))){
			echo '<SCRIPT>alert("En '.$en.'\n'.$msg_existe_si.'");</SCRIPT>';
			$boton="Verificar";
		}
	}elseif($sino=='no'){
		if(!$reg=mysql_fetch_array(mysql_query($sql))){
			echo '<SCRIPT>alert("En '.$en.'\n'.$msg_existe_no.'");</SCRIPT>';
			$boton="Verificar";	
		}	
	}
	return $boton;
}
function comp_num_exist($campos,$valores,$tabla,$boton,$max,$en)
{
	include ('../comunes/mensajes.php');
	$sql="select COUNT($campos) as cont from $tabla where $campos = '$valores'";
	if($reg=mysql_fetch_array(mysql_query($sql))){
		if($reg['cont']>=$max){
			echo '<SCRIPT>alert("'.$msg_existe_max.' esta cedula\n'.$max.' regimstros en: '.$en.'");</SCRIPT>';
			$boton="Verificar";
		}
	}
	return $boton;
}
include ('valida.php');
// Validar los campos
function validando_campos ($ncampos,$data)
{
	for($i=0;$i<$ncampos;$i++){
	   if (validar_tipocampo ($data[$i])==0)
	   { return; }
	}
	return 1;
}
// Funcion de combos
function combo($nombre, $valor, $tabla, $link, $n_cod, $n_mos, $n_mos2, $n_mos3, $id_opt, $funcion, $boton, $adicional,$nombre_aux){
	if ($boton!='Modificar')
	{
	    $sql = "select * from ".$tabla." ".$adicional;
	    $res=mysql_query($sql,$link);
	    echo "<select name='$nombre' id='$nombre' ".$funcion; echo" >";
	    echo "<option value=''>Seleccione...</option>";
	    while ($row=mysql_fetch_array($res)){
		echo "<option ";
		echo 'id="'.$row[$id_opt].'" name="'.$row[$id_opt].'"';
		if ($row[$n_cod]==$valor) echo " selected";
		echo " value=".$row[$n_cod]."> ".$row[$n_mos]." -- ".$row[$n_mos2]." "; if($n_mos3!=""){ echo "(".$row[$n_mos3].")"; } echo "</option>";
	    }
	    echo "</select> ";
	}
	else
	{ 
	    if ($nombre_aux!='') { $nombre_campo = $nombre_aux; }
	    else { $nombre_campo = $nombre; }
	    $sql = "select * from ".$tabla." WHERE ".$nombre_campo."='".$valor."'";
	    $row=mysql_fetch_array(mysql_query($sql));
	    echo '<input type="hidden" name="'.$nombre.'" id="'.$nombre.'" value="'.$valor.'">';
	    echo $row[$n_mos2];
	}
}
// Funci�n para activar permisos del usuario
function llamar_permisos ($seccion){
	$sql = "select * from permisos WHERE cod_grp=".$_COOKIE['uspriv']." AND cod_sec = ".$seccion;
	$row = mysql_fetch_array (mysql_query($sql));
	$prm[0] = $row["ing_prm"];
	$prm[1] = $row["mod_prm"];
	$prm[2] = $row["con_prm"];
	$prm[3] = $row["eli_prm"];
	return $prm;
}
// Funci�n de auditoria para registrar las acciones de los usuarios sobre la base de datos
function auditoria_func ($accion, $ncampos, $datos, $tabla)
{
	$cod_urs = $_COOKIE['uscod'];
	$fecha = date ("Y-m-d");
	$hora = date(" H:i",time()); 
	if ($accion != "modificar")
	{
	    $contador = 0;
	    while ($contador < $ncampos - 1) 
            { 	
	        $detalle .= $datos[$contador][0].'='.$datos[$contador][2].';';
	        $contador ++;	
	    }
	    if ($contador == $ncampos - 1)
	    {
	        $detalle .= $datos[$contador][0].'='.$datos[$contador][2];
	    }
	}
	else
	{
	    $detalle = $datos;
	}
	$sql  = "insert into auditoria (cod_usr,accion,detalle,tabla,fecha,hora)"; 
	$sql .= "values ('$cod_urs','$accion','$detalle','$tabla','$fecha','$hora')";
	mysql_query($sql);
	$sql = "select * from auditoria";
	$result = mysql_query($sql);
	if (mysql_num_rows($result) == 1000)
	{
	    $sql = "TRUNCATE TABLE auditoria_backup";
  	    mysql_query($sql);
	    $sql = "insert into auditoria_backup SELECT * FROM auditoria";
	    mysql_query($sql);
	    $sql = "TRUNCATE TABLE auditoria";
  	    mysql_query($sql);
	}	
}

// Funci�n para crear una capa
function crear_capa ($nombre,$mostrar,$ancho,$alto,$visibilidad,$borde)
{
?>
<style>
#capa {
	position:absolute;
	width:<?php echo $ancho.'px;'; ?>
	height:<?php echo $alto.'px;'; ?>
	z-index:1;
	overflow:auto;
	background-color: #FFFFFF;
	border: <?php echo $borde.'px;'; ?> solid #000000;
}
</style>
<div id="capa" name="<?php echo $nombre; ?>" style="visibility:<?php echo $visibilidad; ?>">
<table width="100%" border="0" cellpadding="5" cellspacing="5">
  <tr>
    <td>
      <?php include($mostrar);?>
    </td>
  </tr>
</table>
</div>
<?php
}
function abrir_ventana($direccion,$nom_id_bot,$boton,$valor){
?>
<input type="button" name="<?php echo $nom_id_bot; ?>" id="<?php echo $nom_id_bot; ?>" value="<?php echo $boton; ?>" onclick="window.open('<?php echo $direccion; ?>?<?php echo $valor; ?>','_blank','scrollbars=yes,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500')"/>
<?php
}
//funcion para calcular una fecha sumando o restando d�as a una fecha inicial (dias continuos)
function calculo_fecha_continuos ($fecha_ini,$operador_calculo,$dias_calculo)
{ 
   $fecha_nueva = strtotime($fecha_ini);
   $calculo = strtotime($operador_calculo.$dias_calculo." days", $fecha_nueva); //Le suamos los dias d�as
   $fecha_nueva = date("Y-m-d", $calculo);
   return $fecha_nueva;
}
/// Funcion para comprobar si una fecha esta registrada como d�a feriados
function verifica_feriado ($fecha)
{
    $sql_add = date("Y-m-d", $fecha);
    $sql_fer = "Select * from feriados where fch_frd = '".$sql_add."'";
    $res_fer = mysql_query($sql_fer);
    $fecha_ok = 'NO';
    while ($row_fer = mysql_fetch_array($res_fer)) {
        $fecha_ok = 'SI';
    } 
    return $fecha_ok;
}
//funcion para calcular una fecha sumando o restando d�as a una fecha inicial
function calculo_fecha ($fecha_ini,$operador_calculo,$dias_calculo)
{ 
   $fechaComparacion = strtotime($fecha_ini);
   $contador = 1;
   while ($contador <= $dias_calculo)
   {
      /// Verificar si es un feriado sumar un d�a adicional mediante la funcion respectiva
      $fecha_ok='NO';
      while ($fecha_ok=='NO'){
        $calculo = strtotime($operador_calculo."1 days", $fechaComparacion); //Le suamos 1 d�as
        $fer =  verifica_feriado($calculo);
        if ($fer=='SI'){
            $fechaComparacion = $calculo;
            $fecha_ok = 'NO'; 
        }
        else { $fechaComparacion = $calculo; $fecha_ok = 'SI'; }
      }    

      /// Verificar si no es Sabado o Domigo
      if ((date("D",$calculo) != "Sun") && (date("D",$calculo) != "Sat")) //Verificamos que no sea Sabado o Domingo
      {
         $fechaComparacion = $calculo;
         $contador ++;
      }
      /// Verificamos si es Sabado o Domingo
      if ((date("D",$calculo) == "Sat") || (date("D",$calculo) == "Sun")) 
      {
         $calculo= strtotime($operador_calculo."1 days", $fechaComparacion); //Le suamos 1 d�as
         $fechaComparacion = $calculo;
      }
   }
   /// Creamos la fecha
   $fecha_nueva = date("Y-m-d", $calculo);
   
   /// Si hay ajuste para mostrar
   if ($ajuste){
        
   }
   
   /// Devolvemos la fecha
   return $fecha_nueva;
}
// Calculo de dias de la resta de dos fechas
function restaFechas($dFecIni, $dFecFin)
{
$dFecIni = str_replace("-","",$dFecIni);
$dFecIni = str_replace("/","",$dFecIni);
$dFecFin = str_replace("-","",$dFecFin);
$dFecFin = str_replace("/","",$dFecFin);

ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecIni, $aFecIni);
ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecFin, $aFecFin);

$date1 = mktime(0,0,0,$aFecIni[2], $aFecIni[1], $aFecIni[3]);
$date2 = mktime(0,0,0,$aFecFin[2], $aFecFin[1], $aFecFin[3]);

return round(($date2 - $date1) / (60 * 60 * 24));
}
function edad($fecha_nac,$vac)
{
  	$dia=date("j");
	$mes=date("n");
	$anno=date("Y");
    if ($_GET['ano_nom']!="") {
        $fecha_nom = $_GET['ano_nom'].'-'.$_GET['mes_nom'].'-28';
        $anno=substr($fecha_nom,0,4); 
        $mes=substr($fecha_nom,5,2);
        $dia=substr($fecha_nom,7,2);
        ///si ha que pagar vacaciones se incrementa el mes para que tome los nuevos a�os de servicio
        if ($vac==1){ 
            $mes = $mes + 2;
            if ($mes > 12) { $mes = '02'; $anno = $anno + 1; } 
        } 
    }
	$dia_nac=substr($fecha_nac, 8, 2);
	$mes_nac=substr($fecha_nac, 5, 2);
	$anno_nac=substr($fecha_nac, 0, 4);
	if($mes_nac>$mes)
	{
		$calc_edad= $anno-$anno_nac-1;
	}
	else
	{
		if($mes==$mes_nac AND $dia_nac>$dia)
		{
			$calc_edad= $anno-$anno_nac-1; 
		}
		else
		{
			$calc_edad= $anno-$anno_nac;
		}
	}
	$calc_edad=$calc_edad.' a&ntilde;os';
	return $calc_edad;
}
// funcion para calcular los dias del mes


function dias_mes ($mes,$ano){
	return date('t', mktime(0,0,0, $mes, 1, $ano));
}

// funcion para escribir Mes
function escribir_mes ($mes){
    switch ($mes) {
    case 1:
        echo "Enero";
        break;
    case 2:
        echo "Febrero";
        break;
    case 3:
        echo "Marzo";
        break;
    case 4:
        echo "Abril";
        break;
    case 5:
        echo "Mayo";
        break;
    case 6:
        echo "Junio";
        break;
    case 7:
        echo "Julio";
        break;
    case 8:
        echo "Agosto";
        break;
    case 9:
        echo "Septiembre";
        break;
    case 10:
        echo "Octubre";
        break;
    case 11:
        echo "Noviembre";
        break;
    case 12:
        echo "Diciembre";
        break;
    }
}
// funcion para convertir Mes
function convertir_mes ($mes){
    switch ($mes) {
    case 1:
        $mes = "Enero";
        break;
    case 2:
        $mes =  "Febrero";
        break;
    case 3:
        $mes =  "Marzo";
        break;
    case 4:
        $mes =  "Abril";
        break;
    case 5:
        $mes =  "Mayo";
        break;
    case 6:
        $mes =  "Junio";
        break;
    case 7:
        $mes =  "Julio";
        break;
    case 8:
        $mes =  "Agosto";
        break;
    case 9:
        $mes =  "Septiembre";
        break;
    case 10:
        $mes =  "Octubre";
        break;
    case 11:
        $mes =  "Noviembre";
        break;
    case 12:
        $mes =  "Diciembre";
        break;
    }
    return $mes;
}
/////////////////////////////////// funciones especiales para nomina ////////////////////////////////////////////////
// Insersi�n en nomina
function insertar_func_nomina($ncampos,$datos,$tabla)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $campos .= $datos[$contador][0].',';
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $campos .= $datos[$contador][0];
	}
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $valores .= "'".$datos[$contador][2]."',";
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $valores .= "'".$datos[$contador][2]."'";
	}
	$sql = "insert into $tabla ($campos) values ($valores)";
	mysql_query($sql);	
    mysql_error();
//echo $sql; echo mysql_error(); return;

}
// funcion para verificar vacaciones
function verifica_vaciones($mes_nom,$ano_nom,$por_nom,$cod_dep,$cod_tcar,$print,$ced_per,$cod_car,$procesar_val,$prenomina,$bonos)
{   
	if($por_nom==1 OR $por_nom==2 OR $por_nom==3){
		$pagar_vacaciones = "NO";
			/// funcion por medio de fecha de entrada (fch_vac)
			//$sql="select * from vista_cargos_per";
			//if ($ced_per and $cod_car) { $sql.=" WHERE ced_per=".$ced_per." AND cod_car=".$cod_car; }
			$sql = "SELECT * FROM vacaciones v, vista_cargos_per c WHERE v.ced_per = c.ced_per AND v.mes_pag = ".$mes_nom." AND v.ano_pag = ".$ano_nom." AND v.por_nom = ".$por_nom."";
			if ($ced_per and $cod_car) { $sql.=" AND v.ced_per='".$ced_per."'"; }
			//echo $sql;
			$busq=mysql_query($sql);
			if($reg=mysql_fetch_array($busq)){
				do{
					$ano=substr($reg['fch_vac'],0,4);
					$mes=substr($reg['fch_vac'],5,2);
					if ($mes==0) { $mes=12; $ano=$ano-1;}
	//				if($ano<$ano_nom AND $mes==$mes_nom){
					   if ($print=="SI") {
						echo "<tr><td class='lista_tablas'>".$reg['cod_dep']."-".$reg['num_car']."</td>
						<td class='lista_tablas'>".$reg['nom_car']."</td>
						<td class='lista_tablas'>".$reg['ced_per']."</td>
						<td class='lista_tablas'>".$reg['nom_per']."</td>
						<td class='lista_tablas'>".$reg['ape_per']."</td>
						<td class='lista_tablas' align='center'>".ordenar_fecha($reg['fch_asg'])."</td>
						<td class='lista_tablas' align='center'>".ordenar_fecha($reg['fch_vac'])."</td></tr>";
					   }
					   $pagar_vacaciones = "SI";
					   if ($pagar_vacaciones == "SI") {
						  $sql="select * from valores";
						  $res = mysql_query($sql);
					      while ($row = mysql_fetch_array($res))
						  {
						    $$row['des_val'] = $row['val_val'];
						  }
	                      if ($cod_tcar==3) { $fac_vac = $BV; }
	                      if ($cod_tcar==0 || $cod_tcar==1 || $cod_tcar==2 || $cod_tcar==4) { $fac_vac = dias_ant($ced_per,$vac=1); $fac_vac = $fac_vac[1];  }
	                      $vacaciones = redondear ((($reg['mon_sue_ant'] + $bonos) / 30),2,'','.');
	                      //echo $fac_vac;
						  $vacaciones = redondear (($vacaciones * $fac_vac),2,'','.');
						  $vacaciones = redondear ($vacaciones,2,'','.');
					   }
					   if ($print=="Nomina") {
						  echo "<tr><td>Bono Vacacional</td>";
						  echo "<td align='right'>"; echo redondear ($vacaciones,2,'.',','); echo "</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><tr>";
						$tabla_asign = "nomina_asign";	// nombre de la tabla
						$ncampos_asign = "12";		//numero de campos del formulario
						$datos_asign[0] = crear_datos ("ano_asg","A�o",$_GET['ano_nom'],"1","4","numericos");
						$datos_asign[1] = crear_datos ("mes_asg","Mes",$_GET['mes_nom'],"1","2","numericos");
						$datos_asign[2] = crear_datos ("ced_per","Cedula",$reg['ced_per'],"1","12","alfanumericos");
						$datos_asign[3] = crear_datos ("cod_car","Cargo",$reg['cod_car'],"1","11","alfanumericos");
						$datos_asign[4] = crear_datos ("por_nom","Porci�n",$_GET['por_nom'],"1","1","numericos");
						$datos_asign[5] = crear_datos ("cod_dep","Dependencia",$cod_dep,"1","5","alfanumericos");
						$datos_asign[6] = crear_datos ("cod_tcar","Cod tipo cargo",$cod_tcar,"1","11","alfanumericos");
						$datos_asign[7] = crear_datos ("cod_cnp","Cod Asignacion",0,"1","11","numericos");
						$datos_asign[8] = crear_datos ("con_cnp","Concepto de Asignacion","Bono Vacacional","1","100","alfanumericos");
						$datos_asign[9] = crear_datos ("cod_con","Cod Concesion",0,"1","11","numericos");
						$datos_asign[10] = crear_datos ("nom_con","Nombre Concesion","Bono Vacacional","1","50","alfanumericos");
						$datos_asign[11] = crear_datos ("mcuo_cnp","Monto Pagar",$vacaciones,"1","12","decimal");
						if ($procesar_val=="SI"){
						  if ($prenomina==1){$tabla_asign="nominapre_asign";}					
						  insertar_func_nomina($ncampos_asign,$datos_asign,$tabla_asign,"centro.php");
						}
					}
					//}
			   	}while($reg=mysql_fetch_array($busq));
			}
			return $vacaciones;
	}
}
//Redondear con N decimales
function redondear ($numero,$decimales,$sep_mil,$sep_dec){
	$num_formateado = number_format($numero, $decimales, $sep_dec, $sep_mil);
	return $num_formateado;
}

//funcion para calcular numero de lunes
function calculo_lunes ($mes,$ano)
{ 
   $fecha_ini = $ano."-".$mes."-01";
   $mes_act = $mes;
   $fechaComparacion = strtotime($fecha_ini);
   $calculo = $fechaComparacion;
   $contador = 0;
   while ($mes_act == $mes)
   	{
      if (date("D",$calculo) == "Mon")  			//Verificamos si es lunes
      {
         $contador ++;
      }
	  $calculo = strtotime("+1 days", $fechaComparacion); 	//Le suamos 1 d�as
	  $fechaComparacion = $calculo;
	  $mes_act = date("m",$fechaComparacion);
	}
   return $contador;
}

//Calculo de los Asignaciones
function asignaciones($sueldo,$ced_per,$cod_car,$cod_dep,$cod_tcar,$factor,$procesar_val,$prenomina){
	$sql="select * from valores";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res))
	{
		$$row['des_val'] = $row['val_val'];
	}
    $total_asig = $sueldo;
	$sql3="select a.*, c.* from asignaciones a, concesiones c WHERE a.ced_per=".$ced_per." AND a.cod_car=".$cod_car." AND a.cod_con=c.cod_con;";

	$busq3=mysql_query($sql3);
	if($reg3=mysql_fetch_array($busq3)){
		do{
			$monto_asig = 0;
			if ($reg3['por_cnp'] > 0) {
				if (($reg3['ncuo_cnp'] - $reg3['ncp_cnp']) > 0) { 
					if ($reg3['bpor_cnp'] == "B") { $monto_asig = $sueldo * ($reg3['por_cnp']/100);   } 
					if ($reg3['bpor_cnp'] == "I") {  
						$monto_asig = ((($sueldo / 30) * $BV)/12);  // calculo de cuota parte vacaciones
 						$monto_asig += ((($sueldo / 30) * $Ag)/12); // calculo de cuota parte Aguinaldos 
 						$monto_asig += $sueldo;			   // Sueldo
 						$monto_asig = $monto_asig * ($reg3['por_cnp']/100);  
					} 
					if ($reg3['bpor_cnp'] == "U") { $monto_asig = $reg3['por_cnp'] * $UT;  } 
					}
					}
					else
					{ if ($reg3['mcuo_cnp']) { if ( ($reg3['ncuo_cnp'] - $reg3['ncp_cnp']) > 0) { $monto_asig = $reg3['mcuo_cnp']; }} }
					if ($monto_asig) {
						$monto_asig = redondear ($monto_asig,2,'','.');
						//$monto_asig = $monto_asig / $factor;
						$monto_asig = redondear ($monto_asig,2,'','.');
						if ($reg3["con_cnp"]=="BONO NOCTURNO") { $total_asig2[1] = $monto_asig; }
						if ($reg3["des_cnp"]=="S") { $total_asig2[2] += $monto_asig; }
						$total_asig += $monto_asig;
						echo "<tr><td>".$reg3['con_cnp']."</td>";
						echo "<td align='right'>"; echo redondear ($monto_asig,2,'.',','); echo "</td>";
						echo "
						<td>&nbsp;</td>
						<td>&nbsp;</td>	
						<td>&nbsp;</td>						
						</tr>";
						$tabla_asign = "nomina_asign";	// nombre de la tabla
						$ncampos_asign = "12";		//numero de campos del formulario
						$datos_asign[0] = crear_datos ("ano_asg","A�o",$_GET['ano_nom'],"1","4","numericos");
						$datos_asign[1] = crear_datos ("mes_asg","Mes",$_GET['mes_nom'],"1","2","numericos");
						$datos_asign[2] = crear_datos ("ced_per","Cedula",$reg3['ced_per'],"1","12","alfanumericos");
						$datos_asign[3] = crear_datos ("cod_car","Cargo",$reg3['cod_car'],"1","11","alfanumericos");
						$datos_asign[4] = crear_datos ("por_nom","Porci�n",$_GET['por_nom'],"1","1","numericos");
						$datos_asign[5] = crear_datos ("cod_dep","Dependencia",$cod_dep,"1","5","alfanumericos");
						$datos_asign[6] = crear_datos ("cod_tcar","Cod tipo cargo",$cod_tcar,"1","11","alfanumericos");
						$datos_asign[7] = crear_datos ("cod_cnp","Cod Asignacion",$reg3['cod_cnp'],"1","11","numericos");
						$datos_asign[8] = crear_datos ("con_cnp","Concepto de Asignacion",$reg3["con_cnp"],"1","100","alfanumericos");
						$datos_asign[9] = crear_datos ("cod_con","Cod Concesion",$reg3['cod_con'],"1","11","numericos");
						$datos_asign[10] = crear_datos ("nom_con","Nombre Concesion",$reg3['nom_con'],"1","50","alfanumericos");
						$datos_asign[11] = crear_datos ("mcuo_cnp","Monto Pagar",$monto_asig,"1","12","decimal");
						if ($procesar_val=="SI"){
						    if ($prenomina==1) { $tabla_asign = "nominapre_asign"; }					
						    insertar_func_nomina($ncampos_asign,$datos_asign,$tabla_asign,"centro.php");
						    if ($reg3['per_cnp']!="S" && $prenomina!=1) {
						    	$sql_update  = "update asignaciones set ";
						    	$sql_update .= "ncp_cnp = '".($reg3['ncp_cnp']+1)."' ";
			  			    	$sql_update .= "WHERE cod_cnp = ".$reg3['cod_cnp'];
			  			    	mysql_query ($sql_update);
			  			    }
						}					        
					}
		}while($reg3=mysql_fetch_array($busq3));
	}
	$total_asig2[0] = $total_asig;
	return $total_asig2;
}

//Calculo de Bono Vacacional
function Bono_Vacacional($sueldo_men,$primas,$diasvac){
    $Bono_Vac[0] = redondear (($sueldo_men + $primas),2,'','.');
    $Bono_Vac[0] = redondear (($Bono_Vac[0] / 30),2,'','.');
    $Bono_Vac[0] = redondear (($Bono_Vac[0] * $diasvac),2,'','.');
    $Bono_Vac[1] = redondear (($Bono_Vac[0] / 12),2,'','.');
    return $Bono_Vac;
}

//Calculo de Bono Aguinaldos
function Bono_Aguinaldos($sueldo_men,$primas,$diasvac,$diasag){
    $Bono_Vac = Bono_Vacacional($sueldo_men,$primas,$diasvac);
    $Bono_Ag[0] = redondear (($sueldo_men + $primas),2,'','.');
    $Bono_Ag[0] = redondear (($Bono_Ag[0] + $Bono_Vac[1]),2,'','.');
    $Bono_Ag[0] = redondear (($Bono_Ag[0] / 30),2,'','.');
    $Bono_Ag[0] = redondear (($Bono_Ag[0] * $diasag),2,'','.');
    $Bono_Ag[1] = redondear (($Bono_Ag[0] / 12),2,'','.');
    return $Bono_Ag;
}

//Calculo de d�a de Antiguedad
function dias_ant($ced,$vac){
    	$sql="select * from valores";
    	$res = mysql_query($sql);
    	while ($row = mysql_fetch_array($res))
    	{
    		$$row['des_val'] = $row['val_val'];
    	}        
        $sql="select * from cargos where ced_per='$ced'";
	    $res = mysql_query($sql);
	    while ($row = mysql_fetch_array($res))
	    {
                $antiguedad = strtok(edad($row["fch_vac"],$vac)," ");
                /// pago de vacaciones
                if ($antiguedad >= 0)  { $nantiguedad[1] = $VACPAG1; }
                if ($antiguedad >= 6)  { $nantiguedad[1] = $VACPAG2; }
                if ($antiguedad >= 11) { $nantiguedad[1] = $VACPAG3; }
                if ($antiguedad >= 16) { $nantiguedad[1] = $VACPAG4; }
                if ($antiguedad >= 21) { $nantiguedad[1] = $VACPAG5; }
                
                /// prima de antiguedad
                if ($antiguedad >= 3)  { $nantiguedad[0] = $PRMANT0; }
                if ($antiguedad >= 6)  { $nantiguedad[0] = $PRMANT1; }
                if ($antiguedad >= 9)  { $nantiguedad[0] = $PRMANT2; }
                if ($antiguedad >= 12) { $nantiguedad[0] = $PRMANT3; }
                if ($antiguedad >= 15) { $nantiguedad[0] = $PRMANT4; }
                
		// para los contratados
		if ($row["cod_tcar"]==3) { $nantiguedad[1]=15+$antiguedad; }
		// para los obreros
		if ($row["cod_tcar"]==4) { $nantiguedad[1]=15+$antiguedad; }
                return $nantiguedad;
	    }
}


//Calculo de los descuentos de Ley
function descuentos_ley($ced,$sueldo,$lph,$spf,$sso,$cah,$sfu,$factor,$prenomina,$bonos,$fnj){
	$sql="select * from valores";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res))
	{
		$$row['des_val'] = $row['val_val'];
	}
    if ($factor == 2) 
    {
        $sueldo_descuentos = $sueldo * 2; 
    }
    else { $sueldo_descuentos = $sueldo; }
        ///// caulculo de sueldo integral para usar en calculo de LPH
        $nantiguedad = dias_ant($ced);
        $Vacaciones = Bono_Vacacional($sueldo_descuentos,$bonos,$nantiguedad[1]);
        $Aguinaldos = Bono_Aguinaldos($sueldo_descuentos,$bonos,$nantiguedad[1],$AG);
        $sueldo_lph = redondear (($sueldo_descuentos + $bonos),2,'','.');
        $sueldo_lph = redondear (($sueldo_lph + $Vacaciones[1]),2,'','.');
        $sueldo_lph = redondear (($sueldo_lph + $Aguinaldos[1]),2,'','.');
        $int_sue = $sueldo_lph;
	//echo 'cedula: '.$ced.' Integral: '.$sueldo_lph.'<br>';

   //calculo de lph con sueldo normal
	if($lph=="A"){
		$monto_lph = redondear (($SM * $LPH),2,'','.');
		$aport_lph = redondear (($SM * $APLPH),2,'','.');
		$total_ded = $monto_lph;
        $total_aport = $aport_lph;
		echo '<tr><td>FAOV&nbsp;(Ret:'.($LPH * 100).'% )( Aport:'.($APLPH * 100).'% )(Salario M&iacute;nimo)</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_lph,2,".",","); echo '</td>
		<td align="right">'; echo redondear ($aport_lph,2,".",","); echo '</td>
        <td align="right">&nbsp;</td>
		</tr>';							
	}
/* calculo de sueldo lph con salario integral
		if($lph=="A"){
				$monto_lph = redondear (($sueldo_lph * $LPH),2,'','.');
				$aport_lph = redondear (($sueldo_lph * $APLPH),2,'','.');
				$total_ded = $monto_lph;
		        $total_aport = $aport_lph;
				echo '<tr><td>FAOV&nbsp;(Ret:'.($LPH * 100).'% )( Aport:'.($APLPH * 100).'% )(Salario Integral)</td>
				<td align="right">&nbsp;</td>
				<td align="right">'; echo redondear ($monto_lph,2,".",","); echo '</td>
				<td align="right">'; echo redondear ($aport_lph,2,".",","); echo '</td>
		        <td align="right">&nbsp;</td>
				</tr>';							
			}
*/

	if($spf=="A"){
		$sueldo_sso = $sueldo;
		if ($SM && $SMMSSO) { 
		    $sueldo_max = $SM * $SMMSSO;
		    $sueldo_max = redondear($sueldo_max,2,'','.');
		     if ($sueldo_sso >= $sueldo_max) { $sueldo_sso =  $sueldo_max; }  
		}
        if ($factor == 2) 
        {
            $sueldo_sso *= 2; 
        }
        else { $sueldo_sso = $sueldo; }
        $sueldo_base_sso = $sueldo_sso;
        $sueldo_base_sso = redondear (($sueldo_base_sso * 12),2,'','.');
        $sueldo_base_sso = redondear (($sueldo_base_sso / 52),2,'','.');
        $monto_spf = redondear (($sueldo_base_sso * $SPF),2,'','.');
		$monto_spf = $monto_spf * calculo_lunes ($_GET['mes_nom'],$_GET['ano_nom']);
		$monto_spf = redondear ($monto_spf,2,'','.');
		$total_ded += $monto_spf;         
        $aport_spf = redondear (($sueldo_base_sso * $APSPF),2,'','.');


        $aport_spf = $aport_spf * calculo_lunes ($_GET['mes_nom'],$_GET['ano_nom']);
		$aport_spf = redondear ($aport_spf,2,'','.');
		$total_aport += $aport_spf;
		echo '<tr><td>PIE&nbsp;(Ret:'.($SPF * 100).'%)(Aport:'.($APSPF * 100).'%)</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_spf,2,".",","); echo '</td>
		<td align="right">'; echo redondear ($aport_spf,2,".",","); echo '</td>
        <td align="right">&nbsp;</td>
		</tr>';												
	}
	if($sso=="A"){
        $monto_sso = redondear (($sueldo_base_sso * $SSO),2,'','.');
		$monto_sso = $monto_sso * calculo_lunes ($_GET['mes_nom'],$_GET['ano_nom']);
		$monto_sso = redondear ($monto_sso,2,'','.');
		$total_ded += $monto_sso;         
        $aport_sso = redondear (($sueldo_base_sso * $APSSO),2,'','.');
        $aport_sso = $aport_sso * calculo_lunes ($_GET['mes_nom'],$_GET['ano_nom']);
		$aport_sso = redondear ($aport_sso,2,'','.');
		$total_aport += $aport_sso; 
		echo '<tr><td>S.S.O&nbsp;(Ret:'.($SSO * 100).'%)(Aport:'.($APSSO * 100).'%)</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_sso,2,".",","); echo '</td>
		<td align="right">'; echo redondear ($aport_sso,2,".",","); echo '</td>
        <td align="right">&nbsp;</td>
		</tr>';							
	}
	if($cah=="A"){
		$sueldo_caja = redondear(($sueldo_descuentos / 2),2,'','.');
		$monto_cah_ret = redondear (($sueldo_caja * $CAH),2,'','.');
		$monto_cah_apor = redondear (($sueldo_caja * $APCAH),2,'','.');
		$total_ded += $monto_cah_ret;
		$total_aport += $monto_cah_apor;
		echo '<tr><td>Caja de Ahorro&nbsp;(Ret:'.($CAH * 100).'%)(Aport:'.($APCAH * 100).'%)</td>

		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_cah_ret,2,".",","); echo '</td>
		<td align="right">'; echo redondear ($monto_cah_apor,2,".",","); echo '</td>
        <td align="right">&nbsp;</td>
		</tr>';							
	}
	if($DESPIO>0 and $cah=="A"){
		$sueldo_caja = redondear(($sueldo_descuentos / 2),2,'','.');
		$monto_pio = redondear (($sueldo_caja * $DESPIO),2,'','.');
		$total_ded += $monto_pio;
		echo '<tr><td>MONTEPIO&nbsp;('.($DESPIO * 100).'%)</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_pio,2,".",","); echo '</td>
		<td align="right">&nbsp;</td>
        <td align="right">&nbsp;</td>
		</tr>';							
	}
	if($sfu=="A"){
		$monto_sfu = $SFU;
		$monto_sfu = redondear ($monto_sfu,2,'','.');
		$total_aport += $monto_sfu;
		echo '<tr><td>Servicio Funerario</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_sfu,2,".",","); echo '</td>
		<td align="right">&nbsp;</td>
		</tr>';							
	}
	if($fnj=="A"){
	        $reten_fnj = redondear (($sueldo_descuentos * $RETFONJUB),2,'','.');
		$total_ded += $reten_fnj;
	        $aport_fnj = redondear (($sueldo_descuentos * $APRFONJUB),2,'','.');
		$total_aport += $aport_fnj;		
		echo '<tr><td>Fondo de Jubilaciones (Ret:'.($RETFONJUB * 100).'%)(Aport:'.($APRFONJUB * 100).'%)</td>
		  <td align="right">&nbsp;</td>
		  <td align="right">'; echo redondear ($reten_fnj,2,".",","); echo '</td>
		  <td align="right">'; echo redondear ($aport_fnj,2,".",","); echo '</td>
                  <td align="right">&nbsp;</td>
		</tr>';
	}
	$datos_ded_ley[0] = crear_datos ("total_ded","Total Deducciones",$total_ded,"1","12","decimal");
	$datos_ded_ley[1] = crear_datos ("lph","LPH",$monto_lph,"1","12","decimal");
	$datos_ded_ley[2] = crear_datos ("spf","SPF",$monto_spf,"1","12","decimal");
	$datos_ded_ley[3] = crear_datos ("sso","SSO",$monto_sso,"1","12","decimal");
	$datos_ded_ley[4] = crear_datos ("cah","CAH",$monto_cah_ret,"1","12","decimal");
	$datos_ded_ley[5] = crear_datos ("sfu","SFU",$monto_sfu,"1","12","decimal");
	$datos_ded_ley[6] = crear_datos ("total_apr","Total Aportes",$total_aport,"1","12","decimal");
	$datos_ded_ley[7] = crear_datos ("lph","LPH",$aport_lph,"1","12","decimal");
	$datos_ded_ley[8] = crear_datos ("spf","SPF",$aport_spf,"1","12","decimal");
	$datos_ded_ley[9] = crear_datos ("sso","SSO",$aport_sso,"1","12","decimal");
	$datos_ded_ley[10] = crear_datos ("cah","CAH",$monto_cah_apor,"1","12","decimal");
	$datos_ded_ley[11] = crear_datos ("sfu","SFU",$monto_sfu,"1","12","decimal");
	$datos_ded_ley[12] = crear_datos ("ppo","PPO",$DESPIO,"1","12","decimal");
	$datos_ded_ley[13] = crear_datos ("pio","PIO",$monto_pio,"1","12","decimal");
	$datos_ded_ley[14] = crear_datos ("fnj","FNJ",$reten_fnj,"1","12","decimal");
	$datos_ded_ley[15] = crear_datos ("fnj","FNJ",$aport_fnj,"1","12","decimal");
	//// porcentajes
	$datos_ded_ley[16] = crear_datos ("ret_lph_por","LPH",$LPH,"1","12","decimal");
	$datos_ded_ley[17] = crear_datos ("ret_spf_por","SPF",$SPF,"1","12","decimal");
	$datos_ded_ley[18] = crear_datos ("ret_sso_por","SSO",$SSO,"1","12","decimal");
	$datos_ded_ley[19] = crear_datos ("ret_cah_por","CAH",$CAH,"1","12","decimal");
	$datos_ded_ley[20] = crear_datos ("ret_fnj_por","FNJ",$RETFONJUB,"1","12","decimal");
	$datos_ded_ley[21] = crear_datos ("apr_lph_por","LPH",$APLPH,"1","12","decimal");
	$datos_ded_ley[22] = crear_datos ("apr_spf_por","SPF",$APSPF,"1","12","decimal");
	$datos_ded_ley[23] = crear_datos ("apr_sso_por","SSO",$APSSO,"1","12","decimal");
	$datos_ded_ley[24] = crear_datos ("apr_cah_por","CAH",$APCAH,"1","12","decimal");
	$datos_ded_ley[25] = crear_datos ("apr_fnj_por","FNJ",$APRFONJUB,"1","12","decimal");
	$datos_ded_ley[26] = crear_datos ("int_sue","FNJ",$int_sue,"1","12","decimal");
	return $datos_ded_ley;
}

//Calculo de Primas
function primas($ced,$hog,$hij,$ant,$pro,$hje,$jer,$otr,$factor,$prenomina,$bonos,$imprimir,$riesgo,$patru,$coor,$bono_juge,$bono_util){
	//echo $coor;
	$sql="select * from valores";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res))
	{
		$$row['des_val'] = $row['val_val'];
	}
	if($hog=="A"){
		$prima_hog = $UT * $PRMHOG;
		$prima_hog = redondear ($prima_hog,2,'','.');
		$total_prim = $prima_hog;
		if (!$imprimir) {
		echo '<tr><td>Prima por Hogar</td>
		<td align="right">'; echo redondear ($prima_hog,2,".",","); echo '</td>
		<td align="right">&nbsp;</td>
        <td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		</tr>';
		}							
	}
	if($hij=="A"){
        $nhijos = 0;
        $sql="select * from familiares where ced_per='$ced' and par_fam='H'";
	    $res = mysql_query($sql);
	    while ($row = mysql_fetch_array($res))
	    {
            if (edad($row["fnac_fam"])<=18){
                //$nhijos+=1;
                // solo 01 hijo
                $nhijos = 1;
            }
	    }
		$prima_hij = redondear (($UT * $PRMHIJ),2,'','.');
        $prima_hij = redondear (($prima_hij*$nhijos),2,'','.');
		$total_prim += $prima_hij;
        if ($prima_hij>0 && !$imprimir)
        {
		    echo '<tr><td>Prima por Hijos</td>
		    <td align="right">'; echo redondear ($prima_hij,2,".",","); echo '</td>
		    <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
		    <td align="right">&nbsp;</td>
		    </tr>';
        }							
	}
	//bono utiles
	
	if($bono_util=="A"){
        $nhijos = 0;
        $sql="select * from familiares where ced_per='$ced' and par_fam='H'";
	    $res = mysql_query($sql);
	    while ($row = mysql_fetch_array($res))
	    {
            if (edad($row["fnac_fam"])<=18){
                $nhijos+=1;
                // solo 01 hijo
                //$nhijos = 1;
            }
	    }
		$vbono_util = redondear (($UT * $BONOUTIL),2,'','.');
        $vbono_util = redondear (($vbono_util*$nhijos),2,'','.');
		$total_prim += $vbono_util;
        if ($vbono_util>0 && !$imprimir)
        {
		    echo '<tr><td>Bono Utiles Escolares</td>
		    <td align="right">'; echo redondear ($vbono_util,2,".",","); echo '</td>
		    <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
		    <td align="right">&nbsp;</td>
		    </tr>';
        }							
	}
	// bono juguetes
 
	if($bono_juge=="A"){
        $nhijos = 0;
        $sql="select * from familiares where ced_per='$ced' and par_fam='H'";
	    $res = mysql_query($sql);
	    while ($row = mysql_fetch_array($res))
	    {
            if (edad($row["fnac_fam"])<=12){
                $nhijos+=1;
                // solo 01 hijo
                //$nhijos = 1;
            }
	    }
		$vbono_juge = redondear (($UT * $BONOJUGE),2,'','.');
        $vbono_juge = redondear (($vbono_juge*$nhijos),2,'','.');
		$total_prim += $vbono_juge;
        if ($vbono_util>0 && !$imprimir)
        {
		    echo '<tr><td>Bono de Juguetes</td>
		    <td align="right">'; echo redondear ($vbono_juge,2,".",","); echo '</td>
		    <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
		    <td align="right">&nbsp;</td>
		    </tr>';
        }							
	}



	if($ant=="A"){
        $nantiguedad = dias_ant($ced);
        $SM_diario = redondear(($SM/30),2,'','.');
		$prima_ant = redondear (($SM_diario * $nantiguedad[0]),2,'','.');
		$total_prim += $prima_ant;
        if ($prima_ant > 0 && !$imprimir)
        {
    		echo '<tr><td>Prima por Antiguedad</td>
	    	<td align="right">'; echo redondear ($prima_ant,2,".",","); echo '</td>
	    	<td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
	    	<td align="right">&nbsp;</td>
	    	</tr>';
        }							
	}

    //verificar los datos de estudios realizados para ser usado por primas pro y primas otr
    $sql="select * from educacion where ced_per='$ced'";
    $res = mysql_query($sql);
    $est[0] = 0;
    $est[1] = 0;
    while ($row = mysql_fetch_array($res))
    {
        if ($row["niv_edu"]=="TecS") 
        { 
            $est_niv1=$PRMPRO1;
        }
        if ($row["niv_edu"]=="Univ") 
        { 
            $est_niv1=$PRMPRO2;
        }
        if ($row["niv_edu"]=="Esp")
        {
            $est_niv1=$PRMOTR1;
        }
		if ($row["niv_edu"]=="Msc")
        {
            $est_niv1=$PRMOTR2;
        }
        if ($row["niv_edu"]=="Doc" || $row["niv_edu"]=="Phd")
        {
            $est_niv1=$PRMOTR3; 
        }
	$est[0] = $est_niv1;
    }
    ///////////////////////////////////

	if($pro=="A"){
        if ($est[0]>0)
        {
		    $prima_pro = redondear (($est[0] * $UT),2,'','.');
    		$total_prim += $prima_pro;
    		if (!$imprimir) {
    		    echo '<tr><td>Prima Profesionalizaci�n</td>
    		    <td align="right">'; echo redondear ($prima_pro,2,".",","); echo '</td>
    		    <td align="right">&nbsp;</td>
                <td align="right">&nbsp;</td>
    		    <td align="right">&nbsp;</td>
    		    </tr>';
    		}
        }							
	}

	if($hje=="A"){
		$prima_hje = $UT * $PRMHJE;
		$prima_hje = redondear ($prima_hje,2,'','.');
		$total_prim += $prima_hje;
		if (!$imprimir) {
		    echo '<tr><td>Prima por Hijos Excepcionales</td>
		    <td align="right">'; echo redondear ($prima_hje,2,".",","); echo '</td>
		    <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
		    <td align="right">&nbsp;</td>
		    </tr>';
		 }							
	}
	if($jer=="A"){
		$prima_jer = $PRMJER;
		$prima_jer = redondear ($prima_jer,2,'','.');
		$total_prim += $prima_jer;
		if (!$imprimir) {
		    echo '<tr><td>Prima por Jerarquia</td>
		    <td align="right">'; echo redondear ($prima_jer,2,".",","); echo '</td>
		    <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
		    <td align="right">&nbsp;</td>
		    </tr>';
		}							
	}
	if($otr=="A"){
        if ($est[1]>0)
            {
        		$prima_otr = redondear (($est[1] * $UT),2,'','.');
        		$total_prim += $prima_otr;
        		if (!$imprimir) {
        		    echo '<tr><td>Otras Primas</td>
        		    <td align="right">'; echo redondear ($prima_otr,2,".",","); echo '</td>
        		    <td align="right">&nbsp;</td>
                    <td align="right">&nbsp;</td>
        		    <td align="right">&nbsp;</td>
        		    </tr>';
        		}							
            }
	}
	if($riesgo=="A"){
			$prima_rie = $PRIRIE;
			$total_prim += $prima_rie;
			if (!$imprimir) {
			    echo '<tr><td>Prima por Riesgo</td>
			    <td align="right">'; echo redondear ($prima_rie,2,".",","); echo '</td>
			    <td align="right">&nbsp;</td>
	            <td align="right">&nbsp;</td>
			    <td align="right">&nbsp;</td>
			    </tr>';
			}							
		}
	if($patru=="A"){
			$prima_pat = $PRIPAT * $UT;;
			$total_prim += redondear($prima_pat,2,'','.');
			if (!$imprimir) {
			    echo '<tr><td>Prima Patrullero</td>
			    <td align="right">'; echo redondear ($prima_pat,2,".",","); echo '</td>
			    <td align="right">&nbsp;</td>
	            <td align="right">&nbsp;</td>
			    <td align="right">&nbsp;</td>
			    </tr>';
			}							
		}
	if($coor=="A"){
			$prima_coo = $PRICOO * $UT;
			$total_prim += redondear($prima_coo,2,'','.');
			if (!$imprimir) {
			    echo '<tr><td>Prima Coordinador</td>
			    <td align="right">'; echo redondear ($prima_coo,2,".",","); echo '</td>
			    <td align="right">&nbsp;</td>
	            <td align="right">&nbsp;</td>
			    <td align="right">&nbsp;</td>
			    </tr>';
			}							
		}
	$datos_primas[0] = crear_datos ("total_primas","Total Primas",$total_prim,"1","12","decimal");
	$datos_primas[1] = crear_datos ("hog","Hogar",$prima_hog,"0","12","decimal");
	$datos_primas[2] = crear_datos ("hij","Hijos",$prima_hij,"0","12","decimal");
	$datos_primas[3] = crear_datos ("ant","Antigueda",$prima_ant,"0","12","decimal");
	$datos_primas[4] = crear_datos ("pro","Profesionalizacion",$prima_pro,"0","12","decimal");
	$datos_primas[5] = crear_datos ("hje","Hijos Excepcionales",$prima_hje,"0","12","decimal");
	$datos_primas[6] = crear_datos ("jer","Jerarquia",$prima_jer,"0","12","decimal");
	$datos_primas[7] = crear_datos ("otr","Otras Primas",$prima_otr,"0","12","decimal");
	$datos_primas[8] = crear_datos ("rie","Riesgo",$prima_rie,"0","12","decimal");
	$datos_primas[9] = crear_datos ("pat","Patrullero",$prima_pat,"0","12","decimal");
	$datos_primas[10] = crear_datos ("coo","Coordinador",$prima_coo,"0","12","decimal");
	$datos_primas[11] = crear_datos ("bono_util","Utiles",$vbono_util,"0","12","decimal");
	$datos_primas[12] = crear_datos ("bono_juge","Juguetes",$vbono_juge,"0","12","decimal");

	return $datos_primas;
}

//Calculo de D�as No Laborados
function dias_nolaborados($sueldo,$ced_per,$ano_nom,$mes_nom,$por_nom){
    if ($por_nom==1) { 
        $dia_min = 16; $dia_max = 31; $mes_nom = $mes_nom - 1; 
        if ($mes_nom<1) { $mes_nom=12; $ano_nom = $ano_nom - 1; }
    }
    if ($por_nom==2) { $dia_min = 1; $dia_max = 15; $mes_nom = $mes_nom; }
    $sql =  "select * from inasistencias where ced_per = $ced_per And";
    $sql .= " DAY(fch_ina) >= $dia_min and DAY(fch_ina) <= $dia_max And";
    $sql .= " MONTH(fch_ina)=$mes_nom And YEAR(fch_ina) = $ano_nom";
    $sql .= " And tip_ina = 'No Remunerada' ORDER BY fch_ina"  ;
    if ($por_nom==3) {
        $fch_min = $ano_nom.'-'.($mes_nom-1).'-16';
        $fch_max = $ano_nom.'-'.($mes_nom).'-15';
        $sql  =  "select * from inasistencias where ced_per = $ced_per ";
        $sql .= " And fch_ina >= '$fch_min' And fch_ina <= '$fch_max'";
        $sql .= " And tip_ina = 'No Remunerada' ORDER BY fch_ina"  ;
    }
    $res = mysql_query($sql);
    $sueldo_diario = redondear (($sueldo / 30),2,'','.');
    $ndias = 0;
    $dias = "";
    $dias = "(";
    while ($row = mysql_fetch_array($res))
	{
		$ndias = $ndias + 1;
        if ($ndias > 1) { $dias .= ", "; }
        $dias .= substr($row['fch_ina'], 8, 2);
	}
    $dias .= ")";
    $monto_descontar = $sueldo_diario * $ndias;
    $dias_nolaborados[0] = $ndias;
    $dias_nolaborados[1] = $monto_descontar;

    if ($ndias>=1){
	    echo "<tr><td>".$ndias." D�a";
        if ($ndias>1) { echo 's'; }
        echo " no laborado";
        if ($ndias>1) { echo 's'; }
        echo " per�odo anterior: ".$dias."</td>";
	    echo "<td>&nbsp;</td>";
	    echo "
	    <td align='right'>"; echo redondear ($monto_descontar,2,'.',','); echo "</td>
	    <td>&nbsp;</td>	
        <td>&nbsp;</td>					
	    </tr>";
    }
    return $dias_nolaborados;

}

//Calculo de los descuentos
function descuentos($sueldo,$ced_per,$cod_car,$cod_dep,$cod_tcar,$total_ded,$procesar_val,$prenomina){
	$sql="select * from valores";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res))
	{
		$$row['des_val'] = $row['val_val'];
	}
	$sql3="select * from deducciones d, descuentos ds WHERE ced_per=".$ced_per." AND cod_car=".$cod_car." AND d.cod_des=ds.cod_des";
	$busq3=mysql_query($sql3);
	if($reg3=@mysql_fetch_array($busq3)){
		do{
			$monto_ded = 0;
            $concepto ="";
			if ($reg3['por_dsp'] > 0) {
				if (($reg3['ncuo_dsp'] - $reg3['ncp_dsp']) > 0) { 
					if ($reg3['bpor_dsp'] == "B") { $monto_ded = $sueldo * ($reg3['por_dsp']/100);   } 
					if ($reg3['bpor_dsp'] == "I") {  
						$monto_ded = ((($sueldo / 30) * $BV)/12);  // calculo de cuota parte vacaciones
 						$monto_ded += ((($sueldo / 30) * $Ag)/12); // calculo de cuota parte Aguinaldos 
 						$monto_ded += $sueldo;			   // Sueldo
 						$monto_ded = $monto_ded * ($reg3['por_dsp']/100);  
					} 
					if ($reg3['bpor_dsp'] == "U") { $monto_ded = $reg3['por_dsp'] * $UT;  } 
					}
					}
					else
					{ if ($reg3['mcuo_dsp']) { if ( ($reg3['ncuo_dsp'] - $reg3['ncp_dsp']) > 0) { $monto_ded = $reg3['mcuo_dsp']; }} }
					if ($monto_ded) {
						$monto_ded = redondear ($monto_ded,2,'','.');				
						if ($reg3['cod_des']==1 && $_GET['por_nom']==3) {
						    $monto_ded += redondear ($monto_ded,2,'','.');				
						}
    					$total_ded += $monto_ded;
                        $concepto = $reg3['con_dsp'];
       			        if ($reg3['per_dsp']!="S") { 
       			            if ($reg3['cod_des']==1 && $_GET['por_nom']==3) {
       			                $concepto .= " (".($reg3['ncp_dsp']+1)." y ".($reg3['ncp_dsp']+2)." / ".$reg3['ncuo_dsp'].")"; 
       			        	}
       			        	else
       			        	{
       			        	    $concepto .= " (".($reg3['ncp_dsp']+1)."/".$reg3['ncuo_dsp'].")"; 
       			        	}
                        }
						echo "<tr><td>".$concepto."</td>";
						echo "<td>&nbsp;</td>";
						echo "
						<td align='right'>"; echo redondear ($monto_ded,2,'.',','); echo "</td>
						<td>&nbsp;</td>	
                        <td>&nbsp;</td>					
						</tr>";
			$tabla_deduc = "nomina_deduc";	// nombre de la tabla
			$ncampos_deduc = "12";		//numero de campos del formulario
			$datos_deduc[0] = crear_datos ("ano_ded","A�o",$_GET['ano_nom'],"1","4","numericos");
			$datos_deduc[1] = crear_datos ("mes_ded","Mes",$_GET['mes_nom'],"1","2","numericos");
			$datos_deduc[2] = crear_datos ("ced_per","Cedula",$reg3['ced_per'],"1","12","alfanumericos");
			$datos_deduc[3] = crear_datos ("cod_car","Cargo",$reg3['cod_car'],"1","11","alfanumericos");
			$datos_deduc[4] = crear_datos ("por_nom","Porci�n",$_GET['por_nom'],"1","1","numericos");
			$datos_deduc[5] = crear_datos ("cod_dep","Dependencia",$cod_dep,"1","5","alfanumericos");
			$datos_deduc[6] = crear_datos ("cod_tcar","Cod tipo cargo",$cod_tcar,"1","11","alfanumericos");
			$datos_deduc[7] = crear_datos ("cod_dsp","Cod deducciones",$reg3['cod_dsp'],"1","11","numericos");
			$datos_deduc[8] = crear_datos ("con_dsp","Concepto de deducciones",$concepto,"1","100","alfanumericos");
			$datos_deduc[9] = crear_datos ("cod_des","Cod descuento",$reg3['cod_des'],"1","11","numericos");
			$datos_deduc[10] = crear_datos ("nom_des","Nombre Descuento",$reg3['nom_des'],"1","50","alfanumericos");
			$datos_deduc[11] = crear_datos ("mcuo_dsp","Monto Descontar",$monto_ded,"1","12","decimal");
			if ($procesar_val=="SI"){	
			  if ($prenomina==1){ $tabla_deduc = "nominapre_deduc"; }			
			  insertar_func_nomina($ncampos_deduc,$datos_deduc,$tabla_deduc,"centro.php");
			  if ($prenomina!=1){
			     if ($reg3['per_dsp']!="S") {
			     $sql_update  = "update deducciones set ";
			     if ($reg3['cod_des']==1 && $_GET['por_nom']==3) {
			        $sql_update .= "ncp_dsp = '".($reg3['ncp_dsp']+2)."' ";
			     }
			     else
			     {
			        $sql_update .= "ncp_dsp = '".($reg3['ncp_dsp']+1)."' ";
			     }
			     $sql_update .= "WHERE cod_dsp = ".$reg3['cod_dsp'];
			     mysql_query ($sql_update);
			     }
			  }
			}
					}
		}while($reg3=mysql_fetch_array($busq3));
	}
	return $total_ded;
}

/////////////////////////////////// funci�n para cambiar a may�sculas ////////////////////////////////////////////////
/// Funcion para cambiar a mayusculas
function mayuculas($texto){
    $texto=strtoupper($texto);
    $texto=str_replace("�","�", $texto);
    $texto=str_replace("�","�", $texto);
    $texto=str_replace("�","�", $texto);
    $texto=str_replace("�","�", $texto);
    $texto=str_replace("�","�", $texto);
    return $texto;
}

//////////////////////////////////// Funcion para ordernar la fecha /////////////////////////////////////////////////
function ordenar_fecha($fecha){
        if ($fecha){
                $fecha = strtotime($fecha);
                $fecha = date("d-m-Y", $fecha);
        }
        return $fecha;
}


/////////////////////////////////// funciones especiales para nomina ////////////////////////////////////////////////

//Consulta por fecha
function consulta_fecha ($campos,$tabla,$nom_fecha,$dia,$mes,$ano){
	$sql = "select $campos from $tabla where DAYOFMONTH($nom_fecha)=$dia and MONTH($nom_fecha)=$mes And YEAR($nom_fecha) = $ano;";

}

////////////////////////////////// funcion especial para comprobar asistencia /////////////////////////////////////

// Covertir la hora unix almacenada en base de datos biostar a Fecha normal
// Devuelve un arreglo con fecha en posicion 0 y hora en posicion 1
function fch_unix_a_fecha($fch_unix,$primero)
{
    $fch_unix += 16200;
    $fecha = date_create();
    date_timestamp_set($fecha, $fch_unix);
    if ($primero == 'Y') { $tiempo[0]= date_format($fecha, 'Y/m/d'); }
    else { $tiempo[0]= date_format($fecha, 'd/m/Y'); }
    $tiempo[1]= date_format($fecha, 'H:i:s');
    return $tiempo;
}
?>
