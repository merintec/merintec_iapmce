<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE["usnombre"]||$_COOKIE["uspriv"]!=1) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$boton = "Verificar";
$existe = '';
$pagina = "usuarios.php";
$tabla = "usuarios";	// nombre de la tabla
$ncampos = "9";		//numero de campos del formulario
$datos[0] = crear_datos ("cod_usr","Cedula",$_POST['cod_usr'],"1","12","numericos");
$datos[1] = crear_datos ("nom_usr","Nombres",$_POST['nom_usr'],"1","50","alfabeticos");
$datos[2] = crear_datos ("ape_usr","Apellidos",$_POST['ape_usr'],"1","50","alfabeticos");
$datos[3] = crear_datos ("freg_usr","Fecha de Registro",$_POST['freg_usr'],"1","10","alfanumericos");
$datos[4] = crear_datos ("log_usr","Login",$_POST['log_usr'],"1","15","alfanumericos");
$datos[5] = crear_datos ("pas_usr","Password",$_POST['pas_usr'],"1","15","alfanumericos");
$datos[6] = crear_datos ("cod_grp","Grupo",$_POST['cod_grp'],"1","2","numericos");
$datos[7] = crear_datos ("fcam_usr","Fecha de Cambio de contraseña",date("Y-m-d"),"1","10","alfanumericos");
$datos[8] = crear_datos ("sts_usr","Estatus",$_POST['sts_usr'],"1","1","alfabeticos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre";
		$datos[0]="nom_usr";
		$parametro[1]="Login";
		$datos[1]="log_usr";
		busqueda_varios(4,$buscando,$datos,$parametro,"cod_usr");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_usr = $row["cod_usr"];
	    $nom_usr = $row["nom_usr"];
	    $ape_usr = $row["ape_usr"];
		$freg_usr = $row["freg_usr"];
		$log_usr = $row["log_usr"];
		$pas_usr = $row["pas_usr"];
		$sts_usr = $row["sts_usr"];
		$cod_grp = $row["cod_grp"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_usr",$_POST["cod_usr"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si','Usuarios');
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_usr"],"cod_usr",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos del Usuario</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Grupo:</td>
                        <td width="75%"><label><span class="Estilo23">
                        <?php combo('cod_grp', $cod_grp, 'grupos', $link, 0, 0, 1, "", 'cod_grp', "", $boton, ""); ?>
                        </span></label></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Cedula:</td>
                        <td><input name="cod_usr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="cod_usr" value="<?php if(! $existe) { echo $_POST["cod_usr"]; } else { echo $cod_usr; } ?>" <?php if ($boton=='Actualizar') { echo "readonly"; } ?> size="20" title="Cedula del usuario">
                        <?php if ($boton=='Modificar') { echo $cod_usr; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Nombres:</td>
                        <td><input name="nom_usr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_usr" value="<?php if(! $existe) { echo $_POST["nom_usr"]; } else { echo $nom_usr; } ?>" size="35" title="Nombres del usuario">
                        <?php if ($boton=='Modificar') { echo $nom_usr; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Apellidos:</td>
                        <td><input name="ape_usr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ape_usr" value="<?php if(! $existe) { echo $_POST["ape_usr"]; } else { echo $ape_usr; } ?>" size="35" title="Apellidos del usuario">
                        <?php if ($boton=='Modificar') { echo $ape_usr; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Registro:</td>
                        <td><input name="freg_usr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="freg_usr" value="<?php if(! $existe) { echo $_POST['freg_usr']; } else { echo $freg_usr; } ?>" <?php if ($boton=='Actualizar') { echo "readonly"; } ?> size="20" title="fecha de registro"><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].freg_usr,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?>
                        <?php if ($boton=='Modificar') { echo $freg_usr; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Login:</td>
                        <td><input name="log_usr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="log_usr" value="<?php if(! $existe) { echo $_POST["log_usr"]; } else { echo $log_usr; } ?>" size="20" title="Login del usuario" />
                          <?php if ($boton=='Modificar') { echo $log_usr; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Password:</td>
                        <td><input name="pas_usr" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'password'; } ?>" id="pas_usr" value="<?php if(! $existe) { echo $_POST["pas_usr"]; } else { echo $pas_usr; } ?>" size="20" title="Login del usuario">
                        <?php if ($boton=='Modificar') { echo '<a title="'.$pas_usr.'">********</a>'; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Estatus:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="sts_usr">
                          <option>Seleccione...</option>
                          <option value="A" '; if ($sts_usr == "A" || $_POST['sts_usr'] =="A") { echo 'selected'; } echo '>Activo</option>
                          <option value="D" '; if ($sts_usr == "D" || $_POST['sts_usr'] =="D") { echo 'selected'; } echo '>Desactivado</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="sts_usr" id="sts_usr" value="'.$sts_usr.'" >'; 
						    if ($sts_usr == "D") { echo 'Desactivado'; } 
							if ($sts_usr == "A") { echo 'Activado'; }
						}?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =3; 
						$criterios[0] = "Cedula"; 
						$campos[0] ="cod_usr";
						$criterios[1] = "Nombres"; 
						$campos[1] ="nom_usr";
						$criterios[2] = "Apellidos"; 
						$campos[2] ="ape_usr";
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton) ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
